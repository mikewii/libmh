#include <libmh/MHXX/Equipment/Hunter/Weapon/BaseData.hpp>
#include <libmh/tools/log.hpp>
#include <filesystem>
#include <fstream>

#include <vector>
#include <sstream>
#include <stdexcept>

using namespace mh::mhxx::weapon::basedata;
using Type = Data_W00;

std::string path = "assets/mhxx/table/weapon00BaseData.w00d";
std::string filename = std::filesystem::path(path).filename().string();

auto main() -> int
{
    LOG_SET_FLUSH(false);

    std::ifstream fileIn(path, std::ios::in | std::ios::binary);
    std::ofstream fileOut(filename, std::ios::out | std::ios::binary);
    cBaseData<Type> obj;

    if (!fileIn.is_open())
        throw std::runtime_error("Failed to open file " + path);

    if (!parse(fileIn, obj))
        throw std::runtime_error("Failed to parse BaseData");

//    obj.dump(fileOut);

    return 0;
}
