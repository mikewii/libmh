#include <libmh/tools/arc/reader.hpp>
#include "stdfsostreamfun.hpp"

std::string path = "assets/resident_eng.arc";

auto main() -> int
{
    if (!std::filesystem::exists(path))
        return 0;

    STDFSIOstreamFun fun(path);
    mh::tools::arc::Reader arc(path);

    auto iostreamfun = std::bind(  &STDFSIOstreamFun::iostreamfun
                                 , std::ref(fun)
                                 , std::placeholders::_1);

    if (!arc.read())
        throw std::runtime_error("Failed to read arc");

    fun.reserveMeta(arc.getFileHeaders().size());

//    if (!arc.uncompress_all(ostreamfun))
//        throw std::runtime_error("Failed to uncomress");

    if (!arc.uncompress_all_async(path, iostreamfun))
        throw std::runtime_error("Failed to async uncomress");

    if (!fun.dumpMeta(4))
        throw std::runtime_error("Failed to dump meta");

    return 0;
}
