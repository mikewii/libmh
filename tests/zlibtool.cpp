#include <libmh/tools/zlibtool.hpp>
#include <libmh/tools/rng.hpp>
#include <libmh/tools/imemstream.hpp>

#include <vector>
#include <sstream>
#include <stdexcept>

auto generateData(void) -> std::vector<uint8_t>
{
    auto size = std::numeric_limits<uint8_t>::max() * 2;
    std::vector<uint8_t> out;

    out.reserve(size);

    for (auto i = 0; i < size; i++) {
        if (i % 2 == 0) {
            auto res = mh::tools::random::randint<int>(  static_cast<uint8_t>(0)
                                                       , std::numeric_limits<uint8_t>::max());

            out.push_back(std::move(static_cast<uint8_t>(res)));
        } else {
            out.push_back(static_cast<uint8_t>(i));
        }
    }

    return out;
}

auto compress(std::vector<uint8_t>& data, std::ostream& ostream) -> bool
{
    mh::tools::imemstream istream(data.data(), data.size());
    mh::tools::ZLIBTool zlib(istream, ostream);

    std::size_t uncompressed, compressed;

    return zlib.compress(  uncompressed
                         , compressed
                         , Z_DEFAULT_COMPRESSION
                         , Z_HUFFMAN_ONLY);
}

auto uncompress(std::vector<uint8_t>& data, std::istream& istream) -> bool
{
    std::stringstream ostream;
    mh::tools::ZLIBTool zlib(istream, ostream);
    std::size_t size = 0;

    if (!zlib.uncompress(-1, size))
        return false;

    if (data.size() != size)
        return false;

    uLong adler = adler32_z(0L, Z_NULL, 0);

    adler = adler32_z(adler, data.data(), data.size());

    if (adler != zlib.checksum())
        return false;

    return true;
}

auto main() -> int
{
    std::stringstream iostream;
    std::vector<uint8_t> data = generateData();

    if (!compress(data, static_cast<std::ostream&>(iostream)))
        throw std::runtime_error("Failed to compress");

    if (!uncompress(data, static_cast<std::istream&>(iostream)))
        throw std::runtime_error("Failed to uncompress");

    return 0;
}
