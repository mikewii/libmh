#pragma once
#include <libmh/tools/noncopyable.hpp>
#include <libmh/tools/nonmovable.hpp>
#include <libmh/tools/arc/common.hpp>
#include <libmh/MH/Resource.hpp>

#include <filesystem>
#include <fstream>
#include <iostream>
#include <mutex>

#include <nlohmann/json.hpp>

class STDFSIOstreamFun
{
private:
    std::filesystem::path m_prefix;
    std::filesystem::path m_outdir;
    nlohmann::ordered_json::array_t m_meta;
    std::mutex m_metaMutex;

public:
    NONCOPYABLE(STDFSIOstreamFun)
    NONMOVABLE(STDFSIOstreamFun)

    STDFSIOstreamFun(const std::string& arcpath)
    {
        std::filesystem::path temp{arcpath};

        m_outdir.replace_filename(temp.filename());
        m_outdir.replace_extension(temp.extension().string() + ".dearc");

        m_prefix = temp.replace_extension().remove_filename();
    }

    auto reserveMeta(const std::size_t size) -> void
    {
        m_meta.reserve(size);
    }

    auto iostreamfun(const mh::tools::arc::FileHeader& header) noexcept(false) -> std::unique_ptr<std::iostream>
    {
        std::filesystem::path fspath{onlyDir(header.getFilePathUnix())};
        std::filesystem::path path;

        path = m_prefix;
        path /= m_outdir;
        path /= fspath;

        std::error_code ec;
        std::filesystem::create_directories(path, ec);

        std::filesystem::path filename{constructFilename(header)};

        path /= filename;

        auto stream = std::make_unique<std::fstream>(  path
                                                     , std::ios::out | std::ios::trunc | std::ios::binary);

        if (!stream->is_open())
            throw std::runtime_error("Failed to open ostream: " + path.string());

        addMeta(header);

        return stream;
    }

    auto dumpMeta(const int indent = -1,
                  const std::string& name = "meta.json") -> bool
    {
        if (m_meta.empty())
            return false;

        std::filesystem::path path;

        path = m_prefix;
        path /= m_outdir;
        path /= name;

        std::ofstream out(path, std::ios::out | std::ios::trunc);

        if (!out.is_open())
            return false;

        out << static_cast<const nlohmann::ordered_json&>(m_meta).dump(indent);

        return true;
    }

private:
    auto addMeta(const mh::tools::arc::FileHeader& header) -> void
    {
        nlohmann::ordered_json meta;
        std::filesystem::path filepath{header.getFilePathUnix()};

        meta["filepath"] = filepath.replace_extension(getExtention(header.ResourceHash));
        meta["resource"] = header.ResourceHash;
        meta["flags"] = header.getFlags();

        {
            std::lock_guard<std::mutex> lock(m_metaMutex);

            m_meta.push_back(std::move(meta));
        }
    }

    auto setPrefix(const std::string& path) noexcept -> void
    {
        m_prefix = path;
    }

    auto onlyDir(const std::string& path) const noexcept -> std::filesystem::path
    {
        std::filesystem::path temp{path};

        return temp.replace_extension().remove_filename();
    }

    auto constructFilename(const mh::tools::arc::FileHeader& header) const noexcept -> std::filesystem::path
    {
        std::string resourceExtention{getExtention(header.ResourceHash)};
        std::filesystem::path temp{header.getFilePathUnix()};
        std::filesystem::path filename;

        filename.replace_filename(temp.filename());
        filename.replace_extension(temp.extension().string() + resourceExtention);

        return filename;
    }

    auto getExtention(const u32 hash) const noexcept -> std::string
    {
        std::string extention;
        const char* str = mh::sResource::getExtention(hash);

        if (str)
            extention = str;
        else
            extention = std::to_string(hash);

        std::transform(  extention.begin()
                       , extention.end()
                       , extention.begin()
                       , [](unsigned char c){ return std::tolower(c); });

        return extention;
    }
};
