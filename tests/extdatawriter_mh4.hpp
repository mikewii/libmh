#pragma once
#include <libmh/MH4U/Extentions/qtds.hpp>
#include <libmh/MH4/Extdata.hpp>
#include <filesystem>
#include <fstream>
#include <vector>
#include <iostream>

class EXTDATAWriter
{
private:
    std::size_t m_current = 0;
    std::filesystem::path m_dirin;
    std::filesystem::path m_dirout;
    std::vector<std::filesystem::path> m_quest[mh::mh4::extdata::FILES_AMMOUNT];

public:
    EXTDATAWriter(const std::filesystem::path& dirin,
                  const std::filesystem::path& dirout)
        : m_dirin{dirin}
        , m_dirout{dirout}
    {
    }

    auto readFiles(void) noexcept -> void
    {
        std::error_code ec;
        auto perm = std::filesystem::directory_options::skip_permission_denied;

        for (const auto& entry : std::filesystem::directory_iterator(m_dirin, perm, ec))
            (void)filter(entry.path());
    }

    auto writeAll(void) -> bool
    {
        for (auto i = 0u; i < mh::mh4::extdata::FILES_AMMOUNT; i++)
            if (!write(i))
                return false;

        return true;
    }

    auto write(const std::size_t n) noexcept -> bool
    {
        if (n >= mh::mh4::extdata::FILES_AMMOUNT)
            return false;

        m_current = n;

        return write();
    }

private:
    auto write(void) -> bool
    {
        std::ofstream out(  m_dirout / ("quest" + std::to_string(m_current + 1))
                          , std::ios::out | std::ios::trunc | std::ios::binary);

        if (!out.is_open())
            return false;

        mh::mh4::extdata::Writer writer(out);

        auto fun = std::bind(  &EXTDATAWriter::istreamfun
                             , std::ref(*this)
                             , std::placeholders::_1);

        return writer.writeAll(fun);
    }

    auto istreamfun(const u16 id) noexcept(false) -> std::optional<std::unique_ptr<std::istream>>
    {
        if (id >= m_quest[m_current].size())
            return std::nullopt;

        auto stream = std::make_unique<std::ifstream>(  m_quest[m_current][id]
                                                      , std::ios::in | std::ios::binary);

        if (!stream->is_open())
            throw std::runtime_error("Failed to open ifstream: " + m_quest[m_current][id].string());

        return stream;
    }

    auto check(const std::filesystem::path& path) const noexcept -> bool
    {
        std::error_code ec;
        auto size = std::filesystem::file_size(path, ec);

        if (ec)
            std::cerr << ec.message() << std::endl;

        if (size > mh::mh4::extdata::QUEST_SIZE)
            return false;

        return true;
    }

    auto filter(const std::filesystem::path& path) -> bool
    {
        // quest1 60001 ~ 60110
        // quest2 60111 ~ 60130
        // quest3 60131 ~ 60136
        // quest4 61001 ~ 61109

        if (!check(path))
            return false;

        std::ifstream in(path);
        mh::mh4u::qtds::cQTDS qtds;

        if (!in.is_open())
            return false;

        if (!mh::mh4u::qtds::parseLite(in, qtds))
            return false;

        auto questID = qtds.getFlags().QuestID;

        if (   questID >= 60000
            && questID <= 60110) {
            m_quest[0].push_back(path);
        } else if (   questID > 60110
                   && questID <= 60130) {
            m_quest[1].push_back(path);
        } else if (   questID > 60130
                   && questID <= 60136) {
            m_quest[2].push_back(path);
        } else if (questID >= 61000) {
            m_quest[3].push_back(path);
        } else {
            throw std::runtime_error("Invalid quest id: " + std::to_string(questID));
        }

        return true;
    }
};
