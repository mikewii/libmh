#pragma once
#include <libmh/tools/noncopyable.hpp>
#include <libmh/tools/nonmovable.hpp>
#include <libmh/tools/arc/common.hpp>
#include <libmh/tools/arc/writer.hpp>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>

class STDFSIstreamFun
{
private:
    std::string m_metaName = "meta.json";
    nlohmann::ordered_json::array_t m_meta;
    std::vector<std::filesystem::path> m_files;
    std::filesystem::path m_prefix;
    std::filesystem::path m_dirpath;
    std::filesystem::path m_outpath;
    int m_compressionLevel;
    int m_compressionStrategy;
    bool m_valid;

public:
    NONCOPYABLE(STDFSIstreamFun)
    NONMOVABLE(STDFSIstreamFun)

    STDFSIstreamFun(const std::string& dirpath,
                    const std::string& outpath)
        : m_dirpath{dirpath}
        , m_outpath{outpath}
        , m_compressionLevel{Z_DEFAULT_COMPRESSION}
        , m_compressionStrategy{Z_DEFAULT_STRATEGY}
    {
        m_valid =    std::filesystem::exists(dirpath)
                  && std::filesystem::is_directory(dirpath);

        if (m_valid)
            getFilesRecursive();
    }

    auto filesNum(void) const noexcept -> u16
    {
        // cant be higher than u16
        return static_cast<u16>(m_files.size());
    }

    auto loadMeta(void) -> bool
    {
        std::ifstream meta(m_dirpath / m_metaName);

        if (!meta.is_open())
            return false;

        m_meta = nlohmann::ordered_json::parse(meta);

        return m_meta.empty() == false;
    }

    auto validateMeta(const bool checkFilesNum = true) -> bool
    {
        std::error_code ec;

        // check fields
        for (const auto& meta : m_meta)
            if (   !meta.contains("filepath")
                || !meta.contains("resource")
                || !meta.contains("flags"))
                return false;

        // check paths
        for (const auto& meta : m_meta)
            if (!std::filesystem::exists(  m_dirpath / meta["filepath"]
                                         , ec))
                return false;

        if (checkFilesNum)
            if (m_files.size() != m_meta.size())
                return false;

        return true;
    }

    auto getFilesRecursive(void) noexcept -> void
    {
        namespace fs = std::filesystem;

        std::error_code ec;
        auto perm = fs::directory_options::skip_permission_denied;
        fs::recursive_directory_iterator it(m_dirpath, perm, ec);

        for (const auto& entry : it) {
            if (entry.is_regular_file(ec)) {
                if (entry.path().filename() == m_metaName)
                    continue;

                m_files.push_back(std::move(entry.path()));
            }
        }
    }

    auto getFiles(void) noexcept -> void
    {
        namespace fs = std::filesystem;

        std::error_code ec;
        auto perm = fs::directory_options::skip_permission_denied;
        fs::directory_iterator it(m_dirpath, perm, ec);

        for (const auto& entry : it) {
            if (entry.is_regular_file(ec)) {
                if (entry.path().filename() == m_metaName)
                    continue;

                m_files.push_back(std::move(entry.path()));
            }
        }
    }

    auto isValid(void) const noexcept -> bool { return m_valid; }

    auto istreamfun(const u16 id) noexcept(false) -> mh::tools::arc::sWriteOp
    {
        if (id >= m_files.size())
            throw std::runtime_error("Bad id");

        auto pathWE = fixPath(m_files[id]);
        auto pathE = fixPath(m_files[id], false);
        mh::tools::arc::sWriteOp op;
        auto in = std::make_unique<std::ifstream>( m_files[id]
                                                  , std::ios::in | std::ios::binary);

        if (!in->is_open())
            throw std::runtime_error("Failed to open ifstream: " + m_files[id].string());

        if (!op.header.setFilePath(pathWE.string()))
            throw std::runtime_error("Failed to set path: " + m_files[id].string());

        bool pass = false;

        auto path = m_files[id].replace_extension();

        for (const auto& meta : m_meta) {
            if (static_cast<const std::string&>(meta["filepath"]) == pathE) {
                std::error_code ec;
                auto uncompressedSize = std::filesystem::file_size(  m_files[id]
                                                                   , ec);

                if (uncompressedSize == 0)
                    throw std::runtime_error("Incorrect file size");

                op.header.Flags = static_cast<u32>(meta["flags"]) & mh::tools::arc::FileHeader::FLAGS_MASK;
                op.header.UncompressedSize = uncompressedSize & mh::tools::arc::FileHeader::UNCOMPRESSED_SIZE_MASK;
                op.header.ResourceHash = meta["resource"];

                pass = true;
                break;
            }
        }

        if (!pass)
            throw std::runtime_error("Not found: " + pathE.string());

        op.op = mh::tools::arc::WriteOp::COMPRESS;
        op.istream = std::move(in);
        op.level = m_compressionLevel;
        op.strategy = m_compressionStrategy;

        return op;
    }

private:
    auto fixPath(std::filesystem::path path,
                 const bool removeExtention = true) -> std::filesystem::path
    {
        if (removeExtention)
            path.replace_extension();

        path = std::filesystem::relative(path, m_dirpath);

        return path;
    }
};
