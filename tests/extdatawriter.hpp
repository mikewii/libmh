#pragma once
#include <libmh/MH4U/Extentions/qtds.hpp>
#include <libmh/MH4U/Extdata.hpp>
#include <libmh/tools/istream.hpp>
#include <libmh/tools/ostream.hpp>
#include <filesystem>
#include <fstream>
#include <vector>
#include <iostream>

class EXTDATAWriter
{
private:
    std::size_t m_current = 0;
    std::filesystem::path m_dirin;
    std::filesystem::path m_dirout;
    std::vector<std::pair<  std::filesystem::path
                          , mh::crypto::Key::e>> m_quest[mh::mh4u::extdata::FILES_AMMOUNT];

public:
    EXTDATAWriter(const std::filesystem::path& dirin,
                  const std::filesystem::path& dirout)
        : m_dirin{dirin}
        , m_dirout{dirout}
    {
    }

    auto readFiles(void) noexcept -> void
    {
        std::error_code ec;
        auto perm = std::filesystem::directory_options::skip_permission_denied;

        for (const auto& entry : std::filesystem::directory_iterator(m_dirin, perm, ec))
            (void)filter(entry.path());
    }

    auto writeAll(void) -> bool
    {
        for (auto i = 0u; i < mh::mh4u::extdata::FILES_AMMOUNT; i++)
            if (!write(i))
                return false;

        return true;
    }

    auto write(const std::size_t n) noexcept -> bool
    {
        if (n >= mh::mh4u::extdata::FILES_AMMOUNT)
            return false;

        m_current = n;

        return write();
    }

private:
    auto write(void) -> bool
    {
        std::ofstream out(  m_dirout / ("quest" + std::to_string(m_current + 1))
                          , std::ios::out | std::ios::trunc | std::ios::binary);

        if (!out.is_open())
            return false;

        mh::mh4u::extdata::Writer writer(out);

        auto fun = std::bind(  &EXTDATAWriter::istreamfun
                             , std::ref(*this)
                             , std::placeholders::_1);

        return writer.writeAll(fun);
    }

    auto istreamfun(const u16 id) noexcept(false) -> std::optional<mh::mh4u::extdata::WriterStreamKey>
    {
        if (id >= m_quest[m_current].size())
            return std::nullopt;

        mh::mh4u::extdata::WriterStreamKey streamKey;

        auto stream = std::make_unique<std::ifstream>(  m_quest[m_current][id].first
                                                      , std::ios::in | std::ios::binary);

        if (!stream->is_open())
            throw std::runtime_error("Failed to open ifstream: " + m_quest[m_current][id].first.string());

        streamKey.Stream = std::move(stream);
        streamKey.Key = m_quest[m_current][id].second;

        return streamKey;
    }

    auto check(const std::filesystem::path& path) const noexcept -> bool
    {
        std::error_code ec;
        auto size = std::filesystem::file_size(path, ec);

        if (ec)
            std::cerr << ec.message() << std::endl;

        if (size == 0 || size > mh::mh4u::extdata::QUEST_SIZE)
            return false;

        return true;
    }

    auto isEncryptedQuest(std::ifstream& ifstream) -> std::pair<mh::crypto::Key::e, u16>
    {
        std::stringstream copy(std::ios::in | std::ios::out | std::ios::binary);
        mh::mh4u::qtds::cQTDS qtds;

        if (mh::mh4u::qtds::parseLite(ifstream, qtds))
            return {mh::crypto::Key::e::NONE, qtds.getFlags().QuestID};

        mh::tools::ostream::copy(  copy
                                 , ifstream
                                 , mh::tools::istream::size(ifstream));

        for (std::size_t i = mh::crypto::Key::e::MH4U_DLC_EUR_NA;
             i < mh::crypto::Key::e::LENGTH;
             i++) {
            copy.seekg(std::ios::beg, std::ios::beg);
            copy.clear();

            std::stringstream ss(std::ios::in | std::ios::out | std::ios::binary);
            mh::mh4u::qtds::cQTDS qtds;
            mh::crypto::Crypto c(copy, ss);

            c.decryptDLC(mh::crypto::Key::e(i));

            if (mh::mh4u::qtds::parseLite(ss, qtds))
                return {mh::crypto::Key::e(i), qtds.getFlags().QuestID};
        }

        return {mh::crypto::Key::e::LENGTH, 0};
    }

    auto filter(const std::filesystem::path& path) -> bool
    {
        // 60000 ~ 61000   quest 1 2 3
        // 61000 ~ 62000   quest 4
        // 62000 >         quest 5

        // quest1: 60001 - 60207
        // quest2: 60208 - 60251
        // quest3: empty in mh4u, 60229 - 60259 in mh4g
        // quest4: 61001 - 61208
        // quest5: 62101 - 62212

        if (!check(path))
            return false;

        std::ifstream in(path);

        if (!in.is_open())
            return false;

        auto keyId = isEncryptedQuest(in);
        auto& key = keyId.first;
        auto& questID = keyId.second;

        if (key == mh::crypto::Key::e::LENGTH)
            return false;

        if (   questID >= 60000
            && questID < 61000) {
            if (m_quest[0].size() < mh::mh4u::extdata::QUEST_FILES_AMMOUNT)
                m_quest[0].push_back({path, key});
            else if (m_quest[1].size() < mh::mh4u::extdata::QUEST_FILES_AMMOUNT)
                m_quest[1].push_back({path, key});
            else
                m_quest[2].push_back({path, key});
        } else if (   questID >= 61000
                   && questID < 62000) {
            m_quest[3].push_back({path, key});
        } else if (questID >= 62000) {
            m_quest[4].push_back({path, key});
        } else {
            throw std::runtime_error("Invalid quest id: " + std::to_string(questID));
        }

        return true;
    }
};
