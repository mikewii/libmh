#include "extdatawriter.hpp"

auto main() -> int
{
    std::error_code ec;
    std::filesystem::create_directory("assets/mh4u/extdata/packed", ec);

    EXTDATAWriter writer("assets/mh4u/extdata/unpacked",
                         "assets/mh4u/extdata/packed");

    writer.readFiles();
    writer.writeAll();

    return 0;
}
