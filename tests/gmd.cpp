#include <libmh/MH3U/Extentions/gmd.hpp>
#include <libmh/MHXX/Extentions/gmd.hpp>
#include <libmh/tools/rng.hpp>
#include <sstream>

template <typename T>
auto createBasicGMD(std::ostream& ostream) -> void
{
    T gmd;

    for (auto i = 0; i < mh::tools::random::randint(5, 10); i++)
        gmd.appendItem("Item" + std::to_string(i));

    if (!gmd.dump(ostream))
        throw std::runtime_error("Failed to dump gmd");
}

template <typename T>
auto parseGMD(std::istream& istream, std::ostream& ostream) -> void
{
    T gmd;

    if (!parse(istream, gmd))
        throw std::runtime_error("Failed to parse gmd");

    if (!gmd.dump(ostream))
        throw std::runtime_error("Failed to dump gmd");
}

template <typename T>
auto basicGMDTest(void) -> void
{
    std::stringstream in, out;

    createBasicGMD<T>(static_cast<std::ostream&>(in));
    parseGMD<T>(  static_cast<std::istream&>(in)
                , static_cast<std::ostream&>(out));

    if (in.str() != out.str())
        throw std::runtime_error("Stream diff failed");
}

auto main() -> int
{
    basicGMDTest<mh::mhxx::gmd::cGMD>();
    basicGMDTest<mh::mh3u::gmd::cGMD>();

    return 0;
}
