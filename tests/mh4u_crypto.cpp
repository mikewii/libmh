#include <libmh/MH/Crypto.hpp>
#include <libmh/tools/endian.hpp>
#include <libmh/tools/istream.hpp>
#include <libmh/tools/ostream.hpp>
#include <fstream>
#include <sstream>

auto encode(void) -> void
{
    std::string inpath = "/home/mw/Documents/test/test.poc.pb.cc";
    std::string outpath = "/home/mw/Documents/test/test.poc.pb.cc.bfsh";

    std::ifstream in(inpath, std::ios::in | std::ios::binary);
    std::ofstream out(outpath, std::ios::out | std::ios::binary);

    if (!in.is_open())
        throw std::runtime_error("Failed to open in");

    mh::crypto::Crypto crypto(  static_cast<std::istream&>(in)
                              , static_cast<std::ostream&>(out));

    //c.encode(Crypto::Key::DLC_EUR_NA);
    crypto.encryptSave();
}

auto decode(void) -> void
{
    std::string inpath = "/home/mw/Documents/test/test.poc.pb.cc.bfsh";
    std::string outpath = "/home/mw/Documents/test/test.poc.pb.cc.dec";

    std::ifstream in(inpath, std::ios::in | std::ios::binary);
    std::ofstream out(outpath, std::ios::out | std::ios::binary);

    mh::crypto::Crypto crypto(  static_cast<std::istream&>(in)
                              , static_cast<std::ostream&>(out));

    //c.decode(Crypto::Key::DLC_EUR_NA);
    crypto.decryptSave();
}

auto decode_cards(void) -> void
{
    std::string inpath = "/home/data/MonsterHunter/MH4U/extdata/mikewii/test/card2";
    std::string outpath = "/home/data/MonsterHunter/MH4U/extdata/mikewii/test/card2.dec";

    std::ifstream in(inpath, std::ios::in | std::ios::binary);
    std::ofstream out(outpath, std::ios::out | std::ios::trunc | std::ios::binary);

    mh::crypto::Crypto crypto(  static_cast<std::istream&>(in)
                              , static_cast<std::ostream&>(out));

    crypto.decryptCard();
}

auto decode_system(void) -> void
{
    std::string inpath = "/home/data/MonsterHunter/MH4U/extdata/mikewii/test/system";
    std::string outpath = "/home/data/MonsterHunter/MH4U/extdata/mikewii/test/system.dec";

    std::ifstream in(inpath, std::ios::in | std::ios::binary);
    std::ofstream out(outpath, std::ios::out | std::ios::trunc | std::ios::binary);

    mh::crypto::Crypto crypto(  static_cast<std::istream&>(in)
                              , static_cast<std::ostream&>(out));

    crypto.decryptSave();
}

auto decode_cardbox(void) -> void
{
    std::string inpath = "/home/data/MonsterHunter/MH4U/extdata/mikewii/test/cardbox";
    std::string outpath = "/home/data/MonsterHunter/MH4U/extdata/mikewii/test/cardbox.dec";

    std::ifstream in(inpath, std::ios::in | std::ios::binary);
    std::ofstream out(outpath, std::ios::out | std::ios::trunc | std::ios::binary);

    mh::crypto::Crypto crypto(  static_cast<std::istream&>(in)
                              , static_cast<std::ostream&>(out));

    crypto.decryptSave();
}

auto decrypt_quest(void) -> void
{
    std::string inpath = "/home/data/MonsterHunter/MH4U/TEST DATA/quest1.enc";
    std::string outpath = "/home/data/MonsterHunter/MH4U/TEST DATA/quest1.dec2";

    std::ifstream in(inpath, std::ios::in | std::ios::binary);
    std::ofstream out(outpath, std::ios::out | std::ios::binary);

    mh::crypto::Crypto crypto(  static_cast<std::istream&>(in)
                              , static_cast<std::ostream&>(out));

    crypto.decryptSave();
}

auto encrypt_quest(void) -> void
{
    std::string inpath = "/home/data/MonsterHunter/MH4U/TEST DATA/quest1.dec2";
    std::string outpath = "/home/data/MonsterHunter/MH4U/TEST DATA/quest1.enc";

    std::ifstream in(inpath, std::ios::in | std::ios::binary);
    std::ofstream out(outpath, std::ios::out | std::ios::binary);

    mh::crypto::Crypto crypto(  static_cast<std::istream&>(in)
                              , static_cast<std::ostream&>(out));

    crypto.encryptSave();
}

auto decode_palico(void) -> void
{
    std::string inpath = "/home/data/MonsterHunter/MH4U/extdata/mikewii/test/otomo1";
    std::string outpath = "/home/data/MonsterHunter/MH4U/extdata/mikewii/test/otomo1.dec";

    std::ifstream in(inpath, std::ios::in | std::ios::binary);
    std::ofstream out(outpath, std::ios::out | std::ios::trunc | std::ios::binary);

    mh::crypto::Crypto crypto(  static_cast<std::istream&>(in)
                              , static_cast<std::ostream&>(out));

    crypto.decryptSave();
}

auto main() -> int
{
    //decode_palico();

    //decode_system();
    //decode_cards();
    decrypt_quest();
    encrypt_quest();
    //decode_cardbox();

//    encode();
//    decode();

    return 0;
}
