#include <libmh/tools/arc/writer.hpp>
#include "stdfsistreamfun.hpp"

std::string path = "assets/resident_eng.arc";

std::string dirpath = "assets/resident_eng.arc.dearc";
std::string outpath = "assets/resident_eng_2.arc";

auto main() -> int
{
    if (!std::filesystem::exists(path))
        return 0;

    STDFSIstreamFun stdifstreamfun(dirpath, outpath);
    mh::tools::arc::Writer writer(path + ".out");
    auto istreamfun = std::bind(  &STDFSIstreamFun::istreamfun
                                , std::ref(stdifstreamfun)
                                , std::placeholders::_1);

    auto version = mh::tools::arc::sVersion::e::MH4U_MHXX_MHGU;

    if (!stdifstreamfun.loadMeta())
        throw std::runtime_error("Failed to load meta");

    if (!stdifstreamfun.validateMeta())
        throw std::runtime_error("Failed to validate meta");

//    if (!writer.compress(  stdifstreamfun.filesNum()
//                         , version
//                         , istreamfun))
//        throw std::runtime_error("Failed to compress");

    if (!writer.compress_async(  stdifstreamfun.filesNum()
                               , version
                               , istreamfun))
        throw std::runtime_error("Failed to async compress");

    return 0;
}
