#include "extdatawriter_mh4.hpp"

auto main() -> int
{
    std::error_code ec;
    std::filesystem::create_directory("assets/mh4/extdata/packed", ec);

    EXTDATAWriter writer("assets/mh4/extdata/unpacked",
                         "assets/mh4/extdata/packed");

    writer.readFiles();
    writer.writeAll();

    return 0;
}
