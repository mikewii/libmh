#include "extdatareader.hpp"
#include <libmh/MH4/Extdata.hpp>
#include <fstream>
#include <filesystem>

auto createDir(const std::filesystem::path& path) -> void
{
    std::error_code ec;
    std::filesystem::create_directory(path, ec);
}

auto main() -> int
{
    auto count = 1;
    std::error_code ec;
    auto perm = std::filesystem::directory_options::skip_permission_denied;
    std::filesystem::path pathin = "assets/mh4/extdata";
    std::filesystem::path pathout = "assets/mh4/extdata/unpacked";

    STDFSIOStreamFun fun(pathout);

    auto iostreamfun = std::bind(  &STDFSIOStreamFun::iostreamfun
                                 , std::ref(fun)
                                 , std::placeholders::_1);

    createDir(pathout);

    for (const auto& entry : std::filesystem::directory_iterator(pathin, perm, ec)) {
        if (   !entry.is_regular_file()
            || std::filesystem::file_size(entry) != mh::mh4::extdata::FILE_FULL_SIZE)
            continue;

        std::ifstream in(entry.path());
        mh::mh4::extdata::Reader reader(in);

        reader.dumpAll(iostreamfun);

        {
            std::filesystem::path rawPath = pathout;

            rawPath /= "raw" + std::to_string(count++);

            std::ofstream out(rawPath);

            reader.dumpRaw(out);
        }
    }

    return 0;
}
