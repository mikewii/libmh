#include <libmh/MHXX/Equipment/Hunter/Weapon/LevelData.hpp>
#include <libmh/tools/log.hpp>
#include <filesystem>
#include <fstream>
#include <vector>
#include <sstream>
#include <stdexcept>
#include <map>

using namespace mh::mhxx::weapon::leveldata;
using Type = Data_W00;

std::string path = "assets/mhxx/table/weapon00LevelData.w00d";
std::string filename = std::filesystem::path(path).filename().string();

template <typename T>
auto edit(cLevelData<T>& obj,
          std::ofstream& ofstream) -> void
{
    std::vector<T> dataVec;
    std::size_t id = 136;

    obj.getData(id, dataVec);

    for (auto& data : dataVec) {
        data.Common.UnknownP1 = 1;
        data.Common.UnknownP2 = 0;
        data.ElementAmmountP2 = 1;
        data.Unk4 = 0; // 18 70 159
        data.SharpnessFlags = 12; // 10
    }

    obj.setData(dataVec);
    obj.dump(ofstream);
}

template <typename T>
auto dumpUpgradeLevels(cLevelData<T>& obj, const std::string& path) -> void
{
    std::ofstream file(path, std::ios::out);
    std::map<u8, u8> map;

    for (const auto& item : obj.getData()) {
        auto id = item.Common.ID;
        auto level = item.Common.UpgradeLevel;

        map[id] = level;
    }

    for (auto pair : map) {
        LOG(  static_cast<int>(pair.first)
            , (int)pair.second);
    }
}

auto main() -> int
{
    LOG_SET_FLUSH(false);

    std::ifstream fileIn(path, std::ios::in | std::ios::binary);
    std::ofstream fileOut(filename, std::ios::out | std::ios::binary);
    cLevelData<Type> obj;

    if (!fileIn.is_open())
        throw std::runtime_error("Failed to open file " + path);

    if (!parse(fileIn, obj))
        throw std::runtime_error("Failed to parse LevelData");

//    edit(obj, fileOut);

//    dumpUpgradeLevels(obj, "/home/data/MonsterHunter/MHXX/TEST DATA/w0.txt");

    return 0;
}
