#pragma once
#include <libmh/tools/noncopyable.hpp>
#include <libmh/tools/nonmovable.hpp>
#include <libmh/MH4U/Extentions/qtds.hpp>
#include <memory>
#include <fstream>
#include <filesystem>
#include <iomanip>
#include <sstream>

class STDFSIOStreamFun
{
private:
    std::filesystem::path m_outdir;

public:
    STDFSIOStreamFun(const std::filesystem::path& outdir)
        : m_outdir{outdir}
    {
    }

    auto toString(const u16 questID) const noexcept -> std::string
    {
        std::stringstream ss;

        ss << std::setw(5) << std::setfill('0') << questID;

        return ss.str();
    }

    auto iostreamfun(const mh::mh4u::qtds::cQTDS& qtds) noexcept(false) -> std::unique_ptr<std::iostream>
    {
        std::filesystem::path path = "q" + toString(qtds.getFlags().QuestID) + ".qtds";
        std::filesystem::path full = m_outdir / path;

        auto iosFlags = std::ios::out | std::ios::trunc | std::ios::binary;

        auto stream = std::make_unique<std::fstream>(full, iosFlags);

        if (!stream->is_open())
            throw std::runtime_error("Failed to open fstream: " + full.string());

        return stream;
    }
};
