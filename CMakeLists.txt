# 3.14 for get_filename_component
cmake_minimum_required(VERSION 3.14)

project(mh VERSION 0.0.0.1 LANGUAGES C CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
option(ENABLE_ITEM_DESCRIPTIONS "Compile with item description strings [247kb]" ON)
option(ENABLE_ARMOR_DESCRIPTIONS "Compile with armor description strings [???kb]" ON)
option(ENABLE_WEAPON_DESCRIPTIONS "Compile with weapon description strings [???kb]" ON)
option(ENABLE_THREAD_LOCAL_RNG "RNG object per thread, uses more RAM" ON)
option(ENABLE_CTRPF "Use smaller buffer size for zlibtool " OFF)
option(ENABLE_NOEXCEPT_ISTREAM "Streams by default set fail bit instead of throwing exception" ON)
option(ENABLE_NOEXCEPT_OSTREAM "Streams by default set fail bit instead of throwing exception" ON)
option(ENABLE_TESTS "" ON)
option(ENABLE_MBEDTLS_RNG_ONCE "Initialize rng once" ON)
option(SKIP_INSTALL_ZLIB "" ON)
option(SKIP_INSTALL_MH "" ON)

# switch
option(ENABLE_READ "Use istream::read" ON)
option(ENABLE_BLOCKING_READ "Uses read instead of readsome if later dont return data" OFF)
option(ENABLE_READSOME "Use istream::readsome" OFF)

file(GLOB_RECURSE PROJECT_SOURCES CONFIGURE_DEPENDS RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/*.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp")
file(GLOB_RECURSE PROJECT_HEADERS CONFIGURE_DEPENDS RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}"
    "${CMAKE_CURRENT_SOURCE_DIR}/include/*.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/include/*.hpp")
file(GLOB_RECURSE PROJECT_TRANSLATION_SOURCES CONFIGURE_DEPENDS RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}"
    "${CMAKE_CURRENT_SOURCE_DIR}/translation/*.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/translation/*.cpp")
file(GLOB_RECURSE PROJECT_TRANSLATION_HEADERS CONFIGURE_DEPENDS RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}"
    "${CMAKE_CURRENT_SOURCE_DIR}/translation/*.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/translation/*.hpp")

set(PROJECT_FILES
    ${PROJECT_SOURCES}
    ${PROJECT_HEADERS}
    ${PROJECT_TRANSLATION_SOURCES}
    ${PROJECT_TRANSLATION_HEADERS})

add_library(${PROJECT_NAME} ${PROJECT_FILES})
add_library(${PROJECT_NAME}::${PROJECT_NAME} ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME} PRIVATE include translation)
target_include_directories(${PROJECT_NAME} SYSTEM INTERFACE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
)

include(cmake/helpers.cmake)
include(cmake/dependencies.cmake)
include(cmake/definitions.cmake)

target_link_options(${PROJECT_NAME} PRIVATE $<$<CONFIG:RELEASE>:-s>)
target_link_options(${PROJECT_NAME} PRIVATE $<$<CONFIG:RELEASE>:-fomit-frame-pointer>)

if (ENABLE_TESTS)
    enable_testing()
    add_subdirectory(tests)
endif()

enable_ccache()
create_target_directory_groups(${PROJECT_NAME})
