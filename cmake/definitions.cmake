if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")

elseif (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    target_compile_options(${PROJECT_NAME} PRIVATE -Wconversion -Wall -Wno-psabi)
elseif (CMAKE_CXX_COMPILER_ID STREQUAL "Intel")

elseif (CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")

endif()

if (ENABLE_DESCRIPTIONS)
    target_compile_definitions(${PROJECT_NAME} PRIVATE -DENABLE_DESCRIPTIONS)
endif()

if (ENABLE_THREAD_LOCAL_RNG)
    target_compile_definitions(${PROJECT_NAME} PRIVATE -DENABLE_THREAD_LOCAL_RNG)
endif()

if (ENABLE_CTRPF)
    target_compile_definitions(${PROJECT_NAME} PRIVATE -DENABLE_CTRPF)
endif()

if (ENABLE_READ)
    target_compile_definitions(${PROJECT_NAME} PRIVATE -DENABLE_READ)
elseif (ENABLE_BLOCKING_READ)
    target_compile_definitions(${PROJECT_NAME} PRIVATE -DENABLE_BLOCKING_READ)
elseif (ENABLE_READSOME)
    target_compile_definitions(${PROJECT_NAME} PRIVATE -DENABLE_READSOME)
endif()

if (ENABLE_NOEXCEPT_ISTREAM)
    target_compile_definitions(${PROJECT_NAME} PRIVATE -DENABLE_NOEXCEPT_ISTREAM)
endif()

if (ENABLE_NOEXCEPT_OSTREAM)
    target_compile_definitions(${PROJECT_NAME} PRIVATE -DENABLE_NOEXCEPT_OSTREAM)
endif()

if (ENABLE_MBEDTLS_RNG_ONCE)
    target_compile_definitions(${PROJECT_NAME} PRIVATE -DENABLE_MBEDTLS_RNG_ONCE)
endif()

if (NOT SKIP_INSTALL_MH)
    install(TARGETS ${PROJECT_NAME}
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
endif()

if (CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
    target_compile_definitions(${PROJECT_NAME} PRIVATE -D_SILENCE_CXX17_C_HEADER_DEPRECATION_WARNING)
endif()
