if (NOT TARGET zlibstatic)
    if (SKIP_INSTALL_ZLIB)
        set(SKIP_INSTALL_ALL ON CACHE BOOL "" FORCE)
    endif()

    add_subdirectory(extern/zlib EXCLUDE_FROM_ALL)
endif()

if (NOT TARGET MbedTLS::mbedtls)
    set(ENABLE_TESTING CACHE BOOL OFF FORCE)
    set(ENABLE_PROGRAMS CACHE BOOL OFF FORCE)
    set(INSTALL_MBEDTLS_HEADERS CACHE BOOL OFF FORCE)

    add_subdirectory(extern/mbedtls EXCLUDE_FROM_ALL)
endif()

#target_link_libraries(${PROJECT_NAME} PUBLIC m)
target_link_libraries(${PROJECT_NAME} PUBLIC zlibstatic)
target_link_libraries(${PROJECT_NAME} PUBLIC MbedTLS::mbedtls)
target_link_libraries(${PROJECT_NAME} PUBLIC MbedTLS::mbedx509)
target_link_libraries(${PROJECT_NAME} PUBLIC MbedTLS::mbedcrypto)
