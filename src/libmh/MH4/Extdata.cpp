#include "libmh/MH4/Extdata.hpp"
#include "libmh/MH/Crypto.hpp"
#include "libmh/tools/istream.hpp"
#include "libmh/tools/ostream.hpp"

namespace mh {
namespace mh4 {
namespace extdata {

Reader::Reader(std::istream& istream)
    : m_istream{istream}
    , m_full{std::ios::in | std::ios::out | std::ios::binary}
{
    (void)crypto();
}

auto Reader::dumpRaw(std::ostream& ostream) noexcept -> bool
{
    m_full.seekg(std::ios::beg, std::ios::beg);

    auto size = tools::istream::size(m_full);

    if (size <= 0)
        return false;

    tools::ostream::copy(  ostream
                         , m_full
                         , size);

    return true;
}

auto Reader::dumpAll(const iostreamFunType& fun, const bool readLanguages) noexcept(false) -> bool
{
    std::size_t questCount = tools::istream::size(m_full) / QUEST_SIZE;

    if (questCount == 0)
        return false;

    for (auto i = 0u; i < questCount; i++) {
        m_full.seekg(  QUEST_SIZE * i
                     , std::ios::beg);

        mh4u::qtds::cQTDS qtds;
        std::stringstream copy{std::ios::in | std::ios::out | std::ios::binary};

        tools::ostream::copy(  copy
                             , m_full
                             , QUEST_SIZE);

        if (!mh4u::qtds::parseLite(copy, qtds, readLanguages))
            continue;

        /// Rewind
        m_full.seekg(  QUEST_SIZE * i
                     , std::ios::beg);

        auto stream = fun(qtds);

        tools::ostream::copy(  *stream
                             , m_full
                             , QUEST_SIZE);
    }

    return true;
}

auto Reader::crypto(void) noexcept -> bool
{
    crypto::Crypto c(m_istream, m_full);

    return c.xorDecrypt();
}

Writer::Writer(std::ostream& ostream)
    : m_ostream{ostream}
    , m_full{std::ios::in | std::ios::out | std::ios::binary}
{
}

auto Writer::writeAll(const istreamFunType& fun) noexcept(false) -> bool
{
    for (u16 i = 0u; i < QUEST_FILES_AMMOUNT; i++) {
        auto stream = fun(i);

        if (stream == std::nullopt) {
            tools::ostream::fill(  m_full
                                 , QUEST_SIZE);

            continue;
        }

        auto size = tools::istream::size(*stream.value());
        mh4u::qtds::cQTDS qtds;

        if (!mh4u::qtds::parseLite(*stream.value(), qtds))
            continue;

        stream.value()->seekg(std::ios::beg, std::ios::beg);

        tools::ostream::copy(  m_full
                             , *stream.value()
                             , size);

        if (QUEST_SIZE - size > 0)
            tools::ostream::fill(  m_full
                                 , QUEST_SIZE - size);
    }

    std::size_t neededPos = FILE_FULL_SIZE - (sizeof(u32) * 2);
    std::size_t pos = m_full.tellp();

    if (pos < neededPos)
        tools::ostream::fill(  m_full
                             , neededPos - pos);

    m_full.seekp(std::ios::beg, std::ios::beg);

    crypto();

    return true;
}

auto Writer::crypto(void) noexcept -> void
{
    crypto::Crypto c(m_full, m_ostream);

    c.xorEncrypt();
}

}; /// namespace extdata
}; /// namespace mh4
}; /// namespace mh
