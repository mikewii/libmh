#include "libmh/tools/hex.hpp"
#include "libmh/tools/log.hpp"
#include <libmh/types.hpp>

#include <sstream>
#include <vector>

namespace mh {
namespace tools {

static auto collectHexLine(const u8* data, std::size_t dataSize) -> std::string
{
    std::stringstream ss;
    std::size_t size = 16 - dataSize <= 0 ? 16 : dataSize;
    std::vector<u32> vec;

    vec.reserve(16);

    for (auto i = 1u; i < size + 1; i++) {
        u32 value = static_cast<u32>(data[i - 1]);

        vec.push_back(value);

        ss << std::uppercase
           << std::hex
           << std::setfill('0')
           << std::setw(2)
           << value
           << ' ';

        if (i % 4 == 0)
            ss << ' ';
    }

    ss << " ";

    if (size < 16) {
        for (auto i = 0u; i < 16 - size; i++) {
            ss << "   ";

            if (i % 4 == 0)
                ss << ' ';
        }
    }

    for (auto value : vec) {
        if (std::isalpha(value))
            ss << static_cast<char>(value);
        else
            ss << '.';
    }

    return ss.str();
}

auto printHex(const void* data, unsigned int dataSize) -> void
{
    const u8* ptr = reinterpret_cast<const u8*>(data);
    std::size_t lastLineSize = dataSize % 16;
    std::size_t iterations = dataSize / 16;
    std::stringstream ss;
    bool restoreFlush = false;

    if (LOG_GET_FLUSH == false) {
        LOG_SET_FLUSH(true);
        restoreFlush = true;
    }

    for (auto i = 0u; i < iterations; i++) {
        std::string str = collectHexLine(ptr, 16);

        LOG(str);

        ptr += 16;
    }

    if (lastLineSize > 0) {
        std::string str = collectHexLine(ptr, lastLineSize);

        LOG(str);
    }


    LOG("");

    if (restoreFlush)
        LOG_SET_FLUSH(false);
}

}; /// namespace mh
}; /// namespace tools
