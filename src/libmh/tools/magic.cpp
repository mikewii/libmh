#include "libmh/tools/magic.hpp"
#include "libmh/tools/istream.hpp"
#include <istream>

namespace mh {
namespace tools {

auto sMagic::read(std::istream &istream,
                  const bool unwind) noexcept -> bool
{
    auto readMagic = [&](std::istream& istream)
    {
        char buffer[sizeof(sMagic)];

        istream::read(istream, buffer, sizeof(buffer));

        Raw = *reinterpret_cast<decltype(sMagic::Raw)*>(&buffer);
    };

    std::istream::pos_type oldPos;
    auto pass = false;

    if (unwind)
        oldPos = istream.tellg();

    readMagic(istream);

    std::streamsize expectedPos =   static_cast<std::streamsize>(oldPos)
                                  + static_cast<std::streamsize>(sizeof(sMagic));

    if (istream.tellg() == expectedPos)
        pass = true;

    if (unwind)
        istream.seekg(oldPos);

    return pass;
}

}; /// namespace mh
}; /// namespace tools
