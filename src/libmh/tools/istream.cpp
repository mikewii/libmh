#include "libmh/tools/istream.hpp"
#include "libmh/tools/log.hpp"
#include <istream>

#ifdef ENABLE_BLOCKING_READ
#include <mutex>
#endif

namespace mh {
namespace tools {
namespace istream {

auto read(std::istream& istream,
          void* out,
          const std::size_t size) NOEXCEPT_ISTREAM -> void
{
#if     defined(ENABLE_READ)
    istream.read(  static_cast<char*>(out)
                 , size);
#elif   defined(ENABLE_BLOCKING_READ)
    static std::mutex guard;

    std::lock_guard<std::mutex> lock(guard);

    istream.read(  static_cast<char*>(out)
                 , size);
#else
    istream.readsome(  static_cast<char*>(out)
                     , size);
#endif
}

auto size(std::istream& istream,
          const bool unwind) NOEXCEPT_ISTREAM -> std::streamsize
{
    std::istream::pos_type oldPos;
    std::istream::pos_type size;

    if (unwind)
        oldPos = istream.tellg();

    istream.seekg(std::ios::beg, std::ios::end);

    size = istream.tellg();

    if (unwind)
        istream.seekg(oldPos);
    else
        istream.seekg(std::ios::beg, std::ios::beg);

    return size;
}

auto readMagic(std::istream& istream,
               const bool unwind) NOEXCEPT_ISTREAM -> u32
{
    std::istream::pos_type pos;
    u32 magic;

    if (unwind)
        pos = istream.tellg();

    istream.read(  reinterpret_cast<char*>(&magic)
                 , sizeof(magic));

    if (unwind)
        istream.seekg(pos);

    return magic;
}

auto readMagic_s(std::istream& istream,
                 const bool unwind) NOEXCEPT_ISTREAM -> u32
{
    auto size = istream::size(istream, unwind);

    if (size < static_cast<decltype(size)>(sizeof(u32)))
        return 0;

    return readMagic(istream, unwind);
}

auto readU16String(std::istream& istream,
                   std::u16string& out) NOEXCEPT_ISTREAM -> bool
{
    std::istream::pos_type pos;
    std::u16string temp;
    char buffer[2];

    do {
        istream.read(buffer, sizeof(buffer));
        pos = istream.tellg();

        if (pos == -1)
            break;

        if (   buffer[0] == 0
            && buffer[1] == 0) {
            out = std::move(temp);

            return true;
        } else {
            temp.push_back(*reinterpret_cast<const utf16*>(buffer));
        }
    } while (!istream.eof());

    return false;
}

auto debug(std::istream& istream) NOEXCEPT_ISTREAM -> void
{
    LOG_DEBUG(  "tellg:    ", istream.tellg(), '\n'
              , "gcount:   ", istream.gcount(), '\n'
              , "good:     ", istream.good(), '\n'
              , "bad:      ", istream.bad(), '\n'
              , "eof:      ", istream.eof(), '\n'
              , "fail:     ", istream.fail(), '\n');
}

auto seekg(std::istream& istream,
           const std::ostream::pos_type pos) NOEXCEPT_ISTREAM -> std::istream&
{
    return istream.seekg(pos);
}

auto seekg(std::istream& istream,
           const std::ostream::off_type off,
           const std::ios_base::seekdir dir) NOEXCEPT_ISTREAM -> std::istream&
{
    return istream.seekg(off, dir);
}

auto clear(std::istream& istream,
           const std::ios_base::iostate state) NOEXCEPT_ISTREAM -> void
{
    istream.clear(state);
}

}; /// namespace istream
}; /// namespace tools
}; /// namespace mh
