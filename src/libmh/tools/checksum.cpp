#include "libmh/tools/checksum.hpp"
#include <zlib.h>
#include <istream>

namespace mh {
namespace tools {

auto checksum(const void *data, const std::size_t size) noexcept -> std::size_t
{
    uLong adler = adler32_z(0L, Z_NULL, 0);

    return adler32_z(  adler
                     , static_cast<const Bytef*>(data)
                     , size);
}

auto checksum(std::istream &istream) noexcept -> std::size_t
{
    char buffer[CHUNK];
    uLong adler = adler32_z(0L, Z_NULL, 0);

    istream.seekg(  std::ios::beg
                  , std::ios::beg);

    do {
        istream.readsome(  buffer
                         , sizeof(buffer));

        auto bytesRead = istream.gcount();

        adler = adler32_z( adler
                          , reinterpret_cast<const Bytef*>(buffer)
                          , bytesRead);

    } while (!istream.eof());

    return adler;
}

auto checksum32(std::istream& istream, const bool unwind) noexcept -> std::size_t
{
    char buffer;
    std::size_t sum = 0;
    std::istream::pos_type pos;

    if (unwind)
        pos = istream.tellg();

    while (!istream.eof()) {
        istream.read(&buffer, sizeof(buffer));

        if (istream.tellg() == -1)
            break;

        if (istream.gcount() > 0) {
            sum += buffer & 0xFF;
        }
    }

    istream.clear();

    if (unwind)
        istream.seekg(pos);

    return sum;
}

}; /// namespace tools
}; /// namespace mh
