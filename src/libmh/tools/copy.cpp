#include "libmh/tools/copy.hpp"
#include <cstring>

namespace mh {
namespace tools {

auto copyBytes(void* dest,
               const void* src,
               const std::size_t size) noexcept -> void*
{
    auto end = reinterpret_cast<std::size_t>(dest) + size;

    // overlap check:
    // if end is in range of [src ... src + size] - its overlapping
    if (   end >= reinterpret_cast<std::size_t>(src)
        && end <= reinterpret_cast<std::size_t>(src) + size)
        return std::memmove(dest, src, size);

    // alignment check:
    // if modulus of dest and ptr size is 0 - its aligned
    if ((reinterpret_cast<std::size_t>(dest) % sizeof(dest)) == 0)
        return std::memcpy(dest, src, size);

    return std::memmove(dest, src, size);
}

}; /// namespace tools
}; /// namespace mh
