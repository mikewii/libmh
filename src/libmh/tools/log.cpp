#include "libmh/tools/log.hpp"
#include <mbedtls/error.h>
#include <mbedtls/psa_util.h>

namespace mh {
namespace tools {
namespace log {

std::mutex mutex;
std::string prefix{};
bool flush{false};

auto printMbedtlsErr(FILE *stream, const bool newline, const char *str, const int errcode) -> bool
{
    std::lock_guard<std::mutex> lock(mutex);
    std::stringstream ss;
    std::string errstr(256, 0);

    if (!prefix.empty())
        addPrefix(ss, prefix);

    mbedtls_strerror(errcode, errstr.data(), errstr.size());

    appendArg(ss, str);
    appendArg(ss, errstr);

    if (newline)
        std::fprintf(stream, "%s\n", ss.str().c_str());
    else
        std::fprintf(stream, "%s", ss.str().c_str());

    if (flush)
        std::fflush(stream);

    return errcode == 0;
}

}; /// namespace log
}; /// namespace tools
}; /// namespace mh
