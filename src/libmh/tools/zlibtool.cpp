#include "libmh/tools/zlibtool.hpp"
#include "libmh/tools/istream.hpp"
#include <ostream>
#include <fstream>

#if defined(ENABLE_BLOCKING_READ)
#include <mutex>
#endif

namespace mh {
namespace tools {

static constexpr auto isZlibError(const int error) noexcept -> bool
{
    switch (error) {
    default:break;
    case Z_NEED_DICT:
    case Z_ERRNO:
    case Z_STREAM_ERROR:
    case Z_DATA_ERROR:
    case Z_MEM_ERROR:
    /* why the fuck is this called error
     * if you suppose to ignore it
     * when you have incoming data
     * compressed wth Z_NO_COMPRESSION ?!?!
     */
//    case Z_BUF_ERROR:
    case Z_VERSION_ERROR:
        return true;
    }

    return false;
}

ZLIBTool::ZLIBTool(std::istream &istream,
                   std::ostream &ostream)
    : m_istream{istream}
    , m_ostream{ostream}
{
}

auto ZLIBTool::uncompress(const std::streamsize compressedSize,
                          std::size_t& uncompressedSize,
                          std::size_t uncompressedSizeLimit) noexcept -> bool
{
    init();

    auto res = Z_OK;

    if (inflateInit(&m_zstream) != Z_OK)
        return false;

    do {
        // not sure if this even needed
        if (   compressedSize != -1
            && m_zstream.total_in == static_cast<uLong>(compressedSize))
            break;

        auto bytesRead = read();

        if (bytesRead <= 0)
            break;

        m_zstream.avail_in = static_cast<decltype(m_zstream.avail_in)>(bytesRead);
        m_zstream.next_in = m_in;

        res = inflate(  uncompressedSize
                      , uncompressedSizeLimit);

        if (isZlibError(res))
            break;

    } while (res != Z_STREAM_END);

    (void)inflateEnd(&m_zstream);

    return res == Z_STREAM_END;
}

auto ZLIBTool::compress(std::size_t& uncompressedSize,
                        std::size_t& compressedSize,
                        const int level,
                        const int strategy) noexcept -> bool
{
    init();

    auto res = Z_OK;
    auto flush = Z_NO_FLUSH;

    if (deflateInit(&m_zstream, level) != Z_OK)
        return false;

    if (deflateParams(&m_zstream, level, strategy) != Z_OK)
        return false;

    do {
        auto bytesRead = read();

        m_zstream.avail_in = static_cast<decltype(m_zstream.avail_in)>(bytesRead);
        m_zstream.next_in = m_in;

        flush = m_istream.eof() ? Z_FINISH : Z_NO_FLUSH;

        res = deflate(flush);

        if (isZlibError(res))
            break;

    } while (flush != Z_FINISH);

    (void)deflateEnd(&m_zstream);

    uncompressedSize = m_zstream.total_in;
    compressedSize = m_zstream.total_out;

    return res == Z_STREAM_END;
}

auto ZLIBTool::checksum(void) const noexcept -> uLong
{
    return m_zstream.adler;
}

auto ZLIBTool::init(void) noexcept -> void
{
    m_zstream.next_in = Z_NULL;
    m_zstream.avail_in = 0;
    m_zstream.total_in = 0;

    m_zstream.next_out = Z_NULL;
    m_zstream.avail_out = 0;
    m_zstream.total_out = 0;

    m_zstream.zalloc = Z_NULL;
    m_zstream.zfree = Z_NULL;
    m_zstream.opaque = Z_NULL;

    m_zstream.adler = 0;
}

auto ZLIBTool::write(const std::size_t size) noexcept -> std::streamsize
{
    auto curPos = m_ostream.tellp();

    m_ostream.write(  reinterpret_cast<char*>(m_out)
                    , size);

    auto nextPos = m_ostream.tellp();
    auto bytesWriten = nextPos - curPos;

    return bytesWriten;
}

auto ZLIBTool::read(void) noexcept -> std::streamsize
{
    tools::istream::read(  m_istream
                         , m_in
                         , CHUNK);

    // clear fail bit
    if (m_istream.eof())
        m_istream.clear(std::ios::eofbit);

    return m_istream.gcount();
}

auto ZLIBTool::inflate(std::size_t& uncompressSize,
                       const std::size_t uncompressSizeLimit) noexcept -> int
{
    auto res = Z_OK;
    auto flush = m_istream.eof() ? Z_FINISH : Z_NO_FLUSH;

    do {
        m_zstream.avail_out = CHUNK;
        m_zstream.next_out = m_out;

        res = ::inflate(&m_zstream, flush);

        if (isZlibError(res)) {
            (void)inflateEnd(&m_zstream);
            return res;
        }

        auto have = CHUNK - m_zstream.avail_out;

        if (   uncompressSizeLimit > 0
            && have >= uncompressSizeLimit) {
            if (write(uncompressSizeLimit) != static_cast<std::streamsize>(uncompressSizeLimit)) {
                (void)inflateEnd(&m_zstream);
                return Z_ERRNO;
            } else {
                uncompressSize += uncompressSizeLimit;
            }

            return Z_STREAM_END;
        }

        if (write(have) != have) {
            (void)inflateEnd(&m_zstream);
            return Z_ERRNO;
        } else {
            uncompressSize += have;
        }
    } while (m_zstream.avail_out == 0);

    return res;
}

int ZLIBTool::deflate(const int flush) noexcept
{
    auto res = Z_OK;

    do {
        m_zstream.avail_out = CHUNK;
        m_zstream.next_out = m_out;

        res = ::deflate(&m_zstream, flush);

        if (res == Z_STREAM_ERROR)
            break;

        auto have = CHUNK - m_zstream.avail_out;

        if (write(have) != have) {
            (void)deflateEnd(&m_zstream);
            return Z_ERRNO;
        }
    } while (m_zstream.avail_out == 0);

    return res;
}

}; /// namespace tools
}; /// namespace mh
