#include "libmh/tools/arc/reader.hpp"
#include <fstream>
#include <future>
#include <deque>
#include <mutex>
#include <numeric>

namespace mh {
namespace tools {
namespace arc {

Reader::Reader()
    : m_istream{nullptr}
    , m_uncompressedSizeLimit{0}
    , m_threadNum{0}
    , m_open{false}
    , m_read{false}
{
}

Reader::Reader(const std::string& path)
    : m_istream{std::make_unique<std::ifstream>(path, std::ios::in | std::ios::binary)}
    , m_filepath{path}
    , m_uncompressedSizeLimit{0}
    , m_threadNum{0}
    , m_read{false}
{
    m_open = static_cast<std::ifstream&>(*m_istream).is_open();
}

Reader::Reader(std::unique_ptr<std::istream>&& istream) noexcept
    : m_istream{std::move(istream)}
    , m_uncompressedSizeLimit{0}
    , m_threadNum{0}
    , m_open{true}
    , m_read{false}
{
}

Reader::Reader(const void* data, const std::size_t size)
    : m_istream{std::make_unique<imemstream>(data, size)}
    , m_uncompressedSizeLimit{0}
    , m_threadNum{0}
    , m_open{true}
    , m_read{false}
{
}

auto Reader::read(void) noexcept -> bool
{
    if (!haveIStream() || !isIStreamOpen())
        return false;

    m_read = arcimpl::Reader::read(*m_istream);

    return m_read;
}

auto Reader::uncompress(const u16 id,
                        std::ostream& ostream) noexcept -> bool
{
    if (   !haveIStream()
        || !isIStreamOpen()
        || !m_read)
        return false;

    const auto& header = getFileHeaders()[id];
    std::size_t uncompressedSize = 0;

    if (!arcimpl::Reader::uncompress(  id
                                     , *m_istream
                                     , ostream
                                     , uncompressedSize
                                     , m_uncompressedSizeLimit))
        return false;

    if (m_uncompressedSizeLimit > 0) {
        if (uncompressedSize != m_uncompressedSizeLimit)
            return false;
    } else {
        if (uncompressedSize != header.UncompressedSize)
            return false;
    }

    return true;
}

auto Reader::uncompress(const FileHeader& header,
                        std::ostream& ostream) noexcept -> bool
{
    if (   !haveIStream()
        || !isIStreamOpen()
        || !m_read)
        return false;

    std::size_t uncompressedSize = 0;
    ZLIBTool zlib(*m_istream, ostream);

    m_istream->seekg(header.pZData, std::ios::beg);

    if (m_istream->tellg() != header.pZData)
        return false;

    if (!zlib.uncompress(  header.CompressedSize
                         , uncompressedSize
                         , m_uncompressedSizeLimit))
        return false;

    if (m_uncompressedSizeLimit > 0) {
        if (uncompressedSize != m_uncompressedSizeLimit)
            return false;
    } else {
        if (uncompressedSize != header.UncompressedSize)
            return false;
    }

    return true;
}

auto Reader::uncompress_all_async(const std::string& path,
                                  iostreamFunType iostreamfun,
                                  std::optional<streamVectorRef> streamVec) noexcept(false) -> bool
{
    if (   !haveIStream()
        || !isIStreamOpen()
        || !m_read)
        return false;

    auto uncompress_a = [&](std::deque<std::size_t>& deque,
                            std::mutex& dequeMutex,
                            std::mutex& vectorMutex) -> bool
    {
        std::ifstream in(path, std::ios::in | std::ios::binary);

        if (!in.is_open())
            return false;

        std::size_t id = 0;

        while (true) {
            {
                std::lock_guard<std::mutex> lock(dequeMutex);

                if (deque.empty())
                    break;

                id = deque.front();
                deque.pop_front();
            }

            const auto& header = getFileHeaders()[id];
            auto&& ostream = iostreamfun(header);
            std::size_t uncompressedSize = 0;

            if (!arcimpl::Reader::uncompress(  id
                                             , in
                                             , *ostream
                                             , uncompressedSize
                                             , m_uncompressedSizeLimit))
                return false;

            if (m_uncompressedSizeLimit > 0) {
                if (uncompressedSize != m_uncompressedSizeLimit)
                    return false;
            } else {
                if (uncompressedSize != header.UncompressedSize)
                    return false;
            }

            if (streamVec == std::nullopt)
                continue;

            {
                std::lock_guard<std::mutex> lock(vectorMutex);

                streamVec->get().emplace_back(std::move(ostream), header);
            }
        }

        return true;
    };

    bool pass = true;
    std::vector<std::future<bool>> futures;
    std::deque<std::size_t> deque(getHeader().FilesNum);
    std::mutex dequeMutex;
    std::mutex vectorMutex;
    std::size_t threads = 0;

    if (m_threadNum == 0)
        threads = std::thread::hardware_concurrency();
    else
        threads = m_threadNum;

    futures.reserve(threads);

    std::iota(  deque.begin()
              , deque.end()
              , 0);

    for (auto i = 0u; i < threads; i++)
        futures.emplace_back(std::async(  std::launch::async
                                        , uncompress_a
                                        , std::ref(deque)
                                        , std::ref(dequeMutex)
                                        , std::ref(vectorMutex)));

    for (auto& f : futures)
        pass &= f.get();

    return pass;
}

auto Reader::uncompress_all(iostreamFunType iostreamfun,
                            std::optional<streamVectorRef> streamVec) noexcept(false) -> bool
{
    if (   !haveIStream()
        || !isIStreamOpen()
        || !m_read)
        return false;

    for (auto i = 0u; i < m_headers.size(); i++) {
        const auto& header = m_headers[i];
        std::size_t uncompressedSize = 0;

        auto&& stream = iostreamfun(header);

        if (!arcimpl::Reader::uncompress(  i
                                         , *m_istream
                                         , *stream
                                         , uncompressedSize
                                         , m_uncompressedSizeLimit))
            return false;

        if (m_uncompressedSizeLimit > 0) {
            if (uncompressedSize != m_uncompressedSizeLimit)
                return false;
        } else {
            if (uncompressedSize != header.UncompressedSize)
                return false;
        }

        if (streamVec == std::nullopt)
            continue;

        streamVec->get().emplace_back(std::move(stream), header);
    }

    return true;
}

auto Reader::setThreadsNum(const std::size_t num) noexcept -> void { m_threadNum = num; }
auto Reader::getThreadsNum(void) const noexcept -> std::size_t { return m_threadNum; }

auto Reader::setIStream(std::unique_ptr<std::istream>&& istream) noexcept -> void
{
    m_istream = std::move(istream);
    m_open = true;
}

auto Reader::setIStream(std::unique_ptr<std::ifstream>&& ifstream) noexcept -> void
{
    m_istream = std::move(ifstream);
    m_open = static_cast<std::ifstream&>(*m_istream).is_open();
}

auto Reader::getIStream(void) -> std::unique_ptr<std::istream>&
{
    return m_istream;
}

auto Reader::getFilePath(void) const noexcept -> const std::string& { return m_filepath; }
auto Reader::setFilePath(const std::string& path) noexcept -> void { m_filepath = path; }

auto Reader::setUncompressedSizeLimit(const std::size_t limit) noexcept -> void { m_uncompressedSizeLimit = limit; }
auto Reader::getUncompressedSizeLimit(void) const noexcept -> std::size_t { return m_uncompressedSizeLimit; }

auto Reader::resetIStream(void) noexcept -> void
{
    m_istream.reset(nullptr);
    m_open = false;
}

auto Reader::haveIStream(void) const noexcept -> bool { return m_istream ? true : false; }
auto Reader::isIStreamOpen(void) const noexcept -> bool { return m_open; }

}; /// namespace arc
}; /// namespace tools
}; /// namespace mh
