#include "libmh/tools/arc/writerimpl.hpp"
#include "libmh/tools/istream.hpp"
#include "libmh/tools/ostream.hpp"

#include <istream>
#include <algorithm>

namespace mh {
namespace tools {
namespace arcimpl {

static constexpr auto align(std::size_t value) noexcept -> std::size_t
{
    constexpr const std::size_t alignment = sizeof(u64) * 2; // file alignment

    while (value % alignment != 0)
        ++value;

    return value;
}

auto Writer::compress(std::istream& istream,
                      std::ostream& ostream,
                      std::size_t& uncompressedSize,
                      std::size_t& compressedSize,
                      const int level,
                      const int strategy) noexcept -> bool
{
    ZLIBTool zlib(istream, ostream);

    return zlib.compress(  uncompressedSize
                         , compressedSize
                         , level
                         , strategy);
}
\
auto Writer::copy(std::istream& istream,
                  std::ostream& ostream,
                  const std::size_t n) noexcept -> void
{
    ostream::copy(ostream, istream, n);
}

auto Writer::copy_s(std::istream& istream,
                    std::ostream& ostream,
                    const std::size_t n) noexcept -> bool
{
    std::streamsize size = mh::tools::istream::size(istream, true);

    std::streamsize IOldPos = istream.tellg();
    std::streamsize OOldPos = ostream.tellp();

    if (static_cast<std::streamsize>(IOldPos + n - 1) >= size)
        return false;

    ostream::copy(ostream, istream, n);

    std::streamsize INewPos = istream.tellg();
    std::streamsize ONewPos = ostream.tellp();

    return    INewPos == static_cast<std::streamsize>(IOldPos + n - 1)
           && ONewPos == static_cast<std::streamsize>(OOldPos + n);
}

auto Writer::seekToZLIBData(const std::size_t arcFileHeadersNum,
                            const std::size_t padding,
                            std::ostream& ostream) noexcept -> bool
{
    auto seekPos =   sizeof(arc::Header)
                   + padding
                   + sizeof(u64) * 2 // TODO: pass version or flag maybe?
                   + (arcFileHeadersNum * sizeof(arc::FileHeader));

    seekPos = align(seekPos);

    ostream.seekp(  seekPos
                  , std::ios::beg);

    auto pos = ostream.tellp();

    return pos == static_cast<decltype(pos)>(seekPos);
}

auto Writer::writeHeader(const arc::Header& header,
                         std::ostream& ostream) noexcept -> bool
{
    auto padding = arc::sVersion::getPadding(header.Version);

    ostream.seekp(  std::ios::beg
                  , std::ios::beg);

    ostream.write(  reinterpret_cast<const char*>(&header)
                  , sizeof(arc::Header));

    ostream.seekp(  padding
                  , std::ios::cur);

    auto pos = ostream.tellp();

    return pos == static_cast<decltype(pos)>(  sizeof(arc::Header)
                                             + padding);
}

auto Writer::writeARCFileHeaders(const std::vector<arc::FileHeader>& headers,
                                 const std::size_t padding,
                                 std::ostream& ostream) noexcept -> bool
{
    ostream.seekp(  sizeof(arc::Header) + padding
                  , std::ios::beg);

    for (auto i = 0u; i < headers.size(); i++)
        ostream.write(  reinterpret_cast<const char*>(&headers[i])
                      , sizeof(arc::FileHeader));

    auto pos = ostream.tellp();

    auto expectedPos =   sizeof(arc::Header)
                       + padding
                       + (headers.size() * sizeof(arc::FileHeader));

    return pos == static_cast<decltype(pos)>(expectedPos);
}

}; /// namespace arc
}; /// namespace tools
}; /// namespace mh
