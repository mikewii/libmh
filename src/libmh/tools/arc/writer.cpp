#include "libmh/tools/arc/writer.hpp"
#include <fstream>
#include <future>
#include <deque>
#include <numeric>
#include <sstream>
#include <condition_variable>

namespace mh {
namespace tools {
namespace arc {

class CVSync
{
public:
    auto increment(void) noexcept -> void
    {
        ++m_id;

        m_cv.notify_all();
    }

    auto wait(const std::size_t id) -> void
    {
        std::unique_lock<std::mutex> lock(m_mutex);

        m_cv.wait(  lock
                  , [&]() { return m_id == id; });
    }

private:
    std::size_t m_id = 0;
    std::mutex m_mutex;
    std::condition_variable m_cv;
};

Writer::Writer()
    : m_ostream{nullptr}
    , m_threadNum{0}
    , m_open{false}
{
}

Writer::Writer(const std::string& path)
    : m_ostream{std::make_unique<std::ofstream>(path, std::ios::out | std::ios::binary)}
    , m_filepath{path}
    , m_threadNum{0}
{
    m_open = static_cast<std::ofstream&>(*m_ostream).is_open();
}

Writer::Writer(std::unique_ptr<std::ostream>&& ostream) noexcept
    : m_ostream{std::move(ostream)}
    , m_threadNum{0}
    , m_open{true}
{
}

auto Writer::compress(const u16 filesNum,
                      const sVersion::e version,
                      istreamFunType istreamFun,
                      const bool isBE) noexcept(false) -> bool
{
    if (!haveOStream() || !isOStreamOpen())
        return false;

    std::vector<FileHeader> headers;

    headers.reserve(filesNum);

    if (!writeHeader(  filesNum
                     , version
                     , isBE))
        return false;

    if (!arcimpl::Writer::seekToZLIBData(  filesNum
                                         , sVersion::getPadding(version)
                                         , *m_ostream))
        return false;

    for (u16 i = 0u; i < filesNum; i++)
        if (!doCompress(  i
                        , istreamFun
                        , isBE
                        , headers))
            return false;

    for (auto& header : headers)
        header.fixFilePath();

    if(!arcimpl::Writer::writeARCFileHeaders(  headers
                                             , sVersion::getPadding(version)
                                             , *m_ostream))
        return false;

    return true;
}

auto Writer::compress_async(const u16 filesNum,
                            const sVersion::e version,
                            istreamFunType istreamFun,
                            const bool isBE) noexcept(false) -> bool
{
    if (!haveOStream() || !isOStreamOpen())
        return false;

    auto compress_a = [&](std::deque<std::size_t>& deque,
                          std::mutex& dequeMutex,
                          CVSync& sync,
                          std::vector<FileHeader>& headers) -> bool
    {
        while (true) {
            std::size_t id = 0;
            std::size_t compressedSize = 0;

            {
                std::lock_guard<std::mutex> lock(dequeMutex);

                if (deque.empty())
                    return true;

                id = deque.front();
                deque.pop_front();
            }

            auto&& writeOp = istreamFun(static_cast<u16>(id));

            if (!writeOp.header.isValid())
                return false;

            switch (writeOp.op) {
            default:break;
            case WriteOp::COPY: {
                sync.wait(id);

                auto pos = writeOp.header.pZData;

                compressedSize = writeOp.header.CompressedSize;

                // seek input stream to pos
                writeOp.istream->seekg(pos);

                // update header to output stream pos
                writeOp.header.pZData = static_cast<u32>(m_ostream->tellp());

                arcimpl::Writer::copy(  *writeOp.istream
                                      , *m_ostream
                                      , compressedSize);

                headers.push_back(std::move(writeOp.header));

                sync.increment();

                break;
            }
            case WriteOp::COMPRESS: {
                std::stringstream buffer(std::ios::in | std::ios::out | std::ios::binary);
                std::size_t uncompressedSize = 0;

                if (!arcimpl::Writer::compress(  *writeOp.istream
                                               , buffer
                                               , uncompressedSize
                                               , compressedSize
                                               , writeOp.level
                                               , writeOp.strategy))
                    return false;

                sync.wait(id);

                writeOp.header.UncompressedSize = uncompressedSize & FileHeader::UNCOMPRESSED_SIZE_MASK;
                writeOp.header.CompressedSize = static_cast<u32>(compressedSize);
                writeOp.header.pZData = static_cast<u32>(m_ostream->tellp());

                arcimpl::Writer::copy(  buffer
                                      , *m_ostream
                                      , compressedSize);

                headers.push_back(std::move(writeOp.header));

                sync.increment();

                break;
            }
            };
        }

        return true;
    };

    std::vector<FileHeader> headers;
    std::vector<std::future<bool>> futures;
    std::deque<std::size_t> deque(filesNum);
    std::mutex dequeMutex;
    CVSync sync;
    std::size_t threads = 0;

    if (m_threadNum == 0)
        threads = std::thread::hardware_concurrency();
    else
        threads= m_threadNum;

    futures.reserve(threads);
    headers.reserve(filesNum);

    if (!writeHeader(  filesNum
                     , version
                     , isBE))
        return false;

    if (!arcimpl::Writer::seekToZLIBData(  filesNum
                                         , sVersion::getPadding(version)
                                         , *m_ostream))
        return false;

    std::iota(  deque.begin()
              , deque.end()
              , 0);

    for (auto i = 0u; i < threads; i++)
        futures.emplace_back(std::async(  std::launch::async
                                        , compress_a
                                        , std::ref(deque)
                                        , std::ref(dequeMutex)
                                        , std::ref(sync)
                                        , std::ref(headers)));

    for (auto& f : futures)
        if (!f.get())
            return false;

    if (isBE)
        for (auto& header : headers)
            header.toBigEndian();

    for (auto& header : headers)
        header.fixFilePath();

    if(!arcimpl::Writer::writeARCFileHeaders(  headers
                                              , sVersion::getPadding(version)
                                              , *m_ostream))
        return false;

    return true;
}

auto Writer::setThreadsNum(const std::size_t num) noexcept -> void { m_threadNum = num; }
auto Writer::getThreadsNum(void) const noexcept -> std::size_t { return m_threadNum; }

auto Writer::setOStream(std::unique_ptr<std::ostream>&& ostream) noexcept -> void
{
    m_ostream = std::move(ostream);
    m_open = true;
}

auto Writer::setOStream(std::unique_ptr<std::ofstream>&& ofstream) noexcept -> void
{
    m_ostream = std::move(ofstream);
    m_open = static_cast<std::ofstream&>(*m_ostream).is_open();
}

auto Writer::getFilePath(void) const noexcept -> const std::string& { return m_filepath; }
auto Writer::setFilePath(const std::string& path) noexcept -> void { m_filepath = path; }

auto Writer::resetOStream(void) noexcept -> void
{
    m_ostream.reset(nullptr);
    m_open = false;
}

auto Writer::haveOStream(void) const noexcept -> bool { return m_ostream ? true : false; }
auto Writer::isOStreamOpen(void) const noexcept -> bool { return m_open; }

auto Writer::writeHeader(const u16 filesNum,
                         const sVersion::e version,
                         const bool isBE) noexcept -> bool
{
    Header header(  version
                  , filesNum
                  , isBE);

    return arcimpl::Writer::writeHeader(  header
                                        , *m_ostream);
}

auto Writer::doCompress(const u16 id,
                        const istreamFunType& istreamFun,
                        const bool isBE,
                        std::vector<FileHeader>& headers) noexcept(false) -> bool
{
    auto&& writeOp = istreamFun(id);

    if (!writeOp.header.isValid())
        return false;

    auto pZData = m_ostream->tellp();

    switch (writeOp.op) {
    default:break;
    case WriteOp::COPY: {
        auto size = writeOp.header.CompressedSize;
        auto pos = writeOp.header.pZData;

        writeOp.istream->seekg(pos);

        arcimpl::Writer::copy(  *writeOp.istream
                              , *m_ostream
                              , size);
        break;
    }
    case WriteOp::COMPRESS: {
        std::size_t uncompressedSize = 0;
        std::size_t compressedSize = 0;

        if (!arcimpl::Writer::compress(  *writeOp.istream
                                       , *m_ostream
                                       , uncompressedSize
                                       , compressedSize
                                       , writeOp.level
                                       , writeOp.strategy))
            return false;

        writeOp.header.UncompressedSize = uncompressedSize & FileHeader::UNCOMPRESSED_SIZE_MASK;
        writeOp.header.CompressedSize = static_cast<u32>(compressedSize);

        break;
    }
    };

    if (isBE)
        writeOp.header.toBigEndian();

    writeOp.header.pZData = static_cast<u32>(pZData);

    headers.push_back(std::move(writeOp.header));

    return true;
}

}; /// namespace arc
}; /// namespace tools
}; /// namespace mh
