#include "libmh/tools/arc/common.hpp"
#include "libmh/tools/endian.hpp"
#include <algorithm>
#include <cstring>

namespace mh {
namespace tools {
namespace arc {

Header::Header(const sVersion::e version,
               const u16 filesNum,
               const bool BE)
    : Version{version}
    , FilesNum{filesNum}
{
    if (BE) {
        Magic = CRA_MAGIC;

        BESwap();
    } else {
        Magic = ARC_MAGIC;
    }
}

auto Header::BESwap(void) noexcept -> void
{
    Version = tools::endian::swap(Version);
    FilesNum = tools::endian::swap(FilesNum);
}

auto Header::isARC(void) const noexcept -> bool
{
    return Magic == ARC_MAGIC;
}

auto Header::isCRA(void) const noexcept -> bool
{
    return Magic == CRA_MAGIC;
}

auto FileHeader::isValid(void) const noexcept -> bool
{
    if (std::strlen(FilePath) <= 0)
        return false;

    if (ResourceHash == 0)
        return false;

    return true;
}

auto FileHeader::toLittleEndian(void) noexcept -> void
{
    ResourceHash        = tools::endian::swap(ResourceHash);
    CompressedSize      = tools::endian::swap(CompressedSize);

    auto bothLE         = tools::endian::swap(Raw);

    UncompressedSize    = (bothLE & UNCOMPRESSED_SIZE_MASK) >> 3;
    Flags               = bothLE & FLAGS_MASK;

    pZData              = tools::endian::swap(pZData);
}

auto FileHeader::toBigEndian(void) noexcept -> void
{
    ResourceHash        = tools::endian::swap(ResourceHash);
    CompressedSize      = tools::endian::swap(CompressedSize);
    Raw                 = tools::endian::swap(Flags | UncompressedSize << 3);
    pZData              = tools::endian::swap(pZData);
}

auto FileHeader::getFilePath(void) const noexcept -> const char* { return static_cast<const char*>(FilePath); }

auto FileHeader::setFilePath(const std::string &path) noexcept -> bool
{
    if (path.empty())
        return false;

    std::memset(  FilePath
                , 0
                , ARC_FILEPATH_MAX);

    if (path.length() <= ARC_FILEPATH_MAX) {
        path.copy(FilePath, path.length());

        FilePath[path.length() + 1] = '\0';

        return true;
    }

    return false;

}

auto FileHeader::getFilePathUnix(void) const noexcept -> std::string
{
    std::string out(FilePath);

    std::replace(  out.begin()
                 , out.end()
                 , '\\'
                 , '/');

    return out;
}

auto FileHeader::fixFilePath(void) noexcept -> void
{
    std::replace(  std::begin(FilePath)
                 , std::end(FilePath)
                 , '/'
                 , '\\');
}

auto FileHeader::getFlags(void) const noexcept -> u32
{
    // lock will be first 4 bits
    return Flags;
}

auto sVersion::contains(const sVersion::size version) noexcept -> bool
{
    switch (version) {
    case e::LP1:
    case e::LP2:
    case e::MH3U_3DS:
    case e::MH4U_MHXX_MHGU:
    case e::MH4U_1:
        return true;
    default:return false;
    };

    return false;
}

auto sVersion::getPadding(const e version) noexcept -> std::size_t
{
    switch (version) {
    case e::LP1: return 0;
    case e::LP2: return 0;
    case e::MH3U_3DS: return 4;
    case e::MH4U_MHXX_MHGU: return 4;
    case e::MH4U_1: return 4;
    default:return 0;
    };

    return 0;
}

}; /// namespace arc
}; /// namespace tools
}; /// namespace mh
