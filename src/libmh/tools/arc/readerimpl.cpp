#include "libmh/tools/arc/readerimpl.hpp"
#include "libmh/tools/endian.hpp"
#include "libmh/tools/istream.hpp"
#include <algorithm>
#include <istream>
#include <fstream>

namespace mh {
namespace tools {
namespace arcimpl {

auto Reader::checkMagic(std::istream& istream) const noexcept -> bool
{
    auto magic = mh::tools::istream::readMagic(istream);

    return    magic == arc::Header::ARC_MAGIC
           || magic == arc::Header::CRA_MAGIC;
}

auto Reader::checkVersion(std::istream& istream) const noexcept -> bool
{
    char buffer[sizeof(arc::Header::Version)];
    arc::sVersion::e version;
    u32 magic;

    magic = mh::tools::istream::readMagic(istream, false);

    istream.read(buffer, sizeof(buffer));

    version = reinterpret_cast<arc::sVersion::e&>(buffer);

    if (magic == arc::Header::CRA_MAGIC)
        version = tools::endian::swap(version);

    return arc::sVersion::contains(version);
}

auto Reader::checkHeaderSize(const std::size_t size) const noexcept -> bool
{
    return size >= sizeof(arc::Header);
}

auto Reader::checkFileHeadersSize(const std::size_t size) const noexcept -> bool
{
    auto expectedSize =   sizeof(arc::Header)
                        + m_padding
                        + m_header.FilesNum * sizeof(arc::FileHeader);

    return size >= expectedSize;
}

auto Reader::checkZLIBDataSize(const std::size_t size) const noexcept -> bool
{
    auto expectedSize =   sizeof(arc::Header)
                        + m_padding
                        + m_header.FilesNum * sizeof(arc::FileHeader);

    for (const auto& header : m_headers)
        expectedSize += header.CompressedSize;

    return size >= expectedSize;
}

auto Reader::readHeader(std::istream& istream) noexcept -> bool
{
    if (   !checkMagic(istream)
        || !checkVersion(istream))
        return false;

    istream.seekg(std::ios::beg);
    istream.read(  reinterpret_cast<char*>(&m_header)
                 , sizeof(m_header));

    if (m_header.isCRA())
        m_header.BESwap();

    return true;
}

auto Reader::readARCFileHeaders(std::istream& istream) noexcept -> void
{
    auto shift = sizeof(arc::Header) + m_padding;

    istream.seekg(shift, std::ios::beg);

    m_headers.reserve(m_header.FilesNum);

    for (auto i = 0u; i < m_header.FilesNum; i++) {
        arc::FileHeader header;

        istream.read(  reinterpret_cast<char*>(&header)
                     , sizeof(header));

        m_headers.push_back(std::move(header));
    }

    if (m_header.isCRA())
        toLittleEndian();
}

auto Reader::uncompress(const std::size_t id,
                        std::istream& istream,
                        std::ostream& ostream,
                        std::size_t& uncompresSize,
                        const std::size_t uncompressSizeLimit) noexcept -> bool
{
    if (id >= m_headers.size())
        return false;

    const auto& header = m_headers[id];
    ZLIBTool zlib(istream, ostream);

    istream.seekg(header.pZData, std::ios::beg);

    return zlib.uncompress(  header.CompressedSize
                           , uncompresSize
                           , uncompressSizeLimit);
}

auto Reader::isValid(void) const noexcept -> bool { return m_valid; }

auto Reader::read(std::istream& istream) noexcept -> bool
{
    auto size = mh::tools::istream::size(istream);

    if (   !checkHeaderSize(size)
        || !readHeader(istream))
        return false;

    m_padding = arc::sVersion::getPadding(m_header.Version);

    if (!checkFileHeadersSize(size))
        return false;

    readARCFileHeaders(istream);

    return true;
}

auto Reader::isBigEndian(void) const noexcept -> bool { return m_isBE; }

auto Reader::getHeader(void) const noexcept -> const arc::Header& { return m_header; }
auto Reader::getFileHeaders(void) const noexcept -> const std::vector<arc::FileHeader>& { return m_headers; }

auto Reader::toLittleEndian(void) noexcept -> void
{
    for (auto it = m_headers.begin(); it != m_headers.end(); it++)
        it->toLittleEndian();
}

auto Reader::toBigEndian(void) noexcept -> void
{
    for (auto it = m_headers.begin(); it != m_headers.end(); it++)
        it->toBigEndian();
}

}; /// namespace arc
}; /// namespace tools
}; /// namespace mh
