#include "libmh/tools/ostream.hpp"
#include <algorithm>
#include <istream>

#define ENBALE_OSTREAM_BETTER_COPY
#define ENABLE_OSTREAM_BETTER_FILL
#define ENABLE_OSTREAM_BETTER_ALIGN

namespace mh {
namespace tools {
namespace ostream {

static constexpr auto align(std::size_t value,
                            const std::size_t alignment) noexcept -> std::size_t
{
    while (value % alignment != 0)
        value++;

    return value;
}

auto write(std::ostream& ostream,
           const void* out,
           const std::size_t size) NOEXCEPT_OSTREAM -> void
{
    ostream.write(  reinterpret_cast<const char*>(out)
                  , size);
}

auto copy(std::ostream& ostream,
          std::istream& istream,
          const std::size_t size) NOEXCEPT_OSTREAM -> void
{
#if defined(ENBALE_OSTREAM_BETTER_COPY)
#if defined(ENABLE_CTRPF)
    static constexpr auto CHUNK = 128 << 0;
#else
    static constexpr auto CHUNK = 128 << 7;
#endif

    if (size == 0)
        return;

    u8 buffer[CHUNK];
    std::streamsize leftovers = size % CHUNK;
    std::streamsize count = size / CHUNK;

    while (!istream.eof()) {
        std::streamsize toRead = count == 0 ? leftovers : CHUNK;

        if (toRead == 0)
            break;

        istream.read(reinterpret_cast<char*>(buffer), toRead);

        std::streamsize readBytes = istream.gcount();

        ostream.write(reinterpret_cast<const char*>(buffer), readBytes);

        if (count == 0)
            break;

        --count;
    }
#else
    std::copy_n(  std::istreambuf_iterator<char>(istream)
                , size
                , std::ostreambuf_iterator<char>(ostream));
#endif
}

auto align(std::ostream& ostream,
           const std::size_t alignment) NOEXCEPT_OSTREAM -> void
{
    std::size_t value = ostream.tellp();
    std::size_t aligned = align(value, alignment);

#if defined(ENABLE_OSTREAM_BETTER_ALIGN)
    if ((aligned - value) > 0)
        mh::tools::ostream::fill(ostream, aligned - value, '\0');
#else
    for (auto i = 0u; i < aligned - value; i++)
        ostream.put('\0');
#endif
}

auto fill(std::ostream& ostream,
          const std::size_t num,
          const u8 ch) NOEXCEPT_OSTREAM -> void
{
#if defined(ENABLE_OSTREAM_BETTER_FILL)
    if (num > 1) {
        auto width = ostream.width();
        auto fill = ostream.fill();

        ostream.fill(ch);
        ostream.width(num);

        ostream << ch;

        ostream.width(width);
        ostream.fill(fill);
    } else {
        ostream << ch;
    }
#else
    for (auto i = 0u; i < num; i++)
        ostream.put(ch);
#endif
}

auto put(std::ostream& ostream,
         const u8 ch) NOEXCEPT_OSTREAM -> void
{
    ostream.put(ch);
}

auto seekp(std::ostream& ostream,
           const std::ostream::pos_type pos) NOEXCEPT_OSTREAM -> std::ostream&
{
    return ostream.seekp(pos);
}

auto seekp(std::ostream& ostream,
           const std::ostream::off_type off,
           const std::ios_base::seekdir dir) NOEXCEPT_OSTREAM -> std::ostream&
{
    return ostream.seekp(off, dir);
}

auto clear(std::ostream& ostream,
           const std::ios_base::iostate state) NOEXCEPT_OSTREAM -> void
{
    ostream.clear(state);
}

}; /// namespace ostream
}; /// namespace tools
}; /// namespace mh
