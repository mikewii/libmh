#include "libmh/tools/magicVersion.hpp"
#include "libmh/tools/istream.hpp"
#include <istream>

namespace mh {
namespace tools {

auto MagicVersion::read(std::istream &istream,
                        const bool unwind) noexcept -> bool
{
    auto readMagic = [&](std::istream& istream)
    {
        char buffer[sizeof(Magic)];

        istream.read(buffer, sizeof(buffer));

        Magic.Raw = *reinterpret_cast<decltype(sMagic::Raw)*>(&buffer);
    };

    auto readVersion = [&](std::istream& istream)
    {
        char buffer[sizeof(Version)];

        istream.read(buffer, sizeof(buffer));

        Version.Raw = *reinterpret_cast<decltype(uVersion::Raw)*>(&buffer);
    };

    std::istream::pos_type oldPos;
    auto pass = false;

    if (unwind)
        oldPos = istream.tellg();

    readMagic(istream);
    readVersion(istream);

    std::streamsize expectedPos =   static_cast<std::streamsize>(oldPos)
                                  + static_cast<std::streamsize>(  sizeof(Magic)
                                                                 + sizeof(Version));

    if (istream.tellg() == expectedPos)
        pass = true;

    if (unwind)
        istream.seekg(oldPos);

    return pass;
}

auto MagicVersion::read_s(std::istream &istream,
                          const bool unwind) noexcept -> bool
{
    auto streamsize = tools::istream::size(istream);

    if (streamsize != sizeof(Magic) + sizeof(Version))
        return false;

    return read(istream, unwind);
}

}; /// namespace tools
}; /// namespace mh
