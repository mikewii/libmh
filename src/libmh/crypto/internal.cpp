#include <psa/crypto.h>
#include <atomic>

namespace mh {
namespace crypto {

static psa_status_t PSA_STATUS{PSA_SUCCESS};
static std::atomic_int_fast64_t ref{0};

auto cryptoInit(void) -> bool
{
    bool res = false;

    if (ref > 0) {
        res = true;
    } else {
        PSA_STATUS = psa_crypto_init();
        res = PSA_STATUS == PSA_SUCCESS;
    }

    ref++;

    return res;
}

auto cryptoExit(void) -> void
{
    --ref;

    if (ref > 0)
        return;

    mbedtls_psa_crypto_free();
}

}; /// namespace crypto
}; /// namespace mh
