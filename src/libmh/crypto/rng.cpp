#include "libmh/crypto/rng.hpp"
#include "libmh/tools/log.hpp"

#include <mbedtls/entropy.h>
#include <cstring>
#include <atomic>
#include <memory>

namespace mh {
namespace crypto {
namespace rng {

struct MessageEntropy {
    static constexpr const char* MESSAGE = "MH";
    static constexpr const std::size_t SIZE = 2;

    static auto poll(void* data,
                     unsigned char* output,
                     size_t len,
                     size_t* olen) -> int
    {
        (void)data;
        std::size_t i = 0;

        for (i = 0u; i < len; i++)
            output[i] = MESSAGE[i % SIZE];

        *olen = i;

        return 0;
    }
};

struct PRNG {
    std::unique_ptr<mbedtls_ctr_drbg_context> m_ctr_drbg;
    std::unique_ptr<mbedtls_entropy_context> m_entropy;

    PRNG() : m_ctr_drbg{nullptr}, m_entropy{nullptr} {}
    ~PRNG() { free(); }

    auto create(void) -> void
    {
        m_ctr_drbg = std::make_unique<mbedtls_ctr_drbg_context>();
        m_entropy = std::make_unique<mbedtls_entropy_context>();

        mbedtls_entropy_init(m_entropy.get());
        mbedtls_ctr_drbg_init(m_ctr_drbg.get());
    }

    auto seed(const char* personalizationString) -> bool
    {
        auto res = 0;
        std::size_t size = std::strlen(personalizationString);

        res = mbedtls_entropy_add_source(  m_entropy.get()
                                         , MessageEntropy::poll
                                         , NULL
                                         , MessageEntropy::SIZE
                                         , MBEDTLS_ENTROPY_SOURCE_WEAK);

        if (res != 0)
            return LOG_DEBUG_MBEDTLS("mbedtls_entropy_add_source", res);



        res = mbedtls_ctr_drbg_seed(  m_ctr_drbg.get()
                                    , mbedtls_entropy_func
                                    , m_entropy.get()
                                    , reinterpret_cast<const unsigned char*>(personalizationString)
                                    , size);

        if (res != 0)
            return LOG_DEBUG_MBEDTLS("mbedtls_ctr_drbg_seed", res);

        return true;
    }

    auto free(void) -> void
    {
        if (m_entropy.get()) {
            mbedtls_entropy_free(m_entropy.get());
            m_entropy.reset();
        }

        if (m_ctr_drbg.get()) {
            mbedtls_ctr_drbg_free(m_ctr_drbg.get());
            m_ctr_drbg.reset();
        }
    }
};

PRNG prng;
static std::atomic_int_fast64_t ref{0};

static auto refCheck(void) -> bool
{
    if (ref > 0)
        return true;

    return ref++, false;
}

auto init(const char* personalizationString) -> bool
{
    if (refCheck())
        return true;

#ifdef ENABLE_MBEDTLS_RNG_ONCE
    static bool once = false;

    if (once)
        return true;
#endif

    prng.create();
    prng.seed(personalizationString);

#ifdef ENABLE_MBEDTLS_RNG_ONCE
    once = true;
#endif

    return true;
}

auto getRNG(void) -> mbedtls_ctr_drbg_context* { return prng.m_ctr_drbg.get(); }

auto exit(void) -> void
{
    --ref;

    if (ref > 0)
        return;

#ifdef ENABLE_MBEDTLS_RNG_ONCE
    return;
#endif

    prng.free();
}

}; /// namespace rng
}; /// namespace crypto
}; /// namespace mh
