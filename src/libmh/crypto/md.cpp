#include "libmh/crypto/md.hpp"
#include "libmh/tools/istream.hpp"
#include "libmh/tools/log.hpp"
#include <mbedtls/psa_util.h>

namespace mh::crypto {
extern auto cryptoInit(void) -> bool;
extern auto cryptoExit(void) -> void;
}; /// namespace mh::crypto

namespace mh {
namespace crypto {

MD::MD(psa_algorithm_t algorithm)
    : m_valid{false}
    , m_algorithm{algorithm}
{
    psa_status_t status = PSA_SUCCESS;

    if (!mh::crypto::cryptoInit())
        return;

    m_hash_operation = PSA_HASH_OPERATION_INIT;
    status = psa_hash_setup(&m_hash_operation, algorithm);

    if (status == PSA_ERROR_NOT_SUPPORTED) {
        LOG("unknown hash algorithm supplied");
        return;
    } else if (status != PSA_SUCCESS) {
        LOG("psa_hash_setup failed");
        return;
    }

    m_valid = true;
}

MD::~MD()
{
    psa_hash_abort(&m_hash_operation);
    mh::crypto::cryptoExit();
}

auto MD::isValid(void) const noexcept -> bool { return m_valid; }

auto MD::update(const void* data, std::size_t size) -> bool
{
    psa_status_t status;

    status = psa_hash_update(  &m_hash_operation
                             , static_cast<const uint8_t*>(data)
                             , size);

    return status == PSA_SUCCESS;
}

auto MD::finish(void) -> bool
{
    psa_status_t status;
    std::size_t hash_length = 0;

    m_hash.resize(PSA_HASH_LENGTH(m_algorithm));

    status = psa_hash_finish(  &m_hash_operation
                             , m_hash.data()
                             , m_hash.size()
                             , &hash_length);

    m_hash.resize(hash_length);

    return status == PSA_SUCCESS;
}

auto MD::digest(std::istream &istream, std::streamsize end) -> bool
{
    if (!m_valid)
        return false;

    psa_status_t status;
    std::size_t hash_length = 0;
    uint8_t in[CHUNK];

    if (end != 0) {
        auto size = tools::istream::size(istream);
        auto endOffset = end > 0 ? end : size - -end;

        if (endOffset <= 0)
            return false;

        auto count = endOffset / CHUNK;
        auto leftovers = endOffset % CHUNK;
        auto counter = 0;

        while (!istream.eof()) {
            auto length = count > 0 ? CHUNK : endOffset;
            auto toRead = counter == count ? leftovers : length;

            tools::istream::read(istream, in, toRead);

            auto readBytes = istream.gcount();

            status = psa_hash_update(  &m_hash_operation
                                     , in
                                     , readBytes);

            if (status != PSA_SUCCESS)
                return false;

            ++counter;

            if (counter > count)
                break;
        }
    } else {
        while (!istream.eof()) {
            tools::istream::read(istream, in, CHUNK);

            auto readBytes = istream.gcount();

            status = psa_hash_update(  &m_hash_operation
                                     , in
                                     , readBytes);

            if (status != PSA_SUCCESS)
                return false;
        }
    }

    m_hash.resize(PSA_HASH_LENGTH(m_algorithm));

    status = psa_hash_finish(  &m_hash_operation
                             , m_hash.data()
                             , m_hash.size()
                             , &hash_length);

    m_hash.resize(hash_length);

    return status == PSA_SUCCESS;
}

auto MD::hash(void) const -> const std::vector<uint8_t>& { return m_hash; }

auto MD::algorithm_mbedtls(void) -> mbedtls_md_type_t
{
    return mbedtls_md_type_from_psa_alg(m_algorithm);
}

auto MD::algorithm_psa(void) -> psa_algorithm_t
{
    return m_algorithm;
}

}; /// namespace crypto
}; /// namespace mh
