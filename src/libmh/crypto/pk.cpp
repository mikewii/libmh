#include "libmh/crypto/pk.hpp"
#include "libmh/crypto/rng.hpp"

#include "libmh/tools/istream.hpp"
#include "libmh/tools/ostream.hpp"
#include "libmh/tools/log.hpp"

#include <ostream>
#include <cstring>
#include <mbedtls/entropy.h>
#include <mbedtls/ctr_drbg.h>

namespace mh::crypto {
extern auto cryptoInit(void) -> bool;
extern auto cryptoExit(void) -> void;

}; /// namespace mh::crypto

namespace mh {
namespace crypto {

PK::PK()
    : m_valid{false}
{
    if (!mh::crypto::cryptoInit())
        return;

    mbedtls_pk_init(&m_pk);

    m_valid = true;
}

PK::~PK()
{
    mbedtls_pk_free(&m_pk);
    mh::crypto::cryptoExit();
}

auto PK::show(void) -> bool
{
    int res = 0;
    mbedtls_mpi N, P, Q, D, E, DP, DQ, QP;

    mbedtls_mpi_init(&N); mbedtls_mpi_init(&P); mbedtls_mpi_init(&Q);
    mbedtls_mpi_init(&D); mbedtls_mpi_init(&E); mbedtls_mpi_init(&DP);
    mbedtls_mpi_init(&DQ); mbedtls_mpi_init(&QP);

    if (mbedtls_pk_get_type(&m_pk) == MBEDTLS_PK_RSA) {
        mbedtls_rsa_context *rsa = mbedtls_pk_rsa(m_pk);

        res = mbedtls_rsa_export(rsa, &N, &P, &Q, &D, &E);

        if (res != 0)
            return LOG_DEBUG_MBEDTLS("mbedtls_rsa_export", res);

        res = mbedtls_rsa_export_crt(rsa, &DP, &DQ, &QP);

        if (res != 0)
            return LOG_DEBUG_MBEDTLS("mbedtls_rsa_export_crt", res);

        mbedtls_mpi_write_file("N:  ",  &N,  16, NULL);
        mbedtls_mpi_write_file("E:  ",  &E,  16, NULL);
        mbedtls_mpi_write_file("D:  ",  &D,  16, NULL);
        mbedtls_mpi_write_file("P:  ",  &P,  16, NULL);
        mbedtls_mpi_write_file("Q:  ",  &Q,  16, NULL);
        mbedtls_mpi_write_file("DP: ",  &DP, 16, NULL);
        mbedtls_mpi_write_file("DQ:  ", &DQ, 16, NULL);
        mbedtls_mpi_write_file("QP:  ", &QP, 16, NULL);
    }

    return res == 0;
}

auto PK::showPublic(void) -> bool
{
    int res = 0;
    mbedtls_mpi N, E;

    mbedtls_mpi_init(&N);
    mbedtls_mpi_init(&E);

    if (mbedtls_pk_get_type(&m_pk) == MBEDTLS_PK_RSA) {
        mbedtls_rsa_context *rsa = mbedtls_pk_rsa(m_pk);

        res = mbedtls_rsa_export(rsa, &N, nullptr, nullptr, nullptr, &E);

        if (res != 0)
            return LOG_DEBUG_MBEDTLS("mbedtls_rsa_export", res);

        mbedtls_mpi_write_file("N:  ",  &N,  16, NULL);
        mbedtls_mpi_write_file("E:  ",  &E,  16, NULL);
    }

    return res == 0;
}

auto PK::writePrivateKey(std::ostream& ostream,
                         Format format) -> bool
{
    int res;
    u8 buffer[CHUNK];
    std::size_t size = 0;
    u8* p = buffer;

    std::memset(buffer, 0, sizeof(buffer));

    if (format == Format::PEM) {
        res = mbedtls_pk_write_key_pem(&m_pk, buffer, sizeof(buffer));

        if (res != 0)
            return LOG_DEBUG_MBEDTLS("mbedtls_pk_write_key_pem", res);

        size = std::strlen(reinterpret_cast<const char*>(buffer));
    } else {
        res = mbedtls_pk_write_key_der(&m_pk, buffer, sizeof(buffer));

        if (res < 0)
            return LOG_DEBUG_MBEDTLS("mbedtls_pk_write_key_der", res);

        size = res;
        p = buffer + sizeof(buffer) - size;
    }

    tools::ostream::write(  ostream
                          , p
                          , size);

    return res >= 0;
}

auto PK::writePublicKey(std::ostream& ostream,
                        Format format) -> bool
{
    int res;
    u8 buffer[CHUNK];
    std::size_t size = 0;
    u8* p = buffer;

    std::memset(buffer, 0, sizeof(buffer));

    if (format == Format::PEM) {
        res = mbedtls_pk_write_pubkey_pem(&m_pk, buffer, sizeof(buffer));

        if (res != 0)
            return LOG_DEBUG_MBEDTLS("mbedtls_pk_write_pubkey_pem", res);

        size = std::strlen(reinterpret_cast<const char*>(buffer));
    } else {
        res = mbedtls_pk_write_pubkey_der(&m_pk, buffer, sizeof(buffer));

        if (res < 0)
            return LOG_DEBUG_MBEDTLS("mbedtls_pk_write_pubkey_der", res);

        size = res;
        p = buffer + sizeof(buffer) - size;
    }

    tools::ostream::write(  ostream
                          , p
                          , size);

    return res >= 0;
}

auto PK::generateRSA(unsigned int size,
                     int exponent) -> bool
{
    int res;

    if (!rng::init())
        return false;

    res = mbedtls_pk_setup(  &m_pk
                           , mbedtls_pk_info_from_type(MBEDTLS_PK_RSA));

    if (res != 0)
        return LOG_DEBUG_MBEDTLS("mbedtls_pk_setup", res);

    res = mbedtls_rsa_gen_key(  mbedtls_pk_rsa(m_pk)
                              , mbedtls_ctr_drbg_random
                              , rng::getRNG()
                              , size
                              , exponent);

    if (res != 0)
        return LOG_DEBUG_MBEDTLS("mbedtls_rsa_gen_key", res);

    rng::exit();

    return res == 0;
}

auto PK::importKey(std::istream &istream, const char *password) -> bool
{
    std::vector<u8> key;
    std::streamsize size = tools::istream::size(istream);

    if (size <= 0)
        return false;

    key.resize(size);

    tools::istream::read(  istream
                         , key.data()
                         , key.size());

    return PK::importKey(key, password);
}

auto PK::importKey(const std::vector<u8>& key,
                   const char *password) -> bool
{
    return PK::importKey(key.data(), key.size(), password);
}

auto PK::importKey(const u8 key[],
                   const std::size_t keySize,
                   const char* password) -> bool
{
    auto res = 0;

    if (!rng::init())
        return false;

    res = mbedtls_pk_parse_key(  &m_pk
                               , key
                               , keySize
                               , reinterpret_cast<const unsigned char*>(password)
                               , password ? std::strlen(password) : 0
                               , mbedtls_ctr_drbg_random
                               , rng::getRNG());

    rng::exit();

    return m_valid = res == 0;
}

auto PK::importKeyFile(const char* path,
                       const char* password) -> bool
{
    auto res = 0;

    if (!rng::init())
        return false;

    res = mbedtls_pk_parse_keyfile(  &m_pk
                                   , path
                                   , password
                                   , mbedtls_ctr_drbg_random
                                   , rng::getRNG());

    rng::exit();

    return m_valid = res == 0;
}

auto PK::importPublicKey(std::istream& istream) -> bool
{
    std::vector<u8> key;
    std::streamsize size = tools::istream::size(istream);

    if (size <= 0)
        return false;

    key.resize(size);

    tools::istream::read(  istream
                         , key.data()
                         , key.size());

    return PK::importPublicKey(key);
}

auto PK::importPublicKey(const std::vector<u8>& key) -> bool
{
    return importPublicKey(key.data(), key.size());
}

auto PK::importPublicKey(const u8 key[],
                         const std::size_t keySize) -> bool
{
    auto res = 0;

    res = mbedtls_pk_parse_public_key(  &m_pk
                                      , key
                                      , keySize);

    return m_valid = res == 0;
}

auto PK::importPublicKeyFile(const char* path) -> bool
{
    auto res = 0;

    res = mbedtls_pk_parse_public_keyfile(&m_pk, path);

    return m_valid = res == 0;
}

auto PK::verify(const std::vector<u8>& hash,
                const std::vector<u8>& signature,
                const mbedtls_md_type_t mdAlgorithm) -> bool
{
    auto res = 0;

    res = mbedtls_pk_verify(  &m_pk
                            , mdAlgorithm
                            , hash.data()
                            , hash.size()
                            , signature.data()
                            , signature.size());

    return res == 0;
}

auto PK::sign(const std::vector<u8>& hash,
              std::vector<u8>& signature,
              const mbedtls_md_type_t mdAlgorithm) -> bool
{
    auto res = 0;
    std::size_t signatureLength = 0;

    signature.resize(MBEDTLS_PK_SIGNATURE_MAX_SIZE);

    if (!rng::init())
        return false;

    res = mbedtls_pk_sign(  &m_pk
                          , mdAlgorithm
                          , hash.data()
                          , hash.size()
                          , signature.data()
                          , signature.size()
                          , &signatureLength
                          , mbedtls_ctr_drbg_random
                          , rng::getRNG());

    rng::exit();

    if (signatureLength > 0)
        signature.resize(signatureLength);

    return res == 0;
}

}; /// namespace crypto
}; /// namespace mh
