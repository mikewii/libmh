#include "libmh/MH/Crypto.hpp"
#include "libmh/tools/checksum.hpp"
#include "libmh/tools/istream.hpp"
#include "libmh/tools/ostream.hpp"
#include "libmh/tools/log.hpp"
#include "libmh/tools/endian.hpp"
#include <istream>
#include <sstream>

#include <cstring>

namespace mh::crypto {
extern auto cryptoInit() -> bool;
extern auto cryptoExit() -> void;
}; /// namespace mh::crypto

namespace mh {
namespace crypto {

#define RNG_SEED 0x484D // 0x484D == MH

std::mt19937 Crypto::m_rng;

union Sum32 {
    u32 raw;

    struct {
        u16 p1;
        u16 p2;
    };

    Sum32() : raw{0} {}
    Sum32(u32 value) : raw{value} {}
};

static constexpr auto xorMH4U(u32& seed, u16 data) noexcept -> u16
{
    if (seed == 0)
        seed = 1;

    seed = seed * 0xB0 % 0xFF53;
    data ^= static_cast<u16>(seed);

    return data;
}

Crypto::Crypto(std::istream& istream, std::ostream& ostream)
    : m_valid{false}
    , m_istream{istream}
    , m_ostream{ostream}
    , m_blowfish(std::make_unique<mbedtls_blowfish_context>())
{
    static bool once = false;

    if (!mh::crypto::cryptoInit())
        return;

    if (!once) {
        once = true;
        m_rng.seed(RNG_SEED);
    }

    mbedtls_blowfish_init(m_blowfish.get());

    m_valid = true;
}

Crypto::~Crypto()
{
    mbedtls_blowfish_free(m_blowfish.get());

    mh::crypto::cryptoExit();
}

auto Crypto::isValid(void) const noexcept -> bool { return m_valid; }

auto Crypto::keySizeCheck(const std::size_t size) noexcept -> bool
{
    if (   (size * MBEDTLS_BLOWFISH_BLOCKSIZE) < MBEDTLS_BLOWFISH_MIN_KEY_BITS
        || (size * MBEDTLS_BLOWFISH_BLOCKSIZE) > MBEDTLS_BLOWFISH_MAX_KEY_BITS)
        return false;

    return true;
}

auto Crypto::decryptSave(const Key::e id) noexcept -> bool
{
    if (!m_valid)
        return false;

    std::stringstream temp(std::ios::in | std::ios::out | std::ios::binary);
    const auto key = Key::getBlowfishKey(id);

    if (!crypt(  key.first
               , key.second
               , m_istream
               , temp
               , MBEDTLS_BLOWFISH_DECRYPT))
        return false;

    if (!xorDecrypt(temp, m_ostream))
        return false;

    return true;
}

auto Crypto::encryptSave(const Key::e id) noexcept -> bool
{
    if (!m_valid)
        return false;

    std::stringstream temp(std::ios::in | std::ios::out | std::ios::binary);
    const auto key = Key::getBlowfishKey(id);

    xorEncrypt(m_istream, temp);

    return crypt(  key.first
                 , key.second
                 , temp
                 , m_ostream
                 , MBEDTLS_BLOWFISH_ENCRYPT);
}

auto Crypto::decryptCard(const Key::e id) noexcept -> bool
{
    if (!m_valid)
        return false;

    static constexpr auto headerSize = 0x18;

    const auto key = Key::getBlowfishKey(id);

    std::stringstream temp(std::ios::in | std::ios::out | std::ios::binary);
    std::stringstream header(std::ios::in | std::ios::out | std::ios::binary);

    if (tools::istream::size(m_istream) <= headerSize)
        return false;

    tools::ostream::copy(header, m_istream, headerSize);

    m_istream.seekg(std::ios::beg, std::ios::beg);

    if (!crypt(  key.first
               , key.second
               , m_istream
               , temp
               , MBEDTLS_BLOWFISH_DECRYPT))
        return false;

    header.seekg(std::ios::beg, std::ios::beg);
    temp.seekp(std::ios::beg, std::ios::beg);

    tools::ostream::copy(temp, header, headerSize);

    if (!xorDecrypt(temp, m_ostream))
        return false;

    return true;
}

auto Crypto::encryptCard(const Key::e id) noexcept -> bool
{
    if (!m_valid)
        return false;

    // TODO: implement
    return true;
}

auto Crypto::decryptEXTData(void) noexcept -> bool
{
    if (!m_valid)
        return false;

    return decryptSave();
}

auto Crypto::encryptEXTData(void) noexcept -> bool
{
    if (!m_valid)
        return false;

    return encryptSave();
}

auto Crypto::decryptDLC(const Key::e id) noexcept -> bool
{
    if (!m_valid)
        return false;

    const auto key = Key::getBlowfishKey(id);

    return crypt(  key.first
                 , key.second
                 , m_istream
                 , m_ostream
                 , MBEDTLS_BLOWFISH_DECRYPT);
}

auto Crypto::encryptDLC(const Key::e id) noexcept -> bool
{
    if (!m_valid)
        return false;

    const auto key = Key::getBlowfishKey(id);

    return crypt(  key.first
                 , key.second
                 , m_istream
                 , m_ostream
                 , MBEDTLS_BLOWFISH_ENCRYPT);
}

auto Crypto::decrypt(const Key::e id) noexcept -> bool
{
    if (!m_valid)
        return false;

    const auto key = Key::getBlowfishKey(id);

    return crypt(  key.first
                 , key.second
                 , m_istream
                 , m_ostream
                 , MBEDTLS_BLOWFISH_DECRYPT);
}

auto Crypto::encrypt(const Key::e id) noexcept -> bool
{
    if (!m_valid)
        return false;

    const auto key = Key::getBlowfishKey(id);

    return crypt(  key.first
                 , key.second
                 , m_istream
                 , m_ostream
                 , MBEDTLS_BLOWFISH_ENCRYPT);
}

auto Crypto::xorDecrypt(void) noexcept -> bool
{
    if (!m_valid)
        return false;

    return xorDecrypt(m_istream, m_ostream);
}

auto Crypto::xorEncrypt(void) noexcept -> void
{
    xorEncrypt(m_istream, m_ostream);
}

auto Crypto::crypt(const u8 blowfishKey[],
                   const std::size_t blowfishKeySize,
                   std::istream& istream,
                   std::ostream& ostream,
                   const int mode) noexcept -> bool
{
    if (!m_valid || !blowfishKey || !keySizeCheck(blowfishKeySize))
        return false;

    int res = 0;
    u8 in[CHUNK];
    u8 out[CHUNK];

    res = mbedtls_blowfish_setkey(  m_blowfish.get()
                                  , blowfishKey
                                  , static_cast<unsigned int>(blowfishKeySize * MBEDTLS_BLOWFISH_BLOCKSIZE));

    if (res != 0)
        return LOG_DEBUG_MBEDTLS("mbedtls_blowfish_setkey", res);

    while (!istream.eof()) {
        tools::istream::read(istream, in, CHUNK);

        auto readBytes = istream.gcount();
        std::streamsize alignment = readBytes % MBEDTLS_BLOWFISH_BLOCKSIZE;

        if (alignment != 0) {
            alignment = MBEDTLS_BLOWFISH_BLOCKSIZE - alignment;

            for (auto i = 0u; i < alignment; i++)
                in[readBytes + i] = 0;
        }

        mh::tools::endian::swapBytes<u8, u32>(in, sizeof(in), readBytes + alignment);

        for (auto i = 0u; i < readBytes + alignment; i += MBEDTLS_BLOWFISH_BLOCKSIZE) {
            res = mbedtls_blowfish_crypt_ecb(  m_blowfish.get()
                                             , mode
                                             , &in[i]
                                             , &out[i]);

            if (res < 0)
                return LOG_DEBUG_MBEDTLS("mbedtls_blowfish_crypt_ecb", res);
        }

        mh::tools::endian::swapBytes<u8, u32>(out, sizeof(out), readBytes + alignment);

        tools::ostream::write(  ostream
                              , out
                              , readBytes);
    }

    return res == 0;
}

auto Crypto::xorDecrypt(std::istream& istream,
                        std::ostream& ostream) noexcept -> bool
{
    if (!m_valid)
        return false;

    u32 seed;
    Sum32 sum;

    tools::istream::read(  istream
                         , &seed
                         , sizeof(seed));

    tools::istream::read(  istream
                         , &sum.raw
                         , sizeof(sum.raw));

    seed = seed >> 16;

    sum.p1 = crypto::xorMH4U(seed, sum.p1);
    sum.p2 = crypto::xorMH4U(seed, sum.p2);

    xorMH4U(seed, istream, ostream);

    istream.clear();
    istream.seekg(std::ios::beg, std::ios::beg);

    if (sum.raw == tools::checksum32(istream))
        return true;

    return false;
}

auto Crypto::xorEncrypt(std::istream& istream,
                        std::ostream& ostream) noexcept -> void
{
    u16 random; // must be u16
    u32 seed, seedHeader;
    u32 checksum;

    random      = static_cast<u16>(m_rng());
    seed        = seedHeader = random;
    seedHeader  = (seedHeader << 16) + 0x10;
    checksum    = static_cast<u32>(tools::checksum32(istream));

    // Cant seek stringstream so instead do dummy write
    tools::ostream::write(  ostream
                          , &seedHeader
                          , sizeof(seedHeader));

    Sum32 sum(checksum);

    sum.p1 = crypto::xorMH4U(seed, sum.p1);
    sum.p2 = crypto::xorMH4U(seed, sum.p2);

    tools::ostream::write(  ostream
                          , &sum.raw
                          , sizeof(sum.raw));

    xorMH4U(seed, istream, ostream);

    ostream.seekp(std::ios::beg, std::ios::beg);

    tools::ostream::write(  ostream
                          , &seedHeader
                          , sizeof(seedHeader));
}

auto Crypto::xorMH4U(u32 seed,
                     std::istream& istream,
                     std::ostream& ostream) noexcept -> void
{
    u16 data = 0;

    while (!istream.eof()) {
        tools::istream::read(  istream
                             , &data
                             , sizeof(data));

        if (istream.gcount() < static_cast<std::streamsize>(sizeof(data)))
            break;

        data = crypto::xorMH4U(seed, data);

        tools::ostream::write(  ostream
                              , &data
                              , sizeof(data));
    }
}

}; /// namespace crypto
}; /// namespace mh
