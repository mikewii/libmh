#include "libmh/MH/SignX.hpp"
#include "libmh/crypto/md.hpp"
#include "libmh/crypto/pk.hpp"
#include "libmh/tools/istream.hpp"
#include "libmh/tools/ostream.hpp"
#include <istream>
#include <ostream>

namespace mh {
namespace crypto {

static auto extractSignature(std::istream &istream,
                             std::streamsize offset = RSA_SIGNATURE_SIZE) -> std::vector<uint8_t>
{
    std::vector<uint8_t> signature(RSA_SIGNATURE_SIZE);

    mh::tools::istream::seekg(istream, -offset, std::ios::end);
    mh::tools::istream::read(istream, signature.data(), signature.size());

    return signature;
}

auto SignX::verify(std::istream& istream,
                   const Key::e id,
                   const std::size_t offset) -> bool
{
    const u8* key = Key::getRSAPublicKey(id).first;
    const std::size_t keySize = Key::getRSAPublicKey(id).second;

    if (!key)
        return false;

    return SignX::verify(istream, key, keySize, offset);
}

auto SignX::verify(std::istream& istream,
                   const char* RSAPKeyFilePath,
                   const std::size_t offset) -> bool
{
    mh::crypto::PK pk;
    mh::crypto::MD md(PSA_ALG_SHA_256);

    tools::istream::clear(istream);
    tools::istream::seekg(istream, std::ios::beg, std::ios::beg);

    if (!pk.importPublicKeyFile(RSAPKeyFilePath))
        return false;

    if (!md.digest(istream, -offset))
        return false;

    auto signature = extractSignature(istream, offset);

    tools::istream::clear(istream);
    tools::istream::seekg(istream, std::ios::beg, std::ios::beg);

    return pk.verify(  md.hash()
                     , signature
                     , md.algorithm_mbedtls());
}

auto SignX::verify(std::istream& istream,
                   const std::vector<u8>& key,
                   const std::size_t offset) -> bool
{
    return SignX::verify(istream, key.data(), key.size(), offset);
}

auto SignX::verify(std::istream& istream,
                   const u8 key[],
                   const std::size_t keySize,
                   const std::size_t offset) -> bool
{
    mh::crypto::PK pk;
    mh::crypto::MD md(PSA_ALG_SHA_256);

    tools::istream::clear(istream);
    tools::istream::seekg(istream, std::ios::beg, std::ios::beg);

    if (!pk.importPublicKey(key, keySize))
        return false;

    if (!md.digest(istream, -offset))
        return false;

    auto signature = extractSignature(istream, offset);

    tools::istream::clear(istream);
    tools::istream::seekg(istream, std::ios::beg, std::ios::beg);

    return pk.verify(  md.hash()
                     , signature
                     , md.algorithm_mbedtls());
}

auto SignX::verify(std::istream& istream,
                   std::istream& keyStream) -> bool
{
    std::vector<u8> key;
    std::streamsize size;

    size = tools::istream::size(keyStream);

    if (size <= 0)
        return false;

    key.resize(size);

    tools::istream::read(  keyStream
                         , key.data()
                         , key.size());

    return SignX::verify(istream, key.data(), key.size());
}

auto SignX::sign(std::iostream& iostream,
                 std::istream& keyStream) -> bool
{
    std::vector<u8> key;
    std::streamsize size;

    size = tools::istream::size(keyStream);

    if (size <= 0)
        return false;

    key.resize(size);

    tools::istream::read(  keyStream
                         , key.data()
                         , key.size());

    return SignX::sign(iostream, key.data(), key.size());
}

auto SignX::sign(std::iostream& iostream,
                 const std::vector<u8>& key) -> bool
{
    return sign(iostream, key.data(), key.size());
}

auto SignX::sign(std::iostream& iostream,
                 const u8 key[],
                 const std::size_t keySize) -> bool
{
    mh::crypto::PK pk;
    mh::crypto::MD md(PSA_ALG_SHA_256);
    std::vector<u8> signature;

    tools::istream::clear(iostream);
    tools::istream::seekg(iostream, std::ios::beg, std::ios::beg);

    if (!pk.importKey(key, keySize))
        return false;

    if (!md.digest(iostream))
        return false;

    if (!pk.sign(  md.hash()
                 , signature
                 , md.algorithm_mbedtls()))
        return false;

    tools::istream::clear(iostream);

    tools::ostream::write(  iostream
                          , signature.data()
                          , signature.size());

    tools::istream::clear(iostream);
    tools::istream::seekg(iostream, std::ios::beg, std::ios::beg);

    return true;
}

}; /// namespace crypto
}; /// namespace mh
