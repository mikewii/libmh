#include "libmh/MH/CryptoX.hpp"
#include "libmh/crypto/md.hpp"
#include "libmh/crypto/rng.hpp"

#include "libmh/tools/log.hpp"

#include "libmh/tools/istream.hpp"
#include "libmh/tools/ostream.hpp"
#include "libmh/tools/endian.hpp"

#define RSA_SIGNATURE_SIZE 256

namespace mh::crypto {
extern auto cryptoInit() -> bool;
extern auto cryptoExit() -> void;
}; /// namespace mh::crypto

namespace mh {
namespace crypto {

CryptoX::CryptoX(std::istream& istream,
                 std::ostream& ostream)
    : m_valid{false}
    , m_istream{istream}
    , m_ostream{ostream}
    , m_nonce{0}
    , m_blowfish(std::make_unique<mbedtls_blowfish_context>())
{
    if (!mh::crypto::cryptoInit())
        return;

    mbedtls_blowfish_init(m_blowfish.get());

    m_valid = true;
}

CryptoX::~CryptoX()
{
    mbedtls_blowfish_free(m_blowfish.get());

    mh::crypto::cryptoExit();
}

auto CryptoX::isValid(void) const noexcept -> bool { return m_valid; }

auto CryptoX::keySizeCheck(const std::size_t size) noexcept -> bool
{
    if (   (size * MBEDTLS_BLOWFISH_BLOCKSIZE) < MBEDTLS_BLOWFISH_MIN_KEY_BITS
        || (size * MBEDTLS_BLOWFISH_BLOCKSIZE) > MBEDTLS_BLOWFISH_MAX_KEY_BITS)
        return false;

    return true;
}

auto CryptoX::decryptDLC(const Key::e id) noexcept -> bool
{
    if (!m_valid)
        return false;

    auto blowfishKey = Key::getBlowfishKey(id);

    if (!blowfishKey.first)
        return false;

    extractNonce(m_istream);

    return decrypt(blowfishKey.first, blowfishKey.second);
}

auto CryptoX::encryptDLC(const Key::e id) noexcept -> bool
{
    if (!m_valid)
        return false;

    auto key = Key::getBlowfishKey(id);

    if (!key.first)
        return false;

    return encrypt(key.first, key.second);
}

auto CryptoX::decryptDLC(const u8 blowfishKey[],
                         const std::size_t blowfishKeySize) noexcept -> bool
{
    if (!m_valid)
        return false;

    return decrypt(  blowfishKey
                   , blowfishKeySize
                   , m_istream
                   , m_ostream);
}

auto CryptoX::decrypt(const u8 blowfishKey[],
                      const std::size_t blowfishKeySize) noexcept -> bool
{
    CryptoX::extractNonce(m_istream);

    return decrypt(  blowfishKey
                   , blowfishKeySize
                   , m_istream
                   , m_ostream);
}

auto CryptoX::encrypt(const u8 blowfishKey[],
                      const std::size_t blowfishKeySize) noexcept -> bool
{
    return encrypt(  blowfishKey
                   , blowfishKeySize
                   , m_istream
                   , m_ostream);
}

auto CryptoX::decrypt(const u8 blowfishKey[],
                      const std::size_t blowfishKeySize,
                      std::istream& istream,
                      std::ostream& ostream) noexcept -> bool
{
    if (!m_valid || !blowfishKey || !keySizeCheck(blowfishKeySize))
        return false;

    int res = 0;
    u8 in[CHUNK];
    u8 out[CHUNK];
    u8 nonce_counter[MBEDTLS_BLOWFISH_BLOCKSIZE];
    u8 stream_block[MBEDTLS_BLOWFISH_BLOCKSIZE];
    std::size_t nonceOffset = 0;
    mh::crypto::MD md(PSA_ALG_SHA_1);
    std::vector<uint8_t> hash(PSA_HASH_LENGTH(PSA_ALG_SHA_1));
    std::streamsize streamSize = tools::istream::size(istream);
    std::streamsize garbageSize = sizeof(m_nonce) + PSA_HASH_LENGTH(PSA_ALG_SHA_1) + (RSA_SIGNATURE_SIZE * 2);
    std::streamsize leftovers = 0;
    std::streamsize alignedLastBytes = 0;

    if (streamSize <= garbageSize)
        return false;

    std::streamsize endOffset = streamSize - garbageSize;
    std::streamsize count = endOffset / CHUNK;
    std::streamsize counter = 0;

    res = mbedtls_blowfish_setkey(  m_blowfish.get()
                                  , blowfishKey
                                  , static_cast<unsigned int>(blowfishKeySize * MBEDTLS_BLOWFISH_BLOCKSIZE));

    if (res != 0)
        return LOG_DEBUG_MBEDTLS("mbedtls_blowfish_setkey", res);

    /// Making sure we read CHUNK size bytes from stream
    /// but dont read last 24 + alignedLastBytes bytes
    alignedLastBytes = PSA_HASH_LENGTH(PSA_ALG_SHA_1) % MBEDTLS_BLOWFISH_BLOCKSIZE;
    alignedLastBytes = (MBEDTLS_BLOWFISH_BLOCKSIZE - alignedLastBytes) + PSA_HASH_LENGTH(PSA_ALG_SHA_1);

    if (endOffset % alignedLastBytes != 0)
        alignedLastBytes = endOffset % alignedLastBytes;

    endOffset -= alignedLastBytes;
    leftovers = endOffset % CHUNK;

    /// CTR
    mbedtls_platform_zeroize(nonce_counter, sizeof(nonce_counter));
    mbedtls_platform_zeroize(stream_block, sizeof(stream_block));
    *reinterpret_cast<decltype(m_nonce)*>(&nonce_counter[0]) = m_nonce;

    /// Decrypt Data
    while (!istream.eof()) {
        auto length = count > 0 ? CHUNK : endOffset;
        auto toRead = counter == count ? leftovers : length;

        tools::istream::read(istream, in, toRead);

        std::streamsize readBytes = istream.gcount();
        std::streamsize alignment = readBytes % MBEDTLS_BLOWFISH_BLOCKSIZE;

        if (alignment != 0) {
            alignment = MBEDTLS_BLOWFISH_BLOCKSIZE - alignment;

            for (auto i = 0u; i < alignment; i++)
                in[readBytes + i] = 0;
        }

        mh::tools::endian::swapBytes<u8, u32>(in, sizeof(in), readBytes + alignment);

        res = mbedtls_blowfish_crypt_ctr_alt(  m_blowfish.get()
                                             , readBytes + alignment
                                             , &nonceOffset
                                             , nonce_counter
                                             , stream_block
                                             , in
                                             , out
                                             , true
                                             , sizeof(m_nonce)
                                             , 0);

        if (res < 0)
            return LOG_DEBUG_MBEDTLS("mbedtls_blowfish_crypt_ctr_alt", res);

        mh::tools::endian::swapBytes<u8, u32>(out, sizeof(out), readBytes + alignment);

        if (!md.update(out, readBytes))
            return false;

        tools::ostream::write(  ostream
                              , out
                              , readBytes);

        ++counter;

        if (counter > count) {
            tools::istream::read(istream, in, PSA_HASH_LENGTH(PSA_ALG_SHA_1) + alignedLastBytes);

            std::streamsize readBytes = istream.gcount();
            std::streamsize alignment = readBytes % MBEDTLS_BLOWFISH_BLOCKSIZE;

            if (alignment != 0) {
                alignment = MBEDTLS_BLOWFISH_BLOCKSIZE - alignment;

                for (auto i = 0u; i < alignment; i++)
                    in[readBytes + i] = 0;
            }

            mh::tools::endian::swapBytes<u8, u32>(in, sizeof(in), readBytes + alignment);

            res = mbedtls_blowfish_crypt_ctr_alt(  m_blowfish.get()
                                                 , readBytes + alignment
                                                 , &nonceOffset
                                                 , nonce_counter
                                                 , stream_block
                                                 , in
                                                 , out
                                                 , true
                                                 , sizeof(m_nonce)
                                                 , 0);

            if (res < 0)
                return LOG_DEBUG_MBEDTLS("mbedtls_blowfish_crypt_ctr_alt", res);

            mh::tools::endian::swapBytes<u8, u32>(out, sizeof(out), readBytes + alignment);

            if (!md.update(out, alignedLastBytes))
                return false;

            tools::ostream::write(  ostream
                                  , out
                                  , alignedLastBytes);

            hash.assign(  out + alignedLastBytes
                        , out + alignedLastBytes + PSA_HASH_LENGTH(PSA_ALG_SHA_1));

            break;
        }
    }

    if (!md.finish())
        return false;

    return hash == md.hash();
}

auto CryptoX::encrypt(const u8 blowfishKey[],
                      const std::size_t blowfishKeySize,
                      std::istream& istream,
                      std::ostream& ostream) noexcept -> bool
{
    if (!m_valid || !blowfishKey || !keySizeCheck(blowfishKeySize))
        return false;

    int res = 0;
    u8 in[CHUNK];
    u8 out[CHUNK];
    u8 nonce_counter[MBEDTLS_BLOWFISH_BLOCKSIZE];
    u8 stream_block[MBEDTLS_BLOWFISH_BLOCKSIZE];
    std::size_t nonceOffset = 0;
    mh::crypto::MD md(PSA_ALG_SHA_1);
    std::streamsize streamSize = tools::istream::size(istream);
    std::streamsize leftovers = 0;
    std::streamsize alignedLastBytes = 0;
    std::streamsize endOffset = streamSize;
    std::streamsize count = endOffset / CHUNK;
    std::streamsize counter = 0;

    res = mbedtls_blowfish_setkey(  m_blowfish.get()
                                  , blowfishKey
                                  , static_cast<unsigned int>(blowfishKeySize));

    if (res != 0)
        return LOG_DEBUG_MBEDTLS("mbedtls_blowfish_setkey", res);

    /// Making sure we read CHUNK size bytes from stream
    /// but dont read last unaligned bytes before providing hash
    alignedLastBytes = MBEDTLS_BLOWFISH_BLOCKSIZE;

    if (endOffset % alignedLastBytes != 0)
        alignedLastBytes = endOffset % alignedLastBytes;

    endOffset -= alignedLastBytes;
    leftovers = endOffset % CHUNK;

    /// CTR
    mbedtls_platform_zeroize(nonce_counter, sizeof(nonce_counter));
    mbedtls_platform_zeroize(stream_block, sizeof(stream_block));

    if (!rng::init())
        return false;

    res = mbedtls_ctr_drbg_random(  rng::getRNG()
                                  , reinterpret_cast<unsigned char*>(&m_nonce)
                                  , sizeof(m_nonce));

    if (res != 0)
        return LOG_DEBUG_MBEDTLS("mbedtls_ctr_drbg_random", res);

    rng::exit();

    *reinterpret_cast<decltype(m_nonce)*>(&nonce_counter[0]) = m_nonce;

    md.digest(istream);

    auto hash = md.hash();

    tools::istream::clear(istream);
    tools::istream::seekg(istream, std::ios::beg, std::ios::beg);

    /// Decrypt Data
    while (!istream.eof()) {
        auto length = count > 0 ? CHUNK : endOffset;
        auto toRead = counter == count ? leftovers : length;

        tools::istream::read(istream, in, toRead);

        std::streamsize readBytes = istream.gcount();
        std::streamsize alignment = readBytes % MBEDTLS_BLOWFISH_BLOCKSIZE;

        if (alignment != 0) {
            alignment = MBEDTLS_BLOWFISH_BLOCKSIZE - alignment;

            for (auto i = 0u; i < alignment; i++)
                in[readBytes + i] = 0;
        }

        mh::tools::endian::swapBytes<u8, u32>(in, sizeof(in), readBytes + alignment);

        res = mbedtls_blowfish_crypt_ctr_alt(  m_blowfish.get()
                                             , readBytes + alignment
                                             , &nonceOffset
                                             , nonce_counter
                                             , stream_block
                                             , in
                                             , out
                                             , true
                                             , sizeof(m_nonce)
                                             , 0);

        if (res < 0)
            return LOG_DEBUG_MBEDTLS("mbedtls_blowfish_crypt_ctr_alt", res);

        mh::tools::endian::swapBytes<u8, u32>(out, sizeof(out), readBytes + alignment);

        tools::ostream::write(  ostream
                              , out
                              , readBytes);

        ++counter;

        if (counter > count) {
            tools::istream::read(istream, in, alignedLastBytes);

            std::copy(hash.begin(), hash.end(), in + alignedLastBytes);

            std::streamsize readBytes = istream.gcount() + hash.size();
            std::streamsize alignment = readBytes % MBEDTLS_BLOWFISH_BLOCKSIZE;

            if (alignment != 0) {
                alignment = MBEDTLS_BLOWFISH_BLOCKSIZE - alignment;

                for (auto i = 0u; i < alignment; i++)
                    in[readBytes + i] = 0;
            }

            mh::tools::endian::swapBytes<u8, u32>(in, sizeof(in), readBytes + alignment);

            res = mbedtls_blowfish_crypt_ctr_alt(  m_blowfish.get()
                                                 , readBytes + alignment
                                                 , &nonceOffset
                                                 , nonce_counter
                                                 , stream_block
                                                 , in
                                                 , out
                                                 , true
                                                 , sizeof(m_nonce)
                                                 , 0);

            if (res < 0)
                return LOG_DEBUG_MBEDTLS("mbedtls_blowfish_crypt_ctr_alt", res);

            mh::tools::endian::swapBytes<u8, u32>(out, sizeof(out), readBytes + alignment);

            tools::ostream::write(  ostream
                                  , out
                                  , readBytes);

            break;
        }
    }

    m_nonce = tools::endian::swap(m_nonce);

    tools::ostream::write(  ostream
                          , &m_nonce
                          , sizeof(m_nonce));

    return hash == md.hash();
}

auto CryptoX::extractNonce(std::istream& istream) -> void
{
    tools::istream::seekg(  istream
                          , -(2 * RSA_SIGNATURE_SIZE + sizeof(m_nonce))
                          , std::ios::end);

    mh::tools::istream::read(istream, &m_nonce, sizeof(m_nonce));

    m_nonce = mh::tools::endian::swap(m_nonce);

    tools::istream::seekg(istream, std::ios::beg, std::ios::beg);
}

}; /// namespace crypto
}; /// namespace mh
