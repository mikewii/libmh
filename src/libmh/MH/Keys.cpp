#include "libmh/MH/Keys.hpp"
#include <mbedtls/blowfish.h>

namespace mh {
namespace crypto {

constexpr u8 BLOWFISH_MH4U_EXTData[]        = "blowfish key iorajegqmrna4itjeangmb agmwgtobjteowhv9mope";
constexpr u8 BLOWFISH_MH4U_DLC_EUR_NA[]     = "AgK2DYheaCjyHGPB";
constexpr u8 BLOWFISH_MH4U_DLC_JPN[]        = "AgK2DYheaCjyHGP8";
constexpr u8 BLOWFISH_MH4U_DLC_KOR[]        = "AgK2DYheaOjyHGP8";
constexpr u8 BLOWFISH_MH4U_DLC_TW[]         = "Capcom123 ";

constexpr u8 BLOWFISH_MHX_DLC_JP[]          = "Du9mgSyBmA7aQ5SX";
constexpr u8 BLOWFISH_MHX_DLC_US[]          = "N3uExdqeA6r8jusbnN7mBRVWS8qe9sDaLe6viEVG";
constexpr u8 BLOWFISH_MHX_DLC_EU[]          = "Xx9WzvFbJ5j3UxM6s2Q4bwYcG3vSmpX7H5eNuWQy";
constexpr u8 BLOWFISH_MHX_DLC_TW[]          = "b4EeLZMUGw2N9Bun";

constexpr u8 BLOWFISH_MHXX_DLC_JP[]         = "5FtVyACe2sRus5Zga4QC3BxQ";

constexpr u8 RSA_PKEY_MHX_DLC[294] = {
    0x30, 0x82, 0x01, 0x22, 0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01,
    0x01, 0x05, 0x00, 0x03, 0x82, 0x01, 0x0f, 0x00, 0x30, 0x82, 0x01, 0x0a, 0x02, 0x82, 0x01, 0x01,
    0x00, 0xa9, 0x88, 0x82, 0x7b, 0xc7, 0xbe, 0x56, 0xbe, 0xaa, 0x28, 0x89, 0xb0, 0x96, 0x18, 0x82,
    0xab, 0x96, 0x55, 0xb3, 0x71, 0x89, 0xbd, 0xff, 0x83, 0xbe, 0x03, 0x1a, 0x4a, 0x38, 0x73, 0xce,
    0xe8, 0x53, 0xc9, 0x2b, 0xf2, 0x4e, 0xfa, 0xf9, 0x0c, 0x21, 0xea, 0x6a, 0xf3, 0x26, 0x1e, 0x29,
    0x10, 0x6e, 0x5b, 0xf5, 0x4c, 0xac, 0x03, 0x06, 0x8c, 0xdd, 0x57, 0xe1, 0xf8, 0x30, 0x67, 0x26,
    0x17, 0x2f, 0x0c, 0x54, 0x18, 0x8e, 0x1f, 0xbc, 0xec, 0xab, 0x11, 0x11, 0x2f, 0x29, 0x53, 0x06,
    0x9c, 0x05, 0xa6, 0x74, 0xd2, 0x8f, 0x9c, 0xca, 0x80, 0x02, 0x79, 0x79, 0x2c, 0x89, 0xb0, 0x32,
    0x16, 0x91, 0x2e, 0xca, 0xe2, 0xd3, 0xcb, 0x5d, 0x7a, 0xab, 0xa5, 0x5f, 0x85, 0xb9, 0xe1, 0xf7,
    0x76, 0x1c, 0x02, 0x44, 0x7f, 0x43, 0x8f, 0x0c, 0x1b, 0x63, 0x88, 0x35, 0x7b, 0x1e, 0xab, 0xe0,
    0x41, 0x48, 0x9b, 0xe5, 0x40, 0xf0, 0x6e, 0x01, 0x5d, 0x17, 0x61, 0x1f, 0x82, 0x58, 0xed, 0x27,
    0x4c, 0xee, 0x21, 0xd2, 0x7e, 0xbd, 0x9e, 0x62, 0x8d, 0x27, 0x8d, 0x8c, 0x2b, 0x43, 0x48, 0xd3,
    0xa1, 0x1d, 0x03, 0xcb, 0x06, 0x9a, 0x80, 0xd7, 0xf7, 0x0c, 0x2c, 0xfc, 0x1a, 0x3d, 0x6a, 0xce,
    0xea, 0xfb, 0xa0, 0xeb, 0x0d, 0x02, 0x32, 0x93, 0x7f, 0xc7, 0x78, 0x16, 0x34, 0xf1, 0x27, 0xe6,
    0x2e, 0x16, 0x7c, 0xef, 0x6e, 0x21, 0xe2, 0x5a, 0xef, 0xb7, 0xb6, 0x3c, 0x3a, 0x8b, 0x3b, 0xaf,
    0xd4, 0x58, 0xa1, 0xb0, 0x70, 0x92, 0x0d, 0x8f, 0x0a, 0x4b, 0x67, 0x20, 0x64, 0xf7, 0xdb, 0xb6,
    0xe8, 0xae, 0x92, 0x2c, 0xa1, 0xd9, 0xaa, 0xa3, 0x31, 0xda, 0xe7, 0xbc, 0x60, 0x23, 0x2d, 0x52,
    0xcc, 0x70, 0x99, 0x7c, 0x1c, 0xfb, 0xbf, 0x36, 0x1c, 0x6b, 0x8e, 0x42, 0x6a, 0xb4, 0x53, 0xe9,
    0xfb, 0x02, 0x03, 0x01, 0x00, 0x01
};

constexpr u8 RSA_PKEY_MHXX_DLC[294] = {
    0x30, 0x82, 0x01, 0x22, 0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01,
    0x01, 0x05, 0x00, 0x03, 0x82, 0x01, 0x0f, 0x00, 0x30, 0x82, 0x01, 0x0a, 0x02, 0x82, 0x01, 0x01,
    0x00, 0xa9, 0x65, 0x10, 0x7d, 0xf0, 0xcf, 0xb3, 0x76, 0x63, 0xff, 0x3e, 0xec, 0x6c, 0x3a, 0x97,
    0x00, 0x74, 0x0d, 0x6a, 0x5b, 0x6a, 0x21, 0x78, 0x8d, 0x03, 0x19, 0x19, 0x63, 0xb8, 0x96, 0xf3,
    0x75, 0xfd, 0x18, 0x56, 0x11, 0x84, 0xc6, 0x6f, 0x97, 0x1e, 0x26, 0xb1, 0x94, 0x2a, 0xf9, 0x34,
    0xd3, 0x5b, 0x2a, 0x8a, 0x02, 0xcf, 0xfe, 0x14, 0x94, 0x8d, 0xb0, 0x71, 0x3c, 0xad, 0x03, 0x49,
    0x2e, 0xee, 0x49, 0x90, 0x3f, 0x9f, 0xfc, 0x4c, 0xb1, 0x34, 0xf5, 0xae, 0xaf, 0xc3, 0x42, 0xb5,
    0xf4, 0x9a, 0x7b, 0x70, 0x6b, 0x0d, 0xa3, 0x12, 0x03, 0x0c, 0x35, 0x68, 0xfc, 0xfc, 0x16, 0x16,
    0x58, 0xff, 0x72, 0x72, 0xc4, 0x60, 0x10, 0xbf, 0xe9, 0xf9, 0xb7, 0xe1, 0x4c, 0x7f, 0x8e, 0x98,
    0x8a, 0xc4, 0x9f, 0xaa, 0x2d, 0xd5, 0x27, 0x65, 0x9c, 0x10, 0x51, 0xda, 0x4e, 0xa7, 0x68, 0x7d,
    0xdf, 0xdf, 0x93, 0x8a, 0x35, 0xff, 0x9f, 0x78, 0xb3, 0x26, 0x19, 0x26, 0xf9, 0x32, 0x98, 0x8e,
    0x32, 0x53, 0x25, 0x61, 0x09, 0xc2, 0xef, 0x8d, 0x50, 0xfa, 0x5c, 0x44, 0xcd, 0x49, 0xb7, 0x81,
    0x4c, 0x08, 0x18, 0xdf, 0x74, 0x2b, 0xa6, 0x0a, 0x92, 0xf0, 0x20, 0xc0, 0xd2, 0x06, 0x3d, 0xdb,
    0xde, 0x74, 0x7c, 0xd3, 0xb8, 0x66, 0xa0, 0x98, 0x89, 0x21, 0xcc, 0x8b, 0xb0, 0xfa, 0x54, 0x7d,
    0xe9, 0x87, 0x83, 0x3b, 0x35, 0x1e, 0xb4, 0x3f, 0xe4, 0xd9, 0x60, 0x53, 0xb1, 0x78, 0xe2, 0x63,
    0x08, 0x3f, 0x21, 0x0c, 0xdb, 0x8d, 0xe4, 0xed, 0xc6, 0x1e, 0x2a, 0x62, 0x0f, 0x13, 0x5f, 0xb0,
    0xbb, 0x0d, 0x6c, 0xe4, 0x2e, 0x7d, 0x00, 0xf8, 0xe8, 0xf4, 0xa1, 0x69, 0xd5, 0x1d, 0x83, 0x1c,
    0x93, 0x02, 0x02, 0xdd, 0x74, 0x10, 0xd9, 0xc8, 0xfb, 0xf3, 0x1a, 0x9b, 0x5e, 0x2e, 0x11, 0x25,
    0x4b, 0x02, 0x03, 0x01, 0x00, 0x01
};

auto Key::getBlowfishKey(e id) -> std::pair<const u8*, std::size_t>
{

    switch (id) {
    default:return {nullptr, 0};
    case Key::MH4U_EXT_DATA: return {BLOWFISH_MH4U_EXTData, (sizeof(BLOWFISH_MH4U_EXTData) - 1)};
    case Key::MH4U_DLC_EUR_NA: return {BLOWFISH_MH4U_DLC_EUR_NA, (sizeof(BLOWFISH_MH4U_DLC_EUR_NA) - 1)};
    case Key::MH4U_DLC_JPN: return {BLOWFISH_MH4U_DLC_JPN, (sizeof(BLOWFISH_MH4U_DLC_JPN) - 1)};
    case Key::MH4U_DLC_KOR: return {BLOWFISH_MH4U_DLC_KOR, (sizeof(BLOWFISH_MH4U_DLC_KOR) - 1)};
    case Key::MH4U_DLC_TW: return {BLOWFISH_MH4U_DLC_TW, (sizeof(BLOWFISH_MH4U_DLC_TW) - 1)};

    case Key::MHX_DLC_JP: return {BLOWFISH_MHX_DLC_JP, (sizeof(BLOWFISH_MHX_DLC_JP) - 1)};
    case Key::MHX_DLC_US: return {BLOWFISH_MHX_DLC_US, (sizeof(BLOWFISH_MHX_DLC_US) - 1)};
    case Key::MHX_DLC_EU: return {BLOWFISH_MHX_DLC_EU, (sizeof(BLOWFISH_MHX_DLC_EU) - 1)};
    case Key::MHX_DLC_TW: return {BLOWFISH_MHX_DLC_TW, (sizeof(BLOWFISH_MHX_DLC_TW) - 1)};

    case Key::MHXX_DLC_JP: return {BLOWFISH_MHXX_DLC_JP, (sizeof(BLOWFISH_MHXX_DLC_JP) - 1)};
    };

    return {nullptr, 0};
}

auto Key::getRSAPublicKey(e id) -> std::pair<const u8*, std::size_t>
{
    switch (id) {
    default: return {nullptr, 0};
    case Key::MHX_DLC_JP:
    case Key::MHX_DLC_US:
    case Key::MHX_DLC_EU:
    case Key::MHX_DLC_TW: return {RSA_PKEY_MHX_DLC, sizeof(RSA_PKEY_MHX_DLC)};

    case Key::MHXX_DLC_JP: return {RSA_PKEY_MHXX_DLC, sizeof(RSA_PKEY_MHXX_DLC)};
    };

    return {nullptr, 0};
}

}; /// namespace crypto
}; /// namespace mh
