#include "libmh/MH/Resource.hpp"

namespace mh {

constexpr std::array<sResource::MergedResource, 310> sResource::resource = {{
      {0x000692f5, 0x41f00000, "amskl"}             // rAmuletSkillData
    , {0x0026e7ff, 0x004c4343, "CCL"}               // rChainCol
    , {0x00ad4093, 0x40066666, "w12d"}              // rWeapon12BaseData
    , {0x00e0bb1c, 0x434b0000, "qdl"}               // rQuestLink
    , {0x017ca1b2, 0x42180000, "ased"}              // rArmorSeData
    , {0x01a1196b, 0x3f800000, "doi"}               // rDLCOtomoInfo
    , {0x02358e1a, 0x004b5053, "SPK"}
    , {0x02929600, 0x3f800000, "qdm"}               // rQuestDaily
    , {0x02dd067f, 0x3f8ccccd, "kod"}               // rKowareObjData
    , {0x02f3a8c4, 0x3f99999a, "w10d"}              // rWeapon10LevelData
    , {0x0315e81f, 0x52534453, "SDSR"}              // rSoundDirectionalSet
    , {0x041925d7, 0x3f800000, "mvp"}               // rMonNyanVillagePoint
    , {0x0430075d, 0x3f800000, "nis"}               // rNpcInitScript
    , {0x0437bcf2, 0x00575247, "GRW"}
    , {0x056ccdbc, 0x41aa6666, "tucyl"}             // rTutorialCylinderData
    , {0x058ace42, 0x006e6361, "acn"}               // rAreaConnect
    , {0x060678f9, 0x3f99999a, "w12d"}              // rWeapon12LevelData
    , {0x0620fd81, 0x3f9ae148, "oec"}               // rOtEquipCreate
    , {0x065375d5, 0x00534953, "SIS"}               // rStageInfoSet
    , {0x06c7327b, 0x3f99999a, "fmt"}               // rFueMusicData
    , {0x07437cce, 0x00534658, "XFS"}
    , {0x07f768af, 0x00494947, "GII"}               // rGUIIconInfo
    , {0x08f105e6, 0x004d4344, "DCM"}
    , {0x0947f3c8, 0x00534658, "XFS"}               // rAreaPatrolData
    , {0x0a0e48d4, 0x00646d65, "emd"}
    , {0x0a7db17e, 0x41aa6666, "deco"}              // rDecoData
    , {0x0aaf2db2, 0x00534658, "XFS"}
    , {0x0ab54515, 0x40000000, "bui"}               // rBui
    , {0x0b1808be, 0x3f99999a, "w14d"}              // rWeapon14LevelData
    , {0x0c0fab06, 0x3f8ccccd, "otml"}              // rOtMessageLot
    , {0x0c6016b9, 0x00445448, "HTD"}
    , {0x0c6c4dbf, 0x3f800000, "kcr"}               // rKitchenListSkillRandom
    , {0x0c6d4399, 0x3f800000, "sid"}               // rSetItemData
    , {0x0c7468ea, 0x3f800000, "kcm"}               // rKitchenListMenu
    , {0x0dd0e47a, 0x00534658, "XFS"}               // rAngleLimitData
    , {0x0ecd7df4, 0x52534353, "SCSR"}              // rSoundCurveSet
    , {0x0f86c052, 0x3f99999a, "oskl"}              // FIX: no reference
    , {0x10cedaea, 0x3f800000, "pcl"}               // rPieceCreateList
    , {0x10ebe843, 0x0053534d, "MSS"}               // rFestaSoundSequence
    , {0x117d7cd0, 0x3f800000, "mcm"}               // rMonNyanCommonMaterial
    , {0x11c35522, 0x53524750, "PGRS"}              // rGrass2
    , {0x11de9ef6, 0x40000000, "isd"}               // rInsectData
    , {0x12a794d8, 0x00534658, "XFS"}
    , {0x141c243a, 0x40000000, "insectabirity"}     // rInsectAbirity
    , {0x148b6f89, 0x0046454d, "MEF"}
    , {0x14b5c8e6, 0x42444e53, "SNDB"}
    , {0x15302ef4, 0x0074796c, "lyt"}               // rLayout
    , {0x156a8085, 0x3f99999a, "fmi"}               // rFueMusicInfData
    , {0x15d782fb, 0x524b4253, "SBKR"}              // rSoundBank
    , {0x15e98d21, 0x00534658, "XFS"}               // rAreaSelectData
    , {0x15fce6c6, 0x3f800000, "wpd"}               // rWeaponProcessData
    , {0x160329f8, 0x3f800000, "rem"}               // rGuestRemData
    , {0x164f60ff, 0x3f800000, "nld"}               // rNpcLocateData
    , {0x167dbbff, 0x52515453, "STQR"}              // rSoundStreamRequest
    , {0x176fadab, 0x00534658, "XFS"}
    , {0x17d654d0, 0x40066666, "w13d"}              // rWeapon13BaseData
    , {0x1800eb37, 0x00534658, "XFS"}               // rEmSizeYureTbl
    , {0x18a9225c, 0x40c00000, "pma"}               // rPlayerManyAttacks
    , {0x19278d07, 0x74736b73, "skst"}              // rSwkbdMessageStyleTable
    , {0x197e4d7a, 0x0044544d, "MTD"}               // rMapTimeData
    , {0x19c994c4, 0x3f800000, "mex"}               // rMonNyanExp
    , {0x19f2ab31, 0x3f9ae148, "tpil"}              // rTradePointItemList
    , {0x1a032ce2, 0x4d454d44, "DMEM"}
    , {0x1a273e8b, 0x40e9999a, "npcSd"}             // rNpcSubData
    , {0x1a3e9cba, 0x3f800000, "oar"}               // rNpcTalkData
    , {0x1ac7b52d, 0x00444442, "BDD"}               // rBodyData
    , {0x1bbc291e, 0x00505444, "DTP"}               // rEnemyDtBaseParts
    , {0x1bbfd18e, 0x434b0000, "QTDS"}              // rQuestData
    , {0x1bcc4966, 0x52515253, "SRQR"}              // rSoundRequest
    , {0x1c755227, 0x40066666, "w04d"}              // rWeapon04BaseData
    , {0x1e329efc, 0x00534658, "XFS"}               // rRelationData
    , {0x1eb12c38, 0x00534658, "XFS"}               // rShell | rShell::cShellInfo | rShellEffectParam | rShellEffectParam::cParam
    , {0x1ee1ed64, 0x00534658, "XFS"}               // rAreaCommonLink
    , {0x1f149aec, 0x00534658, "XFS"}               // rEmDouMouKaData
    , {0x20ed9750, 0x00504550, "PEP"}               // rProofEffectParamScript
    , {0x21a16c7d, 0x3f99999a, "w07d"}              // rWeapon07LevelData
    , {0x21f33e01, 0x41f00000, "amslt"}             // rAmuletSlotData
    , {0x2229d051, 0x3f800000, "dcd"}               // rDecoCreateData
    , {0x22948394, 0x00495547, "GUI"}               // rGUI
    , {0x232e228c, 0x52564552, "REVR"}              // rSoundReverb
    , {0x233a2c4d, 0x40000000, "plgmktype"}         // rPlayerGimmickType
    , {0x233a9f13, 0x3f800000, "kcg"}               // rKitchenListGrillItem
    , {0x237e6120, 0x3f800000, "kc1"}               // rKitchenListSuccessTable1
    , {0x238d25d2, 0x40000000, "w07m"}              // rWeapon07MsgData
    , {0x24080fff, 0x40000000, "fht"}               // rFreeHuntData
    , {0x241f5deb, 0x00584554, "TEX"}               // rTexture
    , {0x242bb29a, 0x00444d47, "GMD"}               // rGUIMessage
    , {0x24f0a487, 0x3f800000, "kcs"}               // rKitchenListSkillSet
    , {0x252bd342, 0x3f800000, "ebcd"}              // rEquipBaseColorData
    , {0x25334dde, 0x3f9ae148, "trdl"}              // rTradeDeliveryList
    , {0x254309c9, 0x004c5350, "PSL"}               // rProofEffectMotSequenceList
    , {0x2543b93e, 0x00534553, "SES"}               // rFestaSoundEmitter
    , {0x254a0337, 0x006c7672, "rvl"}
    , {0x2553701d, 0x3f800000, "sem"}               // rSetEmMain
    , {0x25a60bc4, 0x3f800000, "ssjje"}             // rSansaijijiExchange
    , {0x25f86ee2, 0x40066666, "w07d"}              // rWeapon07BaseData
    , {0x25fd693f, 0x004c5049, "IPL"}               // rItemPopList
    , {0x2618de3f, 0x51455253, "SREQ"}
    , {0x26bec21c, 0x3f800000, "qdp"}               // rQuestPlus
    , {0x2749c8a8, 0x004c524d, "MRL"}               // rMaterial
    , {0x2754877d, 0x3f9ae148, "trll"}              // rTradeLotList
    , {0x27c72b28, 0x40000000, "w03m"}              // rWeapon03MsgData
    , {0x284acc07, 0x3f99999a, "w03d"}              // rWeapon03LevelData
    , {0x29482ccb, 0x40000000, "w00m"}              // rWeapon00MsgData
    , {0x294a5e8d, 0x3fb33333, "hta"}               // rHunterArtsData
    , {0x29751294, 0x3f9ae148, "tril"}              // rTradeItemList
    , {0x2a03657d, 0x40000000, "slw02"}             // rEquipShopListW02
    , {0x2a110586, 0x40000000, "slw11"}             // rEquipShopListW11
    , {0x2a68813f, 0x3f800000, "mlc"}               // rMonNyanLotCommon
    , {0x2a8800ee, 0x00494153, "SAI"}               // rStageAreaInfo
    , {0x2b40ae8f, 0x52555145, "EQUR"}              // rSoundEQ::cEQData
    , {0x2bcfb893, 0x3f800000, "otil"}              // rOtIniLot
    , {0x2c59eea9, 0x67736b73, "sksg"}              // rSwkbdSubGroup
    , {0x2cbf1c3a, 0x3f99999a, "w01d"}              // rWeapon01LevelData
    , {0x2d022231, 0x40000000, "w04m"}              // rWeapon04MsgData
    , {0x2d462600, 0x00444647, "GFD"}               // FIX: no reference | GuiFont
    , {0x2d6ea164, 0x40000000, "slw06"}             // rEquipShopListW06
    , {0x2e5b6815, 0x40066666, "w10d"}              // rWeapon10BaseData
    , {0x2e897056, 0x00524145, "EAR"}
    , {0x2f1c6767, 0x3f851eb8, "saou"}              // rOtSupportActionOtUnique
    , {0x2fc0c6c5, 0x41aa6666, "tuto"}              // rTutorialFlowData
    , {0x300db281, 0x40000000, "igf"}               // rInsectGrowFeed
    , {0x30991f46, 0x00534658, "XFS"}               // rFestaResourceList
    , {0x30bc3f6b, 0x40000000, "w13m"}              // rWeapon13MsgData
    , {0x327e327e, 0x42180000, "abd"}               // rArmorBuildData
    , {0x32837aa1, 0x40066666, "w06d"}              // rWeapon06BaseData
    , {0x32ca92f8, 0x006c7365, "esl"}               // rEmSetList
    , {0x330a34c7, 0x40000000, "slw01"}             // rEquipShopListW01
    , {0x3318543c, 0x40000000, "slw12"}             // rEquipShopListW12
    , {0x332cf371, 0x3f800000, "ssjjp"}             // rSansaijijiPresent
    , {0x33a84e14, 0x14120103, "hgi"}               // rHagi
    , {0x3443f314, 0x40000000, "iaf"}               // rInsectAttrFeed
    , {0x3464995e, 0x3f9ae148, "ots"}               // rOtodokeSetList
    , {0x3488527b, 0x4139999a, "npcMd"}             // rNpcMoveData
    , {0x3516c3d2, 0x0064666c, "lfd"}               // rLayoutFont
    , {0x360737e0, 0x40000000, "w09m"}              // rWeapon09MsgData
    , {0x368e9519, 0x3f800000, "bgsd"}              // rBowgunShellData
    , {0x36c909fd, 0x3f800000, "mre"}               // rMonNyanRewardEnemy
    , {0x3756ee15, 0x00585450, "PTX"}               // rLtProceduralTexture
    , {0x38534e81, 0x40000000, "isa"}               // rInsectAttr
    , {0x39207c56, 0x40066666, "w11d"}              // rWeapon11BaseData
    , {0x39c52040, 0x004d434c, "LCM"}               // rCameraList
    , {0x39c8dc68, 0x42080000, "skd"}               // rSkillData
    , {0x3a6a5a4d, 0x51525453, "STRQ"}
    , {0x3a77309a, 0x3f800000, "kc2"}               // rKitchenListSuccessTable2
    , {0x3a793672, 0x40000000, "w14m"}              // rWeapon14MsgData
    , {0x3aabba02, 0x00434d45, "EMC"}               // rEnemyCmd
    , {0x3af73507, 0x00544d45, "EMT"}
    , {0x3b04f5a1, 0x40000000, "ape"}               // rAcPlayerEquip
    , {0x3bfedd61, 0x004d4d45, "EMM"}
    , {0x3c285cc6, 0x3f800000, "otd"}               // rOtTensionData
    , {0x3c51e03c, 0x42180000, "ard"}               // rArmorResistData
    , {0x3d8bbbac, 0x00647465, "etd"}
    , {0x3dd1bcf5, 0x40000000, "slw09"}             // rEquipShopListW09
    , {0x3e0e02fc, 0x3f800000, "fsh"}               // rFishData
    , {0x3e2a4d8e, 0x41f00000, "amlt"}              // rAmuletData
    , {0x3e333888, 0x40000000, "w10m"}              // rWeapon10MsgData
    , {0x3f13de3c, 0x3f9ae148, "ots"}               // rResearchReinforce
    , {0x3f685cce, 0x3f99999a, "w09d"}              // rWeapon09LevelData
    , {0x3fa5ad6a, 0x41aa6666, "ict"}               // rItemCategoryTypeData
    , {0x4199032b, 0x40066666, "w00d"}              // rWeapon00BaseData
    , {0x41e33404, 0x006e616c, "lan"}               // rLayoutAnime
    , {0x42354dc6, 0x3f800000, "wcd"}               // rWeaponCreateData
    , {0x43057c7c, 0x3f800000, "alc"}               // rAlchemyData
    , {0x43062a33, 0x427a0000, "npcId"}             // rNpcBaseData_ID
    , {0x430f0258, 0x3f800000, nullptr}
    , {0x4360c048, 0x40000000, "slw04"}             // rEquipShopListW04
    , {0x437d7704, 0x3f99999a, "w00d"}              // rWeapon00LevelData
    , {0x440d0451, 0x40000000, "slw00"}             // rEquipShopListW00
    , {0x441f64aa, 0x40000000, "slw13"}             // rEquipShopListW13
    , {0x450a16e3, 0x00505052, "RPP"}               // pntpos / rPointPos
    , {0x4541367c, 0x00534658, "XFS"}               // cFestaPelTiedSe
    , {0x458de878, 0x3f800000, "sad"}               // rSpActData
    , {0x4788a739, 0x3f99999a, "w02d"}              // rWeapon02LevelData
    , {0x482ddcbe, 0x004c4d45, "EML"}
    , {0x486b233a, 0x00534658, "XFS"}
    , {0x48b16938, 0x40000000, "isl"}               // rInsectLevel
    , {0x48e8ac29, 0x00545444, "DTT"}               // rEnemyDtTune
    , {0x496f8f22, 0x00505546, "FUP"}               // rFreeUseParam
    , {0x4a4b677c, 0x00564552, "REV"}
    , {0x4a91ddb9, 0x3f800000, "mle"}               // rMonNyanLotEnemy
    , {0x4a96d77e, 0x3f99999a, "w04d"}              // rWeapon04LevelData
    , {0x4aa69872, 0x00534658, "XFS"}               // rShell
    , {0x4ad68c63, 0x40000000, "slw08"}             // rEquipShopListW08
    , {0x4b4c2bd3, 0x00534658, "XFS"}               // rPlCmdTblList
    , {0x4b51e836, 0x3f800000, "olvl"}              // FIX: no reference
    , {0x4bf84f6f, 0x00534658, "XFS"}               // rAreaLinkData
    , {0x4c0db839, 0x004c4453, "SDL"}               // rScheduler
    , {0x4c3942e3, 0x42080000, "skt"}               // rSkillTypeData
    , {0x4d70000c, 0x3f800000, "kc3"}               // rKitchenListSuccessTable3
    , {0x4dee8265, 0x00534d45, "EMS"}
    , {0x4e2ef008, 0x00504448, "HDP"}               // rHitDataPlayer
    , {0x4e397417, 0x004e4145, "EAN"}               // rEffectAnim
    , {0x4e399ffe, 0x504b4700, "GKP"}
    , {0x4e630743, 0x3f99999a, "w06d"}              // rWeapon06LevelData
    , {0x4eb68cee, 0x00534448, "HDS"}               // rHitDataShell
    , {0x50aa37f0, 0x3f99999a, "w08d"}              // rWeapon08LevelData
    , {0x5139901d, 0x00534658, "XFS"}               // rPlBaseCmd
    , {0x51fc779f, 0xff434253, "SBC"}               // rCollision
    , {0x525bbf16, 0x00515345, "ESQ"}
    , {0x52776ab1, 0x00535049, "IPS"}               // rItemPopSet
    , {0x535d969f, 0x00435443, "CTC"}               // rCnsTinyChain
    , {0x540d49c1, 0x40000000, "plweplist"}         // rPlayerWeaponList
    , {0x542767ec, 0x41f80000, "cskd"}              // rCatSkillData
    , {0x54539aee, 0x3f800000, "sup"}               // rSupplyList
    , {0x54ae0df9, 0x00534658, "XFS"}
    , {0x55fca0f5, 0x3f9ae148, "tlil"}              // rTradeLimitedItemList
    , {0x5653c1b0, 0x00535448, "HTS"}               // FIX: no reference | rHitSize
    , {0x56d892d0, 0x00534658, "XFS"}               // rAreaEatData
    , {0x56e21768, 0x40066666, "w01d"}              // rWeapon01BaseData
    , {0x57d1cecd, 0x00534658, "XFS"}
    , {0x57dc1ed1, 0x0050464c, "LFP"}
    , {0x58072136, 0x41000000, "sfsa"}              // rSeFsAse
    , {0x5812ac77, 0x3f800000, "gpd"}               // rGeyserPointData
    , {0x583f70b0, 0x00424452, "RDB"}               // rEnemyResidentDtBase
    , {0x58a15856, 0x00444f4d, "MOD"}               // rModel
    , {0x58bc1c29, 0x3f800000, "gqd"}               // rGuestQuestData
    , {0x59d9c3da, 0x00425444, "DTB"}               // rEnemyDtBase
    , {0x5a525c16, 0x004c4550, "PEL"}               // rProofEffectList
    , {0x5a6991f2, 0x40000000, "slw07"}             // rEquipShopListW07
    , {0x5a7bf109, 0x40000000, "slw14"}             // rEquipShopListW14
    , {0x5adc692c, 0x3f800000, "emsizetbl"}         // rEmSizeCalcTblElement
    , {0x5b0dd78a, 0x3f800000, "otp"}               // rOtTrainParam
    , {0x5b3c302d, 0x3f800000, "rem"}               // rRem
    , {0x5b8e6bf3, 0x41aa6666, "itp"}               // rItemPreData
    , {0x5ca6db93, 0x40000000, "sla00"}             // rEquipShopListA00
    , {0x5d0455eb, 0x40000000, "slw03"}             // rEquipShopListW03
    , {0x5d163510, 0x40000000, "slw10"}             // rEquipShopListW10
    , {0x5d2e4199, 0x3f800000, "ane"}               // rAcNyanterEquip
    , {0x5e5b44c8, 0x3f800000, "sls"}               // rShopListSale
    , {0x5ea6aa68, 0x3f800000, "oxpb"}              // rOtQuestExpBias
    , {0x5f36b659, 0x00594157, "WAY"}               // rAIWayPoint
    , {0x5f6a387f, 0x3f800000, "cms"}               // rCommonScript
    , {0x60869a71, 0x00545645, "EVT"}
    , {0x617b0c47, 0x57505344, "DSPW"}
    , {0x619d23df, 0x3f800000, "slt"}               // rShopList
    , {0x61c203a4, 0x3f99999a, "fms"}               // rFueMusicScData
    , {0x61cf79af, 0x40800000, "qsg"}               // rQuestGroup
    , {0x62220853, 0x3f800000, "vfp"}               // rVillageFirstPos
    , {0x62440501, 0x00646d6c, "lmd"}
    , {0x626bc4d6, 0x3f800000, "mrs"}               // rMonNyanRewardSecret
    , {0x628dfb41, 0x00000008, "gr2s"}              // rGrass2
    , {0x630ed7bb, 0x004e414e, "NAN"}               // rEnemyNandoData
    , {0x63f2d70d, 0x3f800000, "apd"}               // rArmorProcessData
    , {0x63f62424, 0x41aa6666, "ipt"}               // rItemPreTypeData
    , {0x64844d84, 0x3f800000, "mri"}               // rMonNyanReward
    , {0x65e22c55, 0x40000000, "w01m"}              // rWeapon01MsgData
    , {0x66c89ed2, 0x00534658, "XFS"}               // rPlayerPartsDisp
    , {0x66d7cd8a, 0x3f800000, "mai"}               // rMonNyanAdventItem
    , {0x67195a2e, 0x5044414d, "MADP"}
    , {0x681fa774, 0x40400000, "owp"}               // rOtWeaponData
    , {0x68cd2933, 0x00534553, "SES"}
    , {0x699ac631, 0x427a0000, "npcMdl"}            // rNpcBaseData_Mdl
    , {0x69b525df, 0x3f800000, "otpt"}              // rOtPointTable
    , {0x69c413c7, 0x3f99999a, "w13d"}              // rWeapon13LevelData
    , {0x6a1670f0, 0x3f8ccccd, "fld"}               // rFloorLvData
    , {0x6b41a2f9, 0x00534658, "XFS"}               // rStageCameraData
    , {0x6b6d2bb6, 0x40000000, "w02m"}              // rWeapon02MsgData
    , {0x6d31c3fa, 0x3f99999a, "w11d"}              // rWeapon11LevelData
    , {0x6d3ab570, 0x40066666, "w09d"}              // rWeapon09BaseData
    , {0x6d5aaa39, 0x427a0000, "npcBd"}             // rNpcBaseData
    , {0x6d5ae854, 0x004c4645, "EFL"}               // rEffectList
    , {0x6d81cfdd, 0x40a00000, "oar"}               // rOtArmorData
    , {0x6d964d19, 0x3f800000, "kca"}               // rKitchenListSkillAlcohol
    , {0x6e171a6e, 0x0053534d, "MSS"}
    , {0x6e765e35, 0x40000000, "insectessenceskill"}// rInsectEssenceSkill
    , {0x6e972f76, 0x3f99999a, "kad"}               // rKireajiData
    , {0x6ec8125c, 0x3f99999a, "raps"}              // rRapidshotData
    , {0x6f27254c, 0x40000000, "w06m"}              // rWeapon06MsgData
    , {0x6f56b442, 0x3f800000, "opl"}               // rOtParamLot
    , {0x6f6f2bad, 0x40066666, "w02d"}              // rWeapon02BaseData
    , {0x6fcc7ad4, 0x00434550, "PEC"}               // rProofEffectColorControl
    , {0x6ff78212, 0x3f8ccccd, "areainfo"}          // rAreaInfo
    , {0x7039f76f, 0x3f800000, "oxpv"}              // rOtQuestExpValue
    , {0x705a17be, 0x00474d45, "EMG"}
    , {0x70709f3f, 0x3f800000, "ctl"}               // rCoinTradeList
    , {0x708e0028, 0x6c6e616c, "lanl"}              // rLayoutAnimeList
    , {0x70bb64ba, 0x00454448, "HDE"}               // rHitDataEnemy
    , {0x70c56d5e, 0x746d6b73, "skmt"}              // rSwkbdMessageTable
    , {0x710688e2, 0x3f99999a, "squs"}              // rSquatshotData
    , {0x7284daf5, 0x00534658, "XFS"}
    , {0x72993816, 0x40000000, "w11m"}              // rWeapon11MsgData
    , {0x737234f5, 0x42180000, "acd"}               // rArmorColorData
    , {0x73850d05, 0x53435241, "ARCS"}              // FIX: no reference | rArchive
    , {0x73b73919, 0x40066666, "w14d"}              // rWeapon14BaseData
    , {0x74a22486, 0x40c00000, "spval"}             // rSupportGaugeValue
    , {0x751aff22, 0x3f99999a, "tams"}              // rTameshotData
    , {0x7522dd13, 0x3f800000, "acrd"}              // rArmorCreateData
    , {0x76820d81, 0x00544d4c, "LMT"}               // rMotionList
    , {0x76a1d9a2, 0x40000000, "isp"}               // rInsectParam
    , {0x77044c04, 0x4c504256, "VBPL"}
    , {0x77d70343, 0x3f800000, "atr"}               // rGuestEffectiveAttr
    , {0x78143fee, 0x40066666, "w03d"}              // rWeapon03BaseData
    , {0x7896b60a, 0x42180000, "asd"}               // rArmorSeriesData
    , {0x790203b0, 0x40c00000, "angryprm"}          // rAngryParam
    , {0x79c47b59, 0x5044414d, "MADP"}              // rSoundSourceADPCM
    , {0x79ff11c9, 0x3f800000, "olos"}              // rOtLotOwnSupport
    , {0x7a395cb7, 0x3f851eb8, "sab"}               // rOtSupportActionBase
    , {0x7a41a133, 0x40066666, "w08d"}              // rWeapon08BaseData
    , {0x7aad377e, 0x40000000, "w08m"}              // rWeapon08MsgData
    , {0x7b54b600, 0x40a00000, "atd"}               // rActivityData
    , {0x7b572569, 0x3f800000, "olsk"}              // rOtLotOwnSkill
    , {0x7bea3086, 0x004c4643, "CFL"}
    , {0x7c163ff5, 0x40000000, "w12m"}              // rWeapon12MsgData
    , {0x7c4883a8, 0x41aa6666, "itm"}               // rItemData
    , {0x7c5e6060, 0x3f800000, "osa"}               // rOtSpecialAction
    , {0x7cd0e77e, 0x00534658, "XFS"}               // rMonsterPartsManager
    , {0x7d025838, 0x3f800000, "mla"}               // rMonNyanLotAdvent
    , {0x7f2e2ee0, 0x00544141, "AAT"}               // rAreaActTblData
    , {0x7f416ce5, 0x3f800000, "mcn"}               // rMonNyanCirclePattern
}};

}; /// namespace mh
