#include "libmh/MH/Extentions/gmd.hpp"
#include "libmh/function_name.hpp"
#include <stdexcept>

namespace mh {

auto GMDBase::isValid(void) const noexcept -> bool { return m_valid; }

auto GMDBase::getItemsNum(void) const noexcept -> u32 { return static_cast<u32>(m_items.size()); }
auto GMDBase::getLabelsNum(void) const -> u32 { return static_cast<u32>(m_labels.size()); }

auto GMDBase::getLabel(u32 id) noexcept(false) -> std::string&
{
    if (id < m_labels.size())
        return m_labels[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto GMDBase::getLabel(u32 id) const noexcept(false) -> const std::string&
{
    if (id < m_labels.size())
        return m_labels[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto GMDBase::getLabel(u32 id, std::string& str) const noexcept -> bool
{
    if (id < m_labels.size()) {
        str = m_labels[id];
        return true;
    }

    return false;
}

auto GMDBase::getItem(u32 id) noexcept(false) -> std::string&
{
    if (id < m_items.size())
        return m_items[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto GMDBase::getItem(u32 id) const noexcept(false) -> const std::string&
{
    if (id < m_items.size())
        return m_items[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto GMDBase::getItem(u32 id, std::string& str) const noexcept -> bool
{
    if (id < m_items.size()) {
        str = m_items[id];
        return true;
    }

    return false;
}

auto GMDBase::appendLabel(const std::string& str) noexcept -> void
{
    m_labels.push_back(str);
}

auto GMDBase::replaceLabel(u32 id, const std::string& str) noexcept -> bool
{
    if (id < m_labels.size()) {
        m_labels[id] = str;
        return true;
    }

    return false;
}

auto GMDBase::removeLabel(u32 id) noexcept -> bool
{
    if (id < m_labels.size()) {
        m_labels.erase(m_labels.begin() + id);
        return true;
    }

    return false;
}

auto GMDBase::appendItem(const std::string& str) noexcept -> void
{
    m_items.push_back(str);
}

auto GMDBase::replaceItem(u32 id, const std::string& str) noexcept -> bool
{
    if (id < m_items.size()) {
        m_items[id] = str;
        return true;
    }

    return false;
}

auto GMDBase::removeItem(u32 id) noexcept -> bool
{
    if (id < m_items.size()) {
        m_items.erase(m_items.begin() + id);
        return true;
    }

    return false;
}

auto GMDBase::getFilename(void) noexcept -> std::string& { return m_filename; }
auto GMDBase::getFilename(void) const noexcept -> const std::string& { return m_filename; }
auto GMDBase::setFilename(const std::string& str) noexcept -> void { m_filename = str; }

auto GMDBase::getLabels(void) noexcept -> std::vector<std::string>& { return m_labels; }
auto GMDBase::getLabels(void) const noexcept -> const std::vector<std::string>& { return m_labels; }

auto GMDBase::getItems(void) noexcept -> std::vector<std::string>& { return m_items; }
auto GMDBase::getItems(void) const noexcept -> const std::vector<std::string>& { return m_items; }

}; /// namespace mh
