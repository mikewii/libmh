#include "libmh/MH4U/Extdata.hpp"
#include "libmh/MH/Crypto.hpp"
#include "libmh/tools/istream.hpp"
#include "libmh/tools/ostream.hpp"

#include <sstream>
#include <fstream>
#include <iomanip>

namespace mh {
namespace mh4u {
namespace extdata {

Reader::Reader(std::istream& istream)
    : m_istream{istream}
    , m_full{std::ios::in | std::ios::out | std::ios::binary}
{
    (void)crypto();
}

auto Reader::dumpRaw(std::ostream &ostream) noexcept -> bool
{
    m_full.seekg(std::ios::beg, std::ios::beg);

    auto size = tools::istream::size(m_full);

    if (size <= 0)
        return false;

    tools::ostream::copy(  ostream
                         , m_full
                         , size);

    return true;
}

auto Reader::dumpAll(const iostreamFunType& fun, const bool readLanguages) noexcept(false) -> bool
{
    std::size_t questCount = tools::istream::size(m_full) / QUEST_SIZE;

    if (questCount == 0)
        return false;

    for (auto i = 0u; i < questCount; i++) {
        m_full.seekg(  QUEST_SIZE * i
                     , std::ios::beg);

        qtds::cQTDS qtds;
        std::stringstream copy{std::ios::in | std::ios::out | std::ios::binary};

        tools::ostream::copy(  copy
                             , m_full
                             , QUEST_SIZE);

        if (!qtds::parseLite(copy, qtds, readLanguages))
            continue;

        /// Rewind
        m_full.seekg(  QUEST_SIZE * i
                     , std::ios::beg);

        auto stream = fun(qtds);

        tools::ostream::copy(  *stream
                             , m_full
                             , QUEST_SIZE);
    }

    return true;
}

auto Reader::crypto(void) noexcept -> bool
{
    crypto::Crypto c(m_istream, m_full);

    return c.decryptEXTData();
}

Writer::Writer(std::ostream& ostream)
    : m_ostream{ostream}
    , m_full{std::ios::in | std::ios::out | std::ios::binary}
{
}

auto Writer::writeAll(const istreamFunType& fun) noexcept(false) -> bool
{
    auto copyToArchive = [this](std::istream& ifstream) -> void
    {
        constexpr auto ROWLENGTH = 16;
        auto size = tools::istream::size(ifstream);
        qtds::cQTDS qtds;
        std::stringstream questName;
        std::streamsize startPos = m_full.tellp();
        std::streamsize curPos;

        if (!qtds::parseLite(ifstream, qtds))
            return;

        ifstream.seekg(std::ios::beg, std::ios::beg);
        ifstream.clear();

        tools::ostream::copy(  m_full
                             , ifstream
                             , size);

        /// Fill everything past quest data with 0s, except last ROWLENGTH bytes
        if (QUEST_SIZE - size > 0)
            tools::ostream::fill(  m_full
                                 , QUEST_SIZE - size);

        curPos = m_full.tellp();

        /// Rewind ROWLENGTH bytes back
        if (curPos - startPos == QUEST_SIZE)
            m_full.seekp(curPos - ROWLENGTH);

        questName << 'm';
        questName << std::setw(5)
                  << std::setfill('0')
                  << std::to_string(qtds.getFlags().QuestID);
        questName << ".mib";

        /// Replace last ROWLENGTH bytes with quest name
        tools::ostream::copy(  m_full
                             , questName
                             , questName.str().size());

        /// Fill the rest with 0s
        tools::ostream::fill(  m_full
                             , ROWLENGTH - questName.str().size());
    };

    for (u16 i = 0u; i < QUEST_FILES_AMMOUNT; i++) {
        auto f = fun(i);

        /// If at i no quest stream then fill that space with 0s
        if (f == std::nullopt) {
            tools::ostream::fill(  m_full
                                 , QUEST_SIZE);

            continue;
        }

        auto& key = f->Key;
        auto& stream = f->Stream;

        if (key == crypto::Key::e::NONE) {
            copyToArchive(*stream.get());
        } else {
            std::stringstream ss(std::ios::in | std::ios::out | std::ios::binary);
            crypto::Crypto c(*stream, ss);

            c.decryptDLC(key);

            ss.seekg(std::ios::beg, std::ios::beg);

            copyToArchive(ss);
        }
    }

    std::size_t neededPos = FILE_DATA_SIZE - (sizeof(u32) * 2);
    std::size_t pos = m_full.tellp();

    if (pos < neededPos)
        tools::ostream::fill(  m_full
                             , neededPos - pos);

    m_full.seekp(std::ios::beg, std::ios::beg);

    if (!crypto())
        return false;

    return lastTouch();
}

auto Writer::crypto(void) noexcept -> bool
{
    crypto::Crypto c(m_full, m_ostream);

    return c.encryptEXTData();
}

auto Writer::lastTouch(void) noexcept -> bool
{
    m_ostream.seekp(  FILE_DATA_SIZE
                    , std::ios::beg);

    tools::ostream::fill(  m_ostream
                         , FILE_FULL_SIZE - FILE_DATA_SIZE);

    return m_ostream.tellp() == FILE_FULL_SIZE;
}

}; /// namespace extdata
}; /// namespace mh4u
}; /// namespace mh
