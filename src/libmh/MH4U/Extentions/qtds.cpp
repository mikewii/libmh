#include "libmh/MH4U/Extentions/qtds.hpp"
#include "libmh/tools/magicVersion.hpp"
#include "libmh/tools/istream.hpp"
#include "libmh/tools/ostream.hpp"
#include "libmh/tools/checksum.hpp"
#include <istream>
#include <ostream>
#include <cstring>

namespace mh {
namespace mh4u {
namespace qtds {

static constexpr auto align(std::size_t value) noexcept -> std::size_t
{
    constexpr const std::size_t alignment = sizeof(u64) * 2; // file alignment

    while (value % alignment != 0)
        ++value;

    return value;
}

static auto readRelativePointers(std::istream& istream,
                                 const std::vector<u32>& terminators) noexcept -> std::vector<u32>
{
    auto done = [&terminators](const u32 rPtr) noexcept -> bool
    {
        for (const auto t : terminators)
            if (rPtr == t)
                return true;

        return false;
    };

    std::vector<u32> out;

    while (true) {
        u32 rPtr = 0;

        tools::istream::read(  istream
                             , &rPtr
                             , sizeof(rPtr));

        if (done(rPtr))
            break;

        out.push_back(std::move(rPtr));
    }

    return out;
}

auto sFlags::getQuestType(void) const noexcept -> sQuestType::e { return QuestType; }
auto sFlags::setQuestType(const sQuestType::e type) noexcept -> void { QuestType = type; }
auto sFlags::getFlags0(void) const noexcept -> sQuestFlags0::e { return Flags0; }
auto sFlags::setFlags0(const sQuestFlags0::e flags) noexcept -> void { Flags0 = flags; }
auto sFlags::getFlags1(void) const noexcept -> sQuestFlags1::e { return Flags1; }
auto sFlags::setFlags1(const sQuestFlags1::e flags) noexcept -> void { Flags1 = flags; }
auto sFlags::getFlags2(void) const noexcept -> sQuestFlags2::e { return Flags2; }
auto sFlags::setFlags2(const sQuestFlags2::e flags) noexcept -> void { Flags2 = flags; }
auto sFlags::getPostingFee(void) const noexcept -> u32 { return PostingFee; }
auto sFlags::setPostingFee(const u32 value) noexcept -> void { PostingFee = value; }
auto sFlags::getRewardZenny(void) const noexcept -> u32 { return RewardZenny; }
auto sFlags::setRewardZenny(const u32 value) noexcept -> void { RewardZenny = value; }
auto sFlags::getPenaltyZenny(void) const noexcept -> u32 { return PenaltyZenny; }
auto sFlags::setPenaltyZenny(const u32 value) noexcept -> void { PenaltyZenny = value; }
auto sFlags::getSubRewardZenny(void) const noexcept -> u32 { return SubRewardZenny; }
auto sFlags::setSubRewardZenny(const u32 value) noexcept -> void { SubRewardZenny = value; }
auto sFlags::getQuestTimeMinutes(void) const noexcept -> u32 { return QuestTime; }
auto sFlags::setQuestTimeMinutes(const u32 value) noexcept -> void { QuestTime = value; }
auto sFlags::getIntruderProbability(void) const noexcept -> u32 { return IntruderProbabilityPercent; }
auto sFlags::setIntruderProbability(const u32 value) noexcept -> void { IntruderProbabilityPercent = value; }
auto sFlags::getQuestId(void) const noexcept -> u16 { return QuestID; }
auto sFlags::setQuestId(const u16 id) noexcept -> void { QuestID = id; }
auto sFlags::getQuestStars(void) const noexcept -> sQuestStars::e { return QuestStars; }
auto sFlags::setQuestStars(const sQuestStars::e stars) noexcept -> void { QuestStars = stars; }
auto sFlags::getMap(void) const noexcept -> sMap::e { return Map; }
auto sFlags::setMap(const sMap::e id) noexcept -> void { Map = id; }
auto sFlags::getRequirements0(void) const noexcept -> sRequirements::e { return Requirements[0]; }
auto sFlags::setRequirements0(const sRequirements::e value) noexcept -> void { Requirements[0] = value; }
auto sFlags::getRequirements1(void) const noexcept -> sRequirements::e { return Requirements[1]; }
auto sFlags::setRequirements1(const sRequirements::e value) noexcept -> void { Requirements[1] = value; }
auto sFlags::getDialogID(void) const noexcept -> u8 { return DialogID; }
auto sFlags::setDialogID(const u8 value) noexcept -> void { DialogID = value; }
auto sFlags::getObjectiveMain1(void) const noexcept -> sObjective { return Main1; }
auto sFlags::setObjectiveMain1(const sObjective value) noexcept -> void { Main1 = value; }
auto sFlags::getObjectiveMain2(void) const noexcept -> sObjective { return Main2; }
auto sFlags::setObjectiveMain2(const sObjective value) noexcept -> void { Main2 = value; }
auto sFlags::getObjectiveSub(void) const noexcept -> sObjective { return Sub; }
auto sFlags::setObjectiveSub(const sObjective value) noexcept -> void { Sub = value; }
auto sFlags::getIcon0(void) const noexcept -> u16 { return QuestIconID[0]; }
auto sFlags::getIcon1(void) const noexcept -> u16 { return QuestIconID[1]; }
auto sFlags::getIcon2(void) const noexcept -> u16 { return QuestIconID[2]; }
auto sFlags::getIcon3(void) const noexcept -> u16 { return QuestIconID[3]; }
auto sFlags::getIcon4(void) const noexcept -> u16 { return QuestIconID[4]; }
auto sFlags::setIcon0(const u16 value) noexcept -> void { QuestIconID[0] = value; }
auto sFlags::setIcon1(const u16 value) noexcept -> void { QuestIconID[1] = value; }
auto sFlags::setIcon2(const u16 value) noexcept -> void { QuestIconID[2] = value; }
auto sFlags::setIcon3(const u16 value) noexcept -> void { QuestIconID[3] = value; }
auto sFlags::setIcon4(const u16 value) noexcept -> void { QuestIconID[4] = value; }

auto parse(std::istream& istream, cQTDS& obj) noexcept -> bool
{
    if (!obj.basicCheck(istream))
        return false;

    if (!obj.readHeader(istream))
        return false;

    if (!obj.readFlags(istream))
        return false;

    if (!obj.readLanguages(istream))
        return false;

    if (!obj.readChallengePresets(istream))
        return false;

    if (!obj.readSupplyBoxes(istream))
        return false;

    if (!obj.readRewardBoxes(istream))
        return false;

    if (!obj.readLargeMonsterWaves(istream))
        return false;

    if (!obj.readSmallMonsterWaves(istream))
        return false;

    if (!obj.readIntruders(istream))
        return false;

    obj.m_valid = true;

    return true;
}

auto parseLite(std::istream& istream, cQTDS& obj, const bool readLanguages) noexcept -> bool
{
    if (!obj.basicCheck(istream))
        return false;

    if (!obj.readHeader(istream))
        return false;

    if (!obj.readFlags(istream))
        return false;

    if (readLanguages)
        if (!obj.readLanguages(istream))
            return false;

    obj.m_valid = true;

    return true;
}

auto cQTDS::isValid(void) const noexcept -> bool { return m_valid; }

auto cQTDS::dump(std::ostream& ostream) noexcept -> bool
{
    auto pos = qtds::align(sizeof(Header) + sizeof(sFlags));

    ostream.seekp(pos, std::ios::beg);

    auto rpLanguages = writeLanguages(ostream);

    if (rpLanguages == 0)
        return false;

    auto rpChallengePresets = writeChallengePresets(ostream);

    auto rpSupplyBoxes = writeSupplyBoxes(ostream);
    auto rpRewardBoxesMainA = writeRewardBoxes(ostream, m_rewardBoxesMainA);
    auto rpRewardBoxesMainB = writeRewardBoxes(ostream, m_rewardBoxesMainB);
    auto rpRewardBoxesSub = writeRewardBoxes(ostream, m_rewardBoxesSub);

    auto rpSmallEnemyWaves = writeSmallMonsterWaves(ostream);
    auto rpLargeEnemyWaves = writeLargeMonsterWaves(ostream);
    auto rpIntruders = writeIntruders(ostream);











    m_flags.rpsTextLanguages = rpLanguages;

    if (rpChallengePresets > 0)
        m_flags.rpChallengePresets = rpChallengePresets;

    m_header.rpSupplyBoxes = rpSupplyBoxes;
    m_header.rpMainRewardBoxesA = rpRewardBoxesMainA;
    m_header.rpMainRewardBoxesB = rpRewardBoxesMainB;
    m_header.rpSubRewardBoxes = rpRewardBoxesSub;
    m_header.rpLargeMonsterWaves = rpLargeEnemyWaves;
    m_header.rpSmallMonsterWaves = rpSmallEnemyWaves;
    m_header.rpIntruderMonster = rpIntruders;

    ostream.seekp(sizeof(Header), std::ios::beg);
    auto flagsPos = ostream.tellp();

    tools::ostream::write(  ostream
                          , &m_flags
                          , sizeof(m_flags));

    m_header.rpsFlags = static_cast<u32>(flagsPos);

    ostream.seekp(std::ios::beg, std::ios::beg);

    tools::ostream::write(  ostream
                          , &m_header
                          , sizeof(m_header));

    return true;
}

auto cQTDS::getHeader(void) const noexcept -> const Header& { return m_header; }
auto cQTDS::getFlags(void) const noexcept -> const sFlags& { return m_flags; }
auto cQTDS::getLanguageVector(void) const noexcept -> const std::vector<textArray>& { return m_languageVector; }
auto cQTDS::getChallengePresets(void) const noexcept -> const std::vector<challengePreset>& { return m_challengePresets; }
auto cQTDS::getMainSupplyBox(void) const noexcept -> const supplyBox& { return m_mainSupplyBox; }
auto cQTDS::getExtraSupplyBoxes(void) const noexcept -> const std::vector<supplyBox>& { return m_extraSupplyBoxes; }
auto cQTDS::getRewardBoxesMainA(void) const noexcept -> const std::vector<rewardBox>& { return m_rewardBoxesMainA; }
auto cQTDS::getRewardBoxesMainB(void) const noexcept -> const std::vector<rewardBox>& { return m_rewardBoxesMainB; }
auto cQTDS::getRewardBoxesSub(void) const noexcept -> const std::vector<rewardBox>& { return m_rewardBoxesSub; }
auto cQTDS::getLargeEnemyWaves(void) const noexcept -> const std::vector<enemyWave>& { return m_largeEnemyWaves; }
auto cQTDS::getSmallEnemyWaves(void) const noexcept -> const std::vector<smallEnemyGroup>& { return m_smallEnemyWaves; }
auto cQTDS::getIntruders(void) const noexcept -> const std::vector<sIntruder>& { return m_intruders; }

auto cQTDS::basicCheck(std::istream& istream) noexcept -> bool
{
    tools::MagicVersion magicVersion;
    auto streamsize = tools::istream::size(istream);

    if (streamsize <= static_cast<std::streamsize>(sizeof(Header)))
        return false;

    magicVersion.read(istream);

    if (magicVersion.Version.Raw != Header::VERSION)
        return false;

    return true;
}

auto cQTDS::readHeader(std::istream& istream) noexcept -> bool
{
    if (tools::istream::size(istream) < static_cast<std::streamsize>(sizeof(Header)))
        return false;

    tools::istream::read(  istream
                         , &m_header
                         , sizeof(Header));

    return istream.gcount() == sizeof(Header);
}

auto cQTDS::readFlags(std::istream& istream) noexcept -> bool
{
    istream.seekg(  m_header.rpsFlags
                  , std::ios::beg);

    if (istream.tellg() == -1)
        return false;

    tools::istream::read(  istream
                         , &m_flags
                         , sizeof(sFlags));

    return istream.gcount() == sizeof(sFlags);
}

auto cQTDS::readLanguages(std::istream& istream) noexcept -> bool
{
    static constexpr auto count = static_cast<std::size_t>(eQuestLanguage::COUNT);
    u32 rPtr[count] = {0};

    istream.seekg(  m_flags.rpsTextLanguages
                  , std::ios::beg);

    if (istream.tellg() == -1)
        return false;

    for (auto i = 0u; i < count; i++) {
        tools::istream::read(  istream
                             , &rPtr[i]
                             , sizeof(u32));

        if (rPtr[i] == 0)
            return false;
    }

    for (auto i = 0u; i < count; i++) {
        istream.seekg(rPtr[i], std::ios::beg);

        if (istream.tellg() == -1)
            return false;

        textArray array;

        if (!readText(istream, array))
            return false;

        m_languageVector.push_back(std::move(array));
    }

    return true;
}

auto cQTDS::readText(std::istream& istream,
                     textArray& array) noexcept -> bool
{
    static constexpr auto count = static_cast<std::size_t>(eText::COUNT);
    u32 rPtr[count] = {0};

    for (auto i = 0u; i < count; i++) {
        tools::istream::read(  istream
                             , &rPtr[i]
                             , sizeof(u32));

        if (rPtr[i] == 0)
            return false;
    }

    for (auto i = 0u; i < count; i++) {
        istream.seekg(rPtr[i]);

        if (istream.tellg() == -1)
            return false;

        std::u16string str;

        if (!tools::istream::readU16String(istream, str))
            return false;

        array[i] = std::move(str);
    }

    return true;
}

auto cQTDS::readChallengePresets(std::istream& istream) noexcept -> bool
{
    if (m_flags.rpChallengePresets == 0)
        return true;

    istream.seekg(  m_flags.rpChallengePresets
                  , std::ios::beg);

    if (istream.tellg() == -1)
        return false;

    for (auto i = 0u; i < 5; i++) {
        challengeEquipment equipment {};
        sHunterItems pouch {};

        for (auto j = 0u; j < equipment.size(); j++)
            tools::istream::read(  istream
                                 , &equipment[j]
                                 , sizeof(uEquipment));

        tools::istream::read(  istream
                             , &pouch
                             , sizeof(pouch));

        m_challengePresets.emplace_back(  std::move(equipment)
                                        , std::move(pouch));
    }

    return true;
}

auto cQTDS::readSupplyBoxes(std::istream& istream) noexcept -> bool
{
    auto readBox = [](std::istream& istream,
                      const sItemBox& box,
                      std::vector<sItem>& out) noexcept -> bool
    {
        std::size_t limit = box.Flags.SlotsNum > SUPPLY_BOX_MAX_ITEMS ? SUPPLY_BOX_MAX_ITEMS
                                                                      : box.Flags.SlotsNum;

        istream.seekg(  box.pItemsBox
                      , std::ios::beg);

        if (istream.tellg() == -1)
            return false;

        for (auto i = 0u; i < limit; i++) {
            sItem item;

            tools::istream::read(  istream
                                 , &item
                                 , sizeof(item));

            out.push_back(std::move(item));
        }

        return true;
    };

    if (m_header.rpSupplyBoxes == 0)
        return true;

    istream.seekg(  m_header.rpSupplyBoxes
                  , std::ios::beg);

    if (istream.tellg() == -1)
        return false;

    std::vector<sItemBox> itemBoxes;

    // TODO: limit to 5?
    while (true) {
        sItemBox itemBox;

        tools::istream::read(  istream
                             , &itemBox
                             , sizeof(itemBox));

        if (istream.tellg() == -1)
            return false;

        if (itemBox.Flags.ID == 0xFF)
            break;

        itemBoxes.push_back(std::move(itemBox));
    };

    for (auto i = 0u; i < itemBoxes.size(); i++) {
        if (   itemBoxes[i].pItemsBox == 0
            || itemBoxes[i].Flags.SlotsNum == 0)
            continue;

        std::vector<sItem> box;

        if (!readBox(  istream
                     , itemBoxes[i]
                     , box))
            return false;

        if (i == 0) {
            m_mainSupplyBox.first = std::move(itemBoxes[i].Flags);
            m_mainSupplyBox.second = std::move(box);
        } else {
            m_extraSupplyBoxes.emplace_back(  std::move(itemBoxes[i].Flags)
                                            , std::move(box));
        }
    }

    return true;
}

auto cQTDS::readRewardBoxes(std::istream& istream) noexcept -> bool
{
    auto readBox = [](std::istream& istream,
                      const sItemBox& box,
                      std::vector<sRewardBoxItem>& out) noexcept -> bool
    {
        istream.seekg(box.pItemsBox);

        if (istream.tellg() == -1)
            return false;

        while (true) {
            sRewardBoxItem item;

            tools::istream::read(  istream
                                 , &item
                                 , sizeof(item));

            if (istream.tellg() == -1)
                return false;

            if (item.Chance == 0xFFFF)
                break;

            out.push_back(std::move(item));
        };

        return true;
    };

    std::array<std::pair<u32, std::vector<rewardBox>&>, 3> map = {{
          { m_header.rpMainRewardBoxesA, m_rewardBoxesMainA }
        , { m_header.rpMainRewardBoxesB, m_rewardBoxesMainB }
        , { m_header.rpSubRewardBoxes, m_rewardBoxesSub }
    }};

    for (const auto& item : map) {
        if (item.first == 0)
            continue;

        istream.seekg(item.first);

        if (istream.tellg() == -1)
            return false;

        std::vector<sItemBox> itemBoxes;

        // TODO: limit to 2?
        while (true) {
            sItemBox itemBox;

            tools::istream::read(  istream
                                 , &itemBox
                                 , sizeof(itemBox));

            if (istream.tellg() == -1)
                return false;

            if (   itemBox.Flags.ID == 0xFF
                && itemBox.Flags.SlotsNum == 0xFF)
                break;

            itemBoxes.push_back(std::move(itemBox));
        }

        for (auto i = 0u; i < itemBoxes.size(); i++) {
            std::vector<sRewardBoxItem> rewardBox;

            if (!readBox(  istream
                         , itemBoxes[i]
                         , rewardBox))
                return false;

            item.second.emplace_back(  std::move(itemBoxes[i].Flags)
                                     , std::move(rewardBox));
        }
    }

    return true;
}

auto cQTDS::readEms(std::istream& istream,
                    enemyWave& wave) noexcept -> bool
{
    std::istream::pos_type pos = istream.tellg();
    u32 buffer = 0;

    // TODO: limit?
    while (true) {
        tools::istream::read(  istream
                             , &buffer
                             , sizeof(buffer));

        if (buffer == 0xFFFFFFFF)
            break;

        istream.seekg(pos);

        sEm em;

        tools::istream::read(  istream
                             , &em
                             , sizeof(em));

        wave.push_back(std::move(em));

        pos = istream.tellg();

        if (pos == -1)
            return false;
    }

    return true;
}

auto cQTDS::readLargeMonsterWaves(std::istream& istream) noexcept -> bool
{
    if (m_header.rpLargeMonsterWaves == 0)
        return true;

    istream.seekg(  m_header.rpLargeMonsterWaves
                  , std::ios::beg);

    if (istream.tellg() == -1)
        return false;

    auto waves = readRelativePointers(istream, {0});

    if (waves.empty())
        return true;

    for (auto i = 0u; i < waves.size(); i++) {
        istream.seekg(waves[i], std::ios::beg);

        if (istream.tellg() == -1)
            return false;

        enemyWave wave;

        if (!readEms(istream, wave))
            return false;

        m_largeEnemyWaves.push_back(std::move(wave));
    }

    return true;
}

auto cQTDS::readSmallMonsterWaves(std::istream& istream) noexcept -> bool
{
    if (m_header.rpSmallMonsterWaves == 0)
        return true;

    istream.seekg(  m_header.rpSmallMonsterWaves
                  , std::ios::beg);

    if (istream.tellg() == -1)
        return false;

    auto rPtrs = readRelativePointers(  istream
                                      , {0});

    if (rPtrs.empty())
        return true;

    for (auto i = 0u; i < rPtrs.size(); i++) {
        std::vector<enemyWave> group;

        istream.seekg(  rPtrs[i]
                      , std::ios::beg);

        if (istream.tellg() == -1)
            return false;

        auto waves = readRelativePointers(  istream
                                          , {0});

        if (waves.empty())
            return true;

        for (auto j = 0u; j < waves.size(); j++) {
            istream.seekg(waves[i], std::ios::beg);

            if (istream.tellg() == -1)
                return false;

            enemyWave wave;

            if (!readEms(istream, wave))
                return false;

            group.push_back(std::move(wave));
        }

        m_smallEnemyWaves.push_back(std::move(group));
    }

    return true;
}

auto cQTDS::readIntruders(std::istream& istream) noexcept -> bool
{
    if (m_header.rpIntruderMonster == 0)
        return true;

    istream.seekg(  m_header.rpIntruderMonster
                  , std::ios::beg);

    std::istream::pos_type pos = istream.tellg();
    u32 buffer = 0;

    if (pos == -1)
        return false;

    while (true) {
        tools::istream::read(  istream
                             , &buffer
                             , sizeof(buffer));

        if (buffer == 0x0000FFFF)
            break;

        istream.seekg(pos);

        sIntruder intruder;

        tools::istream::read(  istream
                             , &intruder
                             , sizeof(intruder));

        m_intruders.push_back(std::move(intruder));

        pos = istream.tellg();

        if (pos == -1)
            break;
    }

    return true;
}

auto cQTDS::writeLanguages(std::ostream& ostream) noexcept -> u32
{
    std::array<u32, static_cast<std::size_t>(eQuestLanguage::COUNT)> ptrs;
    u32 ptr = 0;

    if (   m_languageVector.empty()
        || m_languageVector.size() > ptrs.size())
        return ptr;

    for (auto i = 0u; i < m_languageVector.size(); i++) {
        auto rPtr = writeTextArray(ostream, m_languageVector[i]);

        if (rPtr == 0)
            return ptr;

        ptrs[i] = std::move(rPtr);
    }

    if (ptrs.size() != m_languageVector.size())
        for (auto i = m_languageVector.size(); i < ptrs.size(); i++)
            ptrs[i] = ptrs[0];

    tools::ostream::align(ostream);

    ptr = static_cast<u32>(ostream.tellp());

    for (auto i = 0u; i < ptrs.size(); i++)
        tools::ostream::write(  ostream
                              , &ptrs[i]
                              , sizeof(u32));

    tools::ostream::align(ostream);
    tools::ostream::fill(ostream, sizeof(u64) * 2);

    return ptr;
}

auto cQTDS::writeTextArray(std::ostream& ostream,
                           const textArray& array) noexcept -> u32
{
    using charT = textArray::value_type::value_type;

    std::array<u32, static_cast<std::size_t>(eText::COUNT)> ptrs;
    u32 ptr = 0;

    tools::ostream::align(ostream);

    for (auto i = 0u; i < array.size(); i++) {
        ptrs[i] = static_cast<u32>(ostream.tellp());

        tools::ostream::write(  ostream
                              , array[i].c_str()
                              , array[i].size() * sizeof(charT));

        tools::ostream::fill(ostream, sizeof(charT));

        auto posBeofre = ostream.tellp();

        tools::ostream::align(ostream);

        auto posAfter = ostream.tellp();

        if (posBeofre != posAfter)
            tools::ostream::fill(ostream, sizeof(u64) * 2);
    }

    ptr = static_cast<u32>(ostream.tellp());

    for (auto i = 0u; i < ptrs.size(); i++)
        tools::ostream::write(  ostream
                              , &ptrs[i]
                              , sizeof(u32));

    for (auto i = 0u; i < sizeof(u32); i++)
        tools::ostream::put(ostream, '\0');

    return ptr;
}

auto cQTDS::writeChallengePresets(std::ostream& ostream) noexcept -> u32
{
    u32 ptr = 0;

    if (m_challengePresets.empty())
        return ptr;

    ptr = static_cast<u32>(ostream.tellp());

    for (auto i = 0u; i < m_challengePresets.size(); i++) {
        const auto& pair = m_challengePresets[i];

        /// Write equimpent
        for (auto j = 0u; j < pair.first.size(); j++)
            tools::ostream::write(  ostream
                                  , pair.first[j].Raw
                                  , sizeof(pair.first[j].Raw));

        /// Write items
        tools::ostream::write(  ostream
                              , &pair.second
                              , sizeof(pair.second));
    }

    tools::ostream::align(ostream);

    return ptr;
}

auto cQTDS::writeSupplyBoxes(std::ostream& ostream) noexcept -> u32
{
    u32 ptr = 0;
    u32 rpMainSupplyBox = 0;
    std::vector<u32> rpExtraSupplyBoxes(m_extraSupplyBoxes.size());

    // TODO: main supply box size must be at least 8 items

    /// Write boxes contents:
    rpMainSupplyBox = writeSupplyBox(ostream, m_mainSupplyBox);

    for (auto i = 0u; i < m_extraSupplyBoxes.size(); i++)
        rpExtraSupplyBoxes[i] = writeSupplyBox(ostream, m_extraSupplyBoxes[i]);

    tools::ostream::align(ostream);

    ptr = static_cast<u32>(ostream.tellp());

    /// Write boxes flags:
    writeItemBoxFlags(  ostream
                      , m_mainSupplyBox.first
                      , rpMainSupplyBox);

    for (auto i = 0u; i < m_extraSupplyBoxes.size(); i++)
        writeItemBoxFlags(  ostream
                          , m_extraSupplyBoxes[i].first
                          , rpExtraSupplyBoxes[i]);

    tools::ostream::put(ostream, 0xFF);
    tools::ostream::align(ostream);
    tools::ostream::fill(ostream, sizeof(u64) * 2);

    return ptr;
}

auto cQTDS::writeSupplyBox(std::ostream& ostream,
                           const supplyBox& box) noexcept -> u32
{
    u32 ptr = static_cast<u32>(ostream.tellp());

    for (auto i = 0u; i < box.second.size(); i++) {
        const auto& item = box.second[i];

        tools::ostream::write(  ostream
                              , &item
                              , sizeof(item));
    }

    return ptr;
}

auto cQTDS::writeItemBoxFlags(std::ostream& ostream,
                              const sItemBoxFlags& flags,
                              const u32 ptr) noexcept -> void
{
    tools::ostream::write(  ostream
                          , &flags
                          , sizeof(flags));

    tools::ostream::write(  ostream
                          , &ptr
                          , sizeof(ptr));
}

auto cQTDS::writeRewardBoxes(std::ostream& ostream,
                             const std::vector<rewardBox>& boxes) noexcept -> u32
{
    if (boxes.empty())
        return 0;

    std::vector<u32> ptrs(boxes.size());
    u32 ptr = 0;

    /// Write boxes contents:
    for (auto i = 0u; i < boxes.size(); i++)
        ptrs[i] = writeRewardBox(ostream, boxes[i]);

    tools::ostream::put(ostream, 0xFF);
    tools::ostream::put(ostream, 0xFF);

    tools::ostream::align(ostream);

    ptr = static_cast<u32>(ostream.tellp());

    /// Write boxes flags:
    for (auto i = 0u; i < boxes.size(); i++)
        writeItemBoxFlags(  ostream
                          , boxes[i].first
                          , ptrs[i]);

    tools::ostream::put(ostream, 0xFF);
    tools::ostream::put(ostream, 0xFF);
    tools::ostream::align(ostream);
    tools::ostream::fill(ostream, sizeof(u64) * 2);

    return ptr;
}

auto cQTDS::writeRewardBox(std::ostream& ostream,
                           const rewardBox& box) noexcept -> u32
{
    u32 ptr = static_cast<u32>(ostream.tellp());

    for (auto i = 0u; i < box.second.size(); i++) {
        const auto& item = box.second[i];

        tools::ostream::write(  ostream
                              , &item
                              , sizeof(item));
    }

    return ptr;
}

auto cQTDS::writeSmallMonsterWaves(std::ostream& ostream) noexcept -> u32
{
    // TODO: deduplication version, overlap version

    u32 ptr = 0;

    if (isSmallMonsterWavesEmpty()) {
        auto groupCount = m_smallEnemyWaves.size();
        u32 waveCount = 0u;

        for (const auto& group : m_smallEnemyWaves)
            if (waveCount < group.size())
                waveCount = static_cast<u32>(group.size());

        u32 wavePos = static_cast<u32>(ostream.tellp());

        tools::ostream::fill(ostream, sizeof(u32), 0xFF);
        tools::ostream::fill(ostream, sizeof(u32));
        tools::ostream::put(ostream, 0xFF);
        tools::ostream::align(ostream);
        tools::ostream::fill(ostream, sizeof(u64) * 2);
        tools::ostream::fill(ostream, sizeof(u64));

        u32 wavesPos = static_cast<u32>(ostream.tellp());

        for (auto i = 0u; i < waveCount; i++)
            tools::ostream::write(  ostream
                                  , &wavePos
                                  , sizeof(wavePos));

        tools::ostream::fill(ostream, sizeof(u32));

        ptr = static_cast<u32>(ostream.tellp());

        for (auto i = 0u; i < groupCount; i++)
            tools::ostream::write(  ostream
                                  , &wavesPos
                                  , sizeof(wavesPos));
    } else {
        std::vector<u32> groupRptrs(m_smallEnemyWaves.size());

        for (auto i = 0u; i < m_smallEnemyWaves.size(); i++) {
            const auto& group = m_smallEnemyWaves[i];
            std::vector<u32> waveRptrs(group.size());

            for (auto j = 0u; j < group.size(); j++) {
                const auto& wave = group[j];

                waveRptrs[j] = writeMonsterWave(ostream, wave);

                tools::ostream::fill(ostream, sizeof(u32), 0xFF);
                tools::ostream::fill(ostream, sizeof(u32));
                tools::ostream::fill(ostream, sizeof(u64) * 4);
                tools::ostream::align(ostream);
            }

            groupRptrs[i] = static_cast<u32>(ostream.tellp());

            for (auto j = 0u; j < waveRptrs.size(); j++)
                tools::ostream::write(  ostream
                                      , &waveRptrs[j]
                                      , sizeof(waveRptrs[j]));

            tools::ostream::fill(ostream, sizeof(u32));
        }

        tools::ostream::fill(ostream, sizeof(u32));

        ptr = static_cast<u32>(ostream.tellp());

        for (auto i = 0u; i < groupRptrs.size(); i++)
            tools::ostream::write(  ostream
                                  , &groupRptrs[i]
                                  , sizeof(groupRptrs[i]));
    }

    tools::ostream::fill(ostream, sizeof(u32));
    tools::ostream::align(ostream);

    // optional?
    {
        tools::ostream::fill(ostream, sizeof(u64) * 2);
        tools::ostream::fill(ostream, sizeof(u32), 0xFF);
        tools::ostream::fill(ostream, sizeof(u32));
        tools::ostream::put(ostream, 0xFF);
        tools::ostream::align(ostream);

        for (auto i = 0u; i < 3; i++)
            tools::ostream::fill(ostream, sizeof(u64) * 2);
    }

    return ptr;
}

auto cQTDS::writeLargeMonsterWaves(std::ostream& ostream) noexcept -> u32
{
    u32 ptr = 0;

    if (isLargeMonsterWavesEmpty()) {
        u32 wavePos = static_cast<u32>(ostream.tellp());

        tools::ostream::fill(ostream, sizeof(u32), 0xFF);
        tools::ostream::fill(ostream, sizeof(u32));
        tools::ostream::put(ostream, 0xFF);
        tools::ostream::align(ostream);
        tools::ostream::fill(ostream, sizeof(u64) * 3);

        ptr = static_cast<u32>(ostream.tellp());

        auto count = m_largeEnemyWaves.empty() ? 1
                                               : m_largeEnemyWaves.size();

        for (auto i = 0u; i < count; i++)
            tools::ostream::write(  ostream
                                  , &wavePos
                                  , sizeof(wavePos));
    } else {
        std::vector<u32> rPtrs(m_largeEnemyWaves.size());

        for (auto i = 0u; i < m_largeEnemyWaves.size(); i++) {
            rPtrs[i] = static_cast<u32>(ostream.tellp());

            if (!writeMonsterWave(  ostream
                                  , m_largeEnemyWaves[i]))
                return 0;

            // Wave terminator:
            tools::ostream::fill(ostream, sizeof(u32), 0xFF);
            tools::ostream::fill(ostream, sizeof(u32));
            tools::ostream::put(ostream, 0xFF);
            tools::ostream::align(ostream);
        }

        tools::ostream::fill(ostream, sizeof(u64) * 2);

        ptr = static_cast<u32>(ostream.tellp());

        for (auto i = 0u; i < rPtrs.size(); i++)
            tools::ostream::write(  ostream
                                  , &rPtrs[i]
                                  , sizeof(rPtrs[i]));
    }

    tools::ostream::fill(ostream, sizeof(u32));
    tools::ostream::align(ostream);

    return ptr;
}

auto cQTDS::writeIntruders(std::ostream& ostream) noexcept -> u32
{
    u32 ptr = static_cast<u32>(ostream.tellp());

    if (m_intruders.empty()) {
        tools::ostream::fill(ostream, sizeof(u16), 0xFF);
        tools::ostream::fill(ostream, sizeof(u16));
        tools::ostream::fill(ostream, sizeof(u32), 0xFF);
        tools::ostream::fill(ostream, sizeof(u32));
        tools::ostream::put(ostream, 0xFF);
        tools::ostream::align(ostream);
    } else {
        for (auto i = 0u; i < m_intruders.size(); i++)
            tools::ostream::write(  ostream
                                  , &m_intruders[i]
                                  , sizeof(m_intruders[i]));
    }

    tools::ostream::fill(ostream, sizeof(u64) * 6);

    return ptr;
}

auto cQTDS::writeMonsterWave(std::ostream& ostream,
                             const enemyWave& wave) noexcept -> bool
{
    if (wave.empty())
        return false;

    for (auto i = 0u; i < wave.size(); i++)
        tools::ostream::write(  ostream
                              , &wave[i]
                              , sizeof(wave[i]));

    return true;
}

auto cQTDS::isSmallMonsterWavesEmpty(void) const noexcept -> bool
{
    for (const auto& group : m_smallEnemyWaves)
        for (const auto& wave : group)
            if (!isMonsterWaveEmpty(wave))
                return false;

    return true;
}

auto cQTDS::isLargeMonsterWavesEmpty(void) const noexcept -> bool
{
    for (const auto& wave : m_largeEnemyWaves)
        if (!isMonsterWaveEmpty(wave))
            return false;

    return true;
}

auto cQTDS::isMonsterWaveEmpty(const enemyWave& wave) const noexcept -> bool
{
    static sEm dummy;

    for (const auto& em : wave)
        if (em != dummy)
            return false;

    return true;
}

sEm::sEm()
{
    std::memset(  this
                , 0
                , sizeof(sEm));
}

auto sEm::operator == (const sEm& other) const noexcept -> bool
{
    return    tools::checksum(this, sizeof(sEm))
           == tools::checksum(&other, sizeof(sEm));
}

auto sEm::operator != (const sEm& other) const noexcept -> bool
{
    return ! operator==(other);
}

auto sEmHash::operator () (const sEm& other) const noexcept -> std::size_t
{
    return tools::checksum(&other, sizeof(sEm));
}

}; /// namespace qtds
}; /// namespace mh4u
}; /// namespace mh
