#include "libmh/MH4U/Extentions/lmd.hpp"
#include "libmh/tools/magicVersion.hpp"
#include "libmh/tools/istream.hpp"
#include "libmh/tools/ostream.hpp"

#include <istream>

namespace mh {
namespace mh4u {
namespace lmd {

inline static constexpr auto align(std::size_t size) -> std::size_t
{
    while (size % 4 != 0)
        ++size;

    return size;
}

auto parse(std::istream& istream, cLMD& obj) noexcept -> bool
{
    if (!obj.basicCheck(istream))
        return false;

    if (!obj.readHeader(istream))
        return false;

    if (tools::istream::size(istream) < static_cast<std::streamsize>(obj.getExpectedSize(false)))
        return false;

    if (   !obj.readData0(istream)
        || !obj.readData1(istream)
        || !obj.readStringInfo(istream))
        return false;

    if (!obj.readStrings(istream))
        return false;

    if (!obj.readFilename(istream))
        return false;

    obj.m_valid = true;

    return true;
}

auto cLMD::isValid(void) const noexcept -> bool { return m_valid; }

auto cLMD::dump(std::ostream& ostream) noexcept -> bool
{
    Header header;
    std::streamsize pData0;
    std::streamsize pData1;
    std::streamsize pStringInfo;
    std::streamsize pFilename;
    std::size_t stringInfoSize = m_stringInfo.size() * sizeof(StringInfo);
    std::size_t totalSize =   sizeof(Header)
                            + m_data0.size() * sizeof(Data0)
                            + m_data1.size() * sizeof(Data1);

    ostream.seekp(  sizeof(Header)
                  , std::ios::beg);

    pData0 = static_cast<decltype(pData0)>(ostream.tellp());
    if (pData0 == -1)
        return false;

    for (auto i = 0u; i < m_data0.size(); i++)
        tools::ostream::write(  ostream
                              , &m_data0[i]
                              , sizeof(Data0));

    pData1 = static_cast<decltype(pData1)>(ostream.tellp());
    if (pData1 == -1)
        return false;

    for (auto i = 0u; i < m_data1.size(); i++)
        tools::ostream::write(  ostream
                              , &m_data1[i]
                              , sizeof(Data1));

    pStringInfo = static_cast<decltype(pStringInfo)>(ostream.tellp());
    if (pStringInfo == -1)
        return false;

    // jump to strings position
    ostream.seekp(  stringInfoSize
                  , std::ios::cur);

    if (m_strings.size() != m_stringInfo.size())
        m_stringInfo.resize(m_strings.size());

    for (auto i = 0u; i < m_strings.size(); i++) {
        m_stringInfo[i].Size = static_cast<u32>(m_strings[i].size());
        m_stringInfo[i].SizeDuplicate = static_cast<u32>(m_strings[i].size());
        m_stringInfo[i].pU16Str = static_cast<u32>(ostream.tellp());

        std::size_t size = m_strings[i].size() * sizeof(char16_t);
        std::size_t alignedSize = align(size + NULL_TERMINATOR_SIZE);

        tools::ostream::write(  ostream
                              , m_strings[i].c_str()
                              , size);

        for (auto i = 0u; i < alignedSize - size; i++)
            ostream.put('\0');

        totalSize += alignedSize;
    }

    pFilename = static_cast<decltype(pFilename)>(ostream.tellp());
    if (pFilename == -1)
        return false;

    tools::ostream::write(  ostream
                          , m_filename.c_str()
                          , m_filename.size());

    ostream.put('\0');

    ostream.seekp(pStringInfo);

    for (auto i = 0u; i < m_stringInfo.size(); i++)
        tools::ostream::write(  ostream
                              , &m_stringInfo[i]
                              , sizeof(StringInfo));

    totalSize += m_stringInfo.size() * sizeof(StringInfo);
    totalSize += m_filename.size() + 1;

    header.Magic = Header::MAGIC;
    header.Version = Header::VERSION;

    header.Data0Num = static_cast<u32>(m_data0.size());
    header.Data1Num = static_cast<u32>(m_data1.size());
    header.StringInfoNum = static_cast<u32>(m_strings.size());

    header.pData0 = static_cast<u32>(pData0);
    header.pData1 = static_cast<u32>(pData1);
    header.pStringInfo = static_cast<u32>(pStringInfo);
    header.pFilename = static_cast<u32>(pFilename);

    ostream.seekp(  std::ios::beg
                  , std::ios::beg);

    ostream.write(  reinterpret_cast<char*>(&header)
                  , sizeof(Header));

    ostream.seekp(  std::ios::beg
                  , std::ios::end);

    return ostream.tellp() == static_cast<std::ostream::pos_type>(totalSize);
}

auto cLMD::replaceString(const std::u16string& str, const std::size_t id) noexcept -> bool
{
    if (id >= m_strings.size())
        return false;

    m_strings[id] = str;

    return true;
}

auto cLMD::getHeader(void) const noexcept -> const Header& { return m_header; }
auto cLMD::getData0(void) const noexcept -> const std::vector<Data0>& { return m_data0; }
auto cLMD::getData1(void) const noexcept -> const std::vector<Data1>& { return m_data1; }
auto cLMD::getStringInfo(void) const noexcept -> const std::vector<StringInfo>& { return m_stringInfo; }
auto cLMD::getStrings(void) const noexcept -> const std::vector<std::u16string>& { return m_strings; }
auto cLMD::getFilename(void) const noexcept -> const std::string& { return m_filename; }

auto cLMD::getExpectedSize(const bool) const noexcept -> std::size_t
{
    auto totalSize =   sizeof(Header)
                     + m_header.Data0Num * sizeof(Data0)
                     + m_header.Data0Num * sizeof(Data0)
                     + m_header.StringInfoNum * sizeof(StringInfo);

    return totalSize;
}

auto cLMD::basicCheck(std::istream& istream) noexcept -> bool
{
    tools::MagicVersion magicVersion;

    if (tools::istream::size(istream) <= static_cast<std::streamsize>(sizeof(Header)))
        return false;

    magicVersion.read(istream);

    if (!magicVersion.check(Header::MAGIC, Header::VERSION))
        return false;

    return true;
}

auto cLMD::readHeader(std::istream& istream) noexcept -> bool
{
    tools::istream::read(  istream
                         , &m_header
                         , sizeof(Header));

    return istream.gcount() == sizeof(Header);
}

auto cLMD::readFilename(std::istream& istream) noexcept -> bool
{
    std::string str;

    istream.seekg(  m_header.pFilename
                  , std::ios::beg);

    if (istream.fail())
        return false;

    std::getline(  istream
                 , str
                 , '\0');

    if (str.empty())
        return false;

    m_filename = std::move(str);

    return true;
}

auto cLMD::readData0(std::istream& istream) noexcept -> bool
{
    if (   m_header.pData0 == 0
        || m_header.Data0Num == 0)
        return true;

    std::streamsize expectedPos =   m_header.pData0
                                  + m_header.Data0Num * sizeof(Data0);

    m_data0.reserve(m_header.Data0Num);

    istream.seekg(m_header.pData0);

    for (auto i = 0u; i < m_header.Data0Num; i++) {
        Data0 data;

        tools::istream::read(  istream
                             , &data
                             , sizeof(Data0));

        m_data0.push_back(std::move(data));
    }

    return istream.tellg() == expectedPos;
}

auto cLMD::readData1(std::istream& istream) noexcept -> bool
{
    if (   m_header.pData1 == 0
        || m_header.Data1Num == 0)
        return true;

    std::streamsize expectedPos =   m_header.pData1
                                  + m_header.Data1Num * sizeof(Data1);

    m_data1.reserve(m_header.Data1Num);

    istream.seekg(m_header.pData1);

    for (auto i = 0u; i < m_header.Data1Num; i++) {
        Data1 data;

        tools::istream::read(  istream
                             , &data
                             , sizeof(Data1));

        m_data1.push_back(std::move(data));
    }

    return istream.tellg() == expectedPos;
}

auto cLMD::readStringInfo(std::istream& istream) noexcept -> bool
{
    if (   m_header.pStringInfo == 0
        || m_header.StringInfoNum == 0)
        return true;

    std::streamsize expectedPos =   m_header.pStringInfo
                                  + m_header.StringInfoNum * sizeof(StringInfo);

    m_stringInfo.reserve(m_header.StringInfoNum);

    istream.seekg(m_header.pStringInfo);

    for (auto i = 0u; i < m_header.StringInfoNum; i++) {
        StringInfo info;

        tools::istream::read(  istream
                             , &info
                             , sizeof(StringInfo));

        m_stringInfo.push_back(std::move(info));
    }

    return istream.tellg() == expectedPos;
}

auto cLMD::readStrings(std::istream& istream) noexcept -> bool
{
    if (m_header.StringInfoNum == 0)
        return true;

    std::istream::pos_type pos;
    std::u16string temp;
    char buffer[2];

    m_strings.reserve(m_header.StringInfoNum);
    temp.reserve(1024);

    do {
        istream.read(buffer, sizeof(buffer));
        pos = istream.tellg();

        if (pos == -1)
            break;

        if (   buffer[0] == 0
            && buffer[1] == 0) {
            if (temp.empty())
                continue;

            m_strings.push_back(temp);

            temp.clear();
        } else {
            temp.push_back(*reinterpret_cast<const utf16*>(buffer));
        }
    } while (   pos != m_header.pFilename
             || istream.eof());

    return m_strings.size() == m_header.StringInfoNum;
}

}; /// namespace lmd
}; /// namespace mh4u
}; /// namespace mh
