#include "libmh/MH4U/Save/Savefile.hpp"
#include "libmh/MH/Crypto.hpp"

#include "libmh/tools/istream.hpp"
#include "libmh/tools/ostream.hpp"

#include <istream>
#include <sstream>

namespace mh {
namespace mh4u {
namespace savefile {

auto _parse(std::istream& istream, cSaveFile& obj) noexcept -> bool
{
    if (!obj.readHeader(istream))
        return false;

    obj.m_valid = true;

    return true;
}

auto parse(std::istream& istream, cSaveFile& obj, const bool encrypted) noexcept -> bool
{
    if (encrypted) {
        std::stringstream temp(std::ios::in | std::ios::out | std::ios::binary);
        crypto::Crypto crypto(istream, temp);

        crypto.decryptSave();

        return _parse(temp, obj);
    }

    return _parse(istream, obj);
}

cSaveFile::cSaveFile()
    : m_header{std::make_unique<Header>()}
{
}

auto cSaveFile::isValid(void) const noexcept -> bool { return m_valid; }

auto cSaveFile::getPlayerName(void) const noexcept -> std::u16string
{
    std::u16string out;

    out.reserve(NAME_LENGTH_MAX);

    for (auto i = 0u; i < NAME_LENGTH_MAX + 2; i++)
        if (m_header->PlayerName[i] != 0)
            out += m_header->PlayerName[i];

    return out;
}

auto cSaveFile::setPlayerName(const std::u16string& name) noexcept -> bool
{
    if (   name.empty()
        || name.size() > NAME_LENGTH_MAX)
        return false;

    for (auto i = 0u; i < NAME_LENGTH_MAX + 2; i++)
        m_header->PlayerName[i] = 0;

    for (auto i = 0u; i < name.size(); i++)
        m_header->PlayerName[i] = name[i];

    return true;
}

auto cSaveFile::getGender(void) const noexcept -> sGender::e { return m_header->Gender; }
auto cSaveFile::setGender(const sGender::e gender) noexcept -> void { m_header->Gender = gender; }
auto cSaveFile::getFace(void) const noexcept -> sFace::e { return m_header->Face; }
auto cSaveFile::setFace(const sFace::e face) noexcept -> void { m_header->Face = face; }
auto cSaveFile::getHairstyle(void) const noexcept -> sHairstyle::e { return m_header->HairStyle; }
auto cSaveFile::setHairstyle(const sHairstyle::e hairstyle) noexcept -> void { m_header->HairStyle = hairstyle; }
auto cSaveFile::getClothing(void) const noexcept -> sClothing::e { return m_header->Clothing; }
auto cSaveFile::setClothing(const sClothing::e clothing) noexcept -> void { m_header->Clothing = clothing; }
auto cSaveFile::getVoice(void) const noexcept -> sVoice::e { return m_header->Voice; }
auto cSaveFile::setVoice(const sVoice::e voice) noexcept -> void{ m_header->Voice = voice; }
auto cSaveFile::getEyeColor(void) const noexcept -> sEyeColor::e{ return m_header->EyeColor; }
auto cSaveFile::setEyeColor(const sEyeColor::e eye_color) noexcept -> void { m_header->EyeColor = eye_color; }
auto cSaveFile::getFaceFeatures(void) const noexcept -> sFeatures::e{ return m_header->FaceFeatures; }
auto cSaveFile::setFaceFeatures(const sFeatures::e features) noexcept -> void { m_header->FaceFeatures = features; }

auto cSaveFile::getFaceFeaturesColor(void) const noexcept -> sColor { return m_header->FaceFeatureColor; }
auto cSaveFile::setFaceFeaturesColor(const sColor color) noexcept -> void { m_header->FaceFeatureColor = color; }

auto cSaveFile::getHairColor(void) const noexcept -> sColor { return m_header->HairColor; }
auto cSaveFile::setHairColor(const sColor color) noexcept -> void { m_header->HairColor = color; }

auto cSaveFile::getClothingColor(void) const noexcept -> sColor { return m_header->ClothingColor; }
auto cSaveFile::setClothingColor(const sColor color) noexcept -> void { m_header->ClothingColor = color; }

auto cSaveFile::getSkinTone(void) const noexcept -> sColor { return m_header->SkinTone; }
auto cSaveFile::setSkinTone(const sColor color) noexcept -> void { m_header->SkinTone = color; }

auto cSaveFile::getHunterRank(void) const noexcept -> u32 { return m_header->HunterRank; }
auto cSaveFile::setHunterRank(const u32 value) noexcept -> void { m_header->HunterRank = value; }

auto cSaveFile::getHunterRankPoints(void) const noexcept -> u32 { return m_header->HunterRankPoints; }
auto cSaveFile::setHunterRankPoints(const u32 value) noexcept -> void { m_header->HunterRankPoints = value; }

auto cSaveFile::getZenny(void) const noexcept -> u32 { return m_header->Zenny; }
auto cSaveFile::setZenny(const u32 value) noexcept -> void { m_header->Zenny = value; }

auto cSaveFile::getPlaytime(void) const noexcept -> u32 { return m_header->Playtime; }
auto cSaveFile::setPlaytime(const u32 value) noexcept -> void { m_header->Playtime = value; }

auto cSaveFile::getEquippedWeapon(void) const noexcept -> const uEquipment& { return m_header->Weapon; }
auto cSaveFile::setEquippedWeapon(const uEquipment& equipment) noexcept -> void { m_header->Weapon = equipment; }
auto cSaveFile::getEquippedHead(void) const noexcept -> const uEquipment& { return m_header->Head; }
auto cSaveFile::setEquippedHead(const uEquipment& equipment) noexcept -> void { m_header->Head = equipment; }
auto cSaveFile::getEquippedChest(void) const noexcept -> const uEquipment& { return m_header->Chest; }
auto cSaveFile::setEquippedChest(const uEquipment& equipment) noexcept -> void { m_header->Chest = equipment; }
auto cSaveFile::getEquippedArms(void) const noexcept -> const uEquipment& { return m_header->Arms; }
auto cSaveFile::setEquippedArms(const uEquipment& equipment) noexcept -> void { m_header->Arms = equipment; }
auto cSaveFile::getEquippedWaist(void) const noexcept -> const uEquipment& { return m_header->Waist; }
auto cSaveFile::setEquippedWaist(const uEquipment& equipment) noexcept -> void { m_header->Waist = equipment; }
auto cSaveFile::getEquippedLegs(void) const noexcept -> const uEquipment& { return m_header->Legs; }
auto cSaveFile::setEquippedLegs(const uEquipment& equipment) noexcept -> void { m_header->Legs = equipment; }
auto cSaveFile::getEquippedTalisman(void) const noexcept -> const uEquipment& { return m_header->Talisman; }
auto cSaveFile::setEquippedTalisman(const uEquipment& equipment) noexcept -> void { m_header->Talisman = equipment; }

auto cSaveFile::getWeaponEquipedBoxSlot(void) const noexcept -> u16 { return m_header->WeaponEquipedBoxSlot; }
auto cSaveFile::setWeaponEquipedBoxSlot(const u16 value) noexcept -> void { m_header->WeaponEquipedBoxSlot = value; }
auto cSaveFile::getChestEquipedBoxSlot(void) const noexcept -> u16 { return m_header->ChestEquipedBoxSlot; }
auto cSaveFile::setChestEquipedBoxSlot(const u16 value) noexcept -> void { m_header->ChestEquipedBoxSlot = value; }
auto cSaveFile::getArmsEquipedBoxSlot(void) const noexcept -> u16 { return m_header->ArmsEquipedBoxSlot; }
auto cSaveFile::setArmsEquipedBoxSlot(const u16 value) noexcept -> void { m_header->ArmsEquipedBoxSlot = value; }
auto cSaveFile::getWaistEquipedBoxSlot(void) const noexcept -> u16 { return m_header->WaistEquipedBoxSlot; }
auto cSaveFile::setWaistEquipedBoxSlot(const u16 value) noexcept -> void { m_header->WaistEquipedBoxSlot = value; }
auto cSaveFile::getLegsEquipedBoxSlot(void) const noexcept -> u16 { return m_header->LegsEquipedBoxSlot; }
auto cSaveFile::setLegsEquipedBoxSlot(const u16 value) noexcept -> void { m_header->LegsEquipedBoxSlot = value; }
auto cSaveFile::getHeadEquipedBoxSlot(void) const noexcept -> u16 { return m_header->HeadEquipedBoxSlot; }
auto cSaveFile::setHeadEquipedBoxSlot(const u16 value) noexcept -> void { m_header->HeadEquipedBoxSlot = value; }
auto cSaveFile::getTalismanEquipedBoxSlot(void) const noexcept -> u16 { return m_header->TalismanEquipedBoxSlot; }
auto cSaveFile::setTalismanEquipedBoxSlot(const u16 value) noexcept -> void { m_header->TalismanEquipedBoxSlot = value; }

auto cSaveFile::getBoxItem(const std::size_t id, sBoxItem& item) const noexcept -> bool
{
    if (id < ITEM_BOX_CELLS_MAX) {
        item = m_header->BoxItems[id];

        return true;
    }

    return false;
}

auto cSaveFile::setBoxItem(const std::size_t id, const sBoxItem& item) const noexcept -> bool
{
    if (id < ITEM_BOX_CELLS_MAX) {
        m_header->BoxItems[id] = item;

        return true;
    }

    return false;
}

auto cSaveFile::getEquipmentBoxItem(const std::size_t id, sBoxEquip& item) const noexcept -> bool
{
    if (id < EQUIPMENT_BOX_CELLS_MAX) {
        item = m_header->BoxEquipment[id];

        return true;
    }

    return false;
}

auto cSaveFile::setEquipmentBoxItem(const std::size_t id, const sBoxEquip& item) const noexcept -> bool
{
    if (id < EQUIPMENT_BOX_CELLS_MAX) {
        m_header->BoxEquipment[id] = item;

        return true;
    }

    return false;
}

auto cSaveFile::getPalicoEqBoxItem(const std::size_t id, sBoxPalicoEquip& item) const noexcept -> bool
{
    if (id < PALICO_EQUIPMENT_BOX_CELLS_MAX) {
        item = m_header->BoxPalicoEquip[id];

        return true;
    }

    return false;
}

auto cSaveFile::setPalicoEqBoxItem(const std::size_t id, const sBoxPalicoEquip& item) const noexcept -> bool
{
    if (id < PALICO_EQUIPMENT_BOX_CELLS_MAX) {
        m_header->BoxPalicoEquip[id] = item;

        return true;
    }

    return false;
}

auto cSaveFile::readHeader(std::istream& istream) noexcept -> bool
{
    if (tools::istream::size(istream) < static_cast<std::streamsize>(sizeof(Header)))
        return false;

    tools::istream::read(  istream
                         , &*m_header
                         , sizeof(Header));

    return istream.gcount() == sizeof(Header);
}

}; /// namespace savefile
}; /// namespace mh4u
}; /// namespace mh
