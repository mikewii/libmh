#include "libmh/MH4U/Equipment/Hunter/Weapon/HBG.hpp"
#include "libmh/function_name.hpp"

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

constexpr std::array<sWeapon, sHBG::ITEMS_MAX> sHBG::str = {{
      {"(None)"}
    , {"Bone Shooter"}
    , {"Bone Shooter+"}
    , {"Straw Drumcannon"}
    , {"Shirabeo"}
    , {"Ratatat Drumcannon"}
    , {"Queen's Longfire"}
    , {"Queen's Farflier"}
    , {"Queen's Scionfire"}
    , {"Bone Buster"}
    , {"Bone Blaster"}
    , {"Agnablaster"}
    , {"Agnablazer"}
    , {"Bear Barrel"}
    , {"Arbearalest"}
    , {"Arzuros Gun"}
    , {"Arzuros Gun+"}
    , {"Power Stinger"}
    , {"Power Stinger+"}
    , {"Wild Stinger"}
    , {"Ner Bustergun"}
    , {"Ner Bustergun+"}
    , {"Buster Blaster"}
    , {"Gravios Howl"}
    , {"Gravios Gigahowl"}
    , {"Gravios Roar"}
    , {"Gravios Gigaroar"}
    , {"Mightning"}
    , {"Loyal Thunder"}
    , {"Tigrex Howl"}
    , {"Tigrex Skull"}
    , {"Accursed Roar"}
    , {"Accursed Bellow"}
    , {"Gravecannon"}
    , {"Furious Gravecannon"}
    , {"Pressurized Plaser"}
    , {"Pressurized Plaser+"}
    , {"Pluripotent Plaser"}
    , {"Tropeco Gun"}
    , {"Pecopious Gun"}
    , {"Bite Blaster"}
    , {"Biting Blast"}
    , {"Dual Threat"}
    , {"Diablazooka"}
    , {"Peerless"}
    , {"Masterless"}
    , {"Arahabaki"}
    , {"Daora's Delphinidae"}
    , {"Daora Grande"}
    , {"Teostra's Artillery"}
    , {"Teostra's Flames"}
    , {"Lunastra's Cannon"}
    , {"Lunastra's Flames"}
    , {"Akantor Cannon"}
    , {"Caudalbone Crook"}
    , {"The Omnipotent Blast"}
    , {"Chaos Wing"}
    , {"Albadash"}
    , {"Vor Cannon"}
    , {"Vor Buster"}
    , {"Dual Threat [RED]", eType::RELIC}
    , {"Dual Threat [YELLOW]", eType::RELIC}
    , {"Dual Threat [GREEN]", eType::RELIC}
    , {"Dual Threat [BLUE]", eType::RELIC}
    , {"Dual Threat [PURPLE]", eType::RELIC}
    , {"Chaos Wing [RED]", eType::RELIC}
    , {"Chaos Wing [YELLOW]", eType::RELIC}
    , {"Chaos Wing [GREEN]", eType::RELIC}
    , {"Chaos Wing [BLUE]", eType::RELIC}
    , {"Chaos Wing [PURPLE]", eType::RELIC}
    , {"Tetsu Pyrotechnus"}
    , {"Tetsu Pyropinnacle"}
    , {"Empress's Blossom"}
    , {"Empress's Flower"}
    , {"Empress's Doublefleur"}
    , {"Bone Blaster+"}
    , {"Cartilage Blaster"}
    , {"Crab Buster"}
    , {"Heavy Crab Buster"}
    , {"Violet Punisher"}
    , {"Agna Magnum"}
    , {"Agna Scorpion"}
    , {"Arzuros Ozeki"}
    , {"Arzuros Yokozuna"}
    , {"Tödlicher Abzug"}
    , {"Le Désastre"}
    , {"La Foi"}
    , {"Le Déluge"}
    , {"Verrat/Foi"}
    , {"Burst Stinker"}
    , {"Fatal Stinker"}
    , {"Buster Blaster+"}
    , {"Nightfall Blaster"}
    , {"Vampyr Blaster"}
    , {"Gravios Gigacannon"}
    , {"Oppressor's Wedge"}
    , {"Orcus Quarcus"}
    , {"Brimstren Drakemouth"}
    , {"Stygian Humilitas"}
    , {"Accursed Bellow+"}
    , {"Furious Gravecannon+"}
    , {"Livid Griscannon"}
    , {"Dios Cannon"}
    , {"Dios Cannon+"}
    , {"Demolition Cannon"}
    , {"Haize Demolisher"}
    , {"Lightbreak Cannon"}
    , {"Verdant Washbuckler"}
    , {"Captain Verde"}
    , {"Pecopious Thunder"}
    , {"Precision Pico Peco"}
    , {"Ravager Blast"}
    , {"Yellow Buster"}
    , {"Azure Blast"}
    , {"Azure Buster"}
    , {"Nero's Blazooka"}
    , {"Griffon Blazooka"}
    , {"Monodevilcaster"}
    , {"Monodevilcaster+"}
    , {"Monodevilblaster"}
    , {"Utsurowazaru"}
    , {"Eigoh"}
    , {"Daora's Ceti"}
    , {"Eldaora's Delphinidae"}
    , {"Teostra's Firestorm"}
    , {"Lunastra's Firestorm"}
    , {"Kamaeleon"}
    , {"Akantor Skysounder"}
    , {"Ukanlos Cannon"}
    , {"Ukanlos Skyblaster"}
    , {"Robert the Vortex"}
    , {"Dalamadur Cannon"}
    , {"Militant Dalamadur"}
    , {"Altheos Aetheria"}
    , {"Vor Blaster"}
    , {"Destiny's Arm"}
    , {"Fatalis Enslaver"}
    , {"Sayyad Cannon"}
    , {"Sayyad Cannon+"}
    , {"Seditious Cannon"}
    , {"Furia Sedition"}
    , {"Trembling Lordship"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"Gigant Launcher [RED]", eType::RELIC}
    , {"Gigant Launcher [YELLOW]", eType::RELIC}
    , {"Gigant Launcher [GREEN]", eType::RELIC}
    , {"Gigant Launcher [BLUE]", eType::RELIC}
    , {"Gigant Launcher [PURPLE]", eType::RELIC}
    , {"Cahoe [RED]", eType::RELIC}
    , {"Cahoe [YELLOW]", eType::RELIC}
    , {"Cahoe [GREEN]", eType::RELIC}
    , {"Cahoe [BLUE]", eType::RELIC}
    , {"Cahoe [PURPLE]", eType::RELIC}
    , {"Giant Pan"}
    , {"Romea Crossbow"}
}};

auto sHBG::getStr(const e id) noexcept -> const char *
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id].Name;

    return err.c_str();
}

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
