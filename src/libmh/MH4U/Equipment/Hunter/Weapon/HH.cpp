#include "libmh/MH4U/Equipment/Hunter/Weapon/HH.hpp"
#include "libmh/function_name.hpp"

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

constexpr std::array<sWeapon, sHH::ITEMS_MAX> sHH::str = {{
      {"(None)"}
    , {"Metal Bagpipe"}
    , {"Metal Bagpipe+"}
    , {"Great Bagpipe"}
    , {"Heavy Bagpipe"}
    , {"Heavy Bagpipe+"}
    , {"Fortissimo"}
    , {"Master Bagpipe"}
    , {"Sforzando"}
    , {"Kummerklang"}
    , {"Gramklang"}
    , {"Trauerklang"}
    , {"La Sépulture"}
    , {"Valkyrie Chordmaker"}
    , {"Valkyrie Chordmaker+"}
    , {"Queen's Chordmaker"}
    , {"Gold Chordmaker"}
    , {"Kecha Whappa"}
    , {"Kecha Whappa+"}
    , {"Kechachacha Whappa"}
    , {"Kecha Thumpa"}
    , {"Kecha Clanga"}
    , {"Usurper's Growl"}
    , {"Usurper's Growl+"}
    , {"Despot's Thunderclap"}
    , {"Brimstren Drakesong"}
    , {"Ice Kazoo"}
    , {"Glacial Kazoo"}
    , {"Frost Groaner"}
    , {"Frost General"}
    , {"Bone Horn"}
    , {"Bone Horn+"}
    , {"Hunter's Horn"}
    , {"Hunter's Horn+"}
    , {"Native's Horn"}
    , {"Striped Gong"}
    , {"Tigrex Horn"}
    , {"Accursed Wail"}
    , {"Howling Gravedrum"}
    , {"Dios Bell"}
    , {"Dios Bell+"}
    , {"Demolition Chime"}
    , {"Khezu Horn"}
    , {"Khezu Flute"}
    , {"Blood Horn"}
    , {"Blood Flute"}
    , {"War Drum"}
    , {"War Bongo"}
    , {"War Conga"}
    , {"Jungle Bongo"}
    , {"Jungle Conga"}
    , {"Danger Call"}
    , {"Danger Call+"}
    , {"Hazardous Call"}
    , {"Parahazard Call"}
    , {"Velociprey Balloon"}
    , {"Velociprey Balloon+"}
    , {"Raven Shamisen"}
    , {"Wolf Shamisen"}
    , {"Crow Shamisen"}
    , {"Basarios Rock"}
    , {"Basarios Rock Mk.II"}
    , {"Volcanic Rock"}
    , {"Volcanic Gig"}
    , {"Beating Gunrock"}
    , {"Fleeting Gunrock"}
    , {"Vicello Nulo (W)"}
    , {"Vicello Unu (W)"}
    , {"Vicello Du (W)"}
    , {"Magia Charm"}
    , {"Dragonwood Flute"}
    , {"Worn Horn"}
    , {"Weathered Horn"}
    , {"Avenir's Music Box"}
    , {"Akantor Horn"}
    , {"Veilglare Hymn"}
    , {"The Ascending Strain"}
    , {"Fatalis Menace"}
    , {"dummy78"}
    , {"dummy79"}
    , {"Megaphone"}
    , {"Basarios Rock [RED]", eType::RELIC}
    , {"Basarios Rock [YELLOW]", eType::RELIC}
    , {"Basarios Rock [GREEN]", eType::RELIC}
    , {"Basarios Rock [BLUE]", eType::RELIC}
    , {"Basarios Rock [PURPLE]", eType::RELIC}
    , {"Hellstrings [RED]", eType::RELIC}
    , {"Hellstrings [YELLOW]", eType::RELIC}
    , {"Hellstrings [GREEN]", eType::RELIC}
    , {"Hellstrings [BLUE]", eType::RELIC}
    , {"Hellstrings [PURPLE]", eType::RELIC}
    , {"Swell Shell [RED]", eType::RELIC}
    , {"Swell Shell [YELLOW]", eType::RELIC}
    , {"Swell Shell [GREEN]", eType::RELIC}
    , {"Swell Shell [BLUE]", eType::RELIC}
    , {"Swell Shell [PURPLE]", eType::RELIC}
    , {"Gold Chordmaker [RED]", eType::RELIC}
    , {"Gold Chordmaker [YELLOW]", eType::RELIC}
    , {"Gold Chordmaker [GREEN]", eType::RELIC}
    , {"Gold Chordmaker [BLUE]", eType::RELIC}
    , {"Gold Chordmaker [PURPLE]", eType::RELIC}
    , {"Sforzando+"}
    , {"Rinforzato"}
    , {"La Joie"}
    , {"Le Grégorien"}
    , {"Grimm/Joie"}
    , {"Gold Chordmaker+"}
    , {"Lunar Chordmaker"}
    , {"Kecha Clanga+"}
    , {"Kecha Clanvira"}
    , {"Oppressor's Boon"}
    , {"Brimstren Drakesong+"}
    , {"Stygian Tristitia"}
    , {"Denden Daiko"}
    , {"Denden Doomsounder"}
    , {"Poli'ahu Pipe"}
    , {"Phenomenal Phlute"}
    , {"Howling Gravedrum+"}
    , {"Dancing Grisdrum"}
    , {"Timbral Demolisher"}
    , {"Lightbreak Timbre"}
    , {"Shell Castanet"}
    , {"Shell Castanet+"}
    , {"Colored Castanet"}
    , {"Crabby Castanet"}
    , {"Klick-Klack"}
    , {"Blood Flute+"}
    , {"Brute Flute"}
    , {"Jungle Conga+"}
    , {"Primal Conga"}
    , {"Parahazard Call+"}
    , {"Para Calamity"}
    , {"Naja Warning"}
    , {"Naja Alluvion"}
    , {"Pox Shamisen"}
    , {"Garuga Shamisen"}
    , {"Volcanic Gig+"}
    , {"Conflagrig"}
    , {"Heavy Facemelter"}
    , {"Lullaby Facemelter"}
    , {"Magia Charmbell"}
    , {"Aria Rhota"}
    , {"Dragonwood Godflute"}
    , {"Avenir's Music Box+"}
    , {"Eternal Music Box"}
    , {"Teostra's Tiple"}
    , {"Teostra's Orphée"}
    , {"Daora's Taus"}
    , {"Daora's Baphophone"}
    , {"Eldaora's Taus"}
    , {"Genie's Ocarina"}
    , {"Akantor Dark Melody"}
    , {"Ukanlos Horned Flute"}
    , {"Ukanlos Skysinger"}
    , {"Orgueil the Insane"}
    , {"Dalamadur Song"}
    , {"Bloodied Dalamadur"}
    , {"Fatalis Menace Lute"}
    , {"Fatalis Menace Soul"}
    , {"Fatalis Ancient Lute"}
    , {"Vadya Muse"}
    , {"Vadya Muse+"}
    , {"Seditious Warhorn"}
    , {"Maqam Sedition"}
    , {"Clemency's Peal"}
    , {"Onyx Terpsichore"}
    , {"Glass Royale"}
    , {"Queen's Flute"}
    , {"Handmade Frog"}
    , {"Hunter Master"}
    , {"Emperor's Speech"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"Scorpion Lute"}
    , {"Endless Loop"}
    , {"Sonic Glass [RED]", eType::RELIC}
    , {"Sonic Glass [YELLOW]", eType::RELIC}
    , {"Sonic Glass [GREEN]", eType::RELIC}
    , {"Sonic Glass [BLUE]", eType::RELIC}
    , {"Sonic Glass [PURPLE]", eType::RELIC}
    , {"Hidden Harmonic [RED]", eType::RELIC}
    , {"Hidden Harmonic [YELLOW]", eType::RELIC}
    , {"Hidden Harmonic [GREEN]", eType::RELIC}
    , {"Hidden Harmonic [BLUE]", eType::RELIC}
    , {"Hidden Harmonic [PURPLE]", eType::RELIC}
}};

auto sHH::getStr(const e id) noexcept -> const char *
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id].Name;

    return err.c_str();
}

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
