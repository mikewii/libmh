#include "libmh/MH4U/Equipment/Hunter/Weapon/GS.hpp"
#include "libmh/function_name.hpp"

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

constexpr std::array<sWeapon, sGS::ITEMS_MAX> sGS::str = {{
      {"(None)"}
    , {"Iron Sword"}
    , {"Iron Sword+"}
    , {"Buster Sword"}
    , {"Buster Sword+"}
    , {"Ravager Blade"}
    , {"Ravager Blade+"}
    , {"Lacerator Blade"}
    , {"Devastator Blade"}
    , {"Chrome Razor"}
    , {"Chrome Quietus"}
    , {"Khezu Shock Sword"}
    , {"Khezu Shock Blade"}
    , {"Khezu Shock Full"}
    , {"Santoku Reaver"}
    , {"Funayuki Reaver"}
    , {"Toad Bereaver"}
    , {"Millstone Bereaver"}
    , {"Brazenwall"}
    , {"Crimsonwall"}
    , {"Cragscliff"}
    , {"Rugged Great Sword"}
    , {"Chieftain's Grt Swd"}
    , {"Schattenstolz"}
    , {"Düsterstolz"}
    , {"Verhängnisstolz"}
    , {"L'Apothéose"}
    , {"Usurper's Storm"}
    , {"Despot's Blackstorm"}
    , {"Brimstren Drakepride"}
    , {"Demon Rod"}
    , {"Great Demon Rod"}
    , {"Ludroth Bone Sword"}
    , {"Ludroth Bone Sword+"}
    , {"Cataclysm Sword"}
    , {"Cataclysm Blade"}
    , {"Bone Blade"}
    , {"Bone Blade+"}
    , {"Jawblade"}
    , {"Giant Jawblade"}
    , {"Golem Blade"}
    , {"Tiger Jawblade"}
    , {"Tigrex Great Sword"}
    , {"Accursed Blade"}
    , {"Veined Graveblade"}
    , {"Dios Blade"}
    , {"Dios Blade+"}
    , {"Demolition Sword"}
    , {"Valkyrie Blade"}
    , {"Sieglinde"}
    , {"Titania"}
    , {"Blushing Dame"}
    , {"Auberon"}
    , {"Red Wing"}
    , {"Rathalos Firesword"}
    , {"Rathalos Flamesword"}
    , {"Rathalos Gleamsword"}
    , {"Rathalos Glinsword"}
    , {"Thrash Sword"}
    , {"Thrash Sword+"}
    , {"Thrash Blade"}
    , {"Thrashgash Blade"}
    , {"Immane Blade"}
    , {"Immolator Blade"}
    , {"Scylla Blade"}
    , {"Scylla Razor"}
    , {"Vile Serpentblade"}
    , {"Vile Serpentblade+"}
    , {"Fanged Serpentblade"}
    , {"Deadly Serpentblade"}
    , {"Poison Serpentblade"}
    , {"Chick Decapitator"}
    , {"Rooster Decapitator"}
    , {"Sentoryu Raven"}
    , {"Sentoryu Wolf"}
    , {"Sentoryu Crow"}
    , {"Hidden Blade"}
    , {"Dark of Night"}
    , {"Vulcanis"}
    , {"Vulcamagnon"}
    , {"Plesioth Watercutter"}
    , {"Plesioth Watercutter+"}
    , {"Plesioth Lullaby"}
    , {"Plesioth Lullabane"}
    , {"Frozen Speartuna"}
    , {"Freezer Speartuna"}
    , {"Rusted Great Sword"}
    , {"Tarnished Great Swd"}
    , {"Ancient Blade"}
    , {"Elder Monument"}
    , {"Icesteel Edge"}
    , {"Daora's Decimator"}
    , {"Worn Great Sword"}
    , {"Weathered Great Swd"}
    , {"Epitaph Blade"}
    , {"Teostra Blade"}
    , {"King Teostra Blade"}
    , {"Kirin Thundersword"}
    , {"King Thundersword"}
    , {"Emperor Frostsword"}
    , {"Berserker Sword"}
    , {"Anguish"}
    , {"Consummate Blade"}
    , {"Akantor Broadsword"}
    , {"Swordscale Shard"}
    , {"The Lethal Graze"}
    , {"Fatalis Blade"}
    , {"dummy107"}
    , {"dummy108"}
    , {"dummy109"}
    , {"dummy110"}
    , {"dummy111"}
    , {"dummy112"}
    , {"dummy113"}
    , {"Universal Chrome"}
    , {"Siegmund [RED]", eType::RELIC}
    , {"Siegmund [YELLOW]", eType::RELIC}
    , {"Siegmund [GREEN]", eType::RELIC}
    , {"Siegmund [BLUE]", eType::RELIC}
    , {"Siegmund [PURPLE]", eType::RELIC}
    , {"Bolt [RED]", eType::RELIC}
    , {"Bolt [YELLOW]", eType::RELIC}
    , {"Bolt [GREEN]", eType::RELIC}
    , {"Bolt [BLUE]", eType::RELIC}
    , {"Bolt [PURPLE]", eType::RELIC}
    , {"Type 41 Wyvernator [RED]", eType::RELIC}
    , {"Type 41 Wyvernator [YELLOW]", eType::RELIC}
    , {"Type 41 Wyvernator [GREEN]", eType::RELIC}
    , {"Type 41 Wyvernator [BLUE]", eType::RELIC}
    , {"Type 41 Wyvernator [PURPLE]", eType::RELIC}
    , {"Flash [RED]", eType::RELIC}
    , {"Flash [YELLOW]", eType::RELIC}
    , {"Flash [GREEN]", eType::RELIC}
    , {"Flash [BLUE]", eType::RELIC}
    , {"Flash [PURPLE]", eType::RELIC}
    , {"Dragonseal [RED]", eType::RELIC}
    , {"Dragonseal [YELLOW]", eType::RELIC}
    , {"Dragonseal [GREEN]", eType::RELIC}
    , {"Dragonseal [BLUE]", eType::RELIC}
    , {"Dragonseal [PURPLE]", eType::RELIC}
    , {"Exemplar Blade"}
    , {"Grandglory Blade"}
    , {"Chrome Hell"}
    , {"Chrome Heaven"}
    , {"Blood Sword"}
    , {"Blood Shock"}
    , {"Rampage Blade"}
    , {"Rampage Temperline"}
    , {"Cragscliff+"}
    , {"Cliffsword Titanius"}
    , {"L'Éclat"}
    , {"L'Origine"}
    , {"Düster/Éclat"}
    , {"Oppressor's Forger"}
    , {"Orcus Tonitrus"}
    , {"Brimstren Drakepride+"}
    , {"Stygian Acedia"}
    , {"Great Demon Hot Rod"}
    , {"Demonlord Rod"}
    , {"Cataclysm Blade+"}
    , {"Veined Graveblade+"}
    , {"Deified Grisblade"}
    , {"Myxo Demolisher"}
    , {"Lightbreak Blade"}
    , {"Executioner"}
    , {"Executioner+"}
    , {"Enforcer's Axe"}
    , {"Cera Cigil"}
    , {"Cera Cymmetry"}
    , {"Red Pincer"}
    , {"Great Pincer"}
    , {"Violet Pincer"}
    , {"Grand Pincer"}
    , {"Blushing Dame+"}
    , {"Brünnhilde"}
    , {"Auberon+"}
    , {"Pale Kaiser"}
    , {"Rathalos Gleamsword+"}
    , {"Rathlord Gleamsword"}
    , {"Rathalos Glinsword+"}
    , {"Rathflame Glinsword"}
    , {"Jala Blade"}
    , {"Jajala Blade"}
    , {"Pavaka Blade"}
    , {"Jvalayati Blade"}
    , {"Scylla Shear"}
    , {"Ebonscylla"}
    , {"Webbonscylla"}
    , {"Snakesmaw"}
    , {"Sentoryu Pox"}
    , {"Garuga Engetsu"}
    , {"Dark of Night+"}
    , {"Avidya Great Sword"}
    , {"Vulcatastrophe"}
    , {"Vulca Vendetta"}
    , {"Plesioth Lullabane+"}
    , {"Plesioth Pelagis"}
    , {"Xiphias Gladius"}
    , {"Eternal Glyph"}
    , {"Daora's Deathmaker"}
    , {"Eldaora's Death"}
    , {"Epitaph Blade+"}
    , {"Epitaph Eternal"}
    , {"Teostra del Sol"}
    , {"Grand Shamshir"}
    , {"Emperor Thundersword"}
    , {"Frostsword"}
    , {"King Frostsword"}
    , {"Doomedge \"Hrunting\""}
    , {"Akantor Broadsword+"}
    , {"Ukanlos Destructor"}
    , {"Ukanlos Skycleaver"}
    , {"Wold the Just"}
    , {"Dalamadur Blade"}
    , {"Dreaming Dalamadur"}
    , {"Black Fatalis Blade"}
    , {"Flame Fatalis Blade"}
    , {"Fatalis Legacy"}
    , {"Cheda Blade"}
    , {"Cheda Blade+"}
    , {"Seditious Cleaver"}
    , {"Rogue Sedition"}
    , {"Monument of Lament"}
    , {"Ostracon Oblivion"}
    , {"G Blade"}
    , {"Black Belt Blade"}
    , {"Commemoration Sword"}
    , {"dummy227"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"Dos Exile"}
    , {"Rath of Stars"}
    , {"Sea-emperor's Sword [RED]", eType::RELIC}
    , {"Sea-emperor's Sword [YELLOW]", eType::RELIC}
    , {"Sea-emperor's Sword [GREEN]", eType::RELIC}
    , {"Sea-emperor's Sword [BLUE]", eType::RELIC}
    , {"Sea-emperor's Sword [PURPLE]", eType::RELIC}
    , {"Jawblade [RED]", eType::RELIC}
    , {"Jawblade [YELLOW]", eType::RELIC}
    , {"Jawblade [GREEN]", eType::RELIC}
    , {"Jawblade [BLUE]", eType::RELIC}
    , {"Jawblade [PURPLE]", eType::RELIC}
}};

auto sGS::getStr(const e id) noexcept -> const char *
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id].Name;

    return err.c_str();
}

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
