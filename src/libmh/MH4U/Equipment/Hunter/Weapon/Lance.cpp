#include "libmh/MH4U/Equipment/Hunter/Weapon/Lance.hpp"
#include "libmh/function_name.hpp"

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

constexpr std::array<sWeapon, sLance::ITEMS_MAX> sLance::str = {{
      {"(None)"}
    , {"Iron Lance"}
    , {"Iron Lance+"}
    , {"Knight Lance"}
    , {"Knight Lance+"}
    , {"Rampart"}
    , {"Rampart+"}
    , {"Babel Spear"}
    , {"Elder Babel Spear"}
    , {"Drill Lance"}
    , {"Megadrill Lance"}
    , {"Eiferschild"}
    , {"Fieberschild"}
    , {"Zornesschild"}
    , {"L'Égide"}
    , {"Venom Lance"}
    , {"Basarios Venom Spear"}
    , {"Gravios Spear"}
    , {"Black Gravios Spear"}
    , {"Diablos Lance"}
    , {"Diablos Spear"}
    , {"Fragrance"}
    , {"Fragrance+"}
    , {"Dark Spear"}
    , {"Darkness"}
    , {"Bone Lance"}
    , {"Bone Lance+"}
    , {"Spiked Spear"}
    , {"Spiked Spear+"}
    , {"Spiked Javelin"}
    , {"Tiger Stinger"}
    , {"Tigrex Lance"}
    , {"Accursed Stinger"}
    , {"Predator Gravelance"}
    , {"Dios Stinger"}
    , {"Dios Stinger+"}
    , {"Demolition Lance"}
    , {"Red Tail"}
    , {"Spear of Prominence"}
    , {"Silver Prominence"}
    , {"Blue Prominence"}
    , {"Twistcrawl"}
    , {"Twistcrawl+"}
    , {"Twisting Crawler"}
    , {"Paratwist Crawler"}
    , {"Expelger"}
    , {"Expelgorer"}
    , {"Expelgorer+"}
    , {"Expelgouger"}
    , {"Rexpelgouger"}
    , {"Gobulu Muruka"}
    , {"Gobuluku Muraaka"}
    , {"Usurper's Coming"}
    , {"Despot's Cacophony"}
    , {"Demon Lance"}
    , {"Great Demon Lance"}
    , {"Brimstren Draketail"}
    , {"Carnivicious"}
    , {"Carnivicious+"}
    , {"Omnivicious"}
    , {"Omnivoracious"}
    , {"Incessant Raven"}
    , {"Incessant Wolf"}
    , {"Incessant Crow"}
    , {"Blue Crater"}
    , {"Doom Crown"}
    , {"Sharq Byte"}
    , {"Sharq Attaq"}
    , {"Aqua Spear"}
    , {"Stream Spear"}
    , {"Emerald Spear"}
    , {"Watery Trishula"}
    , {"Rusted Lance"}
    , {"Tarnished Lance"}
    , {"Undertaker"}
    , {"High Undertaker"}
    , {"Icesteel Spear"}
    , {"Daora's Fang"}
    , {"Worn Spear"}
    , {"Weathered Spear"}
    , {"Skyscraper"}
    , {"Teostra's Howl"}
    , {"Teostra's Roar"}
    , {"Thunderspear"}
    , {"Thunderlance"}
    , {"Grief Lance"}
    , {"Fiendish Tower"}
    , {"Shellslab Prong"}
    , {"The Steadfast Aegis"}
    , {"Fatalis Lance"}
    , {"Ruiner Lance"}
    , {"dummy91"}
    , {"dummy92"}
    , {"Babel Spear [RED]", eType::RELIC}
    , {"Babel Spear [YELLOW]", eType::RELIC}
    , {"Babel Spear [GREEN]", eType::RELIC}
    , {"Babel Spear [BLUE]", eType::RELIC}
    , {"Babel Spear [PURPLE]", eType::RELIC}
    , {"Tusk Lance [RED]", eType::RELIC}
    , {"Tusk Lance [YELLOW]", eType::RELIC}
    , {"Tusk Lance [GREEN]", eType::RELIC}
    , {"Tusk Lance [BLUE]", eType::RELIC}
    , {"Tusk Lance [PURPLE]", eType::RELIC}
    , {"Dragonrider Lance [RED]", eType::RELIC}
    , {"Dragonrider Lance [YELLOW]", eType::RELIC}
    , {"Dragonrider Lance [GREEN]", eType::RELIC}
    , {"Dragonrider Lance [BLUE]", eType::RELIC}
    , {"Dragonrider Lance [PURPLE]", eType::RELIC}
    , {"Frontrider Lance [RED]", eType::RELIC}
    , {"Frontrider Lance [YELLOW]", eType::RELIC}
    , {"Frontrider Lance [GREEN]", eType::RELIC}
    , {"Frontrider Lance [BLUE]", eType::RELIC}
    , {"Frontrider Lance [PURPLE]", eType::RELIC}
    , {"Silver Prominence [RED]", eType::RELIC}
    , {"Silver Prominence [YELLOW]", eType::RELIC}
    , {"Silver Prominence [GREEN]", eType::RELIC}
    , {"Silver Prominence [BLUE]", eType::RELIC}
    , {"Silver Prominence [PURPLE]", eType::RELIC}
    , {"Elder Babel Spear+"}
    , {"Lost Babel"}
    , {"Megadrill Lance+"}
    , {"Gigadrill Lance"}
    , {"Le Sage"}
    , {"Le Paladin"}
    , {"Luzifer/Sage"}
    , {"Gravios Lance"}
    , {"Gravios Fire Lance"}
    , {"Cera Creos"}
    , {"Black Tempest"}
    , {"Rising Tempest"}
    , {"Crimson Lance"}
    , {"Crimson Lance+"}
    , {"Crimson War Pike"}
    , {"Crimson Monoblos"}
    , {"Ceramic Blos Lance"}
    , {"White Catastrophe"}
    , {"Darkness+"}
    , {"Dark Din"}
    , {"Predator Gravelance+"}
    , {"Ruthless Grislance"}
    , {"Albrach Demolisher"}
    , {"Lightbreak Lance"}
    , {"Sol Prominence"}
    , {"Immaculate Soul"}
    , {"Soul of Prominence"}
    , {"Paragyred Spear"}
    , {"Asclepius"}
    , {"Jingana"}
    , {"Jingana+"}
    , {"Caduceus"}
    , {"Bone Claw Lance"}
    , {"Bone Claw Lance+"}
    , {"Shell Claw Lance"}
    , {"Giant Hermitaur Lance"}
    , {"Shuraba Lance"}
    , {"Shula Shuraba"}
    , {"White Shula Shuraba"}
    , {"Gobulaluku Bosca"}
    , {"Gobulaluku Aquir"}
    , {"Oppressor's Genesis"}
    , {"Orcus Galeus"}
    , {"Brimstren Draketail+"}
    , {"Stygian Ira"}
    , {"Demonlance Rajang"}
    , {"Demonray Rajang"}
    , {"The Destructor"}
    , {"Icelance Zamtrios"}
    , {"Carnelian Lance"}
    , {"Crystalis"}
    , {"Tusk Lance"}
    , {"Sabertooth"}
    , {"Incessant Pox"}
    , {"Garuga Incessance"}
    , {"Eterno Crown"}
    , {"Aidion Crown"}
    , {"Sharq Assawlt"}
    , {"Watery Trishula+"}
    , {"Esmeralda's Tide"}
    , {"Neo Undertaker"}
    , {"Daora's Regulus"}
    , {"Eldaora's Fang"}
    , {"Skyscraper+"}
    , {"Skysunderer"}
    , {"Teostra's Fire"}
    , {"Naar Thunderlance"}
    , {"Nero's Atrocity"}
    , {"Ukanlos Calamity"}
    , {"Ukanlos Skysweeper"}
    , {"Ethelred the Ready"}
    , {"Dalamadur Lance"}
    , {"Generous Dalamadur"}
    , {"True Fatalis Lance"}
    , {"True Ruiner Lance"}
    , {"W. Fatalis Lance"}
    , {"Harbah Lance"}
    , {"Harbah Lance+"}
    , {"Seditious Lance"}
    , {"Hadad Sedition"}
    , {"Fettering Rebuke"}
    , {"Aether Geghard"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"Seven-star lance [RED]", eType::RELIC}
    , {"Seven-star lance [YELLOW]", eType::RELIC}
    , {"Seven-star lance [GREEN]", eType::RELIC}
    , {"Seven-star lance [BLUE]", eType::RELIC}
    , {"Seven-star lance [PURPLE]", eType::RELIC}
    , {"Estoc [RED]", eType::RELIC}
    , {"Estoc [YELLOW]", eType::RELIC}
    , {"Estoc [GREEN]", eType::RELIC}
    , {"Estoc [BLUE]", eType::RELIC}
    , {"Estoc [PURPLE]", eType::RELIC}
    , {"Hell Phalanx"}
    , {"Purification"}
}};

auto sLance::getStr(const e id) noexcept -> const char *
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id].Name;

    return err.c_str();
}

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
