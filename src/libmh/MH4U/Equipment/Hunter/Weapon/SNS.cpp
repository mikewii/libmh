#include "libmh/MH4U/Equipment/Hunter/Weapon/SNS.hpp"
#include "libmh/function_name.hpp"

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

constexpr std::array<sWeapon, sSNS::ITEMS_MAX> sSNS::str = {{
      {"(None)"}
    , {"Hunter's Knife"}
    , {"Hunter's Knife+"}
    , {"Soldier's Dagger"}
    , {"Commander's Dagger"}
    , {"Deadly Knife"}
    , {"Deadly Knife+"}
    , {"Natch Knife"}
    , {"Acclimated Knife"}
    , {"Rebellious Knife"}
    , {"Sucher-Ankh"}
    , {"Jäger-Ankh"}
    , {"Sieger-Ankh"}
    , {"Le Détecteur"}
    , {"Princess Rapier"}
    , {"Queen Rapier"}
    , {"Queen's Rose"}
    , {"Royal Rose"}
    , {"Velocidrome Bite"}
    , {"Velocidrome Bite+"}
    , {"Velocidrome Fang"}
    , {"Velocidrome Fang+"}
    , {"Drome Rush Dagger"}
    , {"Hydra Bite"}
    , {"Spiked Circle"}
    , {"Paraspiked Circle"}
    , {"Usurper's Firebolt"}
    , {"Despot's Crookbolt"}
    , {"Brimstren Drakescale"}
    , {"Lagia Sword"}
    , {"High Lagia Sword"}
    , {"Hunter's Dagger"}
    , {"Assassin's Dagger"}
    , {"Viper Bite"}
    , {"Viper Bite+"}
    , {"Deathprize"}
    , {"Azi Dahaka"}
    , {"Almighty Dahaka"}
    , {"Poison Battleaxe"}
    , {"Poison Battleaxe+"}
    , {"Upper Battleaxe"}
    , {"Deadly Battleaxe"}
    , {"Frightbane"}
    , {"Thor Indora"}
    , {"Djinn"}
    , {"Djinn+"}
    , {"Blazing Falchion"}
    , {"Golden Falchion"}
    , {"Dios Edge"}
    , {"Dios Edge+"}
    , {"Demolition Cutter"}
    , {"Bone Kris"}
    , {"Bone Kris+"}
    , {"Chief Kris"}
    , {"Sharkutter"}
    , {"Sharkutter+"}
    , {"Sharfin Sword"}
    , {"Sharfinisher"}
    , {"Rex Talon"}
    , {"Tigrex Sword"}
    , {"Accursed Claw"}
    , {"Roaring Gravesword"}
    , {"Celestial Sword"}
    , {"Celestial Arms"}
    , {"Celestial Brand"}
    , {"Cross Sword"}
    , {"Extraordinary Cross"}
    , {"Dual Cross Brand"}
    , {"Flame Syphos"}
    , {"Flame Syphos+"}
    , {"Black Doom Flame"}
    , {"Scaly Sword"}
    , {"Scaly Sword+"}
    , {"Bloodscale Sword"}
    , {"Bloodsquama Sword"}
    , {"Ludroth's Nail"}
    , {"Royal Claw"}
    , {"Odyssey"}
    , {"Odyssey Blade"}
    , {"Tusk Gear"}
    , {"Skull's Wrath"}
    , {"Catspaw"}
    , {"Catspaw+"}
    , {"Catburglar"}
    , {"Plesioth Finspike"}
    , {"Plesioth Finspike+"}
    , {"Plesioth Direspike"}
    , {"Heroic Blade"}
    , {"Master's Blade"}
    , {"Rusted Sword"}
    , {"Tarnished Sword"}
    , {"Eternal Strife"}
    , {"Eternal Hate"}
    , {"Icesteel Dagger"}
    , {"Daora's Razor"}
    , {"Worn Sword"}
    , {"Weathered Sword"}
    , {"Divine Exodus"}
    , {"Kirin Bolt"}
    , {"Kirin Bolt Ultimus"}
    , {"Kirin Ice Ultimus"}
    , {"Poise"}
    , {"Ame-no-habakiri"}
    , {"Eviscerator & Scutum"}
    , {"The Antimonial Haven"}
    , {"Fatalis Sword"}
    , {"Ruiner Sword"}
    , {"dummy107"}
    , {"Hero's Sword"}
    , {"Master Sword"}
    , {"EX Poise"}
    , {"EX Ame-no-habakiri"}
    , {"Jhen Kodachi [RED]", eType::RELIC}
    , {"Jhen Kodachi [YELLOW]", eType::RELIC}
    , {"Jhen Kodachi [GREEN]", eType::RELIC}
    , {"Jhen Kodachi [BLUE]", eType::RELIC}
    , {"Jhen Kodachi [PURPLE]", eType::RELIC}
    , {"Chak Chak [RED]", eType::RELIC}
    , {"Chak Chak [YELLOW]", eType::RELIC}
    , {"Chak Chak [GREEN]", eType::RELIC}
    , {"Chak Chak [BLUE]", eType::RELIC}
    , {"Chak Chak [PURPLE]", eType::RELIC}
    , {"Hidden Edge [RED]", eType::RELIC}
    , {"Hidden Edge [YELLOW]", eType::RELIC}
    , {"Hidden Edge [GREEN]", eType::RELIC}
    , {"Hidden Edge [BLUE]", eType::RELIC}
    , {"Hidden Edge [PURPLE]", eType::RELIC}
    , {"Master Bang [RED]", eType::RELIC}
    , {"Master Bang [YELLOW]", eType::RELIC}
    , {"Master Bang [GREEN]", eType::RELIC}
    , {"Master Bang [BLUE]", eType::RELIC}
    , {"Master Bang [PURPLE]", eType::RELIC}
    , {"Golden Falchion [RED]", eType::RELIC}
    , {"Golden Falchion [YELLOW]", eType::RELIC}
    , {"Golden Falchion [GREEN]", eType::RELIC}
    , {"Golden Falchion [BLUE]", eType::RELIC}
    , {"Golden Falchion [PURPLE]", eType::RELIC}
    , {"Frost Edge [RED]", eType::RELIC}
    , {"Frost Edge [YELLOW]", eType::RELIC}
    , {"Frost Edge [GREEN]", eType::RELIC}
    , {"Frost Edge [BLUE]", eType::RELIC}
    , {"Frost Edge [PURPLE]", eType::RELIC}
    , {"Rebellious Knife+"}
    , {"Vengeful Knife"}
    , {"Spiked Saber"}
    , {"Clawed Saber"}
    , {"Serevento"}
    , {"La Vérité"}
    , {"Le Limier"}
    , {"Lügen/Vérité"}
    , {"Royal Rose+"}
    , {"Rose Highness"}
    , {"Lunatic Rose"}
    , {"Great Stabberdag"}
    , {"Deadly Poison"}
    , {"Paraspiked Rondo"}
    , {"Paralyzing Rondo"}
    , {"Lepis Prism"}
    , {"Grootslang"}
    , {"Brimstren Drakescale+"}
    , {"Stygian Luxuria"}
    , {"Neo Lagia Sword"}
    , {"Neodignis Whitebolt"}
    , {"Co Dolgan"}
    , {"Coctura Balgang"}
    , {"Gypcer Francisca"}
    , {"Gypcer Francisca+"}
    , {"Thor Rigindora"}
    , {"Golden Radius"}
    , {"Platinum Dawn"}
    , {"Annular Sol"}
    , {"Struxion Demolisher"}
    , {"Lightbreak Sword"}
    , {"Icy Akzar"}
    , {"Zamtrista Akzar"}
    , {"Agate Saw"}
    , {"Zamtrista Saw"}
    , {"Roaring Gravesword+"}
    , {"Reigning Grissword"}
    , {"Crimson Club"}
    , {"Crimson Club+"}
    , {"Monoblos Club"}
    , {"Studded Club"}
    , {"Blackened Soul"}
    , {"Blackest Animus"}
    , {"Rajang Club"}
    , {"Banned Rajang Club"}
    , {"Cursed Rajang Club"}
    , {"Bright Night"}
    , {"Sainted Dame"}
    , {"Gartered Rood"}
    , {"Red Divine Flame"}
    , {"Red Demonic Flame"}
    , {"Red Efreet's Flame"}
    , {"Bloodslaughter"}
    , {"Cold Blood"}
    , {"Mortal Heart"}
    , {"Odyssey Blade+"}
    , {"Master Odyssey"}
    , {"Chalcamine Sword"}
    , {"Chalcomatia"}
    , {"Melynx Tool"}
    , {"Plesiperse Spike"}
    , {"Plesiperse Spike+"}
    , {"Eternal Vengeance"}
    , {"Daora's Maelstrom"}
    , {"Eldaora's Sturm"}
    , {"Divine Insodus"}
    , {"Divine Egression"}
    , {"Teostra's Spada"}
    , {"Teostra's Emblem"}
    , {"Kirin Bolt Maximus"}
    , {"Icemaster Kirin"}
    , {"Ukanlos Soul Hatchet"}
    , {"Ukanlos Skyhatchet"}
    , {"Orochi"}
    , {"Yaegaki"}
    , {"Vaca the Mazelord"}
    , {"Dalamadur Hatchet"}
    , {"Bescaled Dalamadur"}
    , {"True Fatalis Sword"}
    , {"True Ruiner Sword"}
    , {"W. Fatalis Sword"}
    , {"Naab Sword"}
    , {"Naab Sword+"}
    , {"Seditious Fang"}
    , {"Panja Sedition"}
    , {"Valorous Soldier"}
    , {"Kyrie Ruin"}
    , {"Black Belt Sword"}
    , {"Master Sword G"}
    , {"dummy231"}
    , {"Star Knight Sword"}
    , {"dummy233"}
    , {"Ms. M.H. Puppet"}
    , {"Saint Luxseed"}
    , {"Demon Valkcreed"}
    , {"Poison Battleaxe [RED]", eType::RELIC}
    , {"Poison Battleaxe [YELLOW]", eType::RELIC}
    , {"Poison Battleaxe [GREEN]", eType::RELIC}
    , {"Poison Battleaxe [BLUE]", eType::RELIC}
    , {"Poison Battleaxe [PURPLE]", eType::RELIC}
    , {"Giltanas Epee [RED]", eType::RELIC}
    , {"Giltanas Epee [YELLOW]", eType::RELIC}
    , {"Giltanas Epee [GREEN]", eType::RELIC}
    , {"Giltanas Epee [BLUE]", eType::RELIC}
    , {"Giltanas Epee [PURPLE]", eType::RELIC}
}};

auto sSNS::getStr(const e id) noexcept -> const char *
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id].Name;

    return err.c_str();
}

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
