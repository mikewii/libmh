#include "libmh/MH4U/Equipment/Hunter/Weapon/IGKinsect.hpp"
#include "libmh/function_name.hpp"

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

constexpr std::array<sWeapon, sIGKinsect::ITEMS_MAX> sIGKinsect::str = {{
      {"Culldrone"}
    , {"Reavedrone"}
    , {"Alucanid"}
    , {"Monarch Alucanid"}
    , {"Empresswing"}
    , {"Rigiprayne"}
    , {"Cancadaman"}
    , {"Fiddlebrix"}
    , {"Windchopper"}
    , {"Grancathar"}
    , {"Pseudocath"}
    , {"Elscarad"}
    , {"Mauldrone"}
    , {"Pummeldrone"}
    , {"Foebeetle"}
    , {"Carnage Beetle"}
    , {"Bonnetfille"}
    , {"Ladytarge"}
    , {"Ladypavise"}
    , {"Arkmaiden"}
    , {"Gullshad"}
    , {"Bullshroud"}
    , {"Whispervesp"}
    , {"Arginesse"}
    , {"Thunderball"}
    , {"Bilbobrix"}
    , {"Foliacath"}
    , {"Exalted Alucanid"}
    , {"Great Elscarad"}
    , {"Ladytower"}
    , {"Fleetflammer"}
    , {"Gleambeetle"}
    , {"Great Arginesse"}
    , {"Clockmaster"}
    , {"Barrett Hawk"}
}};

auto sIGKinsect::getStr(const e id) noexcept -> const char *
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id].Name;

    return err.c_str();
}

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
