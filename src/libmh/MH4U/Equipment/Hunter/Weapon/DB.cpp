#include "libmh/MH4U/Equipment/Hunter/Weapon/DB.hpp"
#include "libmh/function_name.hpp"

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

constexpr std::array<sWeapon, sDB::ITEMS_MAX> sDB::str = {{
      {"(None)"}
    , {"Matched Slicers"}
    , {"Matched Slicers+"}
    , {"Dual Slicers"}
    , {"Dual Hatchets"}
    , {"Dual Hatchets+"}
    , {"Dual Cleavers+"}
    , {"Hurricane"}
    , {"Cyclone"}
    , {"Rex Slicers"}
    , {"Tigrex Claws"}
    , {"Accursed Slicers"}
    , {"Pouncing Graveclaws"}
    , {"Freezing Daggers"}
    , {"Freezing Daggers+"}
    , {"Icicle Daggers"}
    , {"Frozen Death"}
    , {"Insecticutters"}
    , {"Insecticutters+"}
    , {"Insectiscythes"}
    , {"Alated Insect"}
    , {"Sworn Rapiers"}
    , {"Holy Sabers"}
    , {"Guild Knight Sabers"}
    , {"Master Sabers"}
    , {"Jaggid Shotels"}
    , {"Leader's Shotels"}
    , {"Fledderklauen"}
    , {"Hungerklauen"}
    , {"Aasklauen"}
    , {"Les Apôtres"}
    , {"Cleaving Jaws"}
    , {"Cleaving Jaws+"}
    , {"Cool Cleavers"}
    , {"Frozen Cleavers"}
    , {"Bone Scythes"}
    , {"Bone Scythes+"}
    , {"Chief's Scythes"}
    , {"Dual Battleaxe"}
    , {"Dual Battleaxe+"}
    , {"Double Tabarzin"}
    , {"Terminal Toxin"}
    , {"Mortal Miasma"}
    , {"Dual Chameleos"}
    , {"Twin Flames"}
    , {"High Twin Flames"}
    , {"Wyvern Lovers"}
    , {"Wyvern Strife"}
    , {"Pink Maracas"}
    , {"Funky Maracas"}
    , {"Jungle Maracas"}
    , {"Evergreen"}
    , {"Whirlitwists"}
    , {"Whirlitricks"}
    , {"Whirlitorture"}
    , {"Spiral Edges"}
    , {"Gyro Wedges"}
    , {"Snow Slicers"}
    , {"Snow Sisters"}
    , {"Maulagombs"}
    , {"Brawlagombs"}
    , {"Twin Chainsaws"}
    , {"Usurper's Fulgur"}
    , {"Despot's Blitz"}
    , {"Levin Acrus"}
    , {"Brimstren Drakeclaws"}
    , {"Vprey Claws"}
    , {"High Vprey Claws"}
    , {"Raven Tessen"}
    , {"Wolf Tessen"}
    , {"Crow Tessen"}
    , {"Kut-Ku Pair"}
    , {"Kut-Ku Pair+"}
    , {"Melting Wail"}
    , {"Melting Blazer"}
    , {"Dios Slicers"}
    , {"Dios Slicers+"}
    , {"Demolition Blades"}
    , {"Dual Dragon"}
    , {"Dual Dragon Ultimus"}
    , {"Dual Dragon Ultimus+"}
    , {"Felyne and Melynx"}
    , {"Felyne and Melynx+"}
    , {"Plesioth Cutlasses"}
    , {"Plesioth Machetes"}
    , {"Verdant Slashers"}
    , {"Alluvion Slashers"}
    , {"Glutton's Tools"}
    , {"Gorger's Tools"}
    , {"Worn Blades"}
    , {"Weathered Blades"}
    , {"Enduring Schism"}
    , {"Twin Nails"}
    , {"Fire and Ice"}
    , {"Monarch"}
    , {"Kirin Bolts"}
    , {"Ritual Hailscreamers"}
    , {"Wrath & Rancor"}
    , {"Wrathful Predation"}
    , {"Consummate Pair"}
    , {"Akantor Blades"}
    , {"Blackspine Bundles"}
    , {"The Wrackful Gemini"}
    , {"Ebonblood Twindrakes"}
    , {"dummy104"}
    , {"dummy105"}
    , {"dummy106"}
    , {"Dual Kut-Ku"}
    , {"dummy108"}
    , {"dummy109"}
    , {"Twins Regnant [RED]", eType::RELIC}
    , {"Twins Regnant [YELLOW]", eType::RELIC}
    , {"Twins Regnant [GREEN]", eType::RELIC}
    , {"Twins Regnant [BLUE]", eType::RELIC}
    , {"Twins Regnant [PURPLE]", eType::RELIC}
    , {"Vprey Claws [RED]", eType::RELIC}
    , {"Vprey Claws [YELLOW]", eType::RELIC}
    , {"Vprey Claws [GREEN]", eType::RELIC}
    , {"Vprey Claws [BLUE]", eType::RELIC}
    , {"Vprey Claws [PURPLE]", eType::RELIC}
    , {"Tessen [RED]", eType::RELIC}
    , {"Tessen [YELLOW]", eType::RELIC}
    , {"Tessen [GREEN]", eType::RELIC}
    , {"Tessen [BLUE]", eType::RELIC}
    , {"Tessen [PURPLE]", eType::RELIC}
    , {"Ritual Ephemera [RED]", eType::RELIC}
    , {"Ritual Ephemera [YELLOW]", eType::RELIC}
    , {"Ritual Ephemera [GREEN]", eType::RELIC}
    , {"Ritual Ephemera [BLUE]", eType::RELIC}
    , {"Ritual Ephemera [PURPLE]", eType::RELIC}
    , {"Guild Knight Sabers [RED]", eType::RELIC}
    , {"Guild Knight Sabers [YELLOW]", eType::RELIC}
    , {"Guild Knight Sabers [GREEN]", eType::RELIC}
    , {"Guild Knight Sabers [BLUE]", eType::RELIC}
    , {"Guild Knight Sabers [PURPLE]", eType::RELIC}
    , {"DUMMY"}
    , {"Tornado Hatchets"}
    , {"Typhoon"}
    , {"Pouncing Graveclaws+"}
    , {"Razor Grisclaws"}
    , {"Gelid Mind"}
    , {"Gelid Soul"}
    , {"Alated Insect+"}
    , {"Entomotheos"}
    , {"Master Sabers+"}
    , {"Holy Guild Knight"}
    , {"Le Paradis"}
    , {"Le Shangri-La"}
    , {"Inferno/Paradis"}
    , {"Névé Bite"}
    , {"Blizztrios Fangs"}
    , {"Sandgnashers"}
    , {"Psammophages"}
    , {"Flamestorm"}
    , {"Salamanders"}
    , {"Ignis Noire"}
    , {"Agnaktor Inferno"}
    , {"Chameleos Blades"}
    , {"Wyvern Strife+"}
    , {"Wyvern Conciliation"}
    , {"Evergreen+"}
    , {"Paradise Green"}
    , {"Crossdrills"}
    , {"Crossveldts"}
    , {"Crossenders"}
    , {"Brawlagombs+"}
    , {"Appallagombs"}
    , {"Neo Twin Acrus"}
    , {"Neo Acrus Whitebolt"}
    , {"Brimstren Drakeclaws+"}
    , {"Stygian Superbia"}
    , {"Pox Tessen"}
    , {"Garuga Gunsen"}
    , {"Garuga Lovebirds"}
    , {"Megiddo Rippers"}
    , {"Megiddo Breakers"}
    , {"Spectral Demolisher"}
    , {"Lightbreak Twins"}
    , {"Dual Dragons \"Angst\""}
    , {"Diablos Bashers"}
    , {"Diablos Sinidex"}
    , {"Sinistrus Dextraos"}
    , {"Felyne and Melynx++"}
    , {"Jade Plesioth Fans"}
    , {"Jade Battlefanzers"}
    , {"Savortooths"}
    , {"Enduring Sacrifice"}
    , {"Enduring Surrender"}
    , {"Blizzard and Blaze"}
    , {"Twin Elders"}
    , {"Wunderkirins"}
    , {"Ritual Eidolons"}
    , {"Suzuka Takamaru"}
    , {"Suzuka Otakemaru"}
    , {"Doomfang \"Envoys\""}
    , {"Akantor Shadow Claws"}
    , {"Ukanlos Ruinblades"}
    , {"Ukanlos Skyrippers"}
    , {"Mbale the Acute"}
    , {"Dalamadur Spikes"}
    , {"Amber Dalamadur"}
    , {"Twinbane Blades"}
    , {"Twinbane Twilight"}
    , {"Ultimus Apocalypse"}
    , {"Zakun Twins"}
    , {"Zakun Twins+"}
    , {"Seditious Talons"}
    , {"Serre Sedition"}
    , {"Recriminators"}
    , {"Eternal Leavetakers"}
    , {"Shaka Scarecrows"}
    , {"Chum Scarecrows"}
    , {"Chum-Chum Scarecrows"}
    , {"dummy213"}
    , {"Fan Club Fans"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"Sea-king's Twins [RED]", eType::RELIC}
    , {"Sea-king's Twins [YELLOW]", eType::RELIC}
    , {"Sea-king's Twins [GREEN]", eType::RELIC}
    , {"Sea-king's Twins [BLUE]", eType::RELIC}
    , {"Sea-king's Twins [PURPLE]", eType::RELIC}
    , {"Blizzarioths [RED]", eType::RELIC}
    , {"Blizzarioths [YELLOW]", eType::RELIC}
    , {"Blizzarioths [GREEN]", eType::RELIC}
    , {"Blizzarioths [BLUE]", eType::RELIC}
    , {"Blizzarioths [PURPLE]", eType::RELIC}
    , {"Crooked Blades"}
    , {"Cross Inferno"}
}};

auto sDB::getStr(const e id) noexcept -> const char *
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id].Name;

    return err.c_str();
}

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
