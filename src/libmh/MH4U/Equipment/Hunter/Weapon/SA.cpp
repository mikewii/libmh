#include "libmh/MH4U/Equipment/Hunter/Weapon/SA.hpp"
#include "libmh/function_name.hpp"

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

constexpr std::array<sWeapon, sSA::ITEMS_MAX> sSA::str = {{
      {"(None)"}
    , {"Bone Axe"}
    , {"Bone Axe+"}
    , {"Bone Smasher"}
    , {"Power Gasher"}
    , {"Massive Gasher"}
    , {"Maximal Gasher"}
    , {"Elite Switch Axe"}
    , {"Elite Switch Axe+"}
    , {"Czar Switch Axe"}
    , {"Amber Slash"}
    , {"Galefrost"}
    , {"Rath Flame Splitter"}
    , {"Rath Blaze Splitter"}
    , {"Rathsplicer"}
    , {"Dragonmaiden Axe"}
    , {"Dragonmaiden Axe+"}
    , {"Gríðr's Landmaker"}
    , {"Azure Rose"}
    , {"Vavoom Axe"}
    , {"Vavoom Axe+"}
    , {"Vavoom Verdict"}
    , {"Vavoom Vengeance"}
    , {"Vavoom Vindication"}
    , {"Usurper's Downpour"}
    , {"Despot's Cloudburst"}
    , {"Brimstren Drakefang"}
    , {"Volt Axe"}
    , {"Notched Doom"}
    , {"Notched Doom+"}
    , {"Notched Tyrant"}
    , {"Notched Oppressor"}
    , {"Dios Axe"}
    , {"Dios Axe+"}
    , {"Demolition Axe"}
    , {"Barbarian Axe"}
    , {"Bold Barbarian Axe"}
    , {"Barbarian Outcast"}
    , {"Pecospander"}
    , {"Pecospander+"}
    , {"Qurupexspander"}
    , {"Wild Axe"}
    , {"Wild Axe+"}
    , {"Ground Dasher"}
    , {"Rex Slasher"}
    , {"Rex Gnasher"}
    , {"Accursed Switcher"}
    , {"Sheltered Graveaxe"}
    , {"Eisenleib"}
    , {"Eisengeist"}
    , {"Eisenherz"}
    , {"La Guillotine"}
    , {"Assault Axe"}
    , {"Blitzkrieg"}
    , {"Binding Roller"}
    , {"Vermilingua"}
    , {"Flickaxe"}
    , {"Flickaxe+"}
    , {"Flicker Axe"}
    , {"Paraflick Axe"}
    , {"Great Inceadeus"}
    , {"Rough Edge"}
    , {"Soulbreaker"}
    , {"Grim Cat"}
    , {"Grimmig Katze"}
    , {"Worn Switch Axe"}
    , {"Weathered Switch Axe"}
    , {"Mundus Altus"}
    , {"Rupture"}
    , {"Izanagi"}
    , {"Peal"}
    , {"Kirin Thunderpeal"}
    , {"Kirin Thunderbolt"}
    , {"Tyrannos"}
    , {"Terror Tyrannos"}
    , {"Saligia Vileaxe"}
    , {"Akantor Switch Axe"}
    , {"Keenscale Phalanx"}
    , {"The Exuberant Bite"}
    , {"Fatalis's End"}
    , {"Ruiner's Bane"}
    , {"dummy81"}
    , {"dummy82"}
    , {"dummy83"}
    , {"dummy84"}
    , {"EX Rupture"}
    , {"EX Izanagi"}
    , {"Mighty Switch Axe [RED]", eType::RELIC}
    , {"Mighty Switch Axe [YELLOW]", eType::RELIC}
    , {"Mighty Switch Axe [GREEN]", eType::RELIC}
    , {"Mighty Switch Axe [BLUE]", eType::RELIC}
    , {"Mighty Switch Axe [PURPLE]", eType::RELIC}
    , {"Rough Edge [RED]", eType::RELIC}
    , {"Rough Edge [YELLOW]", eType::RELIC}
    , {"Rough Edge [GREEN]", eType::RELIC}
    , {"Rough Edge [BLUE]", eType::RELIC}
    , {"Rough Edge [PURPLE]", eType::RELIC}
    , {"Switch Axe Regnant [RED]", eType::RELIC}
    , {"Switch Axe Regnant [YELLOW]", eType::RELIC}
    , {"Switch Axe Regnant [GREEN]", eType::RELIC}
    , {"Switch Axe Regnant [BLUE]", eType::RELIC}
    , {"Switch Axe Regnant [PURPLE]", eType::RELIC}
    , {"Hidden Axe [RED]", eType::RELIC}
    , {"Hidden Axe [YELLOW]", eType::RELIC}
    , {"Hidden Axe [GREEN]", eType::RELIC}
    , {"Hidden Axe [BLUE]", eType::RELIC}
    , {"Hidden Axe [PURPLE]", eType::RELIC}
    , {"Dragonmaiden Axe [RED]", eType::RELIC}
    , {"Dragonmaiden Axe [YELLOW]", eType::RELIC}
    , {"Dragonmaiden Axe [GREEN]", eType::RELIC}
    , {"Dragonmaiden Axe [BLUE]", eType::RELIC}
    , {"Dragonmaiden Axe [PURPLE]", eType::RELIC}
    , {"General's Strongaxe"}
    , {"Conqueress"}
    , {"Galefrost+"}
    , {"Giga Galefrost"}
    , {"Rathlagration"}
    , {"Rathscension"}
    , {"Azure Rosebloom"}
    , {"Azure Straybloom"}
    , {"Vavoom Nakha"}
    , {"Weird Wacha Nakha"}
    , {"Tanka Talon"}
    , {"Tatanka Talon"}
    , {"Brimstren Drakefang+"}
    , {"Stygian Vanagloria"}
    , {"Remiel Whitebolt"}
    , {"Nether Excellion"}
    , {"Notched Oppressor+"}
    , {"Notched Diavolo"}
    , {"Allagi Tigermaw"}
    , {"Zamtreacherous Maw"}
    , {"Brisant Demolisher"}
    , {"Lightbreak Axe"}
    , {"Bruiser Axe"}
    , {"Chief Bruiser Axe"}
    , {"Waxglare Wand"}
    , {"Qurupexspander+"}
    , {"Helstrong Firebath"}
    , {"Sheltered Graveaxe+"}
    , {"Crazed Grisaxe"}
    , {"Axe Semper Tyrannis"}
    , {"Axe of Gaia"}
    , {"Axe of Thanatos"}
    , {"Axe of Hades"}
    , {"Le Sursis"}
    , {"La Conclusion"}
    , {"Frevel/Sursis"}
    , {"Vermilion Bloodaxe"}
    , {"Paracrux Axe"}
    , {"Basilisk"}
    , {"Cryoseism"}
    , {"Illuyanka"}
    , {"Ceadeed Axe"}
    , {"Soulcrusher"}
    , {"Helios Crusher"}
    , {"Mundus Altus+"}
    , {"Mundus Regius"}
    , {"Teostra's Arx"}
    , {"Teostra's Castle"}
    , {"Daora's Farasa"}
    , {"Daora's Janah"}
    , {"Eldaora's Farasa"}
    , {"Futara"}
    , {"Yamatsumi"}
    , {"Kirin Thunderpeal+"}
    , {"Archbeast Kirin"}
    , {"Bitterbeast Kirin"}
    , {"Doomaxe \"Downpour\""}
    , {"Akantor Divider"}
    , {"Oda the Conqueror"}
    , {"Dalamadur Axe"}
    , {"Praying Dalamadur"}
    , {"True Fatalis's End"}
    , {"True Ruiner's Bane"}
    , {"Fatalis Force"}
    , {"Aksa Switch"}
    , {"Aksa Switch+"}
    , {"Seditious Axe"}
    , {"Baraq Sedition"}
    , {"Punishable Sin"}
    , {"Wicked Conqueror"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"Shah Bloodseeker"}
    , {"Olvid of the Abyss"}
    , {"Deluge [RED]", eType::RELIC}
    , {"Deluge [YELLOW]", eType::RELIC}
    , {"Deluge [GREEN]", eType::RELIC}
    , {"Deluge [BLUE]", eType::RELIC}
    , {"Deluge [PURPLE]", eType::RELIC}
    , {"Motor Burst [RED]", eType::RELIC}
    , {"Motor Burst [YELLOW]", eType::RELIC}
    , {"Motor Burst [GREEN]", eType::RELIC}
    , {"Motor Burst [BLUE]", eType::RELIC}
    , {"Motor Burst [PURPLE]", eType::RELIC}
}};

auto sSA::getStr(const e id) noexcept -> const char *
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id].Name;

    return err.c_str();
}

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
