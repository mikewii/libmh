#include "libmh/MH4U/Equipment/Hunter/Weapon/IG.hpp"
#include "libmh/function_name.hpp"

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

constexpr std::array<sWeapon, sIG::ITEMS_MAX> sIG::str = {{
      {"(None)"}
    , {"Bone Staff"}
    , {"Bone Staff+"}
    , {"Bone Glaive"}
    , {"Bone Glaive+"}
    , {"Aerial Glaive"}
    , {"Sturdy Glaive"}
    , {"Sturdy Glaive+"}
    , {"Sturdy Glaive Redux"}
    , {"Stealth Glaive"}
    , {"Stealth Glaive+"}
    , {"Stealth Glaive Redux"}
    , {"Shadow Walker"}
    , {"Assault Glaive"}
    , {"Assault Glaive+"}
    , {"Forewalker"}
    , {"Leumundslist"}
    , {"Leumundsgift"}
    , {"Leumundsbruch"}
    , {"Le Traqueur"}
    , {"Indigo Flash"}
    , {"Azure Bolt"}
    , {"Verdant Levin"}
    , {"Violindigo Hail"}
    , {"Violazure Sleet"}
    , {"Violverdant Glaze"}
    , {"Heaven's Rath Glaive"}
    , {"Rathful Blaze Glaive"}
    , {"Unprecedented Rath"}
    , {"Metal Mauler"}
    , {"Metal Mauler+"}
    , {"Metalmangler"}
    , {"Direspike Display"}
    , {"Direspike Display+"}
    , {"The Harmonious Stave"}
    , {"Fatalis Glaive"}
    , {"Fatalis Glaive+"}
    , {"Fatalis Fate"}
    , {"dummy38"}
    , {"Sky-high Glaive"}
    , {"Highest of Glaives"}
    , {"Deep Blue Glaive"}
    , {"Midnight Glaive"}
    , {"Mind Stick"}
    , {"Mind Stick \"Stone\""}
    , {"Mind Stick \"Rock\""}
    , {"Mind Stick \"Mount\""}
    , {"Mind Stick \"Wizard\""}
    , {"Mind Stick \"Wonder\""}
    , {"Mind Stick \"Demon\""}
    , {"Mind Stick \"Fiend\""}
    , {"Shell Intoner"}
    , {"Shell Intoner+"}
    , {"Crab Intoner"}
    , {"Purple Intoner"}
    , {"Hermit Scissors"}
    , {"Spirit Eater"}
    , {"Reaper's Stroke"}
    , {"Silent Eye"}
    , {"Silent Eye+"}
    , {"Nightmare"}
    , {"Needle Ivy"}
    , {"Needle Ivy+"}
    , {"Needle Vine"}
    , {"Paralyzing Vine"}
    , {"Medusa"}
    , {"Medusa+"}
    , {"Medusa Mirror"}
    , {"Névé Rod"}
    , {"Névé Rod+"}
    , {"Névé Frost"}
    , {"Fimbulvetr"}
    , {"Sand Fauchard"}
    , {"Sand Fauchard+"}
    , {"Iörovellir"}
    , {"Sect Happa"}
    , {"Sect Happa+"}
    , {"Trim Happa"}
    , {"Cannibal Happa"}
    , {"Diablos Rod"}
    , {"Diablos Rod+"}
    , {"Tyrant Rod"}
    , {"Thanatos Rod"}
    , {"Thanatos Rod+"}
    , {"Anubis"}
    , {"La Merveille"}
    , {"Le Miracle"}
    , {"List/Confiance"}
    , {"Bruch/Logique"}
    , {"Wehmut/Merveille"}
    , {"Boltbrute"}
    , {"King Boltbrute"}
    , {"Emperor Boltbrute"}
    , {"Icephantom"}
    , {"Propagation"}
    , {"Blossomajesty"}
    , {"Universus"}
    , {"Metalmangler+"}
    , {"Demonlord Cudgel"}
    , {"Demonlord Grudgel"}
    , {"Self-possession"}
    , {"Dalamadur Scepter"}
    , {"Bounding Dalamadur"}
    , {"Dios Rod"}
    , {"Demolition Press"}
    , {"Quantum Demolisher"}
    , {"Lightbreak Press"}
    , {"Ukanlos Staff"}
    , {"Ukanlos Staff+"}
    , {"Ukanlos Skytraveler"}
    , {"Daora's Entom"}
    , {"Daora's Tethidine"}
    , {"Daora's Samudra"}
    , {"Eldaora's Entom"}
    , {"Eldaora's Tethidine"}
    , {"Eldaora's Samudra"}
    , {"Caster's Rod"}
    , {"Caster's Rod+"}
    , {"Hazy Caster"}
    , {"True Fatalis Dyaus"}
    , {"Hellruin Glaive"}
    , {"Hellruin Glaive \"Gift\""}
    , {"Hellruin Glaive \"Omen\""}
    , {"Fatalis Soldier"}
    , {"Fatalis Enforcer"}
    , {"Fatalis Overlord"}
    , {"Altair"}
    , {"Altair+"}
    , {"Seditious Breaker"}
    , {"Lana Sedition"}
    , {"Messenger's Mark"}
    , {"Luminary's Mark"}
    , {"Limbo Lacrimosa"}
    , {"Clockwork"}
    , {"dummy134"}
    , {"dummy135"}
    , {"dummy136"}
    , {"dummy137"}
    , {"Bone Staff [RED]", eType::RELIC}
    , {"Bone Staff [YELLOW]", eType::RELIC}
    , {"Bone Staff [GREEN]", eType::RELIC}
    , {"Bone Staff [BLUE]", eType::RELIC}
    , {"Bone Staff [PURPLE]", eType::RELIC}
    , {"Metal Mauler [RED]", eType::RELIC}
    , {"Metal Mauler [YELLOW]", eType::RELIC}
    , {"Metal Mauler [GREEN]", eType::RELIC}
    , {"Metal Mauler [BLUE]", eType::RELIC}
    , {"Metal Mauler [PURPLE]", eType::RELIC}
    , {"Leumundslist [RED]", eType::RELIC}
    , {"Leumundslist [YELLOW]", eType::RELIC}
    , {"Leumundslist [GREEN]", eType::RELIC}
    , {"Leumundslist [BLUE]", eType::RELIC}
    , {"Leumundslist [PURPLE]", eType::RELIC}
    , {"Sect Happa [RED]", eType::RELIC}
    , {"Sect Happa [YELLOW]", eType::RELIC}
    , {"Sect Happa [GREEN]", eType::RELIC}
    , {"Sect Happa [BLUE]", eType::RELIC}
    , {"Sect Happa [PURPLE]", eType::RELIC}
    , {"Dancing Mad"}
    , {"Hades Shinerod"}
}};

auto sIG::getStr(const e id) noexcept -> const char *
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id].Name;

    return err.c_str();
}

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
