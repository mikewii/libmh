#include "libmh/MH4U/Equipment/Hunter/Weapon/Hammer.hpp"
#include "libmh/function_name.hpp"

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

constexpr std::array<sWeapon, sHammer::ITEMS_MAX> sHammer::str = {{
      {"(None)"}
    , {"War Hammer"}
    , {"War Hammer+"}
    , {"War Mace"}
    , {"Iron Striker"}
    , {"Iron Striker+"}
    , {"Iron Impact"}
    , {"Kurogane"}
    , {"Iron Devil"}
    , {"Fist of Fury"}
    , {"Frozen Core"}
    , {"Cocytus"}
    , {"Gun Hammer"}
    , {"Deadeye Revolver"}
    , {"Sanctioned Gunhammer"}
    , {"Burning Crusher"}
    , {"Purse Bop"}
    , {"Purse Bop+"}
    , {"Bleeding Bopper"}
    , {"Bleeding Basher"}
    , {"Cluster Hammer"}
    , {"Cluster Hammer+"}
    , {"Cluster Musterer"}
    , {"Master Musterer"}
    , {"Usurper's Thunder"}
    , {"Despot's Crackle"}
    , {"Brimstren Drakemaw"}
    , {"Mane Malice"}
    , {"Mane Malice Rajang"}
    , {"Accelerated Hammer"}
    , {"Boosted Hammer"}
    , {"Nitro Boost Hammer"}
    , {"Full Throttle Mace"}
    , {"Overclocker"}
    , {"Wuchtblick"}
    , {"Grimmblick"}
    , {"Ächtungsblick"}
    , {"Le Jugement"}
    , {"Bone Hammer"}
    , {"Bone Hammer+"}
    , {"Bone Bludgeon"}
    , {"Bone Bludgeon+"}
    , {"Heavy Bone Bludgeon"}
    , {"Striped Striker"}
    , {"Tigrex Hammer"}
    , {"Accursed Striker"}
    , {"Tussive Gravemace"}
    , {"Gigaton Hammer"}
    , {"Titan Hammer"}
    , {"Graviton Hammer"}
    , {"Venom Crater"}
    , {"Meaty Smash"}
    , {"Meaty Smash+"}
    , {"Meaty Press"}
    , {"Pressure Cooker"}
    , {"Venom Monster"}
    , {"Venom Monster+"}
    , {"Death Venom Hammer"}
    , {"Purple Mace"}
    , {"Pitohui Bird Mace"}
    , {"Red Bludgeon"}
    , {"Huracan Hammer"}
    , {"Leonid Starcrusher"}
    , {"Huracan Blueblaze"}
    , {"Dios Tailhammer"}
    , {"Dios Tailhammer+"}
    , {"Demolition Hammer"}
    , {"Kut-Ku Pick"}
    , {"Kut-Ku Pick+"}
    , {"Kut-Ku Thwap"}
    , {"Dynamic Hammer"}
    , {"Violence Hammer"}
    , {"Raven Torrent"}
    , {"Wolf Torrent"}
    , {"Crow Torrent"}
    , {"Basarios Blow"}
    , {"Basarios Bash"}
    , {"Oregalore"}
    , {"Gemgalore"}
    , {"Harmingbird"}
    , {"Toxikeet"}
    , {"Potio Bocciolo"}
    , {"Carapace Hammer"}
    , {"Carapace Hammer+"}
    , {"Barroth Hammer"}
    , {"Binding Bludgeon"}
    , {"Armored Gogue"}
    , {"Gaiasp"}
    , {"Great Gaiarch"}
    , {"White Kitty Stamp"}
    , {"White Kitty Hammer"}
    , {"Black Kitty Stamp"}
    , {"Black Kitty Hammer"}
    , {"Rusted Hammer"}
    , {"Tarnished Hammer"}
    , {"Breath Core Hammer"}
    , {"Lava Core Hammer"}
    , {"Teo Cratermaker"}
    , {"Teo Cratergouger"}
    , {"Worn Hammer"}
    , {"Weathered Hammer"}
    , {"Pulsating Core"}
    , {"Icesteel Hammer"}
    , {"Daora's Colossus"}
    , {"Eminence"}
    , {"Kushinada"}
    , {"Devil's Due"}
    , {"Devil's Crush"}
    , {"Consummate Hammer"}
    , {"Helixbone Slab"}
    , {"The Extricating Blow"}
    , {"Fatalis Buster"}
    , {"dummy112"}
    , {"dummy113"}
    , {"dummy114"}
    , {"dummy115"}
    , {"Hidden Breaker [RED]", eType::RELIC}
    , {"Hidden Breaker [YELLOW]", eType::RELIC}
    , {"Hidden Breaker [GREEN]", eType::RELIC}
    , {"Hidden Breaker [BLUE]", eType::RELIC}
    , {"Hidden Breaker [PURPLE]", eType::RELIC}
    , {"Leonid Starcrusher [RED]", eType::RELIC}
    , {"Leonid Starcrusher [YELLOW]", eType::RELIC}
    , {"Leonid Starcrusher [GREEN]", eType::RELIC}
    , {"Leonid Starcrusher [BLUE]", eType::RELIC}
    , {"Leonid Starcrusher [PURPLE]", eType::RELIC}
    , {"Lunastra's Crown [RED]", eType::RELIC}
    , {"Lunastra's Crown [YELLOW]", eType::RELIC}
    , {"Lunastra's Crown [GREEN]", eType::RELIC}
    , {"Lunastra's Crown [BLUE]", eType::RELIC}
    , {"Lunastra's Crown [PURPLE]", eType::RELIC}
    , {"Devil Masher [RED]", eType::RELIC}
    , {"Devil Masher [YELLOW]", eType::RELIC}
    , {"Devil Masher [GREEN]", eType::RELIC}
    , {"Devil Masher [BLUE]", eType::RELIC}
    , {"Devil Masher [PURPLE]", eType::RELIC}
    , {"War Basher [RED]", eType::RELIC}
    , {"War Basher [YELLOW]", eType::RELIC}
    , {"War Basher [GREEN]", eType::RELIC}
    , {"War Basher [BLUE]", eType::RELIC}
    , {"War Basher [PURPLE]", eType::RELIC}
    , {"Kitty Hammer [RED]", eType::RELIC}
    , {"Kitty Hammer [YELLOW]", eType::RELIC}
    , {"Kitty Hammer [GREEN]", eType::RELIC}
    , {"Kitty Hammer [BLUE]", eType::RELIC}
    , {"Kitty Hammer [PURPLE]", eType::RELIC}
    , {"Ancient Blow"}
    , {"Foolhardy Stone Fist"}
    , {"Cocytus+"}
    , {"Pykrete Punisher"}
    , {"Improved Gunhammer"}
    , {"Imperial Gunhammer"}
    , {"Bleeding Basher+"}
    , {"Bleeding Bludgeon"}
    , {"Master Musterer+"}
    , {"Scylla Maelstrom"}
    , {"Scylla Maeltasrophe"}
    , {"Oppressor's Sway"}
    , {"Orcus Adflictus"}
    , {"Brimstren Drakemaw+"}
    , {"Stygian Industria"}
    , {"Mane Master Rajang"}
    , {"Demonlord Hammer"}
    , {"Logicbreaker"}
    , {"Boulverizer"}
    , {"La Naissance"}
    , {"La Terre"}
    , {"Vergehen/Naissance"}
    , {"Ceanataur Head Axe"}
    , {"Carmine Axe"}
    , {"The Great Divider"}
    , {"Tussive Gravemace+"}
    , {"Wracking Grismace"}
    , {"Venom Crater+"}
    , {"Glasya-Labolas"}
    , {"Pressure Cooker+"}
    , {"Morphing Brinicle"}
    , {"Puffer Smash"}
    , {"Pufferpounder"}
    , {"Gypcer Rammer"}
    , {"Gypceros Ramplifi"}
    , {"Leonid Starcrusher+"}
    , {"Svarog Starsmasher"}
    , {"Rathalos Blueblazon"}
    , {"Novagio Demolisher"}
    , {"Lightbreak Hammer"}
    , {"Mysterious Jaw"}
    , {"Pox Torrent"}
    , {"Garuga Effusion"}
    , {"Gemgalore+"}
    , {"Meteorgalore"}
    , {"Bel Fiore"}
    , {"Fiore Fantastico"}
    , {"Barroth Basher"}
    , {"Barroth Grandbasher"}
    , {"Armored Gigagogue"}
    , {"Mother Gaiarch"}
    , {"Gaia Horizon"}
    , {"Gold Cat Hammer"}
    , {"Gold Cat Crusher"}
    , {"Crust Core Hammer"}
    , {"Teostra del Torre"}
    , {"Meteoric Core"}
    , {"Daora's Hyperborea"}
    , {"Eldaora's Colossus"}
    , {"Genie's Expanse"}
    , {"Fugaku"}
    , {"Nadeshiko"}
    , {"Doomhammer \"Blood\""}
    , {"Ukanlos Trampler"}
    , {"Ukanlos Skysmasher"}
    , {"Edward the Tremor"}
    , {"Dalamadur Hammer"}
    , {"Agonized Dalamadur"}
    , {"Fatalis Demolisher"}
    , {"Fatalis Iregard"}
    , {"Fatalis Ruiner"}
    , {"Matraca Hammer"}
    , {"Matraca Hammer+"}
    , {"Seditious Hammer"}
    , {"Qadar Sedition"}
    , {"Empty Cage"}
    , {"Desmos Admonition"}
    , {"Arluq Whammer"}
    , {"Swinging Scarluq"}
    , {"Pride of Harth"}
    , {"Black Belt Hammer"}
    , {"Portsmark"}
    , {"Teddybear"}
    , {"Enormous Ham"}
    , {"Ham of Hams"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"Apocalypse's Herald"}
    , {"H.O.S."}
    , {"Crystal Nova [RED]", eType::RELIC}
    , {"Crystal Nova [YELLOW]", eType::RELIC}
    , {"Crystal Nova [GREEN]", eType::RELIC}
    , {"Crystal Nova [BLUE]", eType::RELIC}
    , {"Crystal Nova [PURPLE]", eType::RELIC}
    , {"Dragonbreaker [RED]", eType::RELIC}
    , {"Dragonbreaker [YELLOW]", eType::RELIC}
    , {"Dragonbreaker [GREEN]", eType::RELIC}
    , {"Dragonbreaker [BLUE]", eType::RELIC}
    , {"Dragonbreaker [PURPLE]", eType::RELIC}
}};

auto sHammer::getStr(const e id) noexcept -> const char *
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id].Name;

    return err.c_str();
}

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
