#include "libmh/MH4U/Equipment/Hunter/Weapon/LS.hpp"
#include "libmh/function_name.hpp"

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

constexpr std::array<sWeapon, sLS::ITEMS_MAX> sLS::str = {{
      {"(None)"}
    , {"Iron Katana"}
    , {"Iron Grace"}
    , {"Iron Gospel"}
    , {"Eager Cleaver"}
    , {"Devil Slicer"}
    , {"Wailing Ghost"}
    , {"Grand Wailing Ghost"}
    , {"Stahlfakt"}
    , {"Stahlrecht"}
    , {"Stahlurteil"}
    , {"La Justicière"}
    , {"Ice Blade"}
    , {"Ice Blade+"}
    , {"Edel's Snowflake"}
    , {"Edel's Ice"}
    , {"Aikuchi"}
    , {"Unbranded Aikuchi"}
    , {"Lieutenant's Aikuchi"}
    , {"Toad Abetter"}
    , {"Accomplished Abetter"}
    , {"Guardian Sword"}
    , {"Imperial Sword"}
    , {"Wyvern Blade \"Fall\""}
    , {"Wyvern Blade \"Blood\""}
    , {"Wyvern Blade \"Maple\""}
    , {"Wyvern Blade \"Pale\""}
    , {"Wyvern Blade \"Blaze\""}
    , {"Dios Katana"}
    , {"Dios Katana+"}
    , {"Demolition Katana"}
    , {"Bone"}
    , {"Large Bone"}
    , {"Canine Katana"}
    , {"Lupine Katana"}
    , {"Cerberus Katana"}
    , {"Tigrine Edge"}
    , {"Tigrine Need"}
    , {"Tigrine Hunger"}
    , {"Tigrine Appetence"}
    , {"Usurper's Boltslicer"}
    , {"Usurper Boltslicer+"}
    , {"Despot's Boltbreaker"}
    , {"Brimstren Drakebone"}
    , {"Coiled Nail"}
    , {"Coiled Nail+"}
    , {"Coiled Saber"}
    , {"Paracoiled Saber"}
    , {"Curviscera"}
    , {"Incurviscera"}
    , {"Incurviscera+"}
    , {"Bicurviscera"}
    , {"Vilcurviscera"}
    , {"Daito Crow"}
    , {"Daito Wolf"}
    , {"Daito Bane"}
    , {"Demon Halberd"}
    , {"Great Demon Halberd"}
    , {"Blood Cry"}
    , {"Blood Cry+"}
    , {"Noble Blood Cry"}
    , {"Bioptimus"}
    , {"Windeater"}
    , {"Notos"}
    , {"Barbarian Blade"}
    , {"Barbarian \"Sharq\""}
    , {"Hairtail's Hairblade"}
    , {"Hairtail Hairblade+"}
    , {"Fresh Hairblade"}
    , {"Worn Long Sword"}
    , {"Weathered Long Sword"}
    , {"Oblivvion"}
    , {"Icesteel Blade"}
    , {"Daora's Raid"}
    , {"Arbiter"}
    , {"Susano-o"}
    , {"Reaver \"Cruelty\""}
    , {"Reaver \"Calamity\""}
    , {"Consummate Katana"}
    , {"Akantor Katana"}
    , {"Tailblade Parallel"}
    , {"The Righteous Swath"}
    , {"Fatalis Sickle"}
    , {"Starlight Gate"}
    , {"Eternal Gate"}
    , {"Wing of Judgment"}
    , {"dummy86"}
    , {"dummy87"}
    , {"dummy88"}
    , {"dummy89"}
    , {"dummy90"}
    , {"EX Arbiter"}
    , {"EX Susano-o"}
    , {"Arbiter [RED]", eType::RELIC}
    , {"Arbiter [YELLOW]", eType::RELIC}
    , {"Arbiter [GREEN]", eType::RELIC}
    , {"Arbiter [BLUE]", eType::RELIC}
    , {"Arbiter [PURPLE]", eType::RELIC}
    , {"Saber [RED]", eType::RELIC}
    , {"Saber [YELLOW]", eType::RELIC}
    , {"Saber [GREEN]", eType::RELIC}
    , {"Saber [BLUE]", eType::RELIC}
    , {"Saber [PURPLE]", eType::RELIC}
    , {"Rusty Claymore [RED]", eType::RELIC}
    , {"Rusty Claymore [YELLOW]", eType::RELIC}
    , {"Rusty Claymore [GREEN]", eType::RELIC}
    , {"Rusty Claymore [BLUE]", eType::RELIC}
    , {"Rusty Claymore [PURPLE]", eType::RELIC}
    , {"Rimeblade [RED]", eType::RELIC}
    , {"Rimeblade [YELLOW]", eType::RELIC}
    , {"Rimeblade [GREEN]", eType::RELIC}
    , {"Rimeblade [BLUE]", eType::RELIC}
    , {"Rimeblade [PURPLE]", eType::RELIC}
    , {"Wyvern Blade [RED]", eType::RELIC}
    , {"Wyvern Blade [YELLOW]", eType::RELIC}
    , {"Wyvern Blade [GREEN]", eType::RELIC}
    , {"Wyvern Blade [BLUE]", eType::RELIC}
    , {"Wyvern Blade [PURPLE]", eType::RELIC}
    , {"Wrathful Beheader"}
    , {"Rightful Beheader"}
    , {"La Loi"}
    , {"Le Décalogue"}
    , {"Chaos/Loi"}
    , {"Crab Cutter"}
    , {"Daimyo Cutter"}
    , {"Daimyo Cutter+"}
    , {"Hermitaur Slicer"}
    , {"Crimson Scythe"}
    , {"Crimson Scythe+"}
    , {"Monoblos Scythe"}
    , {"White Mantis"}
    , {"Weiss Crescent"}
    , {"Yuki Ichimonji"}
    , {"Silver Ichimonji"}
    , {"Hidden Saber"}
    , {"Hidden Saber+"}
    , {"Deepest Night"}
    , {"Megrez's Asterism"}
    , {"Rampage Edge"}
    , {"Rampage Ironsong"}
    , {"Aqua Guardian"}
    , {"Atlantica"}
    , {"Wyvern Blade \"Silver\""}
    , {"Wyvern Blade \"Bloom\""}
    , {"Wyvern Blade \"Rose\""}
    , {"Wyvern Blade \"Awe\""}
    , {"Pyro Demolisher"}
    , {"Lightbreak Edge"}
    , {"Tigrine Appetence+"}
    , {"Tigrine Esurience"}
    , {"Oppressor's Law"}
    , {"Orcus Nitorus"}
    , {"Stygian Gula"}
    , {"Charonian Gula"}
    , {"Paracoiled Edge"}
    , {"Parazagged Edge"}
    , {"Hyddra"}
    , {"Hyddra+"}
    , {"Alphard"}
    , {"Vilcurviscera+"}
    , {"Tamopaha"}
    , {"Tamo Saggrama"}
    , {"Daito Pox"}
    , {"Garuga Daito"}
    , {"Evil Demon Halberd"}
    , {"Demonlord Halberd"}
    , {"Bioptimus+"}
    , {"Gloom"}
    , {"Dark Matter"}
    , {"Notos+"}
    , {"Euros"}
    , {"Kibamaru"}
    , {"Premium Hairblade"}
    , {"Oblivvion+"}
    , {"Memento"}
    , {"Daora's Storm"}
    , {"Eldaora's Raid"}
    , {"Imperial Saber"}
    , {"Imperial Shimmer"}
    , {"Phantom Mirage"}
    , {"Nenekirimaru"}
    , {"Onamuchi"}
    , {"Doomblade \"Slave\""}
    , {"Akantor Strongsword"}
    , {"Ukanlos Slicer"}
    , {"Ukanlos Skydicer"}
    , {"Barwell the Swift"}
    , {"Dalamadur Edge"}
    , {"Clouded Dalamadur"}
    , {"Fatalis Zaggespanon"}
    , {"Fatalis Fotia"}
    , {"Fatalis Ascencia"}
    , {"Dragonwood Blade"}
    , {"Dragonwood Godblade"}
    , {"Dragonwood Cutblade"}
    , {"Khanga Edge"}
    , {"Khanga Edge+"}
    , {"Seditious Edge"}
    , {"Zealous Sedition"}
    , {"Grim Genuflector"}
    , {"Headsman's Requite"}
    , {"Thunderclap"}
    , {"Thunderclap+"}
    , {"Chainslaughter"}
    , {"Centenarian Dagger"}
    , {"Susano Blade"}
    , {"Fan Club Bamboo Rod"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"Crimson Dancer"}
    , {"Nirvana Blade"}
    , {"Netherblade [RED]", eType::RELIC}
    , {"Netherblade [YELLOW]", eType::RELIC}
    , {"Netherblade [GREEN]", eType::RELIC}
    , {"Netherblade [BLUE]", eType::RELIC}
    , {"Netherblade [PURPLE]", eType::RELIC}
    , {"Divine Slasher [RED]", eType::RELIC}
    , {"Divine Slasher [YELLOW]", eType::RELIC}
    , {"Divine Slasher [GREEN]", eType::RELIC}
    , {"Divine Slasher [BLUE]", eType::RELIC}
    , {"Divine Slasher [PURPLE]", eType::RELIC}
}};

auto sLS::getStr(const e id) noexcept -> const char *
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id].Name;

    return err.c_str();
}

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
