#include "libmh/MH4U/Equipment/Hunter/Weapon/Bow.hpp"
#include "libmh/function_name.hpp"

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

constexpr std::array<sWeapon, sBow::ITEMS_MAX> sBow::str = {{
      {"(None)"}
    , {"Hunter's Bow I"}
    , {"Hunter's Bow II"}
    , {"Hunter's Bow III"}
    , {"Hunter's Stoutbow I"}
    , {"Hunter's Stoutbow II"}
    , {"Hunter's Stoutbow III"}
    , {"Icicle Bow I"}
    , {"Icicle Bow II"}
    , {"Dios Flier"}
    , {"Dios Flier+"}
    , {"Demolition Bow"}
    , {"Queen Blaster I"}
    , {"Queen Blaster II"}
    , {"Heartshot Bow I"}
    , {"Heartshot Bow II"}
    , {"Selene Moonbroken"}
    , {"Prominence Bow I"}
    , {"Prominence Bow II"}
    , {"Prominence Bow III"}
    , {"Hyperion"}
    , {"Blue Alighter I"}
    , {"Blue Alighter II"}
    , {"Ice Cold Alighter"}
    , {"Freezing Alighter"}
    , {"Wild Bow"}
    , {"Wild Power Bow I"}
    , {"Wild Power Bow II"}
    , {"Jungle Bow I"}
    , {"Jungle Bow II"}
    , {"Tigrex Archer"}
    , {"Tigrex Whisker"}
    , {"Accursed Bow"}
    , {"Piercing Gravebow"}
    , {"Khezu Bow I"}
    , {"Khezu Bow II"}
    , {"Khezu Bow III"}
    , {"Bloodcurdler"}
    , {"Distant Thunder I"}
    , {"Distant Thunder II"}
    , {"Despot's Earlybolt"}
    , {"Brimstren Drakegaze"}
    , {"Brimstren Drakeglare"}
    , {"Entbehrung"}
    , {"Verzweiflung"}
    , {"Untergang"}
    , {"Le Ravisseur"}
    , {"Heat Haze"}
    , {"Hulking Haze"}
    , {"Feminine Haze"}
    , {"Formidable Haze"}
    , {"Absolute Deviance"}
    , {"Scylla Fist I"}
    , {"Scylla Fist II"}
    , {"Scylla Weave I"}
    , {"Scylla Weave II"}
    , {"Scylla Shredder"}
    , {"Kut-Ku Stave I"}
    , {"Kut-Ku Stave II"}
    , {"Kut-Ku Stave III"}
    , {"Blue Kut-Ku Stave I"}
    , {"Blue Kut-Ku Stave II"}
    , {"Crow Bow"}
    , {"Wolf Bow"}
    , {"Jinx Bow"}
    , {"Arca Raptora"}
    , {"Amnis"}
    , {"Morsel Bowfish"}
    , {"Ample Bowfish"}
    , {"Kelbi Stingshot"}
    , {"Kelbi Strongshot"}
    , {"Supplication"}
    , {"Tsukuyomi"}
    , {"Beast Thunderbow"}
    , {"Beastking Thunderbow"}
    , {"Prayerful Victoria"}
    , {"Icesteel Bow"}
    , {"Daora's Sagittarii"}
    , {"Bow of Hope & Valor"}
    , {"Akantor Bow"}
    , {"Cradlecrush Embrace"}
    , {"The Keen Stratagem"}
    , {"Bow of Rack & Ruin"}
    , {"dummy83"}
    , {"dummy83"}
    , {"Hero's Bow"}
    , {"Sacred Bow"}
    , {"Akantor Bow [RED]", eType::RELIC}
    , {"Akantor Bow [YELLOW]", eType::RELIC}
    , {"Akantor Bow [GREEN]", eType::RELIC}
    , {"Akantor Bow [BLUE]", eType::RELIC}
    , {"Akantor Bow [PURPLE]", eType::RELIC}
    , {"Prominence Bow [RED]", eType::RELIC}
    , {"Prominence Bow [YELLOW]", eType::RELIC}
    , {"Prominence Bow [GREEN]", eType::RELIC}
    , {"Prominence Bow [BLUE]", eType::RELIC}
    , {"Prominence Bow [PURPLE]", eType::RELIC}
    , {"Queen Blaster [RED]", eType::RELIC}
    , {"Queen Blaster [YELLOW]", eType::RELIC}
    , {"Queen Blaster [GREEN]", eType::RELIC}
    , {"Queen Blaster [BLUE]", eType::RELIC}
    , {"Queen Blaster [PURPLE]", eType::RELIC}
    , {"Icesteel Bow [RED]", eType::RELIC}
    , {"Icesteel Bow [YELLOW]", eType::RELIC}
    , {"Icesteel Bow [GREEN]", eType::RELIC}
    , {"Icesteel Bow [BLUE]", eType::RELIC}
    , {"Icesteel Bow [PURPLE]", eType::RELIC}
    , {"Bow of Rack & Ruin [RED]", eType::RELIC}
    , {"Bow of Rack & Ruin [YELLOW]", eType::RELIC}
    , {"Bow of Rack & Ruin [GREEN]", eType::RELIC}
    , {"Bow of Rack & Ruin [BLUE]", eType::RELIC}
    , {"Bow of Rack & Ruin [PURPLE]", eType::RELIC}
    , {"Icicle Bow III"}
    , {"Névé Bow"}
    , {"Warflare Demolisher"}
    , {"Lightbreak Bow"}
    , {"Selene Moonbroken+"}
    , {"Artemis Moonmaker"}
    , {"Hyperion+"}
    , {"Uranus Herald"}
    , {"Freezing Alighter+"}
    , {"Hailstorm"}
    , {"Quartzlight"}
    , {"Crystal Lode"}
    , {"Jungle Bow III"}
    , {"Jungle Bow IV"}
    , {"Piercing Gravebow+"}
    , {"Soaring Grisbow"}
    , {"Bloodcurdler+"}
    , {"Splattika"}
    , {"Oppressor's Wing"}
    , {"Orcus Sagittus"}
    , {"Stygian Patientia"}
    , {"Charonian Patientia"}
    , {"Le Désir"}
    , {"L'Innocence"}
    , {"Verzweiflung/Désir"}
    , {"Brazencord"}
    , {"Gigacles"}
    , {"Gigantomachy"}
    , {"Gold Popillia"}
    , {"Golden Ray"}
    , {"Scylla Webdart"}
    , {"Weblivion"}
    , {"Wide Weblivion"}
    , {"Blue Kut-Ku Stave III"}
    , {"Blue Kut-Ku Stave IV"}
    , {"Pox Bow"}
    , {"Garuga Oyumi"}
    , {"Daimyo's Warbow I"}
    , {"Daimyo's Warbow II"}
    , {"Daimyo's Warbow III"}
    , {"Great Purple Emperor I"}
    , {"Great Purple Emperor II"}
    , {"Diablos Coilbender"}
    , {"Cera Cyclord"}
    , {"Fluctus"}
    , {"Viand Bowfish"}
    , {"Kelbi Strongshot+"}
    , {"Great Kelbi Deershot"}
    , {"Amenokago"}
    , {"Tamanoya"}
    , {"Archbeast Thunderbow"}
    , {"Archbeast Paragon"}
    , {"Prayerful Victoria+"}
    , {"Heedful Elizabeth"}
    , {"Daora's Toxotes"}
    , {"Eldaora's Sagittarii"}
    , {"Courageous Wish"}
    , {"Genie's Grimoire"}
    , {"Akantor Chaos Bow"}
    , {"Ukanlos Bow"}
    , {"Ukanlos Skyflier"}
    , {"Yoichi the Sighted"}
    , {"Dalamadur Bow"}
    , {"Enraged Dalamadur"}
    , {"Exterminator Bow II"}
    , {"Red Heaven Ruiner"}
    , {"Victory and Glory"}
    , {"Shalya Bow"}
    , {"Shalya Bow+"}
    , {"Seditious Arrow"}
    , {"Kama Sedition"}
    , {"Unwelcoming Gaol"}
    , {"Calamitous Cupid"}
    , {"Sacred Bow G"}
    , {"Seahorse Harp"}
    , {"Bamboo Taketori"}
    , {"Bamboo Okina"}
    , {"Bamboo Kaguya"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"Eclipse Bow [RED]", eType::RELIC}
    , {"Eclipse Bow [YELLOW]", eType::RELIC}
    , {"Eclipse Bow [GREEN]", eType::RELIC}
    , {"Eclipse Bow [BLUE]", eType::RELIC}
    , {"Eclipse Bow [PURPLE]", eType::RELIC}
    , {"Dragon Bow [RED]", eType::RELIC}
    , {"Dragon Bow [YELLOW]", eType::RELIC}
    , {"Dragon Bow [GREEN]", eType::RELIC}
    , {"Dragon Bow [BLUE]", eType::RELIC}
    , {"Dragon Bow [PURPLE]", eType::RELIC}
    , {"Moonlit Phoenix"}
    , {"Daybreak Arro"}
}};

auto sBow::getStr(const e id) noexcept -> const char *
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id].Name;

    return err.c_str();
}

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
