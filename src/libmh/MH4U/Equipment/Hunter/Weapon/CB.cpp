#include "libmh/MH4U/Equipment/Hunter/Weapon/CB.hpp"
#include "libmh/function_name.hpp"

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

constexpr std::array<sWeapon, sCB::ITEMS_MAX> sCB::str = {{
      {"(None)"}
    , {"Elite Blade"}
    , {"Elite Blade+"}
    , {"Defender's Blade"}
    , {"Kaiser Blade"}
    , {"Blackguard"}
    , {"Full Blackguard"}
    , {"Sentinel Blade"}
    , {"Sentinel Blade+"}
    , {"Noblest of Lieges"}
    , {"Dear Lutemis"}
    , {"Dear Lutemia"}
    , {"Dear Hecatel"}
    , {"Dear Hecatelia"}
    , {"Axelion Blade"}
    , {"Halberion Blade"}
    , {"Bardichion Blade"}
    , {"Hellsire Blade"}
    , {"Hellspawn Blade"}
    , {"Guardsqual"}
    , {"Guardsqual+"}
    , {"Squaliarma"}
    , {"Squaliarmada"}
    , {"Elendskraft"}
    , {"Leidenskraft"}
    , {"Unheilskraft"}
    , {"L'Oppresseur"}
    , {"Deceadeus"}
    , {"Heroic Deceadeus"}
    , {"Permanence"}
    , {"Izanami"}
    , {"Fatalis Charger"}
    , {"Ruiner Reaver"}
    , {"Starlight Axe"}
    , {"Grandstar Axe"}
    , {"dummy35"}
    , {"dummy36"}
    , {"Full Blackguard+"}
    , {"Schwarzschild"}
    , {"K. Captain's Blade"}
    , {"Die Walküre"}
    , {"Arachnoscale"}
    , {"Arachnoscale+"}
    , {"Arachnoscythe"}
    , {"Belobog's Scythe"}
    , {"Schwarzschlum"}
    , {"Schwarzschlum+"}
    , {"Chernobog's Scythe"}
    , {"Schildsect"}
    , {"Schildsect+"}
    , {"Galvanisect"}
    , {"Dominisect"}
    , {"Maximinsect"}
    , {"Dear Rose"}
    , {"Dear Rosetta"}
    , {"Luna Eostre"}
    , {"Azure Swallow"}
    , {"Azure Skylark"}
    , {"Rathflame Ubernova"}
    , {"Blossomauler"}
    , {"Propensity"}
    , {"Serrated Glace"}
    , {"Trios Tristesse"}
    , {"The Haboob"}
    , {"Desert Rose"}
    , {"L'Essor"}
    , {"Le Salut"}
    , {"Elends/Essor"}
    , {"Diablos Strongarm"}
    , {"Cera Cediment"}
    , {"Ceadeus Regalia"}
    , {"Kyusen"}
    , {"Watatsumi"}
    , {"Daora's Casca"}
    , {"Daora's Thwartoise"}
    , {"Eldaora Casca"}
    , {"Teostra's Striker"}
    , {"Teostra's Nova"}
    , {"Dios Strongarm"}
    , {"Demolition C. Blade"}
    , {"Fulgent Demolisher"}
    , {"Lightbreak C. Blade"}
    , {"Demonlord C. Blade"}
    , {"Demonlord Supersurge"}
    , {"Akantor C. Blade"}
    , {"Akantor Severance"}
    , {"Bebladed Implement"}
    , {"Machina Razer"}
    , {"Adélie the Savior"}
    , {"Dalamadur C. Blade"}
    , {"Patient Dalamadur"}
    , {"True Fatalis Charger"}
    , {"True Ruiner Reaver"}
    , {"Fatalis Mien"}
    , {"Aslat Charger"}
    , {"Aslat Charger+"}
    , {"Seditious C. Blade"}
    , {"Garuda Sedition"}
    , {"Forbidden Coffin"}
    , {"Promised Nadir"}
    , {"dummy100"}
    , {"DUMMY"}
    , {"DUMMY"}
    , {"Awaritia"}
    , {"Fallen Morgenröte"}
    , {"Elite Blade [RED]", eType::RELIC}
    , {"Elite Blade [YELLOW]", eType::RELIC}
    , {"Elite Blade [GREEN]", eType::RELIC}
    , {"Elite Blade [BLUE]", eType::RELIC}
    , {"Elite Blade [PURPLE]", eType::RELIC}
    , {"Dear Lutemis [RED]", eType::RELIC}
    , {"Dear Lutemis [YELLOW]", eType::RELIC}
    , {"Dear Lutemis [GREEN]", eType::RELIC}
    , {"Dear Lutemis [BLUE]", eType::RELIC}
    , {"Dear Lutemis [PURPLE]", eType::RELIC}
    , {"Elendskraft [RED]", eType::RELIC}
    , {"Elendskraft [YELLOW]", eType::RELIC}
    , {"Elendskraft [GREEN]", eType::RELIC}
    , {"Elendskraft [BLUE]", eType::RELIC}
    , {"Elendskraft [PURPLE]", eType::RELIC}
    , {"Deceadeus [RED]", eType::RELIC}
    , {"Deceadeus [YELLOW]", eType::RELIC}
    , {"Deceadeus [GREEN]", eType::RELIC}
    , {"Deceadeus [BLUE]", eType::RELIC}
    , {"Deceadeus [PURPLE]", eType::RELIC}
}};

auto sCB::getStr(const e id) noexcept -> const char *
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id].Name;

    return err.c_str();
}

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
