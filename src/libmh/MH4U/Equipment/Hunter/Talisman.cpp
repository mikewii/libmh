#include "libmh/MH4U/Equipment/Hunter/Talisman.hpp"
#include "libmh/function_name.hpp"

namespace mh {
namespace mh4u {
namespace hunter {

constexpr std::array<const char*, sTalisman::ITEMS_MAX> sTalisman::str = {
    "(None)",
    "Pawn Talisman",
    "Bishop Talisman",
    "Knight Talisman",
    "Rook Talisman",
    "Queen Talisman",
    "King Talisman",
    "Dragon Talisman",
    "Unknowable Talisman",
    "Mystic Talisman",
    "Hero Talisman",
    "Legend Talisman",
    "Creator Talisman",
    "Sage Talisman",
    "Miracle Talisman",
    "Amber Tali.",
    "Jade Tali.",
    "Emery Tali."
};

auto sTalisman::getStr(const e id) noexcept -> const char *
{
    static auto err = __FUNCTION_SIGNATURE_OUT_OF_RANGE__;

    if (id < str.size())
        return str[id];

    return err.c_str();
}

}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
