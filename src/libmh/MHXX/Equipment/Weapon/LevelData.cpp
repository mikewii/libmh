#include "libmh/MHXX/Equipment/Hunter/Weapon/LevelData.hpp"
#include "libmh/tools/magic.hpp"
#include "libmh/tools/istream.hpp"
#include "libmh/tools/ostream.hpp"
#include <istream>
#include <ostream>

namespace mh {
namespace mhxx {
namespace weapon {
namespace leveldata {

template class cLevelData<Data_W00>;
template class cLevelData<Data_W01>;
template class cLevelData<Data_W02>;
template class cLevelData<Data_W03>;
template class cLevelData<Data_W04>;
template class cLevelData<Data_W06>;
template class cLevelData<Data_W07>;
template class cLevelData<Data_W08>;
template class cLevelData<Data_W09>;
template class cLevelData<Data_W10>;
template class cLevelData<Data_W11>;
template class cLevelData<Data_W12>;
template class cLevelData<Data_W13>;
template class cLevelData<Data_W14>;

template auto parse(std::istream& istream, cLevelData<Data_W00>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cLevelData<Data_W01>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cLevelData<Data_W02>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cLevelData<Data_W03>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cLevelData<Data_W04>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cLevelData<Data_W06>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cLevelData<Data_W07>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cLevelData<Data_W08>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cLevelData<Data_W09>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cLevelData<Data_W10>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cLevelData<Data_W11>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cLevelData<Data_W12>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cLevelData<Data_W13>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cLevelData<Data_W14>& obj) noexcept -> bool;

template <typename U>
auto parse(std::istream& istream, cLevelData<U>& obj) noexcept -> bool
{
    if (!obj.basicCheck(istream))
        return false;

    if (   !obj.readHeader(istream)
        || !obj.dataSizeCheck(istream)
        || !obj.readData(istream))
        return false;

    obj.m_valid = true;

    return true;
}

template <typename T>
auto cLevelData<T>::isValid(void) const noexcept -> bool { return m_valid; }

template <typename T>
auto cLevelData<T>::dump(std::ostream &ostream) -> bool
{
    std::ostream::pos_type totalSize =   sizeof(Header)
                                       + (m_header.DataNum * sizeof(T));

    ostream.seekp(std::ios::beg, std::ios::beg);

    tools::ostream::write(  ostream
                          , &m_header
                          , sizeof(m_header));

    for (const auto& data : m_data)
        tools::ostream::write(  ostream
                              , &data
                              , sizeof(data));

    return ostream.tellp() == totalSize;
}

template <typename T>
auto cLevelData<T>::getHeader(void) noexcept -> Header& { return m_header; }

template <typename T>
auto cLevelData<T>::getHeader(void) const noexcept -> const Header& { return m_header; }

template <typename T>
auto cLevelData<T>::getData(void) noexcept -> std::vector<T>& { return m_data; }

template <typename T>
auto cLevelData<T>::getData(void) const noexcept -> const std::vector<T>& { return m_data; }

template <typename T>
auto cLevelData<T>::getData(const std::size_t id, std::vector<T> &output) const -> bool
{
    for (const auto& data : m_data)
        if (data.Common.ID == id)
            output.push_back(data);

    return !output.empty();
}

template <typename T>
auto cLevelData<T>::setData(const std::vector<T> &input) -> bool
{
    if (input.empty())
        return false;

    std::size_t id = input[0].Common.ID;
    std::size_t count = 0;
    std::size_t i = 0;

    for (const auto& data : m_data)
        if (data.Common.ID == id)
            ++count;

    if (count != input.size())
        return false;

    for (auto& data : m_data)
        if (data.Common.ID == id)
            data = input[i++];

    return true;
}

template <typename T>
auto cLevelData<T>::basicCheck(std::istream &istream) noexcept -> bool
{
    tools::sMagic magic;

    if (tools::istream::size(istream) <= static_cast<std::streamsize>(sizeof(Header)))
        return false;

    magic.read(istream);

    if (!magic.check(Header::MAGIC))
        return false;

    return true;
}

template <typename T>
auto cLevelData<T>::readHeader(std::istream &istream) noexcept -> bool
{
    if (tools::istream::size(istream) <= static_cast<std::streamsize>(sizeof(Header)))
        return false;

    tools::istream::read(  istream
                         , &m_header
                         , sizeof(Header));

    return istream.gcount() == sizeof(Header);
}

template <typename T>
auto cLevelData<T>::dataSizeCheck(std::istream &istream) noexcept -> bool
{
    std::streamsize streamSize = tools::istream::size(istream);
    std::streamsize expectedSize = sizeof(Header) + (m_header.DataNum * sizeof(T));

    return streamSize == expectedSize;
}

template <typename T>
auto cLevelData<T>::readData(std::istream &istream) noexcept -> bool
{
    m_data.reserve(m_header.DataNum);

    for (auto i = 0u; i < m_header.DataNum; i++) {
        T data;

        tools::istream::read(  istream
                             , &data
                             , sizeof(data));

        m_data.push_back(std::move(data));
    }

    return m_data.size() == m_header.DataNum;
}

}; /// namespace leveldata
}; /// namespace weapon
}; /// namespace mhxx
}; /// namespace mh
