#include "libmh/MHXX/Equipment/Hunter/Weapon/BaseData.hpp"
#include "libmh/tools/magic.hpp"
#include "libmh/tools/istream.hpp"
#include "libmh/tools/ostream.hpp"
#include <istream>
#include <ostream>

namespace mh {
namespace mhxx {
namespace weapon {
namespace basedata {

template class cBaseData<Data_W00>;
template class cBaseData<Data_W01>;
template class cBaseData<Data_W02>;
template class cBaseData<Data_W03>;
template class cBaseData<Data_W04>;
template class cBaseData<Data_W06>;
template class cBaseData<Data_W07>;
template class cBaseData<Data_W08>;
template class cBaseData<Data_W09>;
template class cBaseData<Data_W10>;
template class cBaseData<Data_W11>;
template class cBaseData<Data_W12>;
template class cBaseData<Data_W13>;
template class cBaseData<Data_W14>;

template auto parse(std::istream& istream, cBaseData<Data_W00>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cBaseData<Data_W01>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cBaseData<Data_W02>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cBaseData<Data_W03>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cBaseData<Data_W04>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cBaseData<Data_W06>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cBaseData<Data_W07>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cBaseData<Data_W08>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cBaseData<Data_W09>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cBaseData<Data_W10>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cBaseData<Data_W11>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cBaseData<Data_W12>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cBaseData<Data_W13>& obj) noexcept -> bool;
template auto parse(std::istream& istream, cBaseData<Data_W14>& obj) noexcept -> bool;

template <typename U>
auto parse(std::istream& istream, cBaseData<U>& obj) noexcept -> bool
{
    if (!obj.basicCheck(istream))
        return false;

    if (   !obj.readHeader(istream)
        || !obj.dataSizeCheck(istream)
        || !obj.readData(istream))
        return false;

    return true;
}

template<typename T>
auto cBaseData<T>::isValid(void) const noexcept -> bool { return m_valid; }

template<typename T>
auto cBaseData<T>::dump(std::ostream &ostream) -> bool
{
    std::ostream::pos_type totalSize =   sizeof(Header)
                                       + (m_header.DataNum * sizeof(T));

    ostream.seekp(std::ios::beg, std::ios::beg);

    tools::ostream::write(  ostream
                          , &m_header
                          , sizeof(m_header));

    for (const auto& data : m_data)
        tools::ostream::write(  ostream
                              , &data
                              , sizeof(data));

    return ostream.tellp() == totalSize;
}

template<typename T>
auto cBaseData<T>::getHeader(void) noexcept -> Header& { return m_header; }

template<typename T>
auto cBaseData<T>::getHeader(void) const noexcept -> const Header& { return m_header; }

template<typename T>
auto cBaseData<T>::getData(void) noexcept -> std::vector<T>& { return m_data; }

template<typename T>
auto cBaseData<T>::getData(void) const noexcept -> const std::vector<T>& { return m_data; }

template<typename T>
auto cBaseData<T>::getData(const std::size_t id, T &output) const noexcept -> bool
{
    bool hit = false;

    for (const auto& data : m_data) {
        if (data.Common.Index == id) {
            hit = true;
            output = data;
        }
    }

    return hit;
}

template<typename T>
auto cBaseData<T>::setData(const T &input) noexcept -> bool
{
    bool hit = false;

    for (auto& data : m_data) {
        if (data.Common.Index == input.Common.Index) {
            hit = true;
            data = input;
        }
    }

    return hit;
}

template <typename T>
auto cBaseData<T>::basicCheck(std::istream &istream) noexcept -> bool
{
    tools::sMagic magic;

    if (tools::istream::size(istream) <= static_cast<std::streamsize>(sizeof(Header)))
        return false;

    magic.read(istream);

    if (!magic.check(Header::MAGIC))
        return false;

    return true;
}

template <typename T>
auto cBaseData<T>::readHeader(std::istream &istream) noexcept -> bool
{
    if (tools::istream::size(istream) <= static_cast<std::streamsize>(sizeof(Header)))
        return false;

    tools::istream::read(  istream
                         , &m_header
                         , sizeof(Header));

    return istream.gcount() == sizeof(Header);
}

template <typename T>
auto cBaseData<T>::dataSizeCheck(std::istream &istream) noexcept -> bool
{
    std::streamsize streamSize = tools::istream::size(istream);
    std::streamsize expectedSize = sizeof(Header) + (m_header.DataNum * sizeof(T));

    return streamSize == expectedSize;
}

template <typename T>
auto cBaseData<T>::readData(std::istream &istream) noexcept -> bool
{
    m_data.reserve(m_header.DataNum);

    for (auto i = 0u; i < m_header.DataNum; i++) {
        T data;

        istream.read(  reinterpret_cast<char*>(&data)
                     , sizeof(data));

        m_data.push_back(std::move(data));
    }

    return m_data.size() == m_header.DataNum;
}


}; /// namespace basedata
}; /// namespace weapon
}; /// namespace mhxx
}; /// namespace mh
