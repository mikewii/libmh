#include "libmh/MHXX/Extentions/gmd.hpp"
#include "libmh/tools/istream.hpp"
#include "libmh/tools/magicVersion.hpp"
#include <istream>

namespace mh {
namespace mhxx {
namespace gmd {

auto parse(std::istream& istream, cGMD& obj) noexcept -> bool
{
    if (!obj.basicCheck(istream))
        return false;

    obj.readHeader(istream);

    if (tools::istream::size(istream) < static_cast<std::streamsize>(obj.getExpectedSize(false)))
        return false;

    if (!obj.readFilename(istream))
        return false;

    auto advanced = obj.getHeader().LabelsNum > 0 ? true : false;

    if (advanced) {
        obj.readAdvanced1(istream);
        obj.readAdvanced2(istream);
    }

    obj.readLabels(istream);
    obj.readItems(istream);

    obj.m_valid = true;

    return true;
}

auto cGMD::isAdvanced(void) const noexcept -> bool
{
    return m_header.LabelsNum > 0 ? true : false;
}

auto cGMD::dump(std::ostream& ostream) -> bool
{
    auto totalSize = updateHeader();

    /// Write header
    ostream.write(  reinterpret_cast<const char*>(&m_header)
                  , sizeof(Header));

    /// Write filename
    ostream.write(  m_filename.c_str()
                  , m_filename.length());

    ostream.put('\0');

    /// Write advanced
    if (!m_advanced1.empty()) {
        for (const auto& item : m_advanced1)
            ostream.write(  reinterpret_cast<const char*>(&item)
                          , sizeof(sGMDAdvanced1));

        ostream.write(  reinterpret_cast<const char*>(m_advanced2.get())
                      , sizeof(sGMDAdvanced2));
    }

    /// Write labels
    auto writeStringVector = [&](const std::vector<std::string>& vec)
    {
        for (const auto& str : vec) {
            ostream.write(  str.c_str()
                          , str.length());

            ostream.put('\0');
        }
    };

    writeStringVector(m_labels);
    writeStringVector(m_items);

    return ostream.tellp() == totalSize;
}

auto cGMD::setHeader(const Header& header) noexcept -> void { m_header = header; }

auto cGMD::readHeader(std::istream& istream) noexcept -> bool
{
    istream.seekg(  std::ios::beg
                  , std::ios::beg);

    tools::istream::read(  istream
                         , &m_header
                         , sizeof(Header));

    return istream.gcount() == sizeof(Header);
}

auto cGMD::readFilename(std::istream& istream) noexcept -> bool
{
    std::string str;

    istream.seekg(  sizeof(Header)
                  , std::ios::beg);

    std::getline(  istream
                 , str
                 , '\0');

    if (str.length() == m_header.FilenameSize) {
        m_filename = std::string(str);

        return true;
    }

    return false;
}

auto cGMD::readAdvanced1(std::istream& istream) noexcept -> void
{
    if (m_header.LabelsNum == 0)
        return;

    auto offset =   sizeof(Header)
                  + m_header.FilenameSize
                  + NULL_TERMINATOR_SIZE;

    m_advanced1.reserve(m_header.LabelsNum);

    istream.seekg(  offset
                  , std::ios::beg);

    for (auto i = 0u; i < m_header.LabelsNum; i++) {
        sGMDAdvanced1 adv;

        istream.read(  reinterpret_cast<char*>(&adv)
                     , sizeof(sGMDAdvanced1));

        m_advanced1.push_back(std::move(adv));
    }
}

auto cGMD::readAdvanced2(std::istream& istream) noexcept -> void
{
    auto advanced1Size = sizeof(sGMDAdvanced1) * m_header.LabelsNum;
    auto offset =   sizeof(Header)
                  + m_header.FilenameSize
                  + NULL_TERMINATOR_SIZE
                  + advanced1Size;

    if (!m_advanced2)
        m_advanced2 = std::make_unique<sGMDAdvanced2>();

    istream.seekg(  offset
                  , std::ios::beg);

    istream.read(  reinterpret_cast<char*>(m_advanced2.get())
                 , sizeof(sGMDAdvanced2));
}

auto cGMD::readLabels(std::istream& istream) noexcept -> void
{
    m_labels.reserve(m_header.LabelsNum);

    for (auto i = 0u; i < m_header.LabelsNum; i++) {
        std::string str;

        std::getline(  istream
                     , str
                     , '\0' );

        m_labels.push_back(std::move(str));
    }
}

auto cGMD::readItems(std::istream& istream) noexcept -> void
{
    m_items.reserve(m_header.ItemsNum);

    for (auto i = 0u; i < m_header.ItemsNum; i++) {
        std::string str;

        std::getline(  istream
                     , str
                     , '\0' );

        m_items.push_back(std::move(str));
    }
}

auto cGMD::updateHeader(bool updateMagicAndVersion) noexcept -> u32
{
    std::size_t filenameSize = 0;
    std::size_t labelsSize = 0;
    std::size_t itemsSize = 0;
    std::size_t totalSize = 0;

    filenameSize = m_filename.length();

    for (const auto& label : m_labels)
        labelsSize += label.length() + NULL_TERMINATOR_SIZE;

    for (const auto& item : m_items)
        itemsSize += item.length() + NULL_TERMINATOR_SIZE;

    totalSize = sizeof(Header)
                + filenameSize
                + NULL_TERMINATOR_SIZE
                + labelsSize
                + itemsSize;

    if (!m_advanced1.empty()) {
        auto advanced1Size = m_advanced1.size() * sizeof(sGMDAdvanced1);
        auto advanced2Size = sizeof(sGMDAdvanced2);

        totalSize += advanced1Size + advanced2Size;
    }

    m_header.FilenameSize = std::move(static_cast<u32>(filenameSize));
    m_header.LabelsSize = std::move(static_cast<u32>(labelsSize));
    m_header.ItemsSize = std::move(static_cast<u32>(itemsSize));
    m_header.LabelsNum = static_cast<u32>(m_labels.size());
    m_header.ItemsNum = static_cast<u32>(m_items.size());

    if (updateMagicAndVersion) {
        m_header.Magic = Header::MAGIC;
        m_header.Version = Header::VERSION;
    }

    return static_cast<u32>(totalSize);
}

auto cGMD::getHeader(void) noexcept -> Header& { return m_header; }
auto cGMD::getHeader(void) const noexcept -> const Header& { return m_header; }

auto cGMD::getAdvanced1(void) noexcept -> std::vector<sGMDAdvanced1>& { return m_advanced1; }
auto cGMD::getAdvanced1(void) const noexcept -> const std::vector<sGMDAdvanced1>& { return m_advanced1; }

auto cGMD::getAdvanced2(void) noexcept -> sGMDAdvanced2& { return *m_advanced2; }
auto cGMD::getAdvanced2(void) const noexcept -> const sGMDAdvanced2& { return *m_advanced2; }

auto cGMD::getExpectedSize(const bool precise) const noexcept -> std::size_t
{
    const Header& header = m_header;
    auto advanced = header.LabelsNum > 0 ? true : false;
    auto expectedSize = sizeof(Header);

    expectedSize += header.FilenameSize + NULL_TERMINATOR_SIZE;

    // Old translations never update those numbers
    // to read translated .gmd dont do precise check
    if (precise)
        expectedSize += header.ItemsSize + header.LabelsSize;

    if (advanced)
        expectedSize += header.LabelsNum * sizeof(sGMDAdvanced1) + sizeof(sGMDAdvanced2);

    return expectedSize;
}

auto cGMD::basicCheck(std::istream& istream) noexcept -> bool
{
    tools::MagicVersion magicVersion;

    if (tools::istream::size(istream) <= static_cast<std::streamsize>(sizeof(Header)))
        return false;

    magicVersion.read(istream);

    if (!magicVersion.check(Header::MAGIC, Header::VERSION))
        return false;

    return true;
}

}; /// namespase gmd
}; /// namespase mhxx
}; /// namespace mh
