#include "libmh/MHXX/Extentions/sem.hpp"
#include "libmh/tools/magicVersion.hpp"
#include "libmh/tools/istream.hpp"
#include <istream>

namespace mh {
namespace mhxx {
namespace sem {

auto parse(std::istream& istream, cSEM& obj) noexcept -> bool
{
    if (!obj.basicCheck(istream))
        return false;

    if (!obj.readHeader(istream))
        return false;

    obj.m_valid = true;

    return true;
}

auto cSEM::isValid(void) const noexcept -> bool { return m_valid; }

auto cSEM::getWaveNo(void) const noexcept -> u32 { return m_header.WaveNo; }
auto cSEM::getAreaNo(void) const noexcept -> u32 { return m_header.AreaNo; }
auto cSEM::getPosition(void) const noexcept -> Geometry4<float> { return m_header.Position; }

auto cSEM::setWaveNo(const u32 num) noexcept -> void { m_header.WaveNo = num; }
auto cSEM::setAreaNo(const u32 num) noexcept -> void { m_header.AreaNo = num; }
auto cSEM::setPosition(const Geometry4<float> &pos) noexcept -> void { m_header.Position = pos; }

auto cSEM::getSetEmMain(void) noexcept -> Header& { return m_header; }
auto cSEM::getSetEmMain(void) const noexcept -> const Header& { return m_header; }

auto cSEM::basicCheck(std::istream& istream) noexcept -> bool
{
    tools::MagicVersion magicVersion;

    if (tools::istream::size(istream) != static_cast<std::streamsize>(sizeof(Header)))
        return false;

    magicVersion.read(istream);

    if (!magicVersion.check(Header::MAGIC, Header::VERSION))
        return false;

    return true;
}

auto cSEM::readHeader(std::istream& istream) noexcept -> bool
{
    tools::istream::read(  istream
                         , &m_header
                         , sizeof(Header));

    return istream.gcount() == sizeof(Header);
}

}; /// namespase sem
}; /// namespase mhxx
}; /// namespace mh
