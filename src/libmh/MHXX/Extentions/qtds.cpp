#include <libmh/MHXX/Extentions/qtds.hpp>
#include "libmh/tools/magicVersion.hpp"
#include "libmh/tools/istream.hpp"
#include "libmh/function_name.hpp"
#include <istream>
#include <ostream>
#include <stdexcept>

namespace mh {
namespace mhxx {
namespace qtds {

auto parse(std::istream& istream, cQTDS& obj) noexcept -> bool
{
    if (!obj.basicCheck(istream))
        return false;

    if (   !obj.readHeader(istream)
        || !obj.readLinks(istream)
        || !obj.readHeader1(istream))
        return false;

    obj.m_valid = true;

    return true;
}

auto cQTDS::isValid(void) const noexcept -> bool { return m_valid; }

auto cQTDS::dump(std::ostream& ostream) noexcept -> bool
{
    fixGMDLinkAmmount();

    std::streamsize totalSize =   sizeof(Header)
                                + m_gmdLinks.size() * sizeof(sGMDLink)
                                + sizeof(Header1);

    /// Write header0
    ostream.write(  reinterpret_cast<char*>(&m_header0)
                  , sizeof(Header));

    /// Write gmd links
    for (auto i = 0u; i < m_gmdLinks.size(); i++)
        ostream.write(  reinterpret_cast<char*>(&m_gmdLinks[i])
                      , sizeof(sGMDLink));

    /// Write header1
    ostream.write(  reinterpret_cast<char*>(&m_header1)
                  , sizeof(Header1));

    return ostream.tellp() == totalSize;
}

auto cQTDS::getIndex(void) const noexcept -> u32 { return m_header0.Index; }
auto cQTDS::setIndex(const u32 val) noexcept -> void { m_header0.Index = val; }

auto cQTDS::getQuestID(void) const noexcept -> u32 { return m_header0.QuestID; }
auto cQTDS::setQuestID(const u32 val) noexcept -> void { m_header0.QuestID = val; }

auto cQTDS::getQuestType0(void) const noexcept -> sQuestType0::e { return m_header0.QuestType0; }
auto cQTDS::setQuestType0(const sQuestType0::e val) noexcept -> void { m_header0.QuestType0 = val; }

auto cQTDS::getQuestType1(void) const noexcept -> sQuestType1::e { return m_header0.QuestType1; }
auto cQTDS::setQuestType1(const sQuestType1::e val) noexcept -> void { m_header0.QuestType1 = val; }

auto cQTDS::getQuestLv(void) const noexcept -> sQuestLv::e { return m_header0.QuestLv; }
auto cQTDS::setQuestLv(const sQuestLv::e val) -> void { m_header0.QuestLv = val; }

auto cQTDS::getBossLv(void) const noexcept -> sEnemyLv::e { return m_header0.BossLv; }
auto cQTDS::setBossLv(const sEnemyLv::e val) -> void { m_header0.BossLv = val; }

auto cQTDS::getMapNo(void) const noexcept -> sMaps::e { return m_header0.MapNo; }
auto cQTDS::setMapNo(const sMaps::e val) noexcept -> void { m_header0.MapNo = val; }

auto cQTDS::getStartType(void) const noexcept -> sStartType::e { return m_header0.StartType; }
auto cQTDS::setStartType(const sStartType::e val) noexcept -> void { m_header0.StartType = val; }

auto cQTDS::getQuestTime(void) const noexcept -> u8 { return m_header0.QuestTime; }
auto cQTDS::setQuestTime(const u8 val) noexcept -> void { m_header0.QuestTime = val; }

auto cQTDS::getQuestLives(void) const noexcept -> u8 { return m_header0.QuestLives; }
auto cQTDS::setQuestLives(const u8 val) noexcept -> void { m_header0.QuestLives = val; }

auto cQTDS::getAcEquipSetNo(void) const noexcept -> u8 { return m_header0.AcEquipSetNo; }
auto cQTDS::setAcEquipSetNo(const u8 val) noexcept -> void { m_header0.AcEquipSetNo = val; }

auto cQTDS::getBGMType(void) const noexcept -> sBGMType::e { return m_header0.BGMType; }
auto cQTDS::setBGMType(const sBGMType::e val) noexcept -> void { m_header0.BGMType = val; }

auto cQTDS::getEntryType(const std::size_t id) const noexcept(false) -> sEntryType::e
{
    if (id < 2)
        return m_header0.EntryType[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cQTDS::getEntryType0(void) const noexcept -> sEntryType::e { return getEntryType(0); }
auto cQTDS::getEntryType1(void) const noexcept -> sEntryType::e { return getEntryType(1); }

auto cQTDS::setEntryType(const std::size_t id, const sEntryType::e val) noexcept -> bool
{
    if (id < 2) {
        m_header0.EntryType[id] = val;

        return true;
    }

    return false;
}

auto cQTDS::setEntryType0(const sEntryType::e val) noexcept -> void { (void)setEntryType(0, val); }
auto cQTDS::setEntryType1(const sEntryType::e val) noexcept -> void { (void)setEntryType(1, val); }

auto cQTDS::getEntryTypeCombo(void) const noexcept -> u8 { return m_header0.EntryTypeCombo; }
auto cQTDS::setEntryTypeCombo(const u8 val) noexcept -> void { m_header0.EntryTypeCombo = val; }

auto cQTDS::getClearType(void) const noexcept -> sClearType::e { return m_header0.ClearType; }
auto cQTDS::setClearType(const sClearType::e val) noexcept -> void { m_header0.ClearType = val; }

auto cQTDS::getGekitaiHP(void) const noexcept -> u8 { return m_header0.GekitaiHP; }
auto cQTDS::setGekitaiHP(const u8 val) noexcept -> void { m_header0.GekitaiHP = val; }

auto cQTDS::getTargetMain(const std::size_t id) const noexcept(false) -> sTarget
{
    if (id < 2)
        return m_header0.TargetMain[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cQTDS::getTargetMain0(void) const noexcept -> sTarget { return getTargetMain(0); }
auto cQTDS::getTargetMain1(void) const noexcept -> sTarget { return getTargetMain(1); }

auto cQTDS::setTargetMain(const std::size_t id, const sTarget val) noexcept -> bool
{
    if (id < 2) {
        m_header0.TargetMain[id] = val;

        return true;
    }

    return false;
}

auto cQTDS::setTargetMain0(const sTarget val) noexcept -> void { (void)setTargetMain(0, val); }
auto cQTDS::setTargetMain1(const sTarget val) noexcept -> void { (void)setTargetMain(1, val); }

auto cQTDS::getTargetSub(void) const noexcept -> sTarget { return m_header0.TargetSub; }
auto cQTDS::setTargetSub(const sTarget val) noexcept -> void { m_header0.TargetSub = val; }

auto cQTDS::getCarvingLv(void) const noexcept -> sCarvingLv::e { return m_header0.CarvingLv; }
auto cQTDS::setCarvingLv(const sCarvingLv::e val) noexcept -> void { m_header0.CarvingLv = val; }

auto cQTDS::getGatheringLv(void) const noexcept -> sGatheringLv::e { return m_header0.GatheringLv; }
auto cQTDS::setGatheringLv(const sGatheringLv::e val) noexcept -> void { m_header0.GatheringLv = val; }

auto cQTDS::getFishingLv(void) const noexcept -> sFishingLv::e { return m_header0.FishingLv; }
auto cQTDS::setFishingLv(const sFishingLv::e val) noexcept -> void { m_header0.FishingLv = val; }

auto cQTDS::getEntryFee(void) const noexcept -> u32 { return m_header0.EntryFee; }
auto cQTDS::setEntryFee(const u32 val) noexcept -> void { m_header0.EntryFee = val; }

auto cQTDS::getVillagePoints(void) const noexcept -> u32 { return m_header0.VillagePoints; }
auto cQTDS::setVillagePoints(const u32 val) noexcept -> void { m_header0.VillagePoints = val; }

auto cQTDS::getMainRewardMoney(void) const noexcept -> u32 { return m_header0.MainRewardMoney; }
auto cQTDS::setMainRewardMoney(const u32 val) noexcept -> void { m_header0.MainRewardMoney = val; }

auto cQTDS::getSubRewardMoney(void) const noexcept -> u32 { return m_header0.SubRewardMoney; }
auto cQTDS::setSubRewardMoney(const u32 val) noexcept -> void { m_header0.SubRewardMoney = val; }

auto cQTDS::getClearRemVillagePoint(void) const noexcept -> u32 { return m_header0.ClearRemVillagePoint; }
auto cQTDS::setClearRemVillagePoint(const u32 val) noexcept -> void { m_header0.ClearRemVillagePoint = val; }

auto cQTDS::getFailedRemVillagePoint(void) const noexcept -> u32 { return m_header0.FailedRemVillagePoint; }
auto cQTDS::setFailedRemVillagePoint(const u32 val) noexcept -> void { m_header0.FailedRemVillagePoint = val; }

auto cQTDS::getSubRemVillagePoint(void) const noexcept -> u32 { return m_header0.SubRemVillagePoint; }
auto cQTDS::setSubRemVillagePoint(const u32 val) noexcept -> void { m_header0.SubRemVillagePoint = val; }

auto cQTDS::getClearRemHunterPoint(void) const noexcept -> u32 { return m_header0.ClearRemHunterPoint; }
auto cQTDS::setClearRemHunterPoint(const u32 val) noexcept -> void { m_header0.ClearRemHunterPoint = val; }

auto cQTDS::getSubRemHunterPoint(void) const noexcept -> u32 { return m_header0.SubRemHunterPoint; }
auto cQTDS::setSubRemHunterPoint(const u32 val) noexcept -> void { m_header0.SubRemHunterPoint = val; }

auto cQTDS::getRemAddFrame(const std::size_t id) const noexcept(false) -> u8
{
    if (id < 2)
        return m_header0.RemAddFrame[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cQTDS::getRemAddFrame0(void) const noexcept -> u8 { return getRemAddFrame(0); }
auto cQTDS::getRemAddFrame1(void) const noexcept -> u8 { return getRemAddFrame(1); }

auto cQTDS::setRemAddFrame(const std::size_t id, const u8 val) noexcept -> bool
{
    if (id < 2) {
        m_header0.RemAddFrame[id] = val;

        return true;
    }

    return false;
}

auto cQTDS::setRemAddFrame0(const u8 val) noexcept -> void { (void)setRemAddFrame(0, val); }
auto cQTDS::setRemAddFrame1(const u8 val) noexcept -> void { (void)setRemAddFrame(1, val); }

auto cQTDS::getSupply(const std::size_t id) const noexcept(false) -> sSupply
{
    if (id < 2)
        return m_header0.Supply[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cQTDS::getSupply0(void) const noexcept -> sSupply { return getSupply(0); }
auto cQTDS::getSupply1(void) const noexcept -> sSupply { return getSupply(1); }

auto cQTDS::setSupply(const std::size_t id, const sSupply val) noexcept -> bool
{
    if (id < 2) {
        m_header0.Supply[id] = val;

        return true;
    }

    return false;
}

auto cQTDS::setSupply0(const sSupply val) noexcept -> void { (void)setSupply(0, val); }
auto cQTDS::setSupply1(const sSupply val) noexcept -> void { (void)setSupply(1, val); }

auto cQTDS::getBoss(const std::size_t id) const noexcept(false) -> sBoss
{
    if (id < 5)
        return m_header0.Boss[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cQTDS::getBoss0(void) const noexcept -> sBoss { return getBoss(0); }
auto cQTDS::getBoss1(void) const noexcept -> sBoss { return getBoss(1); }
auto cQTDS::getBoss2(void) const noexcept -> sBoss { return getBoss(2); }
auto cQTDS::getBoss3(void) const noexcept -> sBoss { return getBoss(3); }
auto cQTDS::getBoss4(void) const noexcept -> sBoss { return getBoss(4); }

auto cQTDS::setBoss(const std::size_t id, const sBoss val) noexcept -> bool
{
    if (id < 5) {
        m_header0.Boss[id] = val;

        return true;
    }

    return false;
}

auto cQTDS::setBoss0(const sBoss val) noexcept -> void { (void)setBoss(0, val); }
auto cQTDS::setBoss1(const sBoss val) noexcept -> void { (void)setBoss(1, val); }
auto cQTDS::setBoss2(const sBoss val) noexcept -> void { (void)setBoss(2, val); }
auto cQTDS::setBoss3(const sBoss val) noexcept -> void { (void)setBoss(3, val); }
auto cQTDS::setBoss4(const sBoss val) noexcept -> void { (void)setBoss(4, val); }

auto cQTDS::getSmallEmHPTbl(void) const noexcept -> u8 { return m_header0.SmallEmHPTbl; }
auto cQTDS::setSmallEmHPTbl(const u8 val) noexcept -> void { m_header0.SmallEmHPTbl = val; }

auto cQTDS::getSmallEmAttackTbl(void) const noexcept -> u8 { return m_header0.SmallEmAttackTbl; }
auto cQTDS::setSmallEmAttackTbl(const u8 val) noexcept -> void { m_header0.SmallEmAttackTbl = val; }

auto cQTDS::getSmallEmOtherTbl(void) const noexcept -> u8 { return m_header0.SmallEmOtherTbl; }
auto cQTDS::setSmallEmOtherTbl(const u8 val) noexcept -> void { m_header0.SmallEmOtherTbl = val; }

auto cQTDS::getAppear(const std::size_t id) const noexcept(false) -> sAppear
{
    if (id < 5)
        return m_header0.Appear[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cQTDS::getAppear0(void) const noexcept -> sAppear { return getAppear(0); }
auto cQTDS::getAppear1(void) const noexcept -> sAppear { return getAppear(1); }
auto cQTDS::getAppear2(void) const noexcept -> sAppear { return getAppear(2); }
auto cQTDS::getAppear3(void) const noexcept -> sAppear { return getAppear(3); }
auto cQTDS::getAppear4(void) const noexcept -> sAppear { return getAppear(4); }

auto cQTDS::getInvaderAppearChance(void) const noexcept -> u8 { return m_header0.InvaderAppearChance; }
auto cQTDS::setInvaderAppearChance(const u8 val) noexcept -> void { m_header0.InvaderAppearChance = val; }

auto cQTDS::getInvaderStartTime(void) const noexcept -> u8 { return m_header0.InvaderStartTime; }
auto cQTDS::setInvaderStartTime(const u8 val) noexcept -> void { m_header0.InvaderStartTime = val; }

auto cQTDS::getInvaderStartRandChance(void) const noexcept -> u8 { return m_header0.InvaderStartRandChance; }
auto cQTDS::setInvaderStartRandChance(const u8 val) noexcept -> void { m_header0.InvaderStartRandChance = val; }

auto cQTDS::getStrayLimit(const std::size_t id) const noexcept(false) -> u8
{
    if (id < 3)
        return m_header0.StrayLimit[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cQTDS::getStrayLimit0(void) const noexcept -> u8 { return getStrayLimit(0); }
auto cQTDS::getStrayLimit1(void) const noexcept -> u8 { return getStrayLimit(1); }
auto cQTDS::getStrayLimit2(void) const noexcept -> u8 { return getStrayLimit(2); }

auto cQTDS::setStrayLimit(const std::size_t id, const u8 val) noexcept -> bool
{
    if (id < 3) {
        m_header0.StrayLimit[id] = val;

        return true;
    }

    return false;
}

auto cQTDS::setStrayLimit0(const u8 val) noexcept -> void { (void)setStrayLimit(0, val); }
auto cQTDS::setStrayLimit1(const u8 val) noexcept -> void { (void)setStrayLimit(1, val); }
auto cQTDS::setStrayLimit2(const u8 val) noexcept -> void { (void)setStrayLimit(2, val); }

auto cQTDS::getStrayRand2(const std::size_t id) const noexcept(false) -> u8
{
    if (id < 3)
        return m_header0.StrayRand2[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cQTDS::getStrayRand2_0(void) const noexcept -> u8 { return getStrayRand2(0); }
auto cQTDS::getStrayRand2_1(void) const noexcept -> u8 { return getStrayRand2(1); }
auto cQTDS::getStrayRand2_2(void) const noexcept -> u8 { return getStrayRand2(2); }

auto cQTDS::setStrayRand2(const std::size_t id, const u8 val) noexcept -> bool
{
    if (id < 3) {
        m_header0.StrayRand2[id] = val;

        return true;
    }

    return false;
}

auto cQTDS::setStrayRand2_0(const u8 val) noexcept -> void { (void)setStrayRand2(0, val); }
auto cQTDS::setStrayRand2_1(const u8 val) noexcept -> void { (void)setStrayRand2(1, val); }
auto cQTDS::setStrayRand2_2(const u8 val) noexcept -> void { (void)setStrayRand2(2, val); }

auto cQTDS::getSPExtraTicketNum(void) const noexcept -> u8 { return m_header0.SPExtraTicketNum; }
auto cQTDS::setSPExtraTicketNum(const u8 val) noexcept -> void { m_header0.SPExtraTicketNum = val; }

auto cQTDS::getIcon(const std::size_t id) const noexcept(false) -> sIcon::e
{
    if (id < 5)
        return m_header0.Icon[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cQTDS::getIcon0(void) const noexcept -> sIcon::e { return getIcon(0); }
auto cQTDS::getIcon1(void) const noexcept -> sIcon::e { return getIcon(1); }
auto cQTDS::getIcon2(void) const noexcept -> sIcon::e { return getIcon(2); }
auto cQTDS::getIcon3(void) const noexcept -> sIcon::e { return getIcon(3); }
auto cQTDS::getIcon4(void) const noexcept -> sIcon::e { return getIcon(4); }

auto cQTDS::setIcon(const std::size_t id, const sIcon::e val) noexcept -> bool
{
    if (id < 5) {
        m_header0.Icon[id] = val;

        return true;
    }

    return false;
}

auto cQTDS::setIcon0(const sIcon::e val) noexcept -> void { (void)setIcon(0, val); }
auto cQTDS::setIcon1(const sIcon::e val) noexcept -> void { (void)setIcon(1, val); }
auto cQTDS::setIcon2(const sIcon::e val) noexcept -> void { (void)setIcon(2, val); }
auto cQTDS::setIcon3(const sIcon::e val) noexcept -> void { (void)setIcon(3, val); }
auto cQTDS::setIcon4(const sIcon::e val) noexcept -> void { (void)setIcon(4, val); }

auto cQTDS::getsGMDLinkCount(void) const noexcept -> std::vector<sGMDLink>::size_type { return m_gmdLinks.size(); }

auto cQTDS::getGMDLink(const std::size_t id) const noexcept(false) -> sGMDLink
{
    if (id < m_gmdLinks.size())
        return m_gmdLinks[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cQTDS::setGMDLink(const std::size_t id, const sGMDLink val) -> bool
{
    if (id < 5) {
        if (m_gmdLinks.size() < id)
            m_gmdLinks.resize(id);

        m_gmdLinks[id] = val;

        return true;
    }

    return false;
}

auto cQTDS::setGMDLink0(const sGMDLink val) -> void { (void)setGMDLink(0, val); }
auto cQTDS::setGMDLink1(const sGMDLink val) -> void { (void)setGMDLink(1, val); }
auto cQTDS::setGMDLink2(const sGMDLink val) -> void { (void)setGMDLink(2, val); }
auto cQTDS::setGMDLink3(const sGMDLink val) -> void { (void)setGMDLink(3, val); }
auto cQTDS::setGMDLink4(const sGMDLink val) -> void { (void)setGMDLink(4, val); }

auto cQTDS::getHeader0(void) noexcept -> Header& { return m_header0; }
auto cQTDS::getHeader0(void) const noexcept -> const Header& { return m_header0; }

auto cQTDS::getGMDLinks(void) noexcept -> std::vector<sGMDLink>& { return m_gmdLinks; }
auto cQTDS::getGMDLinks(void) const noexcept -> const std::vector<sGMDLink>& { return m_gmdLinks; }

auto cQTDS::getHeader1(void) noexcept -> Header1& { return m_header1; }
auto cQTDS::getHeader1(void) const noexcept -> const Header1& { return m_header1; }

auto cQTDS::basicCheck(std::istream& istream) noexcept -> bool
{
    tools::MagicVersion magicVersion;
    std::streamsize expectedSize = sizeof(Header::MAGIC) + sizeof(Header::VERSION);

    if (tools::istream::size(istream) <= expectedSize)
        return false;

    magicVersion.read(istream);

    if (!magicVersion.check(Header::MAGIC, Header::VERSION))
        return false;

    return true;
}

auto cQTDS::readHeader(std::istream& istream) noexcept -> bool
{
    if (tools::istream::size(istream) < static_cast<std::streamsize>(sizeof(Header)))
        return false;

    istream.read(  reinterpret_cast<char*>(&m_header0)
                 , sizeof(Header));

    return istream.gcount() == sizeof(Header);
}

auto cQTDS::readLinks(std::istream& istream) noexcept -> bool
{
    std::size_t linkAmmount = 0u;
    std::streamsize streamsize = tools::istream::size(istream);

    switch (streamsize) {
    default:return false;
    case MHXX_QTDS_SIZE:
        linkAmmount = 1;
        break;
    case MHGU_QTDS_SIZE:
        linkAmmount = 5;
        break;
    };

    std::streamsize expectedsize = sizeof(Header) + sizeof(sGMDLink) * linkAmmount;

    if (streamsize < expectedsize)
        return false;

    m_gmdLinks.reserve(linkAmmount);

    istream.seekg(  sizeof(Header)
                  , std::ios::beg);

    for (auto i = 0u; i < linkAmmount; i++) {
        sGMDLink link;

        istream.read(  reinterpret_cast<char*>(&link)
                     , sizeof(sGMDLink));

        m_gmdLinks.push_back(std::move(link));
    }

    return istream.tellg() == expectedsize;
}

auto cQTDS::readHeader1(std::istream& istream) noexcept -> bool
{
    std::streamsize streamsize = tools::istream::size(istream);
    std::streamsize offset = sizeof(Header) + m_gmdLinks.size() * sizeof(sGMDLink);
    std::streamsize expectedsize = offset + sizeof(Header1);

    if (streamsize < expectedsize)
        return false;

    istream.seekg(  offset
                  , std::ios::beg);

    tools::istream::read(  istream
                         , &m_header1
                         , sizeof(Header1));

    return istream.gcount() == sizeof(Header1);
}

auto cQTDS::fixGMDLinkAmmount(void) -> void
{
    if (   m_gmdLinks.size() > 1
        && m_gmdLinks.size() < 5) {
        m_gmdLinks.reserve(5);

        for (auto i = m_gmdLinks.size(); i < 5; i++)
            m_gmdLinks.push_back(m_gmdLinks[0]);
    }
}

}; /// namespase qtds
}; /// namespase mhxx
}; /// namespace mh
