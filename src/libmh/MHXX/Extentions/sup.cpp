#include "libmh/MHXX/Extentions/sup.hpp"
#include "libmh/tools/istream.hpp"
#include "libmh/tools/magicVersion.hpp"
#include "libmh/function_name.hpp"
#include <istream>
#include <stdexcept>

namespace mh {
namespace mhxx {
namespace sup {

auto parse(std::istream& istream, cSUP& obj) noexcept -> bool
{
    if (!obj.basicCheck(istream))
        return false;

    if (!obj.readHeader(istream))
        return false;

    obj.m_valid = true;

    return true;
}

auto cSUP::isValid(void) const noexcept -> bool { return m_valid; }

auto cSUP::getItem(const u32 id) noexcept(false) -> sSupplyItem&
{
    if (id < Header::ITEMS_MAX)
        return m_header.Items[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cSUP::getItem(const u32 id) const noexcept(false) -> const sSupplyItem&
{
    if (id < Header::ITEMS_MAX)
        return m_header.Items[id];

    throw std::out_of_range(__FUNCTION_SIGNATURE_OUT_OF_RANGE__);
}

auto cSUP::getItem(const u32 id, sSupplyItem& item) const noexcept -> bool
{
    if (id < Header::ITEMS_MAX) {
        item = m_header.Items[id];

        return true;
    }

    return false;
}

auto cSUP::setItem(const u32 id, const sSupplyItem& item) noexcept -> bool
{
    if (id < Header::ITEMS_MAX) {
        m_header.Items[id] = item;

        return true;
    }

    return false;
}

auto cSUP::getSupply(void) noexcept -> Header&
{
    return m_header;
}

auto cSUP::getSupply(void) const noexcept -> const Header&
{
    return m_header;
}

auto cSUP::setSupply(const Header& supply) noexcept -> void
{
    m_header = supply;
}

auto cSUP::basicCheck(std::istream& istream) noexcept -> bool
{
    tools::MagicVersion magicVersion;

    if (tools::istream::size(istream) != static_cast<std::streamsize>(sizeof(Header)))
        return false;

    magicVersion.read(istream);

    if (!magicVersion.check(Header::MAGIC, Header::VERSION))
        return false;

    return true;
}

auto cSUP::readHeader(std::istream& istream) noexcept -> bool
{
    tools::istream::read(  istream
                         , &m_header
                         , sizeof(Header));

    return istream.gcount() == sizeof(Header);
}

}; /// namespase sup
}; /// namespase mhxx
}; /// namespace mh
