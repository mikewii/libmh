#include "libmh/MHXX/Extentions/esl.hpp"
#include "libmh/tools/magicVersion.hpp"
#include "libmh/tools/istream.hpp"
#include <istream>

namespace mh {
namespace mhxx {
namespace esl {

auto parse(std::istream& istream, cESL& obj) noexcept -> bool
{
    if (!obj.basicCheck(istream))
        return false;

    if (   !obj.readHeader(istream)
        || !obj.readESD(istream))
        return false;

    obj.m_valid = true;

    return true;
}

auto cESL::isValid(void) const noexcept -> bool { return m_valid; }

auto cESL::getESLHeader(void) noexcept -> Header& { return m_header; }
auto cESL::getESLHeader(void) const noexcept -> const Header& { return m_header; }

auto cESL::getESDVector(void) noexcept -> std::vector<ESD>& { return m_esdVec; }
auto cESL::getESDVector(void) const noexcept -> const std::vector<ESD>& { return m_esdVec; }

auto cESL::basicCheck(std::istream& istream) noexcept -> bool
{
    tools::MagicVersion magicVersion;

    if (tools::istream::size(istream) <= static_cast<std::streamsize>(sizeof(Header)))
        return false;

    magicVersion.read(istream);

    if (!magicVersion.check(Header::MAGIC, Header::VERSION))
        return false;

    return true;
}

auto cESL::readHeader(std::istream& istream) noexcept -> bool
{
    if (tools::istream::size(istream) <= static_cast<std::streamsize>(sizeof(Header)))
        return false;

    tools::istream::read(  istream
                         , &m_header
                         , sizeof(Header));

    return istream.gcount() == sizeof(Header);
}

auto cESL::readESD(std::istream& istream) -> bool
{
    auto totalESD = 0;

    for (const auto& pESD : m_header.pESD)
        if (pESD != 0)
            ++totalESD;

    m_esdVec.reserve(totalESD);

    for (const auto& offset : m_header.pESD) {
        // Should probably break?
        if (offset == 0)
            continue;

        sESD esData;
        std::vector<sEnemySmall> esdVec;

        istream.seekg(offset);

        istream.read(  reinterpret_cast<char*>(&esData)
                     , sizeof(sESD));

        if (   esData.Magic0 != sESD::MAGIC0
            || esData.Magic1 != sESD::MAGIC1)
            continue;

        esdVec.reserve(esData.EnemySmallNum);

        for (auto i = 0u; i < esData.EnemySmallNum; i++) {
            sEnemySmall enemySmall;

            istream.read(  reinterpret_cast<char*>(&enemySmall)
                         , sizeof(sEnemySmall));

            esdVec.push_back(std::move(enemySmall));
        }

        m_esdVec.emplace_back(std::move(esData), std::move(esdVec));
    }

    return true;
}

}; /// namespase esl
}; /// namespase mhxx
}; /// namespace mh
