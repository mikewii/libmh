#include "libmh/MHXX/Extentions/amlt.hpp"
#include "libmh/tools/magic.hpp"
#include "libmh/tools/istream.hpp"
#include "libmh/tools/ostream.hpp"
#include <istream>
#include <ostream>

namespace mh {
namespace mhxx {
namespace amlt {

auto parse(std::istream& istream, cAMLT& obj) noexcept -> bool
{
    if (!obj.basicCheck(istream))
        return false;

    if (   !obj.readHeader(istream)
        || !obj.dataSizeCheck(istream)
        || !obj.readData(istream))
        return false;

    obj.m_valid = true;

    return true;
}

auto cAMLT::isValid(void) const noexcept -> bool { return m_valid; }

auto cAMLT::dump(std::ostream& ostream) -> bool
{
    std::ostream::pos_type totalSize =   sizeof(Header)
                                       + (m_header.DataNum * sizeof(Data));

    tools::ostream::seekp(  ostream
                          , std::ios::beg
                          , std::ios::beg);

    tools::ostream::write(  ostream
                          , &m_header
                          , sizeof(m_header));

    for (const auto& data : m_data)
        tools::ostream::write(  ostream
                              , &data
                              , sizeof(data));

    return ostream.tellp() == totalSize;
}

auto cAMLT::getHeader(void) noexcept -> Header& { return m_header; }
auto cAMLT::getHeader(void) const noexcept -> const Header& { return m_header; }
auto cAMLT::getData(void) noexcept -> std::vector<Data>& { return m_data; }
auto cAMLT::getData(void) const noexcept -> const std::vector<Data>& { return m_data; }

auto cAMLT::basicCheck(std::istream& istream) const noexcept -> bool
{
    tools::sMagic magic;

    if (tools::istream::size(istream) <= static_cast<std::streamsize>(sizeof(Header)))
        return false;

    magic.read(istream);

    if (!magic.check(Header::MAGIC))
        return false;

    return true;
}

auto cAMLT::dataSizeCheck(std::istream& istream) const noexcept -> bool
{
    std::streamsize streamSize = tools::istream::size(istream);
    std::streamsize expectedSize = sizeof(Header) + (m_header.DataNum * sizeof(Data));

    return streamSize == expectedSize;
}

auto cAMLT::readHeader(std::istream& istream) noexcept -> bool
{
    if (tools::istream::size(istream) <= static_cast<std::streamsize>(sizeof(Header)))
        return false;

    tools::istream::read(  istream
                         , &m_header
                         , sizeof(Header));

    return istream.gcount() == sizeof(Header);
}

auto cAMLT::readData(std::istream& istream) noexcept -> bool
{
    m_data.reserve(m_header.DataNum);

    for (auto i = 0u; i < m_header.DataNum; i++) {
        Data data;

        istream.read(  reinterpret_cast<char*>(&data)
                     , sizeof(data));

        m_data.push_back(std::move(data));
    }

    return m_data.size() == m_header.DataNum;
}

}; /// namespace amlt
}; /// namespace mhxx
}; /// namespace mh
