#include "libmh/MHXX/Extentions/qdl.hpp"
#include "libmh/tools/magicVersion.hpp"
#include "libmh/tools/istream.hpp"
#include <istream>

namespace mh {
namespace mhxx {
namespace qdl {

auto parse(std::istream& istream, cQDL& obj) noexcept -> bool
{
    if (!obj.basicCheck(istream))
        return false;

    if (!obj.readHeader(istream))
        return false;

    obj.m_valid = true;

    return true;
}

auto cQDL::isValid(void) const noexcept -> bool { return m_valid; }

auto cQDL::getItem(const sQDLItemOrder id) const noexcept -> sQDLItem
{
    return m_header.Items[id];
}

auto sQDLItem::getName(void) const noexcept -> std::string
{
    return std::string(Name);
}

auto cQDL::setItem(const sQDLItemOrder id, const sQDLItem& item) noexcept -> void
{
    m_header.Items[id] = item;
}

auto cQDL::getQuestDataLink(void) noexcept -> Header& { return m_header; }
auto cQDL::getQuestDataLink(void) const noexcept -> const Header& { return m_header; }

auto cQDL::basicCheck(std::istream& istream) noexcept -> bool
{
    tools::MagicVersion magicVersion;

    if (tools::istream::size(istream) != static_cast<std::streamsize>(sizeof(Header)))
        return false;

    magicVersion.read(istream);

    if (!magicVersion.check(Header::MAGIC, Header::VERSION))
        return false;

    return true;
}

auto cQDL::readHeader(std::istream& istream) noexcept -> bool
{
    tools::istream::read(  istream
                         , &m_header
                         , sizeof(Header));

    return istream.gcount() == sizeof(Header);
}

auto sQDLItem::setName(const std::string& str) noexcept -> bool
{
    if (str.empty())
        return false;

    if (str.size() <= sQDLItem::QDL_NAME_MAX) {
        str.copy(Name, str.size());

        Name[str.size() + 1] = '\0';

        return true;
    }

    return false;
}

}; /// namespase qdl
}; /// namespase mhxx
}; /// namespace mh
