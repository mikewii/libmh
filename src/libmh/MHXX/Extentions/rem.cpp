#include "libmh/MHXX/Extentions/rem.hpp"
#include "libmh/tools/magicVersion.hpp"
#include "libmh/tools/istream.hpp"
#include <istream>

namespace mh {
namespace mhxx {
namespace rem {

auto parse(std::istream& istream, cREM& obj) noexcept -> bool
{
    if (!obj.basicCheck(istream))
        return false;

    if (!obj.readHeader(istream))
        return false;

    obj.m_valid = true;

    return true;
}

auto cREM::isValid(void) const noexcept -> bool { return m_valid; }

auto cREM::getFlag(const u32 id, sRewardFlag& flag) const noexcept -> bool
{
    if (id < Header::FLAGS_MAX) {
        flag = m_header.Flags[id];

        return true;
    }

    return false;
}

auto cREM::getFlagNum(const u32 id, u8& flagNum) const noexcept -> bool
{
    if (id < Header::FLAGNUMS_MAX) {
        flagNum = m_header.FlagNums[id];

        return true;
    }

    return false;
}

auto cREM::getRewardItem(const u32 id, sRewardItem& rewardItem) const noexcept -> bool
{
    if (id < Header::ITEMS_MAX) {
        rewardItem = m_header.Items[id];

        return true;
    }

    return false;
}

auto cREM::setFlag(const u32 id, const sRewardFlag flag) noexcept -> bool
{
    if (id < Header::FLAGS_MAX) {
        m_header.Flags[id] = flag;

        return true;
    }

    return false;
}

auto cREM::setFlagNum(const u32 id, const s8 flagNum) noexcept -> bool
{
    if (id < Header::FLAGNUMS_MAX) {
        m_header.FlagNums[id] = flagNum;

        return true;
    }

    return false;
}

auto cREM::setRewardItem(const u32 id, const sRewardItem item) noexcept -> bool
{
    if (id < Header::ITEMS_MAX) {
        m_header.Items[id] = item;

        return true;
    }

    return false;
}

auto cREM::getRewardEm(void) noexcept -> Header& { return m_header; }
auto cREM::getRewardEm(void) const noexcept -> const Header& { return m_header; }

auto cREM::basicCheck(std::istream& istream) noexcept -> bool
{
    tools::MagicVersion magicVersion;

    if (tools::istream::size(istream) != static_cast<std::streamsize>(sizeof(Header)))
        return false;

    magicVersion.read(istream);

    if (!magicVersion.check(Header::MAGIC, Header::VERSION))
        return false;

    return true;
}

auto cREM::readHeader(std::istream& istream) noexcept -> bool
{
    tools::istream::read(  istream
                         , &m_header
                         , sizeof(Header));

    return istream.gcount() == sizeof(Header);
}

}; /// namespase rem
}; /// namespase mhxx
}; /// namespace mh
