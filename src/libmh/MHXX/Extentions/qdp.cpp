#include "libmh/MHXX/Extentions/qdp.hpp"
#include "libmh/tools/magicVersion.hpp"
#include "libmh/tools/istream.hpp"
#include <istream>

namespace mh {
namespace mhxx {
namespace qdp {

auto parse(std::istream& istream, cQDP& obj) noexcept -> bool
{
    if (!obj.basicCheck(istream))
        return false;

    if (!obj.readHeader(istream))
        return false;

    obj.m_valid = true;

    return true;
}

auto cQDP::isValid(void) const noexcept -> bool { return m_valid; }

auto cQDP::getIsFence(void) const noexcept -> bool { return m_header.IsFence; }
auto cQDP::getIsFenceFromStart(void) const noexcept -> bool { return m_header.IsFenceFromStart; }
auto cQDP::getFenceOpenTime(void) const noexcept -> u16 { return m_header.FenceOpenTime; }
auto cQDP::getFenceStartTime(void) const noexcept -> u16 { return m_header.FenceStartTime; }
auto cQDP::getFenceReuseTime(void) const noexcept -> u16 { return m_header.FenceReuseTime; }
auto cQDP::getIsDragonator(void) const noexcept -> bool { return m_header.IsDragonator; }
auto cQDP::getDragonatorStartTime(void) const noexcept -> u16 { return m_header.DragonatorStartTime; }
auto cQDP::getDragonatorReuseTime(void) const noexcept -> u16 { return m_header.DragonatorReuseTime; }
auto cQDP::getFortHpS(void) const noexcept -> u16 { return m_header.FortHpS; }
auto cQDP::getFortHpL(void) const noexcept -> u16 { return m_header.FortHpL; }

auto cQDP::setIsFence(const bool value) noexcept -> void{ m_header.IsFence = value; }
auto cQDP::setIsFenceFromStart(const bool value) noexcept -> void { m_header.IsFenceFromStart = value; }
auto cQDP::setFenceOpenTime(const u16 num) noexcept -> void { m_header.FenceOpenTime = num; }
auto cQDP::setFenceStartTime(const u16 num) noexcept -> void { m_header.FenceStartTime = num; }
auto cQDP::setFenceReuseTime(const u16 num) noexcept -> void { m_header.FenceReuseTime = num; }
auto cQDP::setIsDragonator(const bool value) noexcept -> void { m_header.IsDragonator = value; }
auto cQDP::setDragonatorStartTime(const u16 num) noexcept -> void { m_header.DragonatorStartTime = num; }
auto cQDP::setDragonatorReuseTime(const u16 num) noexcept -> void { m_header.DragonatorReuseTime = num; }
auto cQDP::setFortHpS(const u16 num) noexcept -> void { m_header.FortHpS = num; }
auto cQDP::setFortHpL(const u16 num) noexcept -> void { m_header.FortHpL = num; }

auto cQDP::getQuestPlus(void) noexcept -> Header& { return m_header; }
auto cQDP::getQuestPlus(void) const noexcept -> const Header& { return m_header; }

auto cQDP::basicCheck(std::istream& istream) noexcept -> bool
{
    tools::MagicVersion magicVersion;

    if (tools::istream::size(istream) != static_cast<std::streamsize>(sizeof(Header)))
        return false;

    magicVersion.read(istream);

    if (!magicVersion.check(Header::MAGIC, Header::VERSION))
        return false;

    return true;
}

auto cQDP::readHeader(std::istream& istream) noexcept -> bool
{
    tools::istream::read(  istream
                         , &m_header
                         , sizeof(Header));

    return istream.gcount() == sizeof(Header);
}

}; /// namespase qdp
}; /// namespase mhxx
}; /// namespace mh
