#pragma once
#include <libmh/types.hpp>
#include <array>

namespace mh {
namespace mh4u {

struct AppLanguage {
    using size = u8;

    enum e: size {
        EN = 0,     ///< English
        FR,         ///< French
        DE,         ///< German
        IT,         ///< Italian
        ES,         ///< Spanish
        JP,         ///< Japanese
        KO,         ///< Korean

        Count
    };
};

struct Translation {
    static AppLanguage::e CurrentLanguage;
    static const std::array<std::array<const char*, AppLanguage::e::Count>, 124> EnemyNames_s;
    static const std::array<std::array<const char*, AppLanguage::e::Count>, 124> IconNames_s;
    static const std::array<std::array<const char*, AppLanguage::e::Count>, 33> EnemyPartNames_s;
};

}; /// namespace mh4u
}; /// namespace mh
