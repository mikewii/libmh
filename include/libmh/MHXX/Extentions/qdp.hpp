#pragma once
#include <libmh/types.hpp>
#include <libmh/attributes.hpp>
#include <iosfwd>

namespace mh {
namespace mhxx {
namespace qdp {

inline constexpr u32 RESOURCE_HASH = 0x26BEC21C;

PACKED_BEGIN
struct Header {
    static constexpr u32 MAGIC      = 0x3F800000; //nope
    static constexpr u32 VERSION    = 1;

    u32     Magic;
    u32     Version;

    bool    IsFence;            // Rail
    bool    IsFenceFromStart;
    u16     FenceOpenTime;
    u16     FenceStartTime;
    u16     FenceReuseTime;

    bool    IsDragonator;       // Gekiryu
    u16     DragonatorStartTime;
    u16     DragonatorReuseTime;

    u16     FortHpS;                // fort on laoshan quest?
    u16     FortHpL;

} PACKED;
PACKED_END

// QuestPlus
class cQDP
{
private:
    bool m_valid = false;
    Header m_header;

public:
    auto isValid(void) const noexcept -> bool;

    auto getIsFence(void) const noexcept -> bool;
    auto getIsFenceFromStart(void) const noexcept -> bool;
    auto getFenceOpenTime(void) const noexcept -> u16;
    auto getFenceStartTime(void) const noexcept -> u16;
    auto getFenceReuseTime(void) const noexcept -> u16;
    auto getIsDragonator(void) const noexcept -> bool;
    auto getDragonatorStartTime(void) const noexcept -> u16;
    auto getDragonatorReuseTime(void) const noexcept -> u16;
    auto getFortHpS(void) const noexcept -> u16;
    auto getFortHpL(void) const noexcept -> u16;

    auto setIsFence(const bool value) noexcept -> void;
    auto setIsFenceFromStart(const bool value) noexcept -> void;
    auto setFenceOpenTime(const u16 num) noexcept -> void;
    auto setFenceStartTime(const u16 num) noexcept -> void;
    auto setFenceReuseTime(const u16 num) noexcept -> void;
    auto setIsDragonator(const bool value) noexcept -> void;
    auto setDragonatorStartTime(const u16 num) noexcept -> void;
    auto setDragonatorReuseTime(const u16 num) noexcept -> void;
    auto setFortHpS(const u16 num) noexcept -> void;
    auto setFortHpL(const u16 num) noexcept -> void;

    auto getQuestPlus(void) noexcept -> Header&;
    auto getQuestPlus(void) const noexcept -> const Header&;

private:
    auto basicCheck(std::istream& istream) noexcept -> bool;
    auto readHeader(std::istream& istream) noexcept -> bool;

    friend auto parse(std::istream& istream, cQDP& obj) noexcept -> bool;
};

extern auto parse(std::istream& istream, cQDP& obj) noexcept -> bool;

}; /// namespase qdp
}; /// namespase mhxx
}; /// namespace mh
