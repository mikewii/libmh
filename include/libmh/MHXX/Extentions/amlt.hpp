#pragma once
#include <libmh/types.hpp>
#include <libmh/attributes.hpp>
#include <libmh/MHXX/Equipment/Hunter/Equipment.hpp>
#include <vector>
#include <iosfwd>

namespace mh {
namespace mhxx {
namespace amlt {

struct Header {
    static constexpr u32 MAGIC = 0x41F00000; // 30.0f

    u32 Magic;
    u32 DataNum;
};

struct Data { /// 9
    u32 ID;
    u32 Price;
    u8  Rarity;
} PACKED;

/// Amulet data
class cAMLT
{
private:
    bool m_valid = false;
    Header m_header;
    std::vector<Data> m_data;

public:
    auto isValid(void) const noexcept -> bool;

    auto dump(std::ostream& ostream) -> bool;

    auto getHeader(void) noexcept -> Header&;
    auto getHeader(void) const noexcept -> const Header&;

    auto getData(void) noexcept -> std::vector<Data>&;
    auto getData(void) const noexcept -> const std::vector<Data>&;

private:
    auto basicCheck(std::istream& istream) const noexcept -> bool;
    auto dataSizeCheck(std::istream& istream) const noexcept -> bool;
    auto readHeader(std::istream& istream) noexcept -> bool;
    auto readData(std::istream& istream) noexcept -> bool;

    friend auto parse(std::istream& istream, cAMLT& obj) noexcept -> bool;
};

extern auto parse(std::istream& istream, cAMLT& obj) noexcept -> bool;

}; /// namespace amlt
}; /// namespace mhxx
}; /// namespace mh
