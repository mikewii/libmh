#pragma once
#include <libmh/types.hpp>
#include <string>
#include <iosfwd>

namespace mh {
namespace mhxx {
namespace qdl {

inline constexpr u32 RESOURCE_HASH = 0x00E0BB1C;

// TODO: remove...wtf is this?
#define QDL_ERROR 0xF0F0F0F0

enum sQDLItemOrder {
    Boss0 = 0,
    Boss1,
    Boss2,
    Boss3,
    Boss4,
    Intruder, // righ order?
    SmallEm0,
    SmallEm1,
    RewardMainA,
    RewardMainB,
    RewardExtraA,
    RewardExtraB,
    RewardSub,
    Supply,
    QuestPlus,

    COUNT
};

struct sQDLItem {
    static constexpr u32 QDL_NAME_MAX = 15;

    u32     Resource;
    char    Name[QDL_NAME_MAX + 1];

    auto getName(void) const noexcept -> std::string;
    auto setName(const std::string& str) noexcept -> bool;
};

struct Header {
    static constexpr u32 MAGIC      = 0x434B0000;
    static constexpr u32 VERSION    = 1;
    static constexpr u32 ITEMS_MAX  = sQDLItemOrder::COUNT; // 15

    u32         Magic;
    u32         Version;

    sQDLItem    Items[ITEMS_MAX];
};

// QuestDataLink
class cQDL
{
private:
    bool m_valid = false;
    Header m_header;

public:
    auto isValid(void) const noexcept -> bool;

    auto getItem(const sQDLItemOrder id) const noexcept -> sQDLItem;
    auto setItem(const sQDLItemOrder id, const sQDLItem& item) noexcept -> void;

    auto getQuestDataLink(void) noexcept -> Header&;
    auto getQuestDataLink(void) const noexcept -> const Header&;

private:
    auto basicCheck(std::istream& istream) noexcept -> bool;
    auto readHeader(std::istream& istream) noexcept -> bool;

    friend auto parse(std::istream& istream, cQDL& obj) noexcept -> bool;
};

extern auto parse(std::istream& istream, cQDL& obj) noexcept -> bool;

}; /// namespase qdl
}; /// namespase mhxx
}; /// namespace mh
