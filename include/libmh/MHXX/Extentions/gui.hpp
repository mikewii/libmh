#pragma once
#include <libmh/types.hpp>
#include <iosfwd>

namespace mh {
namespace mhxx {
namespace gui {

inline constexpr u32 RESOURCE_HASH = 0x22948394;

struct Header {
    static constexpr u32 MAGIC    = 0x00495547; // GUI\0
    static constexpr u32 VERSION  = 0x00022715;

    u32 Magic;
    u32 Version;

    u32 FileSize;
};

}; /// namespase rem
}; /// namespase mhxx
}; /// namespace mh
