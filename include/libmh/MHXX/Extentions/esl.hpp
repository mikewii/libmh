#pragma once
#include <libmh/attributes.hpp>
#include <libmh/MHXX/Enemy/Enemy.hpp>
#include <vector>
#include <iosfwd>

namespace mh {
namespace mhxx {
namespace esl {

inline constexpr u32 RESOURCE_HASH = 0x32CA92F8;

template <typename T>
struct Geometry4 {
    T X;
    T Y;
    T Z;
    T R;
};

PACKED_BEGIN
struct sEnemySmall {
    sEnemy::ID          EnemyID;
    u8                  Unk0[2];
    u8                  SpawnCondition; // maybe
    u8                  Area;
    u16                 Unk1;
    Geometry4<float>    Position;
    u32                 Unk2[3];
} PACKED;
PACKED_END

PACKED_BEGIN
struct sESD {
    static constexpr u32 MAGIC0 = 0x00445345; // "ESD\0"
    static constexpr u32 MAGIC1 = 0x20151214; // both checked

    u32 Magic0;
    u32 Magic1;

    u16 EnemySmallNum;
} PACKED;
PACKED_END

struct Header {
    static constexpr u32 MAGIC      = 0x006C7365; // "esl\0"
    static constexpr u32 VERSION    = 2; // is it?

    u32 Magic;
    u32 Version;

    u32 Padding0;

    u32 pESD[13]; // Relative pointer to sESData. Max seen 10, possible 13
};

class cESL
{
public:
    using EmSmallDataVector = std::vector<sEnemySmall>;
    using ESD = std::pair<sESD, EmSmallDataVector>;

private:
    bool m_valid = false;
    Header m_header;
    std::vector<ESD> m_esdVec;

public:
    auto isValid(void) const noexcept -> bool;

    auto getESLHeader(void) noexcept -> Header&;
    auto getESLHeader(void) const noexcept -> const Header&;

    auto getESDVector(void) noexcept -> std::vector<ESD>&;
    auto getESDVector(void) const noexcept -> const std::vector<ESD>&;

private:
    auto basicCheck(std::istream& istream) noexcept -> bool;
    auto readHeader(std::istream& istream) noexcept -> bool;
    auto readESD(std::istream& istream) -> bool; // TODO: check boundaries

    friend auto parse(std::istream& istream, cESL& obj) noexcept -> bool;
};

auto parse(std::istream& istream, cESL& obj) noexcept -> bool;

}; /// namespase esl
}; /// namespase mhxx
}; /// namespace mh
