#pragma once
#include <libmh/MH/Extentions/gmd.hpp>
#include <memory>

namespace mh {
namespace mhxx {
namespace gmd {

inline constexpr u32 RESOURCE_HASH = 0x242BB29A; // mhxx

struct Header {
    static constexpr u32 MAGIC   = 0x00444D47; // GMD
    static constexpr u32 VERSION = 0x00010302; // u8 major 2, u8 minor 3, u16 patch 1

    u32     Magic;
    u32     Version;

    u32     Padding0;
    float   Unk;
    u32     Padding1;

    u32     LabelsNum;
    u32     ItemsNum;   // can be higher than labelsnum
    u32     LabelsSize;
    u32     ItemsSize;

    u32     FilenameSize;
};

struct sGMDAdvanced1 {
    /*
     * 0 is index
     * 1
     * 2
     * 3 is relative pointer after advanced2
     * 4
    */

    u32 Index;

    u32 Unk0[2];

    u32 ItemSize; // Incrementing size, or rptr for item if read after Advanced2,

    u32 Unk1;
};

struct sGMDAdvanced2 {
    u32 Unk[256];
};

class cGMD : public GMDBase
{
private:
    Header m_header;
    std::vector<sGMDAdvanced1> m_advanced1;
    std::unique_ptr<sGMDAdvanced2> m_advanced2;

public:
    auto isAdvanced(void) const noexcept -> bool;

    auto dump(std::ostream& ostream) -> bool;

    auto setHeader(const Header& header) noexcept -> void;
    auto getHeader(void) noexcept -> Header&;
    auto getHeader(void) const noexcept -> const Header&;

    auto getAdvanced1(void) noexcept -> std::vector<sGMDAdvanced1>&;
    auto getAdvanced1(void) const noexcept -> const std::vector<sGMDAdvanced1>&;

    auto getAdvanced2(void) noexcept -> sGMDAdvanced2&;
    auto getAdvanced2(void) const noexcept -> const sGMDAdvanced2&;

private:
    auto getExpectedSize(const bool precise = false) const noexcept -> std::size_t;

    auto basicCheck(std::istream& istream) noexcept -> bool;
    auto readHeader(std::istream& istream) noexcept -> bool;
    auto readFilename(std::istream& istream) noexcept -> bool;
    auto readAdvanced1(std::istream& istream) noexcept -> void;
    auto readAdvanced2(std::istream& istream) noexcept -> void;
    auto readLabels(std::istream& istream) noexcept -> void;
    auto readItems(std::istream& istream) noexcept -> void;

    // Returns total size
    auto updateHeader(bool updateMagicAndVersion = true) noexcept -> u32;

    friend auto parse(std::istream& istream, cGMD& obj) noexcept -> bool;
};

extern auto parse(std::istream& istream, cGMD& obj) noexcept -> bool;

}; /// namespase gmd
}; /// namespase mhxx
}; /// namespace mh
