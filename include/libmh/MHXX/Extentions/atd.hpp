#pragma once
#include <libmh/types.hpp>
#include <libmh/attributes.hpp>
#include <libmh/tools/object.hpp>

namespace mh {
namespace mhxx {
namespace atd {

struct Header {
    static constexpr u32 MAGIC = 0x40A00000; // 5.0f

    u32 Magic;
    u32 DataNum;
};

struct Data { /// 55
    u32 Index;
    u8 Type;
    u8 ClearRequirement;
    u8 NpcName;
    u8 VillageType;

    u16 ContributePoints[2];

    u16 Item[3];
    u8 ItemNum[3];

    u32 ClientType;

    u32 QuentNo[3];

    u8 TalkNpc[3];

    u16 RewardItem[3];
    u8 RewardItemNum[3];

    u16 RewardPointNum;

    u16 AcceptFlag;

    u16 CompleteFlag;
} PACKED;

/// Activity data
class cATD : public Object<Header, Data>
{
};

}; /// namespace atd
}; /// namespace mhxx
}; /// namespace mh
