#pragma once
#include <libmh/types.hpp>
#include <libmh/MHXX/Items/Items.hpp>
#include <iosfwd>

namespace mh {
namespace mhxx {
namespace sup {

inline constexpr u32 RESOURCE_HASH = 0x54539AEE;

struct sSupplyItem {
    sItems::ID ID;
    s8 Ammount;
    s8 Unk;      // posible: rate, available after time, flags
};

struct Header {
    static constexpr u32 MAGIC      = 0x3F800000;
    static constexpr u32 VERSION    = 1;
    static constexpr u32 ITEMS_MAX  = 40;

    u32 Magic;
    u32 Version;

    sSupplyItem Items[ITEMS_MAX];
};

// Supply
class cSUP
{
private:
    bool m_valid = false;
    Header m_header;

public:
    auto isValid(void) const noexcept -> bool;

    auto dump(std::ostream& ostream) noexcept -> bool;

    auto getItem(const u32 id) noexcept(false) -> sSupplyItem&;
    auto getItem(const u32 id) const noexcept(false) -> const sSupplyItem&;
    auto getItem(const u32 id, sSupplyItem& item) const noexcept -> bool;
    auto setItem(const u32 id, const sSupplyItem& item) noexcept -> bool;

    auto getSupply(void) noexcept -> Header&;
    auto getSupply(void) const noexcept -> const Header&;
    auto setSupply(const Header& supply) noexcept -> void;

private:
    auto basicCheck(std::istream& istream) noexcept -> bool;
    auto readHeader(std::istream& istream) noexcept -> bool;

    friend auto parse(std::istream& istream, cSUP& obj) noexcept -> bool;
};

extern auto parse(std::istream& istream, cSUP& obj) noexcept -> bool;

}; /// namespase sup
}; /// namespase mhxx
}; /// namespace mh
