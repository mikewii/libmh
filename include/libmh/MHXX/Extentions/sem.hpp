#pragma once
#include <libmh/types.hpp>
#include <iosfwd>

namespace mh {
namespace mhxx {
namespace sem {

inline constexpr u32 RESOURCE_HASH = 0x2553701D;

template <typename T>
struct Geometry4 {
    T X;
    T Y;
    T Z;
    T R;
};

struct Header {
    static constexpr u32 MAGIC = 0x3F800000;
    static constexpr u32 VERSION = 1;

    u32                 Magic;      // maybe
    u32                 Version;    // maybe
    u32                 WaveNo;
    u32                 AreaNo;
    Geometry4<float>    Position;
};

// SetEmMain
class cSEM
{
private:
    bool m_valid = false;
    Header m_header;

public:
    auto isValid(void) const noexcept -> bool;

    auto getWaveNo(void) const noexcept -> u32;
    auto getAreaNo(void) const noexcept -> u32;
    auto getPosition(void) const noexcept -> Geometry4<float>;

    auto setWaveNo(const u32 num) noexcept -> void;
    auto setAreaNo(const u32 num) noexcept -> void;
    auto setPosition(const Geometry4<float>& pos) noexcept -> void;

    auto getSetEmMain(void) noexcept -> Header&;
    auto getSetEmMain(void) const noexcept -> const Header&;

private:
    auto basicCheck(std::istream& istream) noexcept -> bool;
    auto readHeader(std::istream& istream) noexcept -> bool;

    friend auto parse(std::istream& istream, cSEM& obj) noexcept -> bool;
};

extern auto parse(std::istream& istream, cSEM& obj) noexcept -> bool;

}; /// namespase sem
}; /// namespase mhxx
}; /// namespace mh
