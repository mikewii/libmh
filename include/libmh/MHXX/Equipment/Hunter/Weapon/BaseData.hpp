#pragma once
#include <libmh/types.hpp>
#include <libmh/attributes.hpp>
#include <libmh/MHXX/Equipment/Hunter/Equipment.hpp>
#include <iosfwd>
#include <vector>

namespace mh {
namespace mhxx {
namespace weapon {
namespace basedata {

struct Header {
    static constexpr u32 MAGIC = 0x40066666; // 2.1

    u32 Magic;
    u32 DataNum;
};

struct Upgrade_s {
    u8 UnlockLevel;
    u8 ID;
};

PACKED_BEGIN
struct DataCommon {
    u32 Index;
    u16 ModelID;
    u16 SrqrNo; // SoundRequestNo
    u8 SndMaterial;
    u8 MaxLv;
    u8 MaxLvForLimit;
    u8 Rare;
    u32 Price;
    u8 PriceChangeLv;
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W00 { /// 33
    DataCommon Common;
    u8 Element;
    u8 ColorIndex;
    u16 SortID;
    u8 IsNamed;
    u8 SpecialSkill;
    Upgrade_s Upgrade[5];
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W01 { /// 33
    DataCommon Common;
    u8 Element;
    u8 ColorIndex;
    u16 SortID;
    u8 IsNamed;
    u8 SpecialSkill;
    Upgrade_s Upgrade[5];
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W02 { /// 33
    DataCommon Common;
    u8 Element;
    u8 ColorIndex;
    u16 SortID;
    u8 IsNamed;
    u8 SpecialSkill;
    Upgrade_s Upgrade[5];
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W03 { /// 33
    DataCommon Common;
    u8 Element;
    u8 ColorIndex;
    u16 SortID;
    u8 IsNamed;
    u8 SpecialSkill;
    Upgrade_s Upgrade[5];
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W04 { /// 32
    DataCommon Common;
    u8 ColorIndex;
    u16 SortID;
    u8 IsNamed;
    u8 SpecialSkill;
    Upgrade_s Upgrade[5];
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W06 { /// 32
    DataCommon Common;
    u8 ColorIndex;
    u16 SortID;
    u8 IsNamed;
    u8 SpecialSkill;
    Upgrade_s Upgrade[5];
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W07 { /// 33
    DataCommon Common;
    u8 Element;
    u8 ColorIndex;
    u16 SortID;
    u8 IsNamed;
    u8 SpecialSkill;
    Upgrade_s Upgrade[5];
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W08 { /// 34
    DataCommon Common;
    u8 Element;
    u8 PhialType;
    u8 ColorIndex;
    u16 SortID;
    u8 IsNamed;
    u8 SpecialSkill;
    Upgrade_s Upgrade[5];
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W09 { /// 34
    DataCommon Common;
    u8 Element;
    u8 ShootType;
    u8 ColorIndex;
    u16 SortID;
    u8 IsNamed;
    u8 SpecialSkill;
    Upgrade_s Upgrade[5];
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W10 { /// 33
    DataCommon Common;
    u8 Element;
    u8 ColorIndex;
    u16 SortID;
    u8 IsNamed;
    u8 SpecialSkill;
    Upgrade_s Upgrade[5];
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W11 { /// 34
    DataCommon Common;
    u8 Element1;
    u8 Element2;
    u8 ColorIndex;
    u16 SortID;
    u8 IsNamed;
    u8 SpecialSkill;
    Upgrade_s Upgrade[5];
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W12 { /// 33
    DataCommon Common;
    u8 Element;
    u8 ColorIndex;
    u16 SortID;
    u8 IsNamed;
    u8 SpecialSkill;
    Upgrade_s Upgrade[5];
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W13 { /// 34
    DataCommon Common;
    u8 Element;
    u8 InsectType;
    u8 ColorIndex;
    u16 SortID;
    u8 IsNamed;
    u8 SpecialSkill;
    Upgrade_s Upgrade[5];
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W14 { /// 34
    DataCommon Common;
    u8 Element;
    u8 PhialType;
    u8 ColorIndex;
    u16 SortID;
    u8 IsNamed;
    u8 SpecialSkill;
    Upgrade_s Upgrade[5];
} PACKED;
PACKED_END

template <typename T>
class cBaseData
{
private:
    bool m_valid = false;
    Header m_header;
    std::vector<T> m_data;

public:
    auto isValid(void) const noexcept -> bool;

    auto dump(std::ostream& ostream) -> bool;

    auto getHeader(void) noexcept -> Header&;
    auto getHeader(void) const noexcept -> const Header&;

    auto getData(void) noexcept -> std::vector<T>&;
    auto getData(void) const noexcept -> const std::vector<T>&;

    auto getData(const std::size_t id, T& output) const noexcept -> bool;
    auto setData(const T& input) noexcept -> bool;

private:
    auto basicCheck(std::istream& istream) noexcept -> bool;
    auto dataSizeCheck(std::istream& istream) noexcept -> bool;
    auto readHeader(std::istream& istream) noexcept -> bool;
    auto readData(std::istream& istream) noexcept -> bool;

    template <typename U>
    friend auto parse(std::istream& istream, cBaseData<U>& obj) noexcept -> bool;
};

template <typename U>
extern auto parse(std::istream& istream, cBaseData<U>& obj) noexcept -> bool;

}; /// namespace basedata
}; /// namespace weapon
}; /// namespace mhxx
}; /// namespace mh
