#pragma once
#include <libmh/types.hpp>
#include <libmh/attributes.hpp>
#include <iosfwd>
#include <vector>

namespace mh {
namespace mhxx {
namespace weapon {
namespace leveldata {

struct Header {
    static constexpr u32 MAGIC = 0x3F99999A; // 1.2

    u32 Magic;
    u32 DataNum;
};

PACKED_BEGIN
struct DataCommon {
    u32 Index;
    u8 ID;
    u8 UpgradeLevel;
} PACKED;
PACKED_END

PACKED_BEGIN
struct UniqueShell_s {
    u8 Shell;
    u8 HaveShellCount;
    u8 ShellCount;
} PACKED;
PACKED_END

PACKED_BEGIN
struct LoadShell_s {
    u8 ShellCount;
    u8 EnableShell;
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W00 { /// 15
    DataCommon Common;
    u8 SharpnessType; // 0...163
    u8 SharpnessLevel; // flags: 0 2 4 6 8 10 12
    u16 Attack;
    u8 Defence;
    u8 Affinity;
    u16 ElementAmmount;
    u8 Slots;
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W01 { /// 15
    DataCommon Common;
    u8 SharpnessType;
    u8 SharpnessLevel;
    u16 Attack;
    u8 Defence;
    u8 Affinity;
    u16 ElementAmmount;
    u8 Slots;
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W02 { /// 15
    DataCommon Common;
    u8 SharpnessType;
    u8 SharpnessLevel;
    u16 Attack;
    u8 Defence;
    u8 Affinity;
    u16 ElementAmmount;
    u8 Slots;
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W03 { /// 15
    DataCommon Common;
    u8 SharpnessType;
    u8 SharpnessLevel;
    u16 Attack;
    u8 Defence;
    u8 Affinity;
    u16 ElementAmmount;
    u8 Slots;
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W04 { /// 100
    DataCommon Common;
    u16 Attack;
    u8 Defence;
    u8 Affinity;
    u16 ElementAmmount;
    u8 Slots;
    u8 Deviation;
    u8 ReloadSpeed;
    u8 Recoil;
    u8 RapidFire[5];
    UniqueShell_s UniqueShell[5];
    LoadShell_s Shell[32];
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W06 { /// 100
    DataCommon Common;
    u16 Attack;
    u8 Defence;
    u8 Affinity;
    u16 ElementAmmount;
    u8 Slots;
    u8 Deviation;
    u8 ReloadSpeed;
    u8 Recoil;
    u8 RapidFire[5];
    UniqueShell_s UniqueShell[5];
    LoadShell_s Shell[32];
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W07 { /// 15
    DataCommon Common;
    u8 SharpnessType;
    u8 SharpnessLevel;
    u16 Attack;
    u8 Defence;
    u8 Affinity;
    u16 ElementAmmount;
    u8 Slots;
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W08 { /// 17
    DataCommon Common;
    u8 SharpnessType;
    u8 SharpnessLevel;
    u16 Attack;
    u8 Defence;
    u8 Affinity;
    u16 ElementAmmount;
    u16 PhialElementAmmount;
    u8 Slots;
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W09 { /// 16
    DataCommon Common;
    u8 SharpnessType;
    u8 SharpnessLevel;
    u16 Attack;
    u8 Defence;
    u8 Affinity;
    u16 ElementAmmount;
    u8 ShootLevel;
    u8 Slots;
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W10 { /// 31
    DataCommon Common;
    u16 Attack;
    u8 Defence;
    u8 Affinity;
    u16 ElementAmmount;
    u8 Slots;
    u8 Kyoku;
    u8 TameDankai;
    u8 CShotKind[4];
    u8 LoadEnableBottle[12];
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W11 { /// 17
    DataCommon Common;
    u8 SharpnessType;
    u8 SharpnessLevel;
    u16 Attack;
    u8 Defence;
    u8 Affinity;
    u16 ElementAmmount1;
    u16 ElementAmmount2;
    u8 Slots;
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W12 { /// 16
    DataCommon Common;
    u8 SharpnessType;
    u8 SharpnessLevel;
    u16 Attack;
    u8 Defence;
    u8 Affinity;
    u16 ElementAmmount;
    u8 FuelMusicType;
    u8 Slots;
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W13 { /// 15
    DataCommon Common;
    u8 SharpnessType;
    u8 SharpnessLevel;
    u16 Attack;
    u8 Defence;
    u8 Affinity;
    u16 ElementAmmount;
    u8 Slots;
} PACKED;
PACKED_END

PACKED_BEGIN
struct Data_W14 { /// 17
    DataCommon Common;
    u8 SharpnessType;
    u8 SharpnessLevel;
    u16 Attack;
    u8 Defence;
    u8 Affinity;
    u16 ElementAmmount;
    u16 PhialElementAmmount;
    u8 Slots;
} PACKED;
PACKED_END

template <typename T>
class cLevelData
{
private:
    bool m_valid = false;
    Header m_header;
    std::vector<T> m_data;

public:
    auto isValid(void) const noexcept -> bool;

    auto dump(std::ostream& ostream) -> bool;

    auto getHeader(void) noexcept -> Header&;
    auto getHeader(void) const noexcept -> const Header&;

    auto getData(void) noexcept -> std::vector<T>&;
    auto getData(void) const noexcept -> const std::vector<T>&;

    auto getData(const std::size_t id, std::vector<T>& output) const -> bool;
    auto setData(const std::vector<T>& input) -> bool;

private:
    auto basicCheck(std::istream& istream) noexcept -> bool;
    auto dataSizeCheck(std::istream& istream) noexcept -> bool;
    auto readHeader(std::istream& istream) noexcept -> bool;
    auto readData(std::istream& istream) noexcept -> bool;

    template <typename U>
    friend auto parse(std::istream& istream, cLevelData<U>& obj) noexcept -> bool;
};

template <typename U>
auto parse(std::istream& istream, cLevelData<U>& obj) noexcept -> bool;

}; /// namespace leveldata
}; /// namespace weapon
}; /// namespace mhxx
}; /// namespace mh
