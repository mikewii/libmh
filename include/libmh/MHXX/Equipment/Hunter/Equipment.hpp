#pragma once
#include <libmh/types.hpp>

namespace mh {
namespace mhxx {

struct sUpgradeLevel {
    using size = u8;

    enum e:size {
        LEVEL1 = 0,
        LEVEL2,
        LEVEL3,
        LEVEL4,
        LEVEL5,
        LEVEL6,
        LEVEL7,
        LEVEL8,
        LEVEL9,
        LEVEL10,
        LEVEL11,
        LEVEL12,
        LEVEL13,
        LEVEL14,
        LEVEL15
    };
};

struct sTalismanType {
    using size = u8;

    enum e:size {
        PAWN = 0,
        BISHOP,
        KNIGHT,
        ROOK,
        QUEEN,
        KING,
        DRAGON,
        UNKNOWABLE,
        MYSTIC,
        HERO,
        LEGEND,
        CREATOR,
        SAGE,
        MIRACLE,
        AMBER,
        JADE,
        EMERY
    };
};

struct sKinsectLevel {
    using size = u8;

    enum e:size {
        Lv1 = 0,
        Lv2,
        Lv3,
        Lv4,
        Lv5,
        Lv6,
        Lv7,
        Lv8,
        Lv9,
        Lv10,
        Lv11,
        Lv12
    };
};

struct sEquipmentType {
    using size = u8;

    enum e:size {
        NONE = 0,

        CHEST,
        ARMS,
        WAIST,
        LEGS,
        HEAD,

        TALISMAN,

        GS,
        SNS,
        HAMMER,
        LANCE,
        LBG,
        HBG,
        LS,
        SA,
        GL,
        BOW = 1,
        DB,
        HH,
        IG,
        CB
    };
};

struct sType {
    u16 EquipmentType : 4;
    u16 IsWeapon : 1;
    u16 UpgradeLevel : 5;

    u16 Unknown : 6;
};

union uEquipment {
    u8 Raw[28];

    struct sRelic {
    };

    struct sBlademaster {
    };

    struct sArmor {
    };

    struct sTalisman {
    };

    struct sGS {
    };

    struct sSnS {
    };

    struct sHammer {
    };

    struct sLance {
    };

    struct sLBG {
    };

    struct sHBG {
    };

    struct sLS {
    };

    struct sSA {
    };

    struct sGL {
    };

    struct sBow {
    };

    struct sDB {
    };

    struct sHH {
    };

    struct sIG {
    };

    struct sCB {
    };
};

}; /// namespace mhxx
}; /// namespace mh
