#pragma once
/// Aligns a struct (and other types?) to m, making sure that the size of the struct is a multiple of m.
#define ALIGN(m)        __attribute__((aligned(m)))
/// Packs a struct (and other types?) so it won't include padding bytes.

#ifdef __GNUC__
#define PACKED          __attribute__((packed))
#define PACKED_BEGIN
#define PACKED_END
#elif _MSC_VER
#define PACKED
#define PACKED_BEGIN __pragma(pack(push, 1))
#define PACKED_END __pragma(pack(pop))
#endif

#define USED            __attribute__((used))
#define UNUSED          __attribute__((unused))
#define DEPRECATED      __attribute__((deprecated))
#define NAKED           __attribute__((naked))
#define NORETURN        __attribute__((noreturn))
#define ALWAYS_INLINE   __attribute__((always_inline))
#define WEAK            __attribute__((weak))
