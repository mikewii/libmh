#pragma once
#include <libmh/MH/Extentions/gmd.hpp>

namespace mh {
namespace mh3u {
namespace gmd {

inline constexpr const u32 RESOURCE_HASH = 0x242BB29A; // mhxx

struct Header {
    static constexpr u32 MAGIC    = 0x00444D47; // GMD
    static constexpr u32 VERSION  = 0x00010201; // u8 major 1, u8 minor 2, u16 patch 1

    u32 Magic;
    u32 Version;

    u32 Unk0;

    u32 LabelsNum;
    u32 ItemsNum;
    u32 LabelsSize;
    u32 ItemsSize;

    u32 FilenameSize;
};

struct sGMDAdvanced1 {
    u32 Unk[2];
};

class cGMD : public GMDBase
{
private:
    Header m_header;
    std::vector<sGMDAdvanced1> m_advanced1;

public:
    auto dump(std::ostream& ostream) -> bool;

    auto getHeader(void) noexcept -> Header&;
    auto getHeader(void) const noexcept -> const Header&;

    auto getAdvanced1(void) noexcept -> std::vector<sGMDAdvanced1>&;
    auto getAdvanced1(void) const noexcept -> const std::vector<sGMDAdvanced1>&;

private:
    auto getExpectedSize(const bool precise = false) const noexcept -> u32;

    auto readHeader(std::istream& istream) noexcept -> bool;
    auto readFilename(std::istream& istream) noexcept -> bool;
    auto readAdvanced1(std::istream& istream) noexcept -> void;
    auto readLabels(std::istream& istream) noexcept -> void;
    auto readItems(std::istream& istream) noexcept -> void;

    // Returns total size
    auto updateHeader(bool updateMagicAndVersion = true) noexcept -> u32;

    friend auto parse(std::istream& istream, cGMD& obj) noexcept -> bool;
};

extern auto parse(std::istream& istream, cGMD& obj) noexcept -> bool;

}; /// namespase gmd
}; /// namespase mh3u
}; /// namespace mh
