#pragma once
#include <libmh/types.hpp>
#include <array>

namespace mh {
namespace mh4u {
namespace palico {
namespace weapon {

struct sWeapons {
    using size = u16;
    static constexpr u32 ITEMS_MAX = 179;

    enum e : size {
        NONE,
        F_BONE_WEDGE,
        F_DERRING_LANCE,
        F_BONE_HAMMER,
        F_IRON_SWORD,
        F_BELL_ROD,
        F_GRILLED_HAMMER,
        F_ARZUROS_MACE,
        F_KECHA_HARROW,
        F_KUT_KU_CUTTER,
        F_BNAHA_DAGGER,
        F_BULLDROME_TUSK,
        F_RHENO_HAMMER,
        F_JAGGI_KNIFE,
        F_MACHALITE_WEDGE,
        F_GIANT_ACORN,
        F_WOOD_CHOPPER,
        F_BARREL_BASHER,
        F_VPREY_WEDGE,
        F_PAW_PUNCH,
        F_YUKUMO_BOKKEN,
        F_MOSGHARL_BROOM,
        F_LUDROTH_PAW,
        F_SCYLLA_WEDGE,
        F_QURUPECO_AXE,
        F_ZAMTRIOS_PAW,
        F_LAGOMBI_STAFF,
        F_CABRA_POUNDER,
        F_BARROTH_MACE,
        F_SLAGTOTH_LEAF,
        F_VOLVI_LANTERN,
        F_PLESIOTH_BOARD,
        F_TIGREX_WHAMMER,
        F_BAGGI_BILLY,
        F_NIBEL_BLAMMER,
        F_SELTAS_DRILL,
        F_URAGAAN_IRON,
        F_WROGGI_GUITAR,
        F_GUILD_RAPIER,
        F_DIABLOS_HAMMER,
        F_RATHIAN_RAPIER,
        F_KHEZU_WHACCINE,
        F_DURAM_AXE,
        F_MEWSURPER_S_PEAL,
        F_NARGA_SHURIKEN,
        F_RATHALOS_BLADE,
        F_AGNAKTOR_LANCE,
        F_NAJA_PUNGI,
        F_KATZENLAMPE,
        F_LE_COEUR_DE_CHAT,
        F_KAISER_MACE,
        F_VANGIS_MACE,
        F_AKANTOR_SWORD,
        F_ESCADOR_SCYTHE,
        F_KUSHALA_WAND,
        F_UKANLOS_SWORD,
        F_KIRIN_RUMBLEZAP,
        F_GIGGINOX_FORK,
        F_GOLDEN_GADGET,
        F_BRACHY_PUNCH,
        F_BARIOTH_KNIFE,
        F_CURSED_CLOUD,
        F_LAGIA_ANCHOR,
        F_FENCER_SWORD,
        F_BANNERMAN_FLAG,
        F_KITTY_CORNERER,
        F_MARAUDER_BLADE,
        F_MARAUDER_GAVEL,
        DUMMY68,
        DUMMY73,
        F_BLANKA_FISH,
        DUMMY72,
        ALASTOR,
        F_STAR_WORLD,
        F_MONQLO_MALLET,
        F_FAN_MEGAPHONE,
        DUMMY76,
        INVINCIBLE_HAMMER,
        F_BONE_WEDGE_S,
        F_DERRING_LANCE_S,
        F_BONE_HAMMER_S,
        F_CARBALITE_SWORD,
        F_BELL_ROD_S,
        F_FINGERLICKER,
        F_ARZUROS_MACE_S,
        F_KECHA_HARROW_S,
        F_KUT_KU_CUTTER_S,
        F_BNAHA_DAGGER_S,
        F_BULLDROME_TUSK_S,
        F_RHENO_HAMMER_S,
        F_JAGGI_KNIFE_S,
        F_CARBALITE_WEDGE,
        F_GIANT_ACORN_S,
        F_WOOD_CHOPPER_S,
        F_BARREL_BASHER_S,
        F_VPREY_WEDGE_S,
        F_PAW_PUNCH_S,
        F_YUKUMO_BONITO,
        F_MOSGHARL_BROOM_S,
        F_LUDROTH_PAW_S,
        F_SCYLLA_WEDGE_S,
        F_QURUPECO_AXE_S,
        F_ZAMTRIOS_PAW_S,
        F_LAGOMBI_STAFF_S,
        F_CABRA_POUNDER_S,
        F_BARROTH_MACE_S,
        F_SLAGTOTH_LEAF_S,
        F_VOLVI_LANTERN_S,
        F_PLESIOTH_BOARD_S,
        F_TIGREX_WHAMMER_S,
        F_BAGGI_BILLY_S,
        F_NIBEL_BLAMMER_S,
        F_SELTAS_DRILL_S,
        F_URAGAAN_IRON_S,
        F_WROGGI_GUITAR_S,
        F_GUILDCALIBUR,
        F_DIABLOS_HAMMER_S,
        F_QUEEN_RAPIER,
        F_KHEZU_WHACCINE_S,
        F_DURAM_AXE_S,
        F_MEWSURPER_S_ROAR,
        F_NARGA_SHURIKEN_S,
        F_BLAZING_BLADE,
        F_AGNAKTOR_LANCE_S,
        F_NAJA_PUNGI_S,
        F_KATZENFUNZEL,
        F_LE_CHATPHRODITE,
        F_KAISER_MACE_S,
        F_VANGIS_MACE_S,
        F_AKANTOR_SWORD_S,
        F_ESCADOR_SCYTHE_S,
        F_KUSHALA_WAND_S,
        F_UKANLOS_SWORD_S,
        F_KIRIN_BOLT,
        F_GIGGI_FORK_S,
        F_GOLDEN_GADGET_S,
        F_BRACHY_WALLOP,
        F_BARIOTH_KNIFE_S,
        F_CURSED_SKY,
        F_LAGIA_ANCHOR_S,
        F_FENCER_SWORD_S,
        F_BANNERMAN_PRIDE,
        F_KING_CATPETTER,
        MEOWTETSUKEN,
        F_POUNDERPURR,
        F_GENIE_BREATH_S,
        F_GARUGA_FAN_S,
        F_GRAV_BAZOOKA_S,
        F_GYPCEROS_GLARE_S,
        F_DAIMYO_SHEARS_S,
        F_CEPHALOS_BRUSH_S,
        F_BASARIOS_AXE_S,
        F_CONGA_CYMBALS_S,
        F_MONOBLOS_LANCE_S,
        F_SEREGIOS_EDGE_S,
        FIENDLYNE_FETTER,
        DRAGON_EVERTONES,
        DIRTY_F_BLADE,
        BULLDROME_RRRUSH,
        KUT_KU_CACKLE,
        DUMMY160,
        DUMMY161,
        F_MONQLO_MALLET_S,
        F_STAR_WORLD_S,
        DUMMY165,
        SWORD_OF_PURRITY,
        F_CHUN_BRACELETS,
        DUMMY167,
        F_FAN_MEGAPHONE_G,
        INVINCIBLE_HAMMER_S,
        F_MELODIC_BATON,
        DUMMY171,
        F_MAGICAL_BATON,
        RUSHING_HAMMER,
        CALIBURN,
        TENDERPAW_CLOG,
        SENTRY_S_PICKAXE,
        MINDER_S_BINDER,
        BARREL_MALLET
    };

    static const std::array<const char*, ITEMS_MAX> str;

    static auto getStr(const e id) noexcept -> const char*;
};

}; /// namespace weapon
}; /// namespace palico
}; /// namespace mh4u
}; /// namespace mh
