#pragma once
#include <libmh/MH4U/Equipment/Hunter/Weapon/Weapon.hpp>
#include <array>

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

struct sIGKinsect {
    using size = u16;
    static constexpr u32 ITEMS_MAX = 35;

    enum e : size {
        CULLDRONE,
        REAVEDRONE,
        ALUCANID,
        MONARCH_ALUCANID,
        EMPRESSWING,
        RIGIPRAYNE,
        CANCADAMAN,
        FIDDLEBRIX,
        WINDCHOPPER,
        GRANCATHAR,
        PSEUDOCATH,
        ELSCARAD,
        MAULDRONE,
        PUMMELDRONE,
        FOEBEETLE,
        CARNAGE_BEETLE,
        BONNETFILLE,
        LADYTARGE,
        LADYPAVISE,
        ARKMAIDEN,
        GULLSHAD,
        BULLSHROUD,
        WHISPERVESP,
        ARGINESSE,
        THUNDERBALL,
        BILBOBRIX,
        FOLIACATH,
        EXALTED_ALUCANID,
        GREAT_ELSCARAD,
        LADYTOWER,
        FLEETFLAMMER,
        GLEAMBEETLE,
        GREAT_ARGINESSE,
        CLOCKMASTER,
        BARRETT_HAWK
    };

    static const std::array<sWeapon, ITEMS_MAX> str;

    static auto getStr(const e id) noexcept -> const char*;
};

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
