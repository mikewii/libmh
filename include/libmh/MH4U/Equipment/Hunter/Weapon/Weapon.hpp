#pragma once
#include <libmh/types.hpp>

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

enum class eType : u8 {
    NORMAL      = 0,
    RELIC       = 1 << 0
};

struct sWeapon {
    const char* Name = nullptr;
    eType       Type = eType::NORMAL;
};

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
