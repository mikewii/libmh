#pragma once
#include <libmh/MH4U/Equipment/Hunter/Weapon/Weapon.hpp>
#include <array>

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

struct sGS {
    using size = u16;
    static constexpr u32 ITEMS_MAX = 246;

    enum e : size {
        NONE,
        IRON_SWORD,
        IRON_SWORD_PLUS,
        BUSTER_SWORD,
        BUSTER_SWORD_PLUS,
        RAVAGER_BLADE,
        RAVAGER_BLADE_PLUS,
        LACERATOR_BLADE,
        DEVASTATOR_BLADE,
        CHROME_RAZOR,
        CHROME_QUIETUS,
        KHEZU_SHOCK_SWORD,
        KHEZU_SHOCK_BLADE,
        KHEZU_SHOCK_FULL,
        SANTOKU_REAVER,
        FUNAYUKI_REAVER,
        TOAD_BEREAVER,
        MILLSTONE_BEREAVER,
        BRAZENWALL,
        CRIMSONWALL,
        CRAGSCLIFF,
        RUGGED_GREAT_SWORD,
        CHIEFTAIN_S_GRT_SWD,
        SCHATTENSTOLZ,
        DUSTERSTOLZ,
        VERHANGNISSTOLZ,
        L_APOTHEOSE,
        USURPER_S_STORM,
        DESPOT_S_BLACKSTORM,
        BRIMSTREN_DRAKEPRIDE,
        DEMON_ROD,
        GREAT_DEMON_ROD,
        LUDROTH_BONE_SWORD,
        LUDROTH_BONE_SWORD_PLUS,
        CATACLYSM_SWORD,
        CATACLYSM_BLADE,
        BONE_BLADE,
        BONE_BLADE_PLUS,
        JAWBLADE,
        GIANT_JAWBLADE,
        GOLEM_BLADE,
        TIGER_JAWBLADE,
        TIGREX_GREAT_SWORD,
        ACCURSED_BLADE,
        VEINED_GRAVEBLADE,
        DIOS_BLADE,
        DIOS_BLADE_PLUS,
        DEMOLITION_SWORD,
        VALKYRIE_BLADE,
        SIEGLINDE,
        TITANIA,
        BLUSHING_DAME,
        AUBERON,
        RED_WING,
        RATHALOS_FIRESWORD,
        RATHALOS_FLAMESWORD,
        RATHALOS_GLEAMSWORD,
        RATHALOS_GLINSWORD,
        THRASH_SWORD,
        THRASH_SWORD_PLUS,
        THRASH_BLADE,
        THRASHGASH_BLADE,
        IMMANE_BLADE,
        IMMOLATOR_BLADE,
        SCYLLA_BLADE,
        SCYLLA_RAZOR,
        VILE_SERPENTBLADE,
        VILE_SERPENTBLADE_PLUS,
        FANGED_SERPENTBLADE,
        DEADLY_SERPENTBLADE,
        POISON_SERPENTBLADE,
        CHICK_DECAPITATOR,
        ROOSTER_DECAPITATOR,
        SENTORYU_RAVEN,
        SENTORYU_WOLF,
        SENTORYU_CROW,
        HIDDEN_BLADE,
        DARK_OF_NIGHT,
        VULCANIS,
        VULCAMAGNON,
        PLESIOTH_WATERCUTTER,
        PLESIOTH_WATERCUTTER_PLUS,
        PLESIOTH_LULLABY,
        PLESIOTH_LULLABANE,
        FROZEN_SPEARTUNA,
        FREEZER_SPEARTUNA,
        RUSTED_GREAT_SWORD,
        TARNISHED_GREAT_SWD,
        ANCIENT_BLADE,
        ELDER_MONUMENT,
        ICESTEEL_EDGE,
        DAORA_S_DECIMATOR,
        WORN_GREAT_SWORD,
        WEATHERED_GREAT_SWD,
        EPITAPH_BLADE,
        TEOSTRA_BLADE,
        KING_TEOSTRA_BLADE,
        KIRIN_THUNDERSWORD,
        KING_THUNDERSWORD,
        EMPEROR_FROSTSWORD,
        BERSERKER_SWORD,
        ANGUISH,
        CONSUMMATE_BLADE,
        AKANTOR_BROADSWORD,
        SWORDSCALE_SHARD,
        THE_LETHAL_GRAZE,
        FATALIS_BLADE,
        DUMMY107,
        DUMMY108,
        DUMMY109,
        DUMMY110,
        DUMMY111,
        DUMMY112,
        DUMMY113,
        UNIVERSAL_CHROME,
        SIEGMUND__RED_,
        SIEGMUND__YELLOW_,
        SIEGMUND__GREEN_,
        SIEGMUND__BLUE_,
        SIEGMUND__PURPLE_,
        BOLT__RED_,
        BOLT__YELLOW_,
        BOLT__GREEN_,
        BOLT__BLUE_,
        BOLT__PURPLE_,
        TYPE_41_WYVERNATOR__RED_,
        TYPE_41_WYVERNATOR__YELLOW_,
        TYPE_41_WYVERNATOR__GREEN_,
        TYPE_41_WYVERNATOR__BLUE_,
        TYPE_41_WYVERNATOR__PURPLE_,
        FLASH__RED_,
        FLASH__YELLOW_,
        FLASH__GREEN_,
        FLASH__BLUE_,
        FLASH__PURPLE_,
        DRAGONSEAL__RED_,
        DRAGONSEAL__YELLOW_,
        DRAGONSEAL__GREEN_,
        DRAGONSEAL__BLUE_,
        DRAGONSEAL__PURPLE_,
        EXEMPLAR_BLADE,
        GRANDGLORY_BLADE,
        CHROME_HELL,
        CHROME_HEAVEN,
        BLOOD_SWORD,
        BLOOD_SHOCK,
        RAMPAGE_BLADE,
        RAMPAGE_TEMPERLINE,
        CRAGSCLIFF_PLUS,
        CLIFFSWORD_TITANIUS,
        L_ECLAT,
        L_ORIGINE,
        DUSTERECLAT,
        OPPRESSOR_S_FORGER,
        ORCUS_TONITRUS,
        BRIMSTREN_DRAKEPRIDE_PLUS,
        STYGIAN_ACEDIA,
        GREAT_DEMON_HOT_ROD,
        DEMONLORD_ROD,
        CATACLYSM_BLADE_PLUS,
        VEINED_GRAVEBLADE_PLUS,
        DEIFIED_GRISBLADE,
        MYXO_DEMOLISHER,
        LIGHTBREAK_BLADE,
        EXECUTIONER,
        EXECUTIONER_PLUS,
        ENFORCER_S_AXE,
        CERA_CIGIL,
        CERA_CYMMETRY,
        RED_PINCER,
        GREAT_PINCER,
        VIOLET_PINCER,
        GRAND_PINCER,
        BLUSHING_DAME_PLUS,
        BRUNNHILDE,
        AUBERON_PLUS,
        PALE_KAISER,
        RATHALOS_GLEAMSWORD_PLUS,
        RATHLORD_GLEAMSWORD,
        RATHALOS_GLINSWORD_PLUS,
        RATHFLAME_GLINSWORD,
        JALA_BLADE,
        JAJALA_BLADE,
        PAVAKA_BLADE,
        JVALAYATI_BLADE,
        SCYLLA_SHEAR,
        EBONSCYLLA,
        WEBBONSCYLLA,
        SNAKESMAW,
        SENTORYU_POX,
        GARUGA_ENGETSU,
        DARK_OF_NIGHT_PLUS,
        AVIDYA_GREAT_SWORD,
        VULCATASTROPHE,
        VULCA_VENDETTA,
        PLESIOTH_LULLABANE_PLUS,
        PLESIOTH_PELAGIS,
        XIPHIAS_GLADIUS,
        ETERNAL_GLYPH,
        DAORA_S_DEATHMAKER,
        ELDAORA_S_DEATH,
        EPITAPH_BLADE_PLUS,
        EPITAPH_ETERNAL,
        TEOSTRA_DEL_SOL,
        GRAND_SHAMSHIR,
        EMPEROR_THUNDERSWORD,
        FROSTSWORD,
        KING_FROSTSWORD,
        DOOMEDGE_,
        AKANTOR_BROADSWORD_PLUS,
        UKANLOS_DESTRUCTOR,
        UKANLOS_SKYCLEAVER,
        WOLD_THE_JUST,
        DALAMADUR_BLADE,
        DREAMING_DALAMADUR,
        BLACK_FATALIS_BLADE,
        FLAME_FATALIS_BLADE,
        FATALIS_LEGACY,
        CHEDA_BLADE,
        CHEDA_BLADE_PLUS,
        SEDITIOUS_CLEAVER,
        ROGUE_SEDITION,
        MONUMENT_OF_LAMENT,
        OSTRACON_OBLIVION,
        G_BLADE,
        BLACK_BELT_BLADE,
        COMMEMORATION_SWORD,
        DUMMY227,
        DUMMY_228,
        DUMMY_229,
        DUMMY_230,
        DUMMY_231,
        DUMMY_232,
        DUMMY_233,
        DOS_EXILE,
        RATH_OF_STARS,
        SEA_EMPEROR_S_SWORD__RED_,
        SEA_EMPEROR_S_SWORD__YELLOW_,
        SEA_EMPEROR_S_SWORD__GREEN_,
        SEA_EMPEROR_S_SWORD__BLUE_,
        SEA_EMPEROR_S_SWORD__PURPLE_,
        JAWBLADE__RED_,
        JAWBLADE__YELLOW_,
        JAWBLADE__GREEN_,
        JAWBLADE__BLUE_,
        JAWBLADE__PURPLE_
    };

    static const std::array<sWeapon, ITEMS_MAX> str;

    static auto getStr(const e id) noexcept -> const char*;
};

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
