#pragma once
#include <libmh/MH4U/Equipment/Hunter/Weapon/Weapon.hpp>
#include <array>

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

struct sLance {
    using size = u16;
    static constexpr u32 ITEMS_MAX = 216;

    enum e : size {
        NONE,
        IRON_LANCE,
        IRON_LANCE_PLUS,
        KNIGHT_LANCE,
        KNIGHT_LANCE_PLUS,
        RAMPART,
        RAMPART_PLUS,
        BABEL_SPEAR,
        ELDER_BABEL_SPEAR,
        DRILL_LANCE,
        MEGADRILL_LANCE,
        EIFERSCHILD,
        FIEBERSCHILD,
        ZORNESSCHILD,
        L_EGIDE,
        VENOM_LANCE,
        BASARIOS_VENOM_SPEAR,
        GRAVIOS_SPEAR,
        BLACK_GRAVIOS_SPEAR,
        DIABLOS_LANCE,
        DIABLOS_SPEAR,
        FRAGRANCE,
        FRAGRANCE_PLUS,
        DARK_SPEAR,
        DARKNESS,
        BONE_LANCE,
        BONE_LANCE_PLUS,
        SPIKED_SPEAR,
        SPIKED_SPEAR_PLUS,
        SPIKED_JAVELIN,
        TIGER_STINGER,
        TIGREX_LANCE,
        ACCURSED_STINGER,
        PREDATOR_GRAVELANCE,
        DIOS_STINGER,
        DIOS_STINGER_PLUS,
        DEMOLITION_LANCE,
        RED_TAIL,
        SPEAR_OF_PROMINENCE,
        SILVER_PROMINENCE,
        BLUE_PROMINENCE,
        TWISTCRAWL,
        TWISTCRAWL_PLUS,
        TWISTING_CRAWLER,
        PARATWIST_CRAWLER,
        EXPELGER,
        EXPELGORER,
        EXPELGORER_PLUS,
        EXPELGOUGER,
        REXPELGOUGER,
        GOBULU_MURUKA,
        GOBULUKU_MURAAKA,
        USURPER_S_COMING,
        DESPOT_S_CACOPHONY,
        DEMON_LANCE,
        GREAT_DEMON_LANCE,
        BRIMSTREN_DRAKETAIL,
        CARNIVICIOUS,
        CARNIVICIOUS_PLUS,
        OMNIVICIOUS,
        OMNIVORACIOUS,
        INCESSANT_RAVEN,
        INCESSANT_WOLF,
        INCESSANT_CROW,
        BLUE_CRATER,
        DOOM_CROWN,
        SHARQ_BYTE,
        SHARQ_ATTAQ,
        AQUA_SPEAR,
        STREAM_SPEAR,
        EMERALD_SPEAR,
        WATERY_TRISHULA,
        RUSTED_LANCE,
        TARNISHED_LANCE,
        UNDERTAKER,
        HIGH_UNDERTAKER,
        ICESTEEL_SPEAR,
        DAORA_S_FANG,
        WORN_SPEAR,
        WEATHERED_SPEAR,
        SKYSCRAPER,
        TEOSTRA_S_HOWL,
        TEOSTRA_S_ROAR,
        THUNDERSPEAR,
        THUNDERLANCE,
        GRIEF_LANCE,
        FIENDISH_TOWER,
        SHELLSLAB_PRONG,
        THE_STEADFAST_AEGIS,
        FATALIS_LANCE,
        RUINER_LANCE,
        DUMMY91,
        DUMMY92,
        BABEL_SPEAR__RED_,
        BABEL_SPEAR__YELLOW_,
        BABEL_SPEAR__GREEN_,
        BABEL_SPEAR__BLUE_,
        BABEL_SPEAR__PURPLE_,
        TUSK_LANCE__RED_,
        TUSK_LANCE__YELLOW_,
        TUSK_LANCE__GREEN_,
        TUSK_LANCE__BLUE_,
        TUSK_LANCE__PURPLE_,
        DRAGONRIDER_LANCE__RED_,
        DRAGONRIDER_LANCE__YELLOW_,
        DRAGONRIDER_LANCE__GREEN_,
        DRAGONRIDER_LANCE__BLUE_,
        DRAGONRIDER_LANCE__PURPLE_,
        FRONTRIDER_LANCE__RED_,
        FRONTRIDER_LANCE__YELLOW_,
        FRONTRIDER_LANCE__GREEN_,
        FRONTRIDER_LANCE__BLUE_,
        FRONTRIDER_LANCE__PURPLE_,
        SILVER_PROMINENCE__RED_,
        SILVER_PROMINENCE__YELLOW_,
        SILVER_PROMINENCE__GREEN_,
        SILVER_PROMINENCE__BLUE_,
        SILVER_PROMINENCE__PURPLE_,
        ELDER_BABEL_SPEAR_PLUS,
        LOST_BABEL,
        MEGADRILL_LANCE_PLUS,
        GIGADRILL_LANCE,
        LE_SAGE,
        LE_PALADIN,
        LUZIFERSAGE,
        GRAVIOS_LANCE,
        GRAVIOS_FIRE_LANCE,
        CERA_CREOS,
        BLACK_TEMPEST,
        RISING_TEMPEST,
        CRIMSON_LANCE,
        CRIMSON_LANCE_PLUS,
        CRIMSON_WAR_PIKE,
        CRIMSON_MONOBLOS,
        CERAMIC_BLOS_LANCE,
        WHITE_CATASTROPHE,
        DARKNESS_PLUS,
        DARK_DIN,
        PREDATOR_GRAVELANCE_PLUS,
        RUTHLESS_GRISLANCE,
        ALBRACH_DEMOLISHER,
        LIGHTBREAK_LANCE,
        SOL_PROMINENCE,
        IMMACULATE_SOUL,
        SOUL_OF_PROMINENCE,
        PARAGYRED_SPEAR,
        ASCLEPIUS,
        JINGANA,
        JINGANA_PLUS,
        CADUCEUS,
        BONE_CLAW_LANCE,
        BONE_CLAW_LANCE_PLUS,
        SHELL_CLAW_LANCE,
        GIANT_HERMITAUR_LANCE,
        SHURABA_LANCE,
        SHULA_SHURABA,
        WHITE_SHULA_SHURABA,
        GOBULALUKU_BOSCA,
        GOBULALUKU_AQUIR,
        OPPRESSOR_S_GENESIS,
        ORCUS_GALEUS,
        BRIMSTREN_DRAKETAIL_PLUS,
        STYGIAN_IRA,
        DEMONLANCE_RAJANG,
        DEMONRAY_RAJANG,
        THE_DESTRUCTOR,
        ICELANCE_ZAMTRIOS,
        CARNELIAN_LANCE,
        CRYSTALIS,
        TUSK_LANCE,
        SABERTOOTH,
        INCESSANT_POX,
        GARUGA_INCESSANCE,
        ETERNO_CROWN,
        AIDION_CROWN,
        SHARQ_ASSAWLT,
        WATERY_TRISHULA_PLUS,
        ESMERALDA_S_TIDE,
        NEO_UNDERTAKER,
        DAORA_S_REGULUS,
        ELDAORA_S_FANG,
        SKYSCRAPER_PLUS,
        SKYSUNDERER,
        TEOSTRA_S_FIRE,
        NAAR_THUNDERLANCE,
        NERO_S_ATROCITY,
        UKANLOS_CALAMITY,
        UKANLOS_SKYSWEEPER,
        ETHELRED_THE_READY,
        DALAMADUR_LANCE,
        GENEROUS_DALAMADUR,
        TRUE_FATALIS_LANCE,
        TRUE_RUINER_LANCE,
        W__FATALIS_LANCE,
        HARBAH_LANCE,
        HARBAH_LANCE_PLUS,
        SEDITIOUS_LANCE,
        HADAD_SEDITION,
        FETTERING_REBUKE,
        AETHER_GEGHARD,
        DUMMY_200,
        DUMMY_201,
        DUMMY_202,
        DUMMY_203,
        SEVEN_STAR_LANCE__RED_,
        SEVEN_STAR_LANCE__YELLOW_,
        SEVEN_STAR_LANCE__GREEN_,
        SEVEN_STAR_LANCE__BLUE_,
        SEVEN_STAR_LANCE__PURPLE_,
        ESTOC__RED_,
        ESTOC__YELLOW_,
        ESTOC__GREEN_,
        ESTOC__BLUE_,
        ESTOC__PURPLE_,
        HELL_PHALANX,
        PURIFICATION
    };

    static const std::array<sWeapon, ITEMS_MAX> str;

    static auto getStr(const e id) noexcept -> const char*;
};

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
