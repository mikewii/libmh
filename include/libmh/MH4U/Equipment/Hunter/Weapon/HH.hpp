#pragma once
#include <libmh/MH4U/Equipment/Hunter/Weapon/Weapon.hpp>
#include <array>

namespace mh {
namespace mh4u {
namespace hunter {
namespace weapon {

struct sHH {
    using size = u16;
    static constexpr u32 ITEMS_MAX = 185;

    enum e : size {
        NONE,
        METAL_BAGPIPE,
        METAL_BAGPIPE_PLUS,
        GREAT_BAGPIPE,
        HEAVY_BAGPIPE,
        HEAVY_BAGPIPE_PLUS,
        FORTISSIMO,
        MASTER_BAGPIPE,
        SFORZANDO,
        KUMMERKLANG,
        GRAMKLANG,
        TRAUERKLANG,
        LA_SEPULTURE,
        VALKYRIE_CHORDMAKER,
        VALKYRIE_CHORDMAKER_PLUS,
        QUEEN_S_CHORDMAKER,
        GOLD_CHORDMAKER,
        KECHA_WHAPPA,
        KECHA_WHAPPA_PLUS,
        KECHACHACHA_WHAPPA,
        KECHA_THUMPA,
        KECHA_CLANGA,
        USURPER_S_GROWL,
        USURPER_S_GROWL_PLUS,
        DESPOT_S_THUNDERCLAP,
        BRIMSTREN_DRAKESONG,
        ICE_KAZOO,
        GLACIAL_KAZOO,
        FROST_GROANER,
        FROST_GENERAL,
        BONE_HORN,
        BONE_HORN_PLUS,
        HUNTER_S_HORN,
        HUNTER_S_HORN_PLUS,
        NATIVE_S_HORN,
        STRIPED_GONG,
        TIGREX_HORN,
        ACCURSED_WAIL,
        HOWLING_GRAVEDRUM,
        DIOS_BELL,
        DIOS_BELL_PLUS,
        DEMOLITION_CHIME,
        KHEZU_HORN,
        KHEZU_FLUTE,
        BLOOD_HORN,
        BLOOD_FLUTE,
        WAR_DRUM,
        WAR_BONGO,
        WAR_CONGA,
        JUNGLE_BONGO,
        JUNGLE_CONGA,
        DANGER_CALL,
        DANGER_CALL_PLUS,
        HAZARDOUS_CALL,
        PARAHAZARD_CALL,
        VELOCIPREY_BALLOON,
        VELOCIPREY_BALLOON_PLUS,
        RAVEN_SHAMISEN,
        WOLF_SHAMISEN,
        CROW_SHAMISEN,
        BASARIOS_ROCK,
        BASARIOS_ROCK_MK_II,
        VOLCANIC_ROCK,
        VOLCANIC_GIG,
        BEATING_GUNROCK,
        FLEETING_GUNROCK,
        VICELLO_NULO__W_,
        VICELLO_UNU__W_,
        VICELLO_DU__W_,
        MAGIA_CHARM,
        DRAGONWOOD_FLUTE,
        WORN_HORN,
        WEATHERED_HORN,
        AVENIR_S_MUSIC_BOX,
        AKANTOR_HORN,
        VEILGLARE_HYMN,
        THE_ASCENDING_STRAIN,
        FATALIS_MENACE,
        DUMMY78,
        DUMMY79,
        MEGAPHONE,
        BASARIOS_ROCK__RED_,
        BASARIOS_ROCK__YELLOW_,
        BASARIOS_ROCK__GREEN_,
        BASARIOS_ROCK__BLUE_,
        BASARIOS_ROCK__PURPLE_,
        HELLSTRINGS__RED_,
        HELLSTRINGS__YELLOW_,
        HELLSTRINGS__GREEN_,
        HELLSTRINGS__BLUE_,
        HELLSTRINGS__PURPLE_,
        SWELL_SHELL__RED_,
        SWELL_SHELL__YELLOW_,
        SWELL_SHELL__GREEN_,
        SWELL_SHELL__BLUE_,
        SWELL_SHELL__PURPLE_,
        GOLD_CHORDMAKER__RED_,
        GOLD_CHORDMAKER__YELLOW_,
        GOLD_CHORDMAKER__GREEN_,
        GOLD_CHORDMAKER__BLUE_,
        GOLD_CHORDMAKER__PURPLE_,
        SFORZANDO_PLUS,
        RINFORZATO,
        LA_JOIE,
        LE_GREGORIEN,
        GRIMMJOIE,
        GOLD_CHORDMAKER_PLUS,
        LUNAR_CHORDMAKER,
        KECHA_CLANGA_PLUS,
        KECHA_CLANVIRA,
        OPPRESSOR_S_BOON,
        BRIMSTREN_DRAKESONG_PLUS,
        STYGIAN_TRISTITIA,
        DENDEN_DAIKO,
        DENDEN_DOOMSOUNDER,
        POLI_AHU_PIPE,
        PHENOMENAL_PHLUTE,
        HOWLING_GRAVEDRUM_PLUS,
        DANCING_GRISDRUM,
        TIMBRAL_DEMOLISHER,
        LIGHTBREAK_TIMBRE,
        SHELL_CASTANET,
        SHELL_CASTANET_PLUS,
        COLORED_CASTANET,
        CRABBY_CASTANET,
        KLICK_KLACK,
        BLOOD_FLUTE_PLUS,
        BRUTE_FLUTE,
        JUNGLE_CONGA_PLUS,
        PRIMAL_CONGA,
        PARAHAZARD_CALL_PLUS,
        PARA_CALAMITY,
        NAJA_WARNING,
        NAJA_ALLUVION,
        POX_SHAMISEN,
        GARUGA_SHAMISEN,
        VOLCANIC_GIG_PLUS,
        CONFLAGRIG,
        HEAVY_FACEMELTER,
        LULLABY_FACEMELTER,
        MAGIA_CHARMBELL,
        ARIA_RHOTA,
        DRAGONWOOD_GODFLUTE,
        AVENIR_S_MUSIC_BOX_PLUS,
        ETERNAL_MUSIC_BOX,
        TEOSTRA_S_TIPLE,
        TEOSTRA_S_ORPHEE,
        DAORA_S_TAUS,
        DAORA_S_BAPHOPHONE,
        ELDAORA_S_TAUS,
        GENIE_S_OCARINA,
        AKANTOR_DARK_MELODY,
        UKANLOS_HORNED_FLUTE,
        UKANLOS_SKYSINGER,
        ORGUEIL_THE_INSANE,
        DALAMADUR_SONG,
        BLOODIED_DALAMADUR,
        FATALIS_MENACE_LUTE,
        FATALIS_MENACE_SOUL,
        FATALIS_ANCIENT_LUTE,
        VADYA_MUSE,
        VADYA_MUSE_PLUS,
        SEDITIOUS_WARHORN,
        MAQAM_SEDITION,
        CLEMENCY_S_PEAL,
        ONYX_TERPSICHORE,
        GLASS_ROYALE,
        QUEEN_S_FLUTE,
        HANDMADE_FROG,
        HUNTER_MASTER,
        EMPEROR_S_SPEECH,
        DUMMY_171,
        DUMMY_172,
        SCORPION_LUTE,
        ENDLESS_LOOP,
        SONIC_GLASS__RED_,
        SONIC_GLASS__YELLOW_,
        SONIC_GLASS__GREEN_,
        SONIC_GLASS__BLUE_,
        SONIC_GLASS__PURPLE_,
        HIDDEN_HARMONIC__RED_,
        HIDDEN_HARMONIC__YELLOW_,
        HIDDEN_HARMONIC__GREEN_,
        HIDDEN_HARMONIC__BLUE_,
        HIDDEN_HARMONIC__PURPLE_
    };

    static const std::array<sWeapon, ITEMS_MAX> str;

    static auto getStr(const e id) noexcept -> const char*;
};

}; /// namespace weapon
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
