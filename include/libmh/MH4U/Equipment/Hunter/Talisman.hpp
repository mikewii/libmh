#pragma once
#include <libmh/types.hpp>
#include <array>

namespace mh {
namespace mh4u {
namespace hunter {

struct sTalisman {
    using size = u8;
    static constexpr u32 ITEMS_MAX = 18;

    enum e : size {
        NONE,
        PAWN_TALISMAN,
        BISHOP_TALISMAN,
        KNIGHT_TALISMAN,
        ROOK_TALISMAN,
        QUEEN_TALISMAN,
        KING_TALISMAN,
        DRAGON_TALISMAN,
        UNKNOWABLE_TALISMAN,
        MYSTIC_TALISMAN,
        HERO_TALISMAN,
        LEGEND_TALISMAN,
        CREATOR_TALISMAN,
        SAGE_TALISMAN,
        MIRACLE_TALISMAN,
        AMBER_TALI_,
        JADE_TALI_,
        EMERY_TALI_
    };

    static const std::array<const char*, ITEMS_MAX> str;

    static auto getStr(const e id) noexcept -> const char*;
};

}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
