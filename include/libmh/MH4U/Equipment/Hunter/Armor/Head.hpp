#pragma once
#include <libmh/MH4U/Equipment/Hunter/Armor/Armor.hpp>
#include <array>

namespace mh {
namespace mh4u {
namespace hunter {
namespace armor {

struct sHead {
    using size = u16;
    static constexpr u32 ITEMS_MAX = 987;

    enum e : size {
        NONE,
        DERRING_HEADGEAR,
        DERRING_HELM_S,
        DERRING_CAP_S,
        LEATHER_HEADGEAR,
        LEATHER_HELM_S,
        LEATHER_CAP_S,
        CHAINMAIL_HEADGEAR,
        CHAINMAIL_HELM_S,
        CHAINMAIL_CAP_S,
        HUNTER_S_HELM,
        HUNTER_S_CAP,
        HUNTER_S_HELM_S,
        HUNTER_S_CAP_S,
        HUNTER_S_HELM_14,
        HUNTER_S_CAP_15,
        BONE_HELM,
        BONE_CAP,
        BONE_HELM_S,
        BONE_CAP_S,
        ALLOY_HELM,
        ALLOY_CAP,
        ALLOY_HELM_S,
        ALLOY_CAP_S,
        ALLOY_HELM_24,
        ALLOY_CAP_25,
        JAGGI_HELM,
        JAGGI_CAP,
        JAGGI_HELM_S,
        JAGGI_CAP_S,
        LUDROTH_HELM,
        LUDROTH_CAP,
        LUDROTH_HELM_32,
        LUDROTH_CAP_33,
        RHENOPLOS_HELM,
        RHENOPLOS_CAP,
        RHENOPLOS_HELM_S,
        RHENOPLOS_CAP_S,
        BNAHABRA_HAT,
        BNAHABRA_HEADPIECE,
        BNAHABRA_HAT_S,
        BNAHABRA_HEADPIECE_S,
        BARROTH_HELM,
        BARROTH_CAP,
        RATHIAN_HELM,
        RATHIAN_CAP,
        RATHIAN_HELM_S,
        RATHIAN_CAP_S,
        RATH_HEART_HELM,
        RATH_HEART_CAP,
        GOLDEN_LUNEHELM,
        GOLDEN_LUNECAP,
        RATHIAN_HELM_52,
        RATHIAN_CAP_53,
        RATH_HEART_HELM_54,
        RATH_HEART_CAP_55,
        EX_RATHIAN_HELM,
        EX_RATHIAN_CAP,
        RATHALOS_HELM,
        RATHALOS_CAP,
        RATHALOS_HELM_S,
        RATHALOS_CAP_S,
        RATH_SOUL_HELM,
        RATH_SOUL_CAP,
        SILVER_SOLHELM,
        SILVER_SOLCAP,
        RATHALOS_HELM_66,
        RATHALOS_CAP_67,
        RATH_SOUL_HELM_68,
        RATH_SOUL_CAP_69,
        SILVER_SOLHELM_70,
        SILVER_SOLCAP_71,
        EX_RATHALOS_HELM,
        EX_RATHALOS_CAP,
        DIABLOS_HELM,
        DIABLOS_CAP,
        URAGAAN_HELM,
        URAGAAN_CAP,
        VANGIS_HELM,
        VANGIS_CAP,
        ESURIENT_HELM,
        ESURIENT_CAP,
        ESURIENT_HELM_82,
        ESURIENT_CAP_83,
        GLOAMING_HELM,
        ADAMANCE_HELM,
        DAYSPRING_KASA,
        EPOCH_HELM,
        GLOAMING_ARCHHELM,
        ADAMANCE_ARCHHELM,
        DAYSPRING_ARCHKASA,
        EPOCH_ARCHHELM,
        GLOAMING_HELM_92,
        ADAMANCE_HELM_93,
        DAYSPRING_KASA_94,
        EPOCH_HELM_95,
        PLESIOTH_HELM,
        PLESIOTH_CAP,
        NIBELSNARF_HELM,
        NIBELSNARF_CAP,
        DURAMBOROS_HELM,
        DURAMBOROS_CAP,
        ARZUROS_HELM,
        ARZUROS_CAP,
        LAGOMBI_HELM,
        LAGOMBI_CAP,
        LAGOMBI_HELM_S,
        LAGOMBI_CAP_S,
        SLAGTOTH_HOOD,
        SLAGTOTH_COWL,
        SLAGTOTH_HOOD_S,
        SLAGTOTH_COWL_S,
        NARGACUGA_HELM,
        NARGACUGA_CAP,
        TIGREX_HELM,
        TIGREX_CAP,
        TIGREX_HELM_S,
        TIGREX_CAP_S,
        TIGREX_HELM_U,
        TIGREX_CAP_U,
        EXCELLO_HELM,
        EXCELLO_CAP,
        EX_TIGREX_HELM,
        EX_TIGREX_CAP,
        TIGREX_HELM_124,
        TIGREX_CAP_125,
        TIGREX_HELM_U_126,
        TIGREX_CAP_U_127,
        BRACHYDIOS_HELM,
        BRACHYDIOS_CAP,
        BRACHYDIOS_HELM_S,
        BRACHYDIOS_CAP_S,
        AKANTOR_MASK,
        AKANTOR_FANGS,
        AKANTOR_MASK_134,
        AKANTOR_FANGS_135,
        ZINOGRE_HELM,
        ZINOGRE_CAP,
        ZINOGRE_HELM_S,
        ZINOGRE_CAP_S,
        ZINOGRE_HELM_U,
        ZINOGRE_CAP_U,
        ZINOGRE_HELM_U_142,
        ZINOGRE_CAP_U_143,
        EX_KAISER_CROWN,
        EX_KAISER_MASK,
        KECHA_HELM,
        KECHA_CAP,
        KECHA_HELM_S,
        KECHA_CAP_S,
        GORE_HELM,
        GORE_CAP,
        GORE_HELM_S,
        GORE_CAP_S,
        GORE_HELM_154,
        GORE_CAP_155,
        TETSUCABRA_HELM,
        TETSUCABRA_CAP,
        TETSUCABRA_HELM_S,
        TETSUCABRA_CAP_S,
        NERSCYLLA_HELM,
        NERSCYLLA_CAP,
        NERSCYLLA_HELM_S,
        NERSCYLLA_CAP_S,
        NAJARALA_HELM,
        NAJARALA_CAP,
        NAJARALA_HELM_S,
        NAJARALA_CAP_S,
        GARUGA_HELM,
        GARUGA_CAP,
        GARUGA_HELM_S,
        GARUGA_CAP_S,
        BASARIOS_HELM,
        BASARIOS_CAP,
        BASARIOS_HELM_S,
        BASARIOS_CAP_S,
        BASARIOS_HELM_U,
        BASARIOS_CAP_U,
        BATTLE_HELM,
        BATTLE_CAP,
        VELOCIPREY_HELM,
        VELOCIPREY_CAP,
        VELOCIPREY_HELM_S,
        VELOCIPREY_CAP_S,
        GENPREY_HELM,
        GENPREY_CAP,
        GENPREY_HELM_S,
        GENPREY_CAP_S,
        IOPREY_HELM,
        IOPREY_CAP,
        IOPREY_HELM_S,
        IOPREY_CAP_S,
        KUT_KU_HELM,
        KUT_KU_CAP,
        KUT_KU_HELM_S,
        KUT_KU_CAP_S,
        KUT_KU_HELM_U,
        KUT_KU_CAP_U,
        GYPCEROS_HELM,
        GYPCEROS_CAP,
        GYPCEROS_HELM_S,
        GYPCEROS_CAP_S,
        GYPCEROS_HELM_U,
        GYPCEROS_CAP_U,
        GYPCEROS_HELM_204,
        GYPCEROS_CAP_205,
        GYPCEROS_HELM_U_206,
        GYPCEROS_CAP_U_207,
        CONGA_HELM,
        CONGA_CAP,
        CONGA_HELM_S,
        CONGA_CAP_S,
        CONGA_HELM_U,
        CONGA_CAP_U,
        GRAVIOS_HELM,
        GRAVIOS_CAP,
        GRAVIOS_HELM_S,
        GRAVIOS_CAP_S,
        GRAVIOS_HELM_U,
        GRAVIOS_CAP_U,
        KHEZU_HELM,
        KHEZU_CAP,
        KHEZU_HELM_S,
        KHEZU_CAP_S,
        KHEZU_HELM_U,
        KHEZU_CAP_U,
        KHEZU_HELM_226,
        KHEZU_CAP_227,
        KHEZU_HELM_U_228,
        KHEZU_CAP_U_229,
        KIRIN_HORN,
        KIRIN_CREST,
        KIRIN_HORN_S,
        KIRIN_CREST_S,
        KIRIN_HORN_U,
        KIRIN_CREST_U,
        KIRIN_HORN_236,
        KIRIN_CREST_237,
        KIRIN_HORN_U_238,
        KIRIN_CREST_U_239,
        KUSHALA_GLARE,
        KUSHALA_SNARL,
        EX_KUSHALA_GLARE,
        EX_KUSHALA_SNARL,
        KUSHALA_GLARE_244,
        KUSHALA_SNARL_245,
        EMPRESS_CROWN,
        EMPRESS_MASK,
        CEANATAUR_HELM,
        CEANATAUR_CAP,
        CEANATAUR_HELM_250,
        CEANATAUR_CAP_251,
        AUROROS_HELM,
        BOREALIS_CROWN,
        GENESIS_HEADPIECE,
        GLYPH_CROWN,
        AUROROS_HELM_256,
        BOREALIS_CROWN_257,
        GENESIS_HEADPIECE_258,
        GLYPH_CROWN_259,
        LAVA_HELM,
        LAVA_CAP,
        REMOBRA_HEADGEAR,
        REMOBRA_MASK,
        REMOBRA_HEADGEAR_S,
        REMOBRA_MASK_S,
        ZAMTRIOS_HELM,
        ZAMTRIOS_CAP,
        ZAMTRIOS_HELM_S,
        ZAMTRIOS_CAP_S,
        SELTAS_HELM,
        SELTAS_CAP,
        SELTAS_HELM_S,
        SELTAS_CAP_S,
        ARC_HELM,
        ARC_CAP,
        ARC_HELM_S,
        ARC_CAP_S,
        ARC_HELM_278,
        ARC_CAP_279,
        KAISER_CROWN,
        KAISER_MASK,
        GOLDEN_HEADDRESS,
        PUPPETEER_MASK,
        DIVINE_IRE_HEADDRESS,
        CRUSHER_MASK,
        REBELLION_HELM,
        RIOT_CAP,
        REBELLION_HELM_288,
        RIOT_CAP_289,
        DRAGONHEAD_X,
        DRAGONFACE_X,
        DRAGONHEAD,
        DRAGONFACE,
        FATALIS_CRIMSONHEAD,
        FATALIS_CRIMSONFACE,
        IOPREY_HEAD,
        IOPREY_FACE,
        CEPHALOS_HELM,
        CEPHALOS_CAP,
        MIZUHA_CAP,
        CHAM_HAT,
        HERMITAUR_HELM,
        HERMITAUR_CAP,
        HERMITAUR_HELM_304,
        HERMITAUR_CAP_305,
        VESPOID_HELM,
        VESPOID_CAP,
        QURUPECO_HELM,
        QURUPECO_CAP,
        AGNAKTOR_HELM,
        AGNAKTOR_CAP,
        BARIOTH_HELM,
        BARIOTH_CAP,
        VOLVIDON_HELM,
        VOLVIDON_CAP,
        LAGIACRUS_HELM,
        LAGIACRUS_CAP,
        LAGIACRUS_HELM_318,
        LAGIACRUS_CAP_319,
        HELIOS_HELM,
        HELIOS_CAP,
        SELENE_HELM,
        SELENE_CAP,
        GOBUL_HELM,
        GOBUL_CAP,
        YAMATO_KABUTO,
        HYUGA_HACHIGANE,
        MUTSU_EBOSHI,
        AMAGI_HACHIGANE,
        ESCADORA_WISDOM,
        ESCADORA_SAGESSE,
        ESCADORA_WISDOM_332,
        ESCADORA_SAGESSE_333,
        BLANGO_HELM,
        BLANGO_CAP,
        LOBSTER_HELM,
        LOBSTER_CAP,
        UKANLOS_MASK,
        UKANLOS_FANGS,
        INDRA_HELM,
        STEADFAST_HELM,
        YAKSHA_MASK,
        CARNAGE_HELM,
        ARTIAN_HELM_S,
        ARTIAN_CAP_S,
        DOBER_HELM,
        DOBER_CAP,
        INGOT_HELM,
        INGOT_CAP,
        INGOT_HELM_350,
        INGOT_CAP_351,
        AELUCANTH_VERTEX,
        RHOPESSA_VERTEX,
        AELUCANTH_CLYPEUS,
        RHOPESSA_CLYPEUS,
        OBITUARY_VERTEX,
        BUTTERFLY_VERTEX,
        OBITUARY_CLYPEUS,
        BUTTERFLY_CLYPEUS,
        MOSGHARL_VIZOR,
        MOSGHARL_BRIM,
        GUILD_BARD_LOBOS,
        SCHOLAR_S_BERET,
        GUILD_BARD_HAT,
        SCHOLAR_S_CAP,
        SKULL_VISAGE,
        SKULL_MASK,
        SHADOW_SHADES,
        JAGGI_MASK,
        GARGWA_MASK,
        BLACK_BELT_HELM,
        BLACK_BELT_CAP,
        GUARDIAN_HELM,
        HELPER_HOOD,
        GUARDIAN_MASK,
        HELPER_CAP,
        SHINOBI_SKY_MASK,
        SHINOBI_LAND_MASK,
        GUILD_KNIGHT_FEATHER,
        MAIDEN_S_HAT,
        GUILD_KNIGHT_MASK,
        MAIDEN_S_CAP,
        YUKUMO_SKY_KASA,
        YUKUMO_LAND_KASA,
        HIGH_METAL_HELM,
        HIGH_METAL_CAP,
        RANGER_S_HEADGEAR,
        ARCHER_S_TURBAN,
        DRAGONLORD_S_GAZE,
        CHAKRA_EARRING,
        SAILOR_HAT,
        SAILOR_CAP,
        HAWKHAT,
        LECTURER_S_HOOD,
        HAWKCAP,
        LECTURER_S_CAP,
        FRIENDSHIP_FEATHER,
        VICTORY_FEATHER,
        CHAMPION_FEATHER,
        FELYNE_HAIRBAND,
        CUNNING_SPECS,
        BLUE_STAR_LOBOS,
        BLUE_STAR_HAT,
        DUMMY701,
        HELM_OF_ANAT,
        CAP_OF_ANAT,
        UNIQLO_HOOD,
        UNIQLO_CAP,
        STAR_ROOK_HELM,
        STAR_ROOK_CAP,
        DUMMY706,
        DUMMY718,
        DUMMY707,
        DUMMY719,
        ARTIAN_HELM_S_415,
        ARTIAN_CAP_S_416,
        OVERLORD_FEATHER,
        DUMMY_418,
        HERO_S_HAT,
        DUMMY_420,
        HERO_S_CAP,
        DUMMY729,
        DUMMY730,
        EX_REBELLION_HELM,
        EX_RIOT_CAP,
        EX_GORE_HELM,
        EX_GORE_CAP,
        EX_INGOT_HELM,
        EX_INGOT_CAP,
        EX_KIRIN_HORN_U,
        EX_KIRIN_CREST_U,
        EX_ARTIAN_HELM,
        EX_ARTIAN_CAP,
        STORGE_HELM,
        STORGE_CAP,
        STORGE_HELM_S,
        STORGE_CAP_S,
        STORGE_HELM_438,
        STORGE_CAP_439,
        HUNTER_S_HELM_440,
        HUNTER_S_CAP_441,
        ALLOY_HELM_442,
        ALLOY_CAP_443,
        LUDROTH_HELM_444,
        LUDROTH_CAP_445,
        RATHIAN_HELM_446,
        RATHIAN_CAP_447,
        RATH_HEART_HELM_448,
        RATH_HEART_CAP_449,
        RATHALOS_HELM_450,
        RATHALOS_CAP_451,
        RATH_SOUL_HELM_452,
        RATH_SOUL_CAP_453,
        SILVER_SOLHELM_454,
        SILVER_SOLCAP_455,
        ESURIENT_HELM_456,
        ESURIENT_CAP_457,
        GLOAMING_HELM_458,
        ADAMANCE_HELM_459,
        DAYSPRING_KASA_460,
        EPOCH_HELM_461,
        TIGREX_HELM_462,
        TIGREX_CAP_463,
        TIGREX_HELM_U_464,
        TIGREX_CAP_U_465,
        AKANTOR_MASK_466,
        AKANTOR_FANGS_467,
        ZINOGRE_HELM_U_468,
        ZINOGRE_CAP_U_469,
        GORE_HELM_470,
        GORE_CAP_471,
        GYPCEROS_HELM_472,
        GYPCEROS_CAP_473,
        GYPCEROS_HELM_U_474,
        GYPCEROS_CAP_U_475,
        KHEZU_HELM_476,
        KHEZU_CAP_477,
        KHEZU_HELM_U_478,
        KHEZU_CAP_U_479,
        KIRIN_HORN_480,
        KIRIN_CREST_481,
        KIRIN_HORN_U_482,
        KIRIN_CREST_U_483,
        KUSHALA_GLARE_484,
        KUSHALA_SNARL_485,
        CEANATAUR_HELM_486,
        CEANATAUR_CAP_487,
        AUROROS_HELM_488,
        BOREALIS_CROWN_489,
        GENESIS_HEADPIECE_490,
        GLYPH_CROWN_491,
        ARC_HELM_492,
        ARC_CAP_493,
        REBELLION_HELM_494,
        RIOT_CAP_495,
        DRAGONHEAD_X_496,
        DRAGONFACE_X_497,
        HERMITAUR_HELM_498,
        HERMITAUR_CAP_499,
        LAGIACRUS_HELM_500,
        LAGIACRUS_CAP_501,
        ESCADORA_WISDOM_502,
        ESCADORA_SAGESSE_503,
        INGOT_HELM_504,
        INGOT_CAP_505,
        ARTIAN_HELM_S_506,
        ARTIAN_CAP_S_507,
        STORGE_HELM_508,
        STORGE_CAP_509,
        VELOCIPREY_HELM_X,
        VELOCIPREY_CAP_X,
        IOPREY_HELM_X,
        IOPREY_CAP_X,
        KECHA_HELM_X,
        KECHA_CAP_X,
        KECHA_HELM_Z,
        KECHA_CAP_Z,
        TETSUCABRA_HELM_X,
        TETSUCABRA_CAP_X,
        TETSUCABRA_HELM_Z,
        TETSUCABRA_CAP_Z,
        NERSCYLLA_HELM_X,
        NERSCYLLA_CAP_X,
        NERSCYLLA_HELM_Z,
        NERSCYLLA_CAP_Z,
        ZAMTRIOS_HELM_X,
        ZAMTRIOS_CAP_X,
        ZAMTRIOS_HELM_Z,
        ZAMTRIOS_CAP_Z,
        NAJARALA_HELM_X,
        NAJARALA_CAP_X,
        NAJARALA_HELM_Z,
        NAJARALA_CAP_Z,
        SELTAS_HELM_X,
        SELTAS_CAP_X,
        SELTAS_HELM_Z,
        SELTAS_CAP_Z,
        GORE_HELM_X,
        GORE_CAP_X,
        GORE_CASQUE,
        GORE_COIF,
        ARC_HELM_X,
        STORGE_HELM_X,
        ARC_CAP_X,
        STORGE_CAP_X,
        ARC_CASQUE,
        STORGE_CASQUE,
        ARC_COIF,
        STORGE_COIF,
        GOGMA_HELM,
        GOGMA_CAP,
        GYPCEROS_HELM_X,
        GYPCEROS_CAP_X,
        GYPCEROS_CASQUE,
        GYPCEROS_COIF,
        GYPCEROS_HELM_Z,
        GYPCEROS_CAP_Z,
        GYPCEROS_CASQUE_U,
        GYPCEROS_COIF_U,
        LAGOMBI_HELM_X,
        LAGOMBI_CAP_X,
        CONGA_HELM_X,
        CONGA_CAP_X,
        CONGA_HELM_Z,
        CONGA_CAP_Z,
        KHEZU_HELM_X,
        KHEZU_CAP_X,
        KHEZU_CASQUE,
        KHEZU_COIF,
        KHEZU_HELM_Z,
        KHEZU_CAP_Z,
        KHEZU_CASQUE_U,
        KHEZU_COIF_U,
        RATHIAN_HELM_X,
        RATHIAN_CAP_X,
        RATHIAN_CASQUE,
        RATHIAN_COIF,
        RATH_HEART_HELM_Z,
        RATH_HEART_CAP_Z,
        RATH_HEART_CASQUE,
        RATH_HEART_COIF,
        GOLDEN_LUNEHELM_Z,
        GOLDEN_LUNECAP_Z,
        RATHALOS_HELM_X,
        RATHALOS_CAP_X,
        RATHALOS_CASQUE,
        RATHALOS_COIF,
        RATH_SOUL_HELM_Z,
        RATH_SOUL_CAP_Z,
        RATH_SOUL_CASQUE,
        RATH_SOUL_COIF,
        SILVER_SOLHELM_Z,
        SILVER_SOLCAP_Z,
        SILVER_SOLCASQUE,
        SILVER_SOLCOIF,
        ZINOGRE_HELM_X,
        ZINOGRE_CAP_X,
        ZINOGRE_HELM_Z,
        ZINOGRE_CAP_Z,
        ZINOGRE_CASQUE_U,
        ZINOGRE_COIF_U,
        TIGREX_HELM_X,
        TIGREX_CAP_X,
        TIGREX_CASQUE,
        TIGREX_COIF,
        TIGREX_HELM_Z,
        TIGREX_CAP_Z,
        TIGREX_CASQUE_U,
        TIGREX_COIF_U,
        EXCELLO_HELM_Z,
        EXCELLO_CAP_Z,
        GRAVIOS_HELM_X,
        GRAVIOS_CAP_X,
        GRAVIOS_HELM_Z,
        GRAVIOS_CAP_Z,
        BRACHYDIOS_HELM_X,
        BRACHYDIOS_CAP_X,
        BRACHYDIUM_HELM,
        BRACHYDIUM_CAP,
        GRAND_GLOAMING_HELM,
        GRAND_ADAMANCE_HELM,
        GRAND_DAYSPRING_HELM,
        GRAND_EPOCH_HELM,
        GLOAMING_REDHELM,
        ADAMANCE_REDHELM,
        DAYSPRING_REDHELM,
        EPOCH_REDHELM,
        VANGIS_HELM_X,
        VANGIS_CAP_X,
        ESURIENT_HELM_Z,
        ESURIENT_CAP_Z,
        ESURIENT_CASQUE,
        ESURIENT_COIF,
        KAISER_CROWN_X,
        KAISER_MASK_X,
        AKANTOR_MASK_X,
        AKANTOR_FANGS_X,
        AKANTOR_CASQUE,
        AKANTOR_COIF,
        EX_ESCADORA_WISDOM,
        EX_ESCADORA_SAGESSE,
        UKANLOS_MASK_X,
        UKANLOS_FANGS_X,
        KUSHALA_GLARE_X,
        KUSHALA_SNARL_X,
        KUSHALA_CASQUE,
        KUSHALA_COIF,
        KUJULA_GLARE,
        KUJULA_SNARL,
        GRAND_GOD_S_PEER_MASK,
        GRAND_HERMIT_MASK,
        GRAND_DIVINE_IRE_MASK,
        GRAND_CRUSHER_MASK,
        REBELLION_HELM_X,
        RIOT_CAP_X,
        REBELLION_CASQUE,
        RIOT_COIF,
        REBELLION_HELM_Z,
        RIOT_CAP_Z,
        CEPHALOS_HELM_X,
        CEPHALOS_CAP_X,
        HERMITAUR_HELM_X,
        HERMITAUR_CAP_X,
        HERMITAUR_CASQUE,
        HERMITAUR_COIF,
        HERMITAUR_HELM_Z,
        HERMITAUR_CAP_Z,
        MONO_HELM_S,
        MONO_CAP_S,
        MONO_HELM_X,
        MONO_CAP_X,
        MONO_HELM,
        MONO_CAP,
        MONO_CASQUE,
        MONO_COIF,
        MONODEVIL_HELM,
        MONODEVIL_CAP,
        MONODEVIL_HELM_678,
        MONODEVIL_CAP_679,
        MONODEVIL_CASQUE,
        MONODEVIL_COIF,
        DIABLOS_HELM_X,
        DIABLOS_CAP_X,
        DIABLOS_HELM_684,
        DIABLOS_CAP_685,
        DIABLOS_CASQUE,
        DIABLOS_COIF,
        DIABLOS_HELM_Z,
        DIABLOS_CAP_Z,
        DRAGONHEAD_X_690,
        DRAGONFACE_X_691,
        DRAGONCASQUE_X,
        DRAGONCOIF_X,
        FATALIS_CRIMSONHEAD_Z,
        FATALIS_CRIMSONFACE_Z,
        FATALIS_CRIMSONHELM_Z,
        FATALIS_CRIMSONCOIF_Z,
        WROTHHEAD,
        WROTHFACE,
        WHITE_FATALIS_HEAD,
        WHITE_FATALIS_FACE,
        GRAND_MIZUHA_MASK,
        GRAND_CHAM_HAT,
        REGIOS_HELM_S,
        REGIOS_CAP_S,
        REGIOS_HELM_X,
        REGIOS_CAP_X,
        CHAOS_HELM,
        NEPHILIM_HELM,
        CHAOS_CAP,
        NEPHILIM_CAP,
        BASARIOS_HELM_X,
        BASARIOS_CAP_X,
        BASARIOS_HELM_Z,
        BASARIOS_CAP_Z,
        KUT_KU_HELM_X,
        KUT_KU_CAP_X,
        KUT_KU_HELM_Z,
        KUT_KU_CAP_Z,
        GARUGA_HELM_X,
        GARUGA_CAP_X,
        KIRIN_HORN_X,
        KIRIN_CREST_X,
        KIRIN_CASQUE,
        KIRIN_COIF,
        KIRIN_HORN_Z,
        KIRIN_CREST_Z,
        KIRIN_CASQUE_U,
        KIRIN_COIF_U,
        BNAHABRA_HAT_X,
        BNAHABRA_HEADPIECE_X,
        SLAGTOTH_HOOD_X,
        SLAGTOTH_COWL_X,
        RHENOPLOS_HELM_X,
        RHENOPLOS_CAP_X,
        HUNTER_S_CASQUE,
        HUNTER_S_COIF,
        ALLOY_CASQUE,
        ALLOY_COIF,
        ARTIAN_HELM_X,
        ARTIAN_CAP_X,
        ARC_CASQUE_S,
        ARC_COIF_S,
        INGOT_CASQUE,
        INGOT_COIF,
        GUARDIAN_HELM_X,
        HELPER_HOOD_X,
        GUARDIAN_MASK_X,
        HEALER_GLASSES_X,
        GUARDIAN_HELM_Z,
        HELPER_HOOD_Z,
        GUARDIAN_MASK_Z,
        HEALER_BERET_Z,
        G__KNIGHT_FEATHER_X,
        MAIDEN_S_HAT_X,
        G__KNIGHT_MASK_X,
        EXCLUSIVE_GLASSES_X,
        ACE_HEADGEAR,
        SORORAL_HEADGEAR,
        ACE_MASK,
        SORORAL_MASK,
        CEANATAUR_CASQUE,
        CEANATAUR_COIF,
        LAGIACRUS_CASQUE,
        LAGIACRUS_COIF,
        ESCADORA_CASQUE,
        ESCADORA_COIF,
        AUROROS_KERMES_HELM,
        BOREALIS_BELLFLOWER,
        GENESIS_KERMES_HELM,
        GLYPH_BELLFLOWER,
        LUDROTH_CASQUE,
        LUDROTH_COIF,
        HORNET_HELM,
        HORNET_CAP,
        HORNET_CASQUE,
        HORNET_COIF,
        WROGGI_HAT,
        WROGGI_CAP,
        WROGGI_CASQUE,
        WROGGI_COIF,
        GIGGINOX_HELM,
        GIGGINOX_CAP,
        GIGGINOX_CASQUE,
        GIGGINOX_COIF,
        TEMPEST_CROWN,
        WELKIN_CROWN,
        TEMPEST_DIADEM,
        WELKIN_DIADEM,
        DEATH_STENCH_BRAIN,
        DEATH_STENCH_SOUL,
        DEATH_STENCH_CASQUE,
        DEATH_STENCH_COIF,
        DAMASCUS_HELM,
        DAMASCUS_CAP,
        DAMASCUS_CASQUE,
        DAMASCUS_COIF,
        LAGIACRUS_HELM_X,
        LAGIACRUS_CAP_X,
        NARGACUGA_HELM_X,
        NARGACUGA_CAP_X,
        EMPRESS_CROWN_X,
        EMPRESS_MASK_X,
        HELIOS_HELM_X,
        SELENE_HELM_X,
        HELIOS_CAP_X,
        SELENE_CAP_X,
        AGNAKTOR_HELM_X,
        AGNAKTOR_CAP_X,
        QURUPECO_HELM_X,
        QURUPECO_CAP_X,
        GOBUL_HELM_X,
        GOBUL_CAP_X,
        BARIOTH_HELM_X,
        BARIOTH_CAP_X,
        BARROTH_HELM_X,
        BARROTH_CAP_X,
        NIBELSNARF_HELM_X,
        NIBELSNARF_CAP_X,
        DURAMBOROS_HELM_X,
        DURAMBOROS_CAP_X,
        LAVA_HELM_X,
        LAVA_CAP_X,
        GRAND_YAMATO_KABUTO,
        GRAND_H__HACHIGANE,
        GRAND_MUTSU_JINGASA,
        GRAND_AMAGI_JINGASA,
        GRAND_YAKSHA_MASK,
        GRAND_CARNAGE_HELM,
        MIRALIS_DIADEM,
        MIRALIS_CROWN,
        PLESIOTH_HELM_Z,
        PLESIOTH_CAP_Z,
        KITA_STAR_HEADDRESS,
        UMI_OCEAN_HEADDRESS,
        PATISSIER_S_TOQUE,
        PATISSIER_S_CAP,
        HIGH_METAL_HELM_S,
        HIGH_METAL_CAP_S,
        AELUCANTH_VERTEX_X,
        RHOPESSA_VERTEX_X,
        AELUCANTH_CLYPEUS_X,
        RHOPESSA_CLYPEUS_X,
        OBITUARY_VERTEX_X,
        BUTTERFLY_VERTEX_X,
        OBITUARY_CLYPEUS_X,
        BUTTERFLY_CLYPEUS_X,
        EX_AUROROS_HELM,
        EX_BOREALIS_CROWN,
        EX_GENESIS_HEADPIECE,
        EX_GLYPH_CROWN,
        TEMPEST_CROWN_852,
        WELKIN_CROWN_853,
        GX_ESCADORA_CASQUE,
        GX_ESCADORA_COIF,
        GX_DRAGONCASQUE_X,
        GX_DRAGONCOIF_X,
        STAR_KNIGHT_HELM,
        STAR_KNIGHT_CAP,
        HELM_OF_RAGE,
        CAP_OF_RAGE,
        VARIA_SUIT_VISOR,
        ZERO_SUIT_WIG,
        VARIA_SUIT_SCOPE,
        ZERO_SUIT_HAIRPIECE,
        VARIA_SUIT_VISOR_866,
        ZERO_SUIT_PONYTAIL,
        VARIA_SUIT_SCOPE_868,
        ZERO_SUIT_HAIRPIECE_869,
        DUMMY_870,
        DUMMY_871,
        HAWKHAT_X,
        LECTURER_S_HOOD_X,
        HAWKCAP_X,
        LECTURER_S_CAP_X,
        DUMMY_876,
        DUMMY_877,
        DUMMY_878,
        HAWKEYE_EARRING,
        AMPLIFEATHERS,
        CRAFTSMAN_S_SPECS,
        FELYNE_HAIRBAND_X,
        SWORD_SAINT_EARRING,
        BARRAGE_EARRING,
        CHAOSHROOM,
        VELOCIPREY_MASK,
        GIAPREY_MASK,
        GENPREY_MASK,
        MOSSWINE_MASK,
        BULLDROME_MASK,
        GARUGA_MASK,
        FALSE_FELYNE,
        ACORN_HELM,
        PALICO_BRO,
        GX_HUNTER_S_CASQUE,
        GX_HUNTER_S_COIF,
        GX_ESURIENT_CASQUE,
        GX_ESURIENT_COIF,
        GX_TEMPEST_DIADEM,
        GX_WELKIN_DIADEM,
        GX_FATALIS_CRIMSONHELM,
        GX_FATALIS_CRIMSONCOIF,
        HUNTER_S_HELM_903,
        HUNTER_S_CAP_904,
        HUNTER_S_HELM_905,
        HUNTER_S_CAP_906,
        LUDROTH_HELM_907,
        LUDROTH_CAP_908,
        LUDROTH_HELM_909,
        LUDROTH_CAP_910,
        ALLOY_HELM_911,
        ALLOY_CAP_912,
        ALLOY_HELM_913,
        ALLOY_CAP_914,
        GYPCEROS_HELM_915,
        GYPCEROS_CAP_916,
        GYPCEROS_HELM_917,
        GYPCEROS_CAP_918,
        KHEZU_HELM_919,
        KHEZU_CAP_920,
        KHEZU_HELM_921,
        KHEZU_CAP_922,
        INGOT_HELM_923,
        INGOT_CAP_924,
        INGOT_HELM_925,
        INGOT_CAP_926,
        RATHIAN_HELM_927,
        RATHIAN_CAP_928,
        LAGIACRUS_HELM_929,
        LAGIACRUS_CAP_930,
        RATHALOS_HELM_931,
        RATHALOS_CAP_932,
        GYPCEROS_HELM_U_933,
        GYPCEROS_CAP_U_934,
        KHEZU_HELM_U_935,
        KHEZU_CAP_U_936,
        TIGREX_HELM_937,
        TIGREX_CAP_938,
        KIRIN_HORN_939,
        KIRIN_CREST_940,
        KIRIN_HORN_941,
        KIRIN_CREST_942,
        ARTIAN_HELM_S_943,
        ARTIAN_CAP_S_944,
        CEANATAUR_HELM_945,
        CEANATAUR_CAP_946,
        CEANATAUR_HELM_947,
        CEANATAUR_CAP_948,
        GLOAMING_HELM_949,
        DAYSPRING_KASA_950,
        ADAMANCE_HELM_951,
        EPOCH_HELM_952,
        ZINOGRE_HELM_U_953,
        ZINOGRE_CAP_U_954,
        TIGREX_HELM_U_955,
        TIGREX_CAP_U_956,
        GORE_HELM_957,
        GORE_CAP_958,
        AUROROS_HELM_959,
        GENESIS_HEADPIECE_960,
        BOREALIS_CROWN_961,
        GLYPH_CROWN_962,
        KIRIN_HORN_U_963,
        KIRIN_CREST_U_964,
        KIRIN_HORN_U_965,
        KIRIN_CREST_U_966,
        ESURIENT_HELM_967,
        ESURIENT_CAP_968,
        MONOBLOS_HELM,
        MONOBLOS_CAP,
        HORNET_HELM_971,
        HORNET_CAP_972,
        WROGGI_HAT_973,
        WROGGI_CAP_974,
        MONODEVIL_HELM_975,
        MONODEVIL_CAP_976,
        DIABLOS_HELM_977,
        DIABLOS_CAP_978,
        GIGGINOX_HELM_979,
        GIGGINOX_CAP_980,
        DEATH_STENCH_BRAIN_981,
        DEATH_STENCH_SOUL_982,
        DAMASCUS_HELM_983,
        DAMASCUS_CAP_984,
        DUMMY_985,
        DUMMY_986
    };

    static const std::array<sArmor, ITEMS_MAX> str;

    static auto getStr(const e id) noexcept -> const char*;
};

}; /// namespace armor
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
