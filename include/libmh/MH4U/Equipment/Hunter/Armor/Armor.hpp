#pragma once
#include <libmh/types.hpp>
#include <type_traits>

namespace mh {
namespace mh4u {
namespace hunter {
namespace armor {

template<typename Enum, typename std::enable_if<std::is_enum<Enum>::value>::type>
auto operator | (Enum l, Enum r) noexcept -> Enum
{
    using T = typename std::underlying_type<Enum>::type;

    return static_cast<Enum>(static_cast<T>(l) | static_cast<T>(r));
}

template<typename Enum, typename std::enable_if<std::is_enum<Enum>::value>::type>
auto operator & (Enum l, Enum r) noexcept -> Enum
{
    using T = typename std::underlying_type<Enum>::type;

    return static_cast<Enum>(static_cast<T>(l) & static_cast<T>(r));
}

template<typename Enum, typename std::enable_if<std::is_enum<Enum>::value>::type>
auto operator ^ (Enum l, Enum r) noexcept -> Enum
{
    using T = typename std::underlying_type<Enum>::type;

    return static_cast<Enum>(static_cast<T>(l) ^ static_cast<T>(r));
}

enum class eSeries : u8 {
    NONE        = 0,
    ORIGINAL    = 1 << 0,
    FREEDOM     = 1 << 1,
    TRI         = 1 << 2
};

enum class eLetter : u8 {
    NONE        = 0,
    A           = 1 << 0,
    B           = 1 << 1,
    C           = 1 << 2,
    D           = 1 << 3,
    E           = 1 << 4,
    F           = 1 << 5
};

enum class eType : u8 {
    BOTH        = 0,
    BLADEMASTER = 1 << 0,
    GUNNER      = 1 << 1
};

enum class eGender : u8 {
    BOTH        = 0,
    MALE        = 1 << 0,
    FEMALE      = 1 << 1
};

struct sArmor {
    const char* Name = nullptr;
    eType       Type = eType::BOTH;
    eGender     Gender = eGender::BOTH;
    eSeries     Series = eSeries::NONE;
    eLetter     Letter = eLetter::NONE;
};

}; /// namespace armor
}; /// namespace hunter
}; /// namespace mh4u
}; /// namespace mh
