#pragma once
#include <libmh/types.hpp>

namespace mh {
namespace mh4u {

struct sGqAreaInfo {
    u8  Unk[4];
};

struct sGqBossAreaInfo {
    u8  Unk[7];
};

struct GqBoss {
    u16 ID;
    u16 Unk[19];
};

struct sGuildQuest {
    utf16               OriginalOwnerName[10+2];
    u64                 GQuniqueID;
    u16                 Padding;
    u8                  PoogieFeline;
    u8                  PoogieFelineID;
    u8                  PoogiFelineArea;
    u8                  BiasWeapon;
    u8                  BiasArmorSeries;
    u8                  BiasArmor;
    GqBoss              Monster[5];
    sGqBossAreaInfo     MonsterAreaInfo[5];
    u8                  Padding2;
    sGqAreaInfo         AreaInfo[5];
    u8                  InitialLevel;
    u8                  CurrentLevel;
    u8                  IsRare;
    u8                  MonsterFrenzyStatus[5];
};

}; /// namespace mh4u
}; /// namespace mh
