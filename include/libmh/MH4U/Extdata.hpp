#pragma once
#include <libmh/MH4U/Extentions/qtds.hpp>
#include <libmh/MH/Crypto.hpp>

#include <memory>
#include <functional>
#include <optional>
#include <sstream>

namespace mh {
namespace mh4u {
namespace extdata {

inline constexpr auto FILES_AMMOUNT = 5;
inline constexpr auto QUEST_FILES_AMMOUNT = 40;
inline constexpr auto FILE_DATA_SIZE = 0x50300;
inline constexpr auto FILE_FULL_SIZE = 0x50400;
inline constexpr auto QUEST_SIZE = 0x2010;

#define XOR_MH4U_SIZE 8
#define EXT_QUEST_DATA_PADDING 0xf0
#define EXT_QUEST_DATA_SIZE (QUEST_SIZE * EXT_QUEST_DATA_AMMOUNT) + XOR_MH4U_SIZE + 0x80

#define MH4_EXT_QUEST_FILES_AMMOUNT 4
#define MH4_EXT_QUEST_DATA_AMMOUNT 20
#define MH4_EXT_QUEST_DATA_SIZE (QUEST_SIZE * MH4_EXT_QUEST_DATA_AMMOUNT) + XOR_MH4U_SIZE + 0x80

struct WriterStreamKey {
    std::unique_ptr<std::istream> Stream;
    crypto::Key::e Key = crypto::Key::e::NONE;
};

class Reader
{
private:
    std::istream& m_istream;
    std::stringstream m_full;

public:
    using iostreamFunType = std::function<std::unique_ptr<std::iostream>(const mh4u::qtds::cQTDS&) noexcept(false)>;

    Reader(std::istream& istream);

    auto dumpRaw(std::ostream& ostream) noexcept -> bool;
    auto dumpAll(const iostreamFunType& fun, const bool readLanguages = false) noexcept(false) -> bool;

private:
    auto crypto(void) noexcept -> bool;
};

class Writer
{
private:
    std::ostream& m_ostream;
    std::stringstream m_full;

public:
    using istreamFunType = std::function<std::optional<WriterStreamKey>(const u16) noexcept(false)>;

    Writer(std::ostream& ostream);

    auto writeAll(const istreamFunType& fun) noexcept(false) -> bool;

private:
    auto crypto(void) noexcept -> bool;
    auto lastTouch(void) noexcept -> bool;
};

}; /// namespace extdata
}; /// namespace mh4u
}; /// namespace mh
