#pragma once
#include <libmh/types.hpp>
#include <libmh/attributes.hpp>
#include <libmh/MH4U/Equipment/Hunter/Equipment.hpp>

namespace mh {
namespace mh4u {

#define SAVEFILE_SIZE_BYTES 0x13E00
#define NAME_LENGTH_MAX 10
#define PLAYER_NAME_SIZE_BYTES_MAX ((NAME_LENGTH_MAX + 2) * sizeof(utf16))
#define ITEM_BOX_CELLS_MAX 100 * 14
#define EQUIPMENT_BOX_CELLS_MAX 100 * 15
#define PALICO_EQUIPMENT_BOX_CELLS_MAX 100 * 6

struct sPoogie {
    u8      unk[2];
    utf16   name[NAME_LENGTH_MAX + 2];
    u16     costumeID;
};

struct sGender {
    using size = u8;

    enum e : size {
        MALE = 0,
        FEMALE,

        COUNT
    };
};

struct sFace {
    using size = u8;

    enum e : size {
        Face1 = 0,
        Face2,
        Face3,
        Face4,
        Face5,
        Face6,
        Face7,
        Face8,
        Face9,
        Face10,
        Face11,
        Face12,
        Face13,
        Face14,
        Face15,
        Face16,

        COUNT
    };
};

struct sHairstyle {
    using size = u8;

    enum e : size {
        Hair1 = 0,
        Hair2,
        Hair3,
        Hair4,
        Hair5,
        Hair6,
        Hair7,
        Hair8,
        Hair9,
        Hair10,
        Hair11,
        Hair12,
        Hair13,
        Hair14,
        Hair15,
        Hair16,
        Hair17,
        Hair18,
        Hair19,
        Hair20,
        Hair21,
        Hair22,
        Hair23,
        Hair24,
        Hair25,
        Hair26,
        Hair27,
        Hair28,

        COUNT
    };
};

struct sClothing {
    using size = u8;

    enum e : size {
        Clothing1 = 0,
        Clothing2,
        Clothing3,
        Clothing4,
        Clothing5,
        Clothing6,

        COUNT
    };
};

struct sVoice {
    using size = u8;

    enum e : size {
        Voice1 = 0,
        Voice2,
        Voice3,
        Voice4,
        Voice5,
        Voice6,
        Voice7,
        Voice8,
        Voice9,
        Voice10,
        Voice11,
        Voice12,
        Voice13,
        Voice14,
        Voice15,
        Voice16,
        Voice17,
        Voice18,
        Voice19,
        Voice20,

        COUNT
    };
};

struct sEyeColor {
    using size = u8;

    enum e : size {
        EyeColor1 = 0,
        EyeColor2,
        EyeColor3,
        EyeColor4,
        EyeColor5,
        EyeColor6,
        EyeColor7,
        EyeColor8,
        EyeColor9,
        EyeColor10,

        COUNT
    };
};

struct sFeatures {
    using size = u8;

    enum e : size {
        FeaturesNone = 0,
        Features1,
        Features2,
        Features3,
        Features4,
        Features5,
        Features6,
        Features7,
        Features8,
        Features9,
        Features10,
        Features11,
        Features12,

        COUNT
    };
};

struct sGuildCardMonsters {
    using size = u16;

    enum e : size {
        None = 0,
        Rathian,
        Rathalos,
        PinkRathian,
        AzureRathalos,
        GoldRathian,
        SilverRathalos,
        YianKutKu,
        BlueYianKutKu,
        Gypceros,
        PurpleGypceros,
        Tigrex,
        BruteTigrex,
        Gendrome,
        Iodrome,
        GreatJaggi,
        Velocidrome,
        Congalala,
        EmeraldCongalala,
        Rajang,
        KechaWacha,
        Tetsucabra,
        Zamtrios,
        Najarala,
        DalamadurHead,
        Seltas,
        SeltasQueen,
        Nerscylla,
        GoreMagala,
        ShagaruMagala,
        YianGaruga,
        KushalaDaora,
        Teostra,
        Akantor,
        Kirin,
        OroshiKirin,
        Khezu,
        RedKhezu,
        Basarios,
        RubyBasarios,
        Gravios,
        BlackGravios,
        Deviljho,
        SavageDeviljho,
        Brachydios,
        FuriousRajang,
        DahrenMohran,
        Lagombi,
        Zinogre,
        StygianZinogre,
        Gargwa,
        Rhenoplos,
        Aptonoth,
        Popo,
        SlagtothGreen,
        SlagtothRed,
        Jaggi,
        Jaggia,
        Velociprey,
        Genprey,
        Ioprey,
        Remobra,
        Delex,
        Conga,
        Kelbi,
        Felyne,
        Melynx,
        Altaroth,
        BnahabraBlue,
        BnahabraYellow,
        BnahabraGreen,
        BnahabraRed,
        Zamite,
        KonchuYellow,
        KonchuGreen,
        KonchuBlue,
        KonchuRed,
        Fatalis,
        CrimsonFatalis,
        WhiteFatalis,
        MoltenTigrex,
        Rock0,
        RustedKushalaDaora,
        DalamadurTail,
        Rock1,
        Rock2,
        Rock3,
        Rock4,
        Seregios,
        Gogmazios,
        AshKechaWacha,
        BerserkTetsucabra,
        TigerstripeZamtrios,
        TidalNajarala,
        DesertSeltas,
        DesertSeltasQueen,
        ShroudedNerscylla,
        ChaoticGoreMagala,
        RagingBrachydios,
        Diablos,
        BlackDiablos,
        Monoblos,
        WhiteMonoblos,
        Chameleos,
        Rock5,
        Cephadrome,
        Cephalos,
        DaimyoHermitaur,
        PlumDHermitaur,
        Hermitaur,
        ShahDalamadurHead,
        ShahDalamadurTail,
        ApexRajang,
        ApexDeviljho,
        ApexZinogre,
        ApexGravios,
        Ukanlos,
        FlameFatalis,
        Apceros,
        ApexDiablos,
        ApexTidalNajarala,
        ApexTigrex,
        ApexSeregios,

        Total
    };
};

struct sGuildCard_MonsterLog {
    using size = u16;

    enum e : size {
        Seltas = 0,
        DesertSeltas,
        Lagombi,
        GreatJaggi,
        Velocidrome,
        Gendrome,
        Iodrome,
        Cephadrome,
        KechaWacha,

        AshKechaWacha,
        Tetsucabra,
        BerserkTetsucabra,
        Zamtrios,
        TigerstripeZamtrios,
        Najarala,
        TidalNajarala,
        SeltasQueen,
        DesertSeltasQueen,

        Nerscylla,
        ShroudedNerscylla,
        DaimyoHermitaur,
        PlumDHermitaur,
        GoreMagala,
        ShagaruMagala,
        Seregios,
        Rathian,
        PinkRathian,

        GoldRathian,
        Rathalos,
        AzureRathalos,
        SilverRathalos,
        Zinogre,
        StygianZinogre,
        Diablos,
        BlackDiablos,
        Monoblos,

        WhiteMonoblos,
        Brachydios,
        Deviljho,
        Tigrex,
        BruteTigrex,
        MoltenTigrex,
        YianKutKu,
        BlueYianKutKu,
        YianGaruga,

        Gypceros,
        PurpleGypceros,
        Basarios,
        RubyBasarios,
        Gravios,
        BlackGravios,
        Khezu,
        RedKhezu,
        Congalala,

        EmeraldCongalala,
        Rajang,
        Kirin,
        OroshiKirin,
        KushalaDaora,
        Teostra,
        Chameleos,
        Akantor,
        Ukanlos,

        DahrenMohran,
        Dalamadur,
        ShahDalamadur,
        Gogmazios,
        Fatalis
    };
};

PACKED_BEGIN
struct sBoxItem {
    u16 ID;
    u16 Ammount;

    constexpr sBoxItem()
        : ID{0}, Ammount{0} {}
} PACKED;
PACKED_END

union sBoxEquip {
    uEquipment Equipment;
};

}; /// namespace mh4u
}; /// namespace mh
