#pragma once
#include <libmh/MH4U/Save/Common.hpp>
#include <libmh/MH4U/Save/Options.hpp>
#include <libmh/MH4U/Equipment/Palico/Equipment.hpp>
#include <libmh/MH4U/Quest/GuildQuest.hpp>
#include <libmh/MH4U/GuildCard.hpp>
#include <libmh/attributes.hpp>

#include <string>
#include <memory>
#include <iosfwd>

namespace mh {
namespace mh4u {
namespace savefile {

PACKED_BEGIN
struct sColor {
    u8 R;
    u8 G;
    u8 B;

    constexpr sColor()
        : R{0}, G{0}, B{0} {}

    constexpr sColor(const u8 r, const u8 g, const u8 b)
        : R{r}, G{g}, B{b} {}

    constexpr sColor(const int r, const int g, const int b)
        : R{static_cast<u8>(r)}, G{static_cast<u8>(g)}, B{static_cast<u8>(b)} {}
} PACKED;
PACKED_END

struct sMonsterLog_Crowns {
    u16 Small;
    u16 Big;
};

PACKED_BEGIN
struct Header {
    utf16           PlayerName[NAME_LENGTH_MAX + 2];

    sGender::e      Gender;
    sFace::e        Face;
    sHairstyle::e   HairStyle;
    sClothing::e    Clothing;
    sVoice::e       Voice;
    sEyeColor::e    EyeColor;
    sFeatures::e    FaceFeatures;

    sColor          FaceFeatureColor;
    sColor          HairColor;
    sColor          ClothingColor;
    sColor          SkinTone;

    u8              Padding; // ?
    u32             HunterRank;
    u32             HunterRankPoints;
    u32             Zenny;
    u32             Playtime; // seconds
    u32             Padding1;

    uEquipment      Weapon;
    uEquipment      Head;
    uEquipment      Chest;
    uEquipment      Arms;
    uEquipment      Waist;
    uEquipment      Legs;
    uEquipment      Talisman;

    // 0xFF if none equipped
    u16             WeaponEquipedBoxSlot;
    u16             ChestEquipedBoxSlot;
    u16             ArmsEquipedBoxSlot;
    u16             WaistEquipedBoxSlot;
    u16             LegsEquipedBoxSlot;
    u16             HeadEquipedBoxSlot;
    u16             TalismanEquipedBoxSlot;

    // 0x11A flags?

    u8 unk[76];

    sBoxItem        BoxItems[ITEM_BOX_CELLS_MAX];
    sBoxEquip       BoxEquipment[EQUIPMENT_BOX_CELLS_MAX];
    sBoxPalicoEquip BoxPalicoEquip[PALICO_EQUIPMENT_BOX_CELLS_MAX];

    u16             Padding4;
    sPalico         MainPalico;

    /*
     * quest flags
     * story flags
     * world map flags
     * card list
     * hunters for hire
     * current city
     * wycoon
     * village requests
    */

    // Guild card:
    // 0xC598
    u16             GuildCard_MonsterLog_Hunted[static_cast<u16>(sGuildCardMonsters::Total)];
    u8              Padding5[10];

    u16             GuildCard_MonsterLog_Captured[static_cast<u16>(sGuildCardMonsters::Total)];
    u8              Padding6[10];

    u8              Padding7[4];

    // 0xC79C
    sMonsterLog_Crowns GuildCard_MonsterLog_Size[static_cast<u16>(sGuildCardMonsters::Total)];
    u8              Padding8[16];

    // 0xC998
    sPoogie         Poogie;

    u8              Flags[320];

    sBoxItem        GunnerPouch[8];
    sBoxItem        ItemPouch[24];

    options::Options    Options;

    u8              Unk1[4192];

    u8              QuestFlags[64]; // 08377440 | loads at 00C22054

    u8              Unk2[116];

    // TODO: find quest pool
    // 42 gq in quest pool
    sGuildQuest     GuildQuestRegistered[10];
    u8              Unk3[4];
    u32             Fillers[3];
    u32             Padding9[2];
    u32             CaravanPoints;

    u32             Unk4[677];

    // 0xE8A4
    // 0xECB0

    sPalico         FirstStringersPalico[5]; // 0xF338
    sPalico         ReservedPalico[50];
    u32             Unk5[60]; // padding?
    sGuildCard      GuildCard; // 0x12600









    // TODO: finish
} PACKED; // remove once finished
PACKED_END

class cSaveFile
{
private:
    bool m_valid = false;

public:
    cSaveFile();

    auto isValid(void) const noexcept -> bool;

    auto getPlayerName(void) const noexcept -> std::u16string;
    auto setPlayerName(const std::u16string& name) noexcept -> bool;

    auto getGender(void) const noexcept -> sGender::e;
    auto setGender(const sGender::e gender) noexcept -> void;

    auto getFace(void) const noexcept -> sFace::e;
    auto setFace(const sFace::e face) noexcept -> void;

    auto getHairstyle(void) const noexcept -> sHairstyle::e;
    auto setHairstyle(const sHairstyle::e hairstyle) noexcept -> void;

    auto getClothing(void) const noexcept -> sClothing::e;
    auto setClothing(const sClothing::e clothing) noexcept -> void;

    auto getVoice(void) const noexcept -> sVoice::e;
    auto setVoice(const sVoice::e voice) noexcept -> void;

    auto getEyeColor(void) const noexcept -> sEyeColor::e;
    auto setEyeColor(const sEyeColor::e eye_color) noexcept -> void;

    auto getFaceFeatures(void) const noexcept -> sFeatures::e;
    auto setFaceFeatures(const sFeatures::e features) noexcept -> void;

    auto getFaceFeaturesColor(void) const noexcept -> sColor;
    auto setFaceFeaturesColor(const sColor color) noexcept -> void;

    auto getHairColor(void) const noexcept -> sColor;
    auto setHairColor(const sColor color) noexcept -> void;

    auto getClothingColor(void) const noexcept -> sColor;
    auto setClothingColor(const sColor color) noexcept -> void;

    auto getSkinTone(void) const noexcept -> sColor;
    auto setSkinTone(const sColor color) noexcept -> void;

    auto getHunterRank(void) const noexcept -> u32;
    auto setHunterRank(const u32 value) noexcept -> void;

    auto getHunterRankPoints(void) const noexcept -> u32;
    auto setHunterRankPoints(const u32 value) noexcept -> void;

    auto getZenny(void) const noexcept -> u32;
    auto setZenny(const u32 value) noexcept -> void;

    auto getPlaytime(void) const noexcept -> u32;
    auto setPlaytime(const u32 value) noexcept -> void;

    auto getEquippedWeapon(void) const noexcept -> const uEquipment&;
    auto setEquippedWeapon(const uEquipment& equipment) noexcept -> void;
    auto getEquippedHead(void) const noexcept -> const uEquipment&;
    auto setEquippedHead(const uEquipment& equipment) noexcept -> void;
    auto getEquippedChest(void) const noexcept -> const uEquipment&;
    auto setEquippedChest(const uEquipment& equipment) noexcept -> void;
    auto getEquippedArms(void) const noexcept -> const uEquipment&;
    auto setEquippedArms(const uEquipment& equipment) noexcept -> void;
    auto getEquippedWaist(void) const noexcept -> const uEquipment&;
    auto setEquippedWaist(const uEquipment& equipment) noexcept -> void;
    auto getEquippedLegs(void) const noexcept -> const uEquipment&;
    auto setEquippedLegs(const uEquipment& equipment) noexcept -> void;
    auto getEquippedTalisman(void) const noexcept -> const uEquipment&;
    auto setEquippedTalisman(const uEquipment& equipment) noexcept -> void;

    auto getWeaponEquipedBoxSlot(void) const noexcept -> u16;
    auto setWeaponEquipedBoxSlot(const u16 value) noexcept -> void;
    auto getChestEquipedBoxSlot(void) const noexcept -> u16;
    auto setChestEquipedBoxSlot(const u16 value) noexcept -> void;
    auto getArmsEquipedBoxSlot(void) const noexcept -> u16;
    auto setArmsEquipedBoxSlot(const u16 value) noexcept -> void;
    auto getWaistEquipedBoxSlot(void) const noexcept -> u16;
    auto setWaistEquipedBoxSlot(const u16 value) noexcept -> void;
    auto getLegsEquipedBoxSlot(void) const noexcept -> u16;
    auto setLegsEquipedBoxSlot(const u16 value) noexcept -> void;
    auto getHeadEquipedBoxSlot(void) const noexcept -> u16;
    auto setHeadEquipedBoxSlot(const u16 value) noexcept -> void;
    auto getTalismanEquipedBoxSlot(void) const noexcept -> u16;
    auto setTalismanEquipedBoxSlot(const u16 value) noexcept -> void;

    auto getBoxItem(const std::size_t id, sBoxItem& item) const noexcept -> bool;
    auto setBoxItem(const std::size_t id, const sBoxItem& item) const noexcept -> bool;

    auto getEquipmentBoxItem(const std::size_t id, sBoxEquip& item) const noexcept -> bool;
    auto setEquipmentBoxItem(const std::size_t id, const sBoxEquip& item) const noexcept -> bool;

    auto getPalicoEqBoxItem(const std::size_t id, sBoxPalicoEquip& item) const noexcept -> bool;
    auto setPalicoEqBoxItem(const std::size_t id, const sBoxPalicoEquip& item) const noexcept -> bool;

private:
    std::unique_ptr<Header> m_header;

    auto readHeader(std::istream& istream) noexcept -> bool;

    friend auto _parse(std::istream& istream, cSaveFile& obj) noexcept -> bool;
    friend auto parse(std::istream& istream, cSaveFile& obj, const bool encrypted) noexcept -> bool;
};

auto parse(std::istream& istream, cSaveFile& obj, const bool encrypted = true) noexcept -> bool;

}; /// namespace savefile
}; /// namespace mh4u
}; /// namespace mh
