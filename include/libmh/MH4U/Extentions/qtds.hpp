#pragma once
#include <array>

#include <libmh/MH4U/Equipment/Hunter/Equipment.hpp>
#include <libmh/MH4U/Quest/Common.hpp>
#include <libmh/MH4U/Items/Items.hpp>

namespace mh {
namespace mh4u {
namespace qtds {

/*
 * reward box screen 1 have 8*2 and 8*4 cells
 * reward box screen 2 have 4*1(green, sub) 4*1(grey) 2*8(red, base) 3*8(blue, wound/cap)
*/
#define MAIN_REWARD_BOX_A_MAX_ITEMS 2 * 8
#define MAIN_REWARD_BOX_B_MAX_ITEMS 4 * 8
#define SUB_REWARD_BOX_MAX_ITEMS 4 * 1

inline constexpr auto TITLE_CHAR_MAX = 31;
inline constexpr auto MAIN_GOAL_CHAR_MAX = 31;
inline constexpr auto FAILURE_CHAR_MAX = 63;

inline constexpr auto SUPPLY_BOX_MAX_ITEMS = 5 * 8;

template <typename T>
struct Geometry3 {
    T X;
    T Y;
    T Z;
};

enum eQuestLanguage {
    JAPANESE = 0,
    KOREAN = JAPANESE,
    ENGLISH = JAPANESE,
    FRENCH,
    SPANISH,
    GERMAN,
    ITALIAN,

    COUNT
};

enum class eText {
    TITLE = 0,
    MAIN_GOAL,
    FAILURE,
    SUMMARY,
    MAIN_MONSTER,
    CLIENT,
    SUB_QUEST,

    COUNT
};

//////
enum EnemyWaveType {
    LargeEnemy = 0,
    SmallEnemy,
};

struct sArenaFence {
    sArenaFenceEnabled::e   IsEnabled;
    sArenaFenceStatus::e    Status;
    u8                      UpTimeSeconds;
    u8                      DownTimeSeconds;
};

struct sEm {
    sEnemy::e           ID;
    u8                  Padding0[2];

    /* [q0060245] Taiko: Big Bellied Bruisers and
     * [q0000351] have it set to 0
     * but monster still spawns, all other quests have it set at least to 1
     */
    u32                 RespawnCount;
    u8                  SpawnCondition; // for large monsters it always on 255
    sSpawnArea::e       SpawnArea;

    /*
     * [0] 0 1 3 4 5 6      on small monsters only
     * [1] 0 1 2 ... 101    size related?
     * [2] 0 1              size related?
     * [3] 0 1 2 3
     * [4] 0 7 8 9
    */
    u8                  Unk[5];

    u8                  FrenzyStatus;   // 0 1 2 3 9 22 24
    Geometry3<float>    Position;

    /*
     * [0] 91 109 182 40778 65172 65354 on small monsters only
     * [1] various up to u16 max
     * [2] 91 182 21481 65354
    */
    u32                 Unk1[3];


    sEm();

    auto operator == (const sEm& other) const noexcept -> bool;
    auto operator != (const sEm& other) const noexcept -> bool;
};

struct sEmHash {
    auto operator () (const sEm& other) const noexcept -> std::size_t;
};

struct sIntruder {
    u32 AppearChance;
    sEm Intruder;
};

struct sSmallMonsterArea {
    u32 p_SmallMonster;
};

union sEnemyWave {
    // ends with 0x00000000 0x00000000 0x00000000 0xFFFF0000 0xFFFFFFFF and(?) 0x00000000 0xFF000000
    // or just null terminated with 6 u32, 6th must be null
    // if null terminated other wave pointers might be placed right after
    // fpr 1 0x00000000 0x00000000 0x00000000 0xFFFF0000 0xFFFFFFFF and(?) 0x00000000 0xFF000000
    // for 2 0x00000000 0x00000000 0xFFFF0000 0xFFFFFFFF and(?) 0x00000000 0xFF000000
    // for 3 0x00000000 0x00000000 0x00000000 0xFFFF0000 0xFFFFFFFF and(?) 0x00000000 0xFF000000
    u32 p_large_enemy_list;
    u32 p_intruder; // 16 byte aligned, terminated by 0xFFFF0000 0xFFFFFFFF
    u32 p_small_enemy_groups;
};

enum ItemBoxID {
    SUPPLY_BOX = 0,
    REFILL_SUPPLIES_1,
    REFILL_SUPPLIES_2,
    REFILL_SUPPLIES_3,
    MAIN_REWARD_BOX_A,
    MAIN_REWARD_BOX_B,
    SUB_REWARD_BOX
};

struct sItem {
    sItems::ID  ID;
    u16         Ammount;
};

struct sRewardBoxItem {
    u16     Chance;
    sItem   Item;
};

struct sHunterItems {
    static constexpr u32 ITEMS_MAX = 8 * 3;
    static constexpr u32 GUNNER_ITEMS_MAX = 8;

    sItem GunnersPouch[GUNNER_ITEMS_MAX];
    sItem ItemPouch[ITEMS_MAX];
};

struct sItemBoxFlags {
    u8  ID;
    u8  SlotsNum;
    u16 Unk;
};

struct sItemBox {
    sItemBoxFlags Flags;

    u32 pItemsBox;
};

struct sObjective {

    // type0 1 == hunt
    // type0 4 + type1 64 == suppress frenzy | use enemy_id and count
    // type0 4 + type1 32 == topple while mounted
    // type0 4 + type1 2 == break part | use enemy_id
    u8  Type0; // 0 1 2 4 129
    u8  Type1; // 0 1 2 8 32 64
    u16 Padding0;

    union {
        u16 ID;
        sEnemy::e EnemyID;
    };

    union {
        u16 Count;
        sEnemyParts::e Part;   // 0 1 2 3 4 5 7
    };
};

struct sFlags {
    sQuestType::e    QuestType;
    sQuestFlags0::e  Flags0;
    sQuestFlags1::e  Flags1;
    sQuestFlags2::e  Flags2;

    u32 PostingFee;
    u32 RewardZenny;
    u32 PenaltyZenny;
    u32 SubRewardZenny;

    u32 QuestTime; // minutes
    u32 IntruderProbabilityPercent; // 0 20 30 50 60 70 80 100
    u32 rpsTextLanguages;
    u16 QuestID;
    sQuestStars::e QuestStars;
    sMap::e Map;
    sRequirements::e Requirements[2];
    u8  Padding0;   // null on all quests
    u8  DialogID;

    u8  Padding1[2];

    u8  Unk0;   // 1 2 3 - 1 arena single, 2 arena multiple, 3 Steak Your Ground | Gypceros Overload

    sObjective Main1;
    sObjective Main2;
    sObjective Sub;

    u32 rpChallengePresets;
    u16 QuestIconID[5];
    u16 Unk1;


    auto getQuestType(void) const noexcept -> sQuestType::e;
    auto setQuestType(const sQuestType::e type) noexcept -> void;

    auto getFlags0(void) const noexcept -> sQuestFlags0::e;
    auto setFlags0(const sQuestFlags0::e flags) noexcept -> void;

    auto getFlags1(void) const noexcept -> sQuestFlags1::e;
    auto setFlags1(const sQuestFlags1::e flags) noexcept -> void;

    auto getFlags2(void) const noexcept -> sQuestFlags2::e;
    auto setFlags2(const sQuestFlags2::e flags) noexcept -> void;

    auto getPostingFee(void) const noexcept -> u32;
    auto setPostingFee(const u32 value) noexcept -> void;

    auto getRewardZenny(void) const noexcept -> u32;
    auto setRewardZenny(const u32 value) noexcept -> void;

    auto getPenaltyZenny(void) const noexcept -> u32;
    auto setPenaltyZenny(const u32 value) noexcept -> void;

    auto getSubRewardZenny(void) const noexcept -> u32;
    auto setSubRewardZenny(const u32 value) noexcept -> void;

    auto getQuestTimeMinutes(void) const noexcept -> u32;
    auto setQuestTimeMinutes(const u32 value) noexcept -> void;

    auto getIntruderProbability(void) const noexcept -> u32;
    auto setIntruderProbability(const u32 value) noexcept -> void;

    auto getQuestId(void) const noexcept -> u16;
    auto setQuestId(const u16 id) noexcept -> void;

    auto getQuestStars(void) const noexcept -> sQuestStars::e;
    auto setQuestStars(const sQuestStars::e stars) noexcept -> void;

    auto getMap(void) const noexcept -> sMap::e;
    auto setMap(const sMap::e id) noexcept -> void;

    auto getRequirements0(void) const noexcept -> sRequirements::e;
    auto setRequirements0(const sRequirements::e value) noexcept -> void;
    auto getRequirements1(void) const noexcept -> sRequirements::e;
    auto setRequirements1(const sRequirements::e value) noexcept -> void;

    auto getDialogID(void) const noexcept -> u8;
    auto setDialogID(const u8 value) noexcept -> void;

    auto getObjectiveMain1(void) const noexcept -> sObjective;
    auto setObjectiveMain1(const sObjective value) noexcept -> void;

    auto getObjectiveMain2(void) const noexcept -> sObjective;
    auto setObjectiveMain2(const sObjective value) noexcept -> void;

    auto getObjectiveSub(void) const noexcept -> sObjective;
    auto setObjectiveSub(const sObjective value) noexcept -> void;

    auto getIcon0(void) const noexcept -> u16;
    auto getIcon1(void) const noexcept -> u16;
    auto getIcon2(void) const noexcept -> u16;
    auto getIcon3(void) const noexcept -> u16;
    auto getIcon4(void) const noexcept -> u16;
    auto setIcon0(const u16 value) noexcept -> void;
    auto setIcon1(const u16 value) noexcept -> void;
    auto setIcon2(const u16 value) noexcept -> void;
    auto setIcon3(const u16 value) noexcept -> void;
    auto setIcon4(const u16 value) noexcept -> void;

};

struct sMonsterStats {
    u16	SizePercent;                // 0 ... 300
    u8  SizeVariationPercent;       // 0 ... 40
    u8  HealthPercent;              // 0 ... 95
    u8  AttackPercent;              // 0 ... 119
    u8  DefencePercent;             // 0 ... 140
    u8  Stamina;                    // 0 1 2 3 4 table/type?
    u8  Unk;                        // 0 1 2
};

struct sSmallMonsterUnk  {
    u8  Unk[8];
    /*
     * [0] 0 1 2 4 5
     * [1] 0
     * [2] 0
     * [3] 0
     * [4] 0 44 46 ... 73 198
     * [5] 0 3 6
     * [6] 0 1 2 3 ... 58
     * [7] 1 2
    */
};

struct Header {
    static constexpr u32 VERSION = 0x35303076; // "v005"

    u32                 rpsFlags;

    u32                 Version;

    u32                 rpSupplyBoxes;

    u32                 RefillBoxSettings[4];

    u32                 rpMainRewardBoxesA;
    u32                 rpMainRewardBoxesB;
    u32                 rpSubRewardBoxes;

    u32                 rpLargeMonsterWaves;
    u32                 rpSmallMonsterWaves;
    u32                 rpIntruderMonster;

    sMonsterStats       BossStats[5];
    sMonsterStats       SmallMonsStats;
    sSmallMonsterUnk    SmallMonsUnk[2];

    u32                 RewardPoints;
    u32                 PenaltyPoints;
    u32                 SubRewardPoints;

    u8                  Unk3[8];
    /*
     * [0] 0 1 2 3 4 5 6 8 10
     * [1] 0
     * [2] 0 10 20 30 40 50 60 70 80 90 100
     * [3] 0
     * [4] 0 2 4 5 15 99
     * [5] 0
     * [6] 0
     * [7] 0
    */
    u8                  Unk4; // 0 1 2 3 5 7 bitfield?

    sGatheringLv::e     GatheringLevel;
    sCarvingLv::e       CarvingLevel;
    sMonsterAILv::e     MonstersAILevel;
    sPlayerSpawnType::e PlayerSpawnType;

    sArenaFence         ArenaFence;

    u8                  Padding1[15]; // 16 byte alignment
};

class cQTDS
{
public:
    using textArray = std::array<std::u16string, static_cast<std::size_t>(eText::COUNT)>;
    using supplyBox = std::pair<sItemBoxFlags, std::vector<sItem>>;
    using rewardBox = std::pair<sItemBoxFlags, std::vector<sRewardBoxItem>>;
    using enemyWave = std::vector<sEm>;
    using smallEnemyGroup = std::vector<enemyWave>;
    using challengeEquipment = std::array<uEquipment, 8>; // 1 unknown, armorx5, talisman, weapon
    using challengePreset = std::pair<challengeEquipment, sHunterItems>;

private:
    bool m_valid = false;
    Header m_header;
    sFlags m_flags;
    std::vector<textArray> m_languageVector;
    std::vector<challengePreset> m_challengePresets;

    supplyBox m_mainSupplyBox;
    std::vector<supplyBox> m_extraSupplyBoxes;
    std::vector<rewardBox> m_rewardBoxesMainA;
    std::vector<rewardBox> m_rewardBoxesMainB;
    std::vector<rewardBox> m_rewardBoxesSub;

    std::vector<enemyWave> m_largeEnemyWaves;
    std::vector<smallEnemyGroup> m_smallEnemyWaves;
    std::vector<sIntruder> m_intruders;

public:
    auto isValid(void) const noexcept -> bool;

    auto dump(std::ostream& ostream) noexcept -> bool;

    auto getHeader(void) const noexcept -> const Header&;
    auto getFlags(void) const noexcept -> const sFlags&;
    auto getLanguageVector(void) const noexcept -> const std::vector<textArray>&;
    auto getChallengePresets(void) const noexcept -> const std::vector<challengePreset>&;
    auto getMainSupplyBox(void) const noexcept -> const supplyBox&;
    auto getExtraSupplyBoxes(void) const noexcept -> const std::vector<supplyBox>&;
    auto getRewardBoxesMainA(void) const noexcept -> const std::vector<rewardBox>&;
    auto getRewardBoxesMainB(void) const noexcept -> const std::vector<rewardBox>&;
    auto getRewardBoxesSub(void) const noexcept -> const std::vector<rewardBox>&;
    auto getLargeEnemyWaves(void) const noexcept -> const std::vector<enemyWave>&;
    auto getSmallEnemyWaves(void) const noexcept -> const std::vector<smallEnemyGroup>&;
    auto getIntruders(void) const noexcept -> const std::vector<sIntruder>&;

private:
    auto basicCheck(std::istream& istream) noexcept -> bool;
    auto readHeader(std::istream& istream) noexcept -> bool;
    auto readFlags(std::istream& istream) noexcept -> bool;
    auto readLanguages(std::istream& istream) noexcept -> bool;
    auto readText(std::istream& istream, textArray& array) noexcept -> bool;
    auto readChallengePresets(std::istream& istream) noexcept -> bool;
    auto readSupplyBoxes(std::istream& istream) noexcept -> bool;
    auto readRewardBoxes(std::istream& istream) noexcept -> bool;
    auto readEms(std::istream& istream, enemyWave& wave) noexcept -> bool;
    auto readLargeMonsterWaves(std::istream& istream) noexcept -> bool;
    auto readSmallMonsterWaves(std::istream& istream) noexcept -> bool;
    auto readIntruders(std::istream& istream) noexcept -> bool;

    auto isSmallMonsterWavesEmpty(void) const noexcept -> bool;
    auto isLargeMonsterWavesEmpty(void) const noexcept -> bool;
    auto isMonsterWaveEmpty(const enemyWave& wave) const noexcept -> bool;

    auto writeLanguages(std::ostream& ostream) noexcept -> u32;
    auto writeTextArray(std::ostream& ostream, const textArray& array) noexcept -> u32;
    auto writeChallengePresets(std::ostream& ostream) noexcept -> u32;
    auto writeSupplyBoxes(std::ostream& ostream) noexcept -> u32;
    auto writeSupplyBox(std::ostream& ostream, const supplyBox& box) noexcept -> u32;
    auto writeItemBoxFlags(std::ostream& ostream, const sItemBoxFlags& flags, const u32 ptr) noexcept -> void;
    auto writeRewardBoxes(std::ostream& ostream, const std::vector<rewardBox>& boxes) noexcept -> u32;
    auto writeRewardBox(std::ostream& ostream, const rewardBox& box) noexcept -> u32;
    auto writeSmallMonsterWaves(std::ostream& ostream) noexcept -> u32;
    auto writeLargeMonsterWaves(std::ostream& ostream) noexcept -> u32;
    auto writeIntruders(std::ostream& ostream) noexcept -> u32;
    auto writeMonsterWave(std::ostream& ostream, const enemyWave& wave) noexcept -> bool;

    friend auto parse(std::istream& istream, cQTDS& obj) noexcept -> bool;
    friend auto parseLite(std::istream& istream, cQTDS& obj, const bool readLanguages) noexcept -> bool;
};

auto parse(std::istream& istream, cQTDS& obj) noexcept -> bool;
auto parseLite(std::istream& istream, cQTDS& obj, const bool readLanguages = false) noexcept -> bool;

}; /// namespace qtds
}; /// namespace mh4u
}; /// namespace mh
