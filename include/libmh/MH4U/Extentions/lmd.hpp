#pragma once
#include <libmh/types.hpp>
#include <vector>
#include <string>

namespace mh {
namespace mh4u {
namespace lmd {

inline constexpr u32 RESOURCE_HASH = 0x62440501;
inline constexpr u32 NULL_TERMINATOR_SIZE = 2;

struct Data0 {
    u32 Unk[2];
};

struct Data1 {
    u32 Unk[4];
};

struct StringInfo {
    u32 pU16Str;
    u32 Size;
    u32 SizeDuplicate;
};

struct Header {
    static constexpr u32 MAGIC = 0x00646D6C; // "lmd\0"
    static constexpr u32 VERSION = 0x0001010B; // 11 1 1 0

    u32 Magic;
    u32 Version;

    u32 Data0Num;
    u32 Data1Num;
    u32 StringInfoNum;

    u32 pData0;
    u32 pData1;
    u32 pStringInfo;

    u32 pFilename;
};

class cLMD
{
private:
    bool                        m_valid = false;
    Header                      m_header;
    std::vector<Data0>          m_data0;
    std::vector<Data1>          m_data1;
    std::vector<StringInfo>     m_stringInfo;
    std::vector<std::u16string> m_strings;
    std::string                 m_filename;

public:
    auto isValid(void) const noexcept -> bool;

    auto dump(std::ostream& ostream) noexcept -> bool;

    auto replaceString(const std::u16string& str, const std::size_t id) noexcept -> bool;

    auto getHeader(void) const noexcept -> const Header&;
    auto getData0(void) const noexcept -> const std::vector<Data0>&;
    auto getData1(void) const noexcept -> const std::vector<Data1>&;
    auto getStringInfo(void) const noexcept -> const std::vector<StringInfo>&;
    auto getStrings(void) const noexcept -> const std::vector<std::u16string>&;
    auto getFilename(void) const noexcept -> const std::string&;

private:
    auto getExpectedSize(const bool precise = false) const noexcept -> std::size_t;

    auto basicCheck(std::istream& istream) noexcept -> bool;
    auto readHeader(std::istream& istream) noexcept -> bool;
    auto readFilename(std::istream& istream) noexcept -> bool;
    auto readData0(std::istream& istream) noexcept -> bool;
    auto readData1(std::istream& istream) noexcept -> bool;
    auto readStringInfo(std::istream& istream) noexcept -> bool;
    auto readStrings(std::istream& istream) noexcept -> bool;

    friend auto parse(std::istream& istream, cLMD& obj) noexcept -> bool;
};

auto parse(std::istream& istream, cLMD& obj) noexcept -> bool;

}; /// namespace lmd
}; /// namespace mh4u
}; /// namespace mh
