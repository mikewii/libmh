#pragma once
#include <libmh/types.hpp>
#include <iosfwd>
#include <vector>
#include <mbedtls/pk.h>

namespace mh {
namespace crypto {

class PK
{
private:
    static constexpr std::size_t CHUNK = 128 << 7;
    bool m_valid;
    mbedtls_pk_context m_pk;

public:
    enum Format {
        DER,
        PEM
    };

public:
    PK();
    ~PK();

    auto show(void) -> bool;
    auto showPublic(void) -> bool;

    auto writePrivateKey(std::ostream& ostream, Format format = Format::DER) -> bool;
    auto writePublicKey(std::ostream& ostream, Format format = Format::DER) -> bool;

    auto generateRSA(unsigned int size = 2048, int exponent = 65537) -> bool;

    auto importKey(std::istream& istream, const char* password = nullptr) -> bool;
    auto importKey(const std::vector<u8>& key, const char* password = nullptr) -> bool;
    auto importKey(const u8 key[],
                   const std::size_t keySize,
                   const char* password = nullptr) -> bool;
    auto importKeyFile(const char* path, const char* password = nullptr) -> bool;

    auto importPublicKey(std::istream& istream) -> bool;
    auto importPublicKey(const std::vector<u8>& key) -> bool;
    auto importPublicKey(const u8 key[],
                         const std::size_t keySize) -> bool;
    auto importPublicKeyFile(const char* path) -> bool;

    auto verify(const std::vector<u8>& hash,
                const std::vector<u8>& signature,
                const mbedtls_md_type_t mdAlgorithm) -> bool;

    auto sign(const std::vector<u8>& hash,
              std::vector<u8>& signature,
              const mbedtls_md_type_t mdAlgorithm) -> bool;

};

}; /// namespace crypto
}; /// namespace mh
