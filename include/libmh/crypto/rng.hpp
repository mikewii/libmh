#pragma once
#include <mbedtls/ctr_drbg.h>

namespace mh {
namespace crypto {
namespace rng {

extern auto init(const char* personalizationString = "libmh") -> bool;
extern auto exit(void) -> void;
extern auto getRNG(void) -> mbedtls_ctr_drbg_context*;

}; /// namespace rng
}; /// namespace crypto
}; /// namespace mh
