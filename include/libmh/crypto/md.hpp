#pragma once
#include <psa/crypto.h>
#include <iosfwd>
#include <vector>

namespace mh {
namespace crypto {

class MD
{
private:
#if defined(ENABLE_CTRPF)
    inline static constexpr auto CHUNK = 128 << 0;
#else
    inline static constexpr auto CHUNK = 128 << 7;
#endif

    bool m_valid;
    psa_algorithm_t m_algorithm;
    psa_hash_operation_t m_hash_operation;
    std::vector<uint8_t> m_hash;

public:
    MD(psa_algorithm_t algorithm = PSA_ALG_SHA_256);
    ~MD();

    auto isValid(void) const noexcept -> bool;

    // Manual operations
    auto update(const void* data, std::size_t size) -> bool;
    auto finish(void) -> bool;

    // Single shot update and finish
    auto digest(std::istream& istream,
                std::streamsize end = 0) -> bool;

    auto hash(void) const -> const std::vector<uint8_t>&;

    auto algorithm_mbedtls(void) -> mbedtls_md_type_t;
    auto algorithm_psa(void) -> psa_algorithm_t;
};

}; /// namespace crypto
}; /// namespace mh
