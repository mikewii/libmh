#pragma once
#include <cstdio>
#include <mutex>
#include <sstream>
#include <iomanip>

namespace mh {
namespace tools {
namespace log {

extern std::mutex mutex;
extern std::string prefix;
extern bool flush;

inline auto addPrefix(std::stringstream& ss, const std::string& prefix) -> void
{
    auto fmt = ss.flags();

    ss << std::setw(16)
       << std::left
       << prefix
       << ' ';

    ss.flags(fmt);
}

template <typename T>
inline auto appendArg(std::stringstream& ss, const T& arg) -> void
{
    // iomanip functions go here
    if constexpr (std::is_function<T>::value)
        ss << arg;
    else
        ss << arg << ' ';
}

template <typename... Args>
inline auto print(FILE* stream, const bool newline, const Args&... args) -> void
{
    std::lock_guard<std::mutex> lock(mutex);
    std::stringstream ss;

    if (!prefix.empty())
        addPrefix(ss, prefix);

    (appendArg(ss, args), ...);

    if (newline)
        std::fprintf(stream, "%s\n", ss.str().c_str());
    else
        std::fprintf(stream, "%s", ss.str().c_str());

    if (flush)
        std::fflush(stream);
}

auto printMbedtlsErr(FILE* stream, const bool newline, const char* str, const int errcode) -> bool;

inline auto setPrefix(const std::string& str) -> void
{
    std::lock_guard<std::mutex> lock(mutex);

    prefix = str;
}

inline auto setFlush(const bool value) -> void
{
    std::lock_guard<std::mutex> lock(mutex);

    flush = value;
}

inline auto getFlush(void) -> bool
{
    std::lock_guard<std::mutex> lock(mutex);

    return flush;
}

}; /// namespace log
}; /// namespace tools
}; /// namespace mh

#ifndef LOG_GET_FLUSH
#define LOG_GET_FLUSH mh::tools::log::getFlush()
#endif

#ifndef LOG_SET_FLUSH
#define LOG_SET_FLUSH(value) mh::tools::log::setFlush(value)
#endif

#ifndef SET_LOG_PREFIX
#define SET_LOG_PREFIX(str) mh::tools::log::setPrefix(str)
#endif

#ifndef LOG
#define LOG(...) mh::tools::log::print(stdout, true, __VA_ARGS__)
#endif

#ifndef LOG_NO_NEWLINE
#define LOG_NO_NEWLINE(...) mh::tools::log::print(stdout, false, __VA_ARGS__)
#endif

#ifndef LOG_DEBUG
#define LOG_DEBUG(...) mh::tools::log::print(stderr, true, __VA_ARGS__)
#endif

#ifndef LOG_DEBUG_MBEDTLS
#define LOG_DEBUG_MBEDTLS(str, errcode) mh::tools::log::printMbedtlsErr(stderr, true, str, errcode)
#endif
