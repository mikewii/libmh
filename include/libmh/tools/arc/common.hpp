#pragma once
#include <libmh/types.hpp>
#include <string>

namespace mh {
namespace tools {
namespace arc {

struct sVersion {
    using size = u16;

    enum e : size {
        LP1             = 7,
        LP2             = 8,
        MH3U_3DS        = 16,
        MH4U_MHXX_MHGU  = 17,
        MH4U_1          = 19
    };

    static auto contains(const size version) noexcept -> bool;
    static auto getPadding(const arc::sVersion::e version) noexcept -> std::size_t;
};

struct Header {
    static constexpr u32 ARC_MAGIC = 0x00435241; /// "ARC\0"
    static constexpr u32 CRA_MAGIC = 0x41524300; /// "\0CRA"

    u32         Magic;
    sVersion::e Version;
    u16         FilesNum;

    Header() = default;

    Header(const sVersion::e version,
           const u16 filesNum,
           const bool BE = false);

    auto BESwap(void) noexcept -> void;

    auto isARC(void) const noexcept -> bool;
    auto isCRA(void) const noexcept -> bool;
};

struct FileHeader {
    static constexpr u8 ARC_FILEPATH_MAX = 64;
    static constexpr u32 UNCOMPRESSED_SIZE_MASK = 0x1FFFFFFF;
    static constexpr u32 FLAGS_MASK = 0x7;

    char    FilePath[ARC_FILEPATH_MAX];
    u32     ResourceHash;
    u32     CompressedSize;

    union {
        u32 Raw;

        struct {
            u32 UncompressedSize : 29;
            u32 Flags : 3;
        };
    };

    u32     pZData; // 78 9C - zlib header

    auto isValid(void) const noexcept -> bool;

    auto toLittleEndian(void) noexcept -> void;
    auto toBigEndian(void) noexcept -> void;

    auto setFilePath(const std::string& path) noexcept -> bool;
    auto getFilePath(void) const noexcept -> const char*;
    auto getFilePathUnix(void) const noexcept -> std::string;
    auto fixFilePath(void) noexcept -> void;

    auto getFlags(void) const noexcept -> u32;
};

}; /// namespace arc
}; /// namespace tools
}; /// namespace mh
