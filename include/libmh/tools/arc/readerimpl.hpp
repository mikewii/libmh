#pragma once
#include <libmh/types.hpp>
#include <libmh/tools/zlibtool.hpp> // for Z_DEFAULT_COMPRESSION
#include <libmh/tools/arc/common.hpp>
#include <vector>

namespace mh {
namespace tools {
namespace arcimpl {

class Reader
{
protected:
    bool m_valid = false;
    bool m_isBE;

    std::size_t m_padding;
    arc::Header m_header;
    std::vector<arc::FileHeader> m_headers;

protected:
    auto read(std::istream& istream) noexcept -> bool;
    auto readHeader(std::istream& istream) noexcept -> bool;
    auto readARCFileHeaders(std::istream& istream) noexcept -> void;

    auto uncompress(const std::size_t id,
                    std::istream& istream,
                    std::ostream& ostream,
                    std::size_t& uncompresSize,
                    const std::size_t uncompressSizeLimit = 0) noexcept -> bool;
public:
    auto isValid(void) const noexcept -> bool;

    auto isBigEndian(void) const noexcept -> bool;

    auto getHeader(void) const noexcept -> const arc::Header&;
    auto getFileHeaders(void) const noexcept -> const std::vector<arc::FileHeader>&;

private:
    auto checkMagic(std::istream& istream) const noexcept -> bool;
    auto checkVersion(std::istream& istream) const noexcept -> bool;
    auto checkHeaderSize(const std::size_t size) const noexcept -> bool;
    auto checkFileHeadersSize(const std::size_t size) const noexcept -> bool;
    auto checkZLIBDataSize(const std::size_t size) const noexcept -> bool;

    auto toLittleEndian(void) noexcept -> void;
    auto toBigEndian(void) noexcept -> void;
};

}; /// namespace arc
}; /// namespace tools
}; /// namespace mh
