#pragma once
#include <libmh/tools/zlibtool.hpp> // for Z_DEFAULT_COMPRESSION
#include <libmh/tools/arc/common.hpp>
#include <vector>

namespace mh {
namespace tools {
namespace arcimpl {

class Writer
{
public:
    auto seekToZLIBData(const std::size_t arcFileHeadersNum,
                        const std::size_t padding,
                        std::ostream& ostream) noexcept -> bool;

    auto writeHeader(const arc::Header& header,
                     std::ostream& ostream) noexcept -> bool;

    auto writeARCFileHeaders(const std::vector<arc::FileHeader>& headers,
                             const std::size_t padding,
                             std::ostream& ostream) noexcept -> bool;

    auto compress(std::istream& istream,
                  std::ostream& ostream,
                  std::size_t& uncompressedSize,
                  std::size_t& compressedSize,
                  const int level = Z_DEFAULT_COMPRESSION,
                  const int strategy = Z_DEFAULT_STRATEGY) noexcept -> bool;

    auto copy(std::istream& istream,
              std::ostream& ostream,
              const std::size_t n) noexcept -> void;

    auto copy_s(std::istream& istream,
                std::ostream& ostream,
                const std::size_t n) noexcept -> bool;
};

}; /// namespace arc
}; /// namespace tools
}; /// namespace mh
