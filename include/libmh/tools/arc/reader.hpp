#pragma once
#include <libmh/tools/noncopyable.hpp>
#include <libmh/tools/arc/readerimpl.hpp>
#include <libmh/tools/imemstream.hpp>
#include <memory>
#include <functional>
#include <optional>

namespace mh {
namespace tools {
namespace arc {

class Reader : public arcimpl::Reader
{
private:
    std::unique_ptr<std::istream> m_istream;
    std::string m_filepath;
    std::size_t m_uncompressedSizeLimit;
    std::size_t m_threadNum;
    bool m_open;
    bool m_read;

public:
    using streamVector = std::vector<std::pair<std::unique_ptr<std::iostream>, FileHeader>>;
    using streamVectorRef = std::reference_wrapper<streamVector>;
    using iostreamFunType = std::function<std::unique_ptr<std::iostream>(const FileHeader&) noexcept(false)>;

    NONCOPYABLE(Reader)

    Reader();
    Reader(const std::string& path);
    Reader(std::unique_ptr<std::istream>&& istream) noexcept;
    Reader(const void* data, const std::size_t size);


    auto read(void) noexcept -> bool;
    auto uncompress(const u16 id,
                    std::ostream& ostream) noexcept -> bool;

    auto uncompress(const FileHeader& header,
                    std::ostream& ostream) noexcept -> bool;

    auto uncompress_all_async(const std::string& path,
                              iostreamFunType iostreamfun,
                              std::optional<streamVectorRef> streamVec = std::nullopt) noexcept(false) -> bool;

    auto uncompress_all(iostreamFunType iostreamfun,
                        std::optional<streamVectorRef> streamVec = std::nullopt) noexcept(false) -> bool;

    auto setThreadsNum(const std::size_t num) noexcept -> void;
    auto getThreadsNum(void) const noexcept -> std::size_t;

    auto setIStream(std::unique_ptr<std::istream>&& istream) noexcept -> void;
    auto setIStream(std::unique_ptr<std::ifstream>&& ifstream) noexcept -> void;

    auto getIStream(void) -> std::unique_ptr<std::istream>&;

    auto getFilePath(void) const noexcept -> const std::string&;
    auto setFilePath(const std::string& path) noexcept -> void;

    auto setUncompressedSizeLimit(const std::size_t limit) noexcept -> void;
    auto getUncompressedSizeLimit(void) const noexcept -> std::size_t;

    auto resetIStream(void) noexcept -> void;

    auto haveIStream(void) const noexcept -> bool;
    auto isIStreamOpen(void) const noexcept -> bool;
};

}; /// namespace arc
}; /// namespace tools
}; /// namespace mh
