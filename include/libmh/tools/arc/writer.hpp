#pragma once
#include <libmh/tools/arc/writerimpl.hpp>
#include <libmh/tools/arc/reader.hpp>
#include <libmh/tools/imemstream.hpp>
#include <memory>

namespace mh {
namespace tools {
namespace arc {

enum class WriteOp {
    COPY,
    COMPRESS
};

struct sWriteOp {
    WriteOp op;
    FileHeader header;
    std::unique_ptr<std::istream> istream;

    int level = Z_DEFAULT_COMPRESSION;
    int strategy = Z_DEFAULT_STRATEGY;
};

class Writer : public arcimpl::Writer
{
private:
    std::unique_ptr<std::ostream> m_ostream;
    std::string m_filepath;
    std::size_t m_threadNum;
    bool m_open;

public:
    using istreamFunType = std::function<sWriteOp(const u16) noexcept(false)>;

    Writer();
    Writer(const std::string& path);
    Writer(std::unique_ptr<std::ostream>&& ostream) noexcept;

    auto compress(const u16 filesNum,
                  const sVersion::e version,
                  istreamFunType istreamFun,
                  const bool isBE = false) noexcept(false) -> bool;

    auto compress_async(const u16 filesNum,
                        const sVersion::e version,
                        istreamFunType istreamFun,
                        const bool isBE = false) noexcept(false) -> bool;

    auto setThreadsNum(const std::size_t num) noexcept -> void;
    auto getThreadsNum(void) const noexcept -> std::size_t;

    auto setOStream(std::unique_ptr<std::ostream>&& ostream) noexcept -> void;
    auto setOStream(std::unique_ptr<std::ofstream>&& ofstream) noexcept -> void;

    auto getFilePath(void) const noexcept -> const std::string&;
    auto setFilePath(const std::string& path) noexcept -> void;

    auto resetOStream(void) noexcept -> void;

    auto haveOStream(void) const noexcept -> bool;
    auto isOStreamOpen(void) const noexcept -> bool;

private:
    auto writeHeader(const u16 filesNum,
                     const sVersion::e version,
                     const bool isBE) noexcept -> bool;

    auto doCompress(const u16 id,
                    const istreamFunType& istreamFun,
                    const bool isBE,
                    std::vector<FileHeader>& headers) noexcept(false) -> bool;
};

}; /// namespace arc
}; /// namespace tools
}; /// namespace mh
