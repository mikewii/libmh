#pragma once
#include <iosfwd>

namespace mh {
namespace tools {

#if defined(ENABLE_CTRPF)
inline constexpr auto CHUNK = 128 << 0;
#else
inline constexpr auto CHUNK = 128 << 7;
#endif

/// \return adler32 value
extern auto checksum(const void* data, const std::size_t size) noexcept -> std::size_t;

/// \return adler32 value
extern auto checksum(std::istream& istream) noexcept -> std::size_t;

extern auto checksum32(std::istream& istream, const bool unwind = true) noexcept -> std::size_t;

}; /// namespace tools
}; /// namespace mh
