#include <libmh/tools/magic.hpp>
#include <libmh/tools/version.hpp>
#include <iosfwd>

namespace mh {
namespace tools {

struct MagicVersion {
    sMagic Magic;
    uVersion Version;

    MagicVersion() = default;
    constexpr MagicVersion(const u32 magic, const u32 version)
        : Magic{magic}, Version{version} {}

    constexpr auto operator () (const void* data) const noexcept -> bool
    {
        return check(data);
    }

    constexpr auto check(const void* data) const noexcept -> bool
    {
        const MagicVersion* obj = static_cast<const MagicVersion*>(data);

        return    obj->Magic == Magic
               && obj->Version == Version;
    }

    constexpr auto check(sMagic magic, uVersion version) const noexcept -> bool
    {
        return Magic == magic && Version == version;
    }

    auto read(std::istream& istream, const bool unwind = true) noexcept -> bool;
    auto read_s(std::istream& istream, const bool unwind = true) noexcept -> bool;
};

}; /// namespace tools
}; /// namespace mh
