#pragma once
#include <libmh/types.hpp>
#include <istream>
#include <string>

namespace mh {
namespace tools {
namespace istream {

#ifndef NOEXCEPT_ISTREAM
#ifdef  ENABLE_NOEXCEPT_ISTREAM
#define NOEXCEPT_ISTREAM noexcept
#else
#define NOEXCEPT_ISTREAM noexcept(false)
#endif
#endif

extern auto readU16String(std::istream& istream,
                          std::u16string& out) NOEXCEPT_ISTREAM -> bool;

extern auto read(std::istream& istream,
                 void* out,
                 const std::size_t size) NOEXCEPT_ISTREAM -> void;

extern auto size(std::istream& istream,
                 const bool unwind = true) NOEXCEPT_ISTREAM -> std::streamsize;

extern auto readMagic(std::istream& istream,
                      const bool unwind = true) NOEXCEPT_ISTREAM -> u32;

extern auto readMagic_s(std::istream& istream,
                        const bool unwind = true) NOEXCEPT_ISTREAM -> u32;

extern auto seekg(std::istream& istream,
                  const std::ostream::pos_type pos) NOEXCEPT_ISTREAM -> std::istream&;

extern auto seekg(std::istream& istream,
                  const std::ostream::off_type off,
                  const std::ios_base::seekdir dir) NOEXCEPT_ISTREAM -> std::istream&;

extern auto clear(std::istream& istream,
                  const std::ios_base::iostate state = std::ios_base::goodbit) NOEXCEPT_ISTREAM -> void;

extern auto debug(std::istream& istream) NOEXCEPT_ISTREAM -> void;

}; /// namespace istream
}; /// namespace tools
}; /// namespace mh
