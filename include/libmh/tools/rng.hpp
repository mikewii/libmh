#pragma once
#include <random>

namespace mh {
namespace tools {
namespace random {

#ifndef THREAD_LOCAL_RNG
#if     defined(ENABLE_THREAD_LOCAL_RNG)
#define THREAD_LOCAL_RNG thread_local
#else
#define THREAD_LOCAL_RNG
#endif
#endif

template <typename T = std::mt19937>
inline T& rng(typename T::result_type seed = std::random_device{}())
{
    static THREAD_LOCAL_RNG T engine(seed);

    return engine;
}

template <typename T = std::mt19937>
void seed(typename T::result_type seed = std::random_device{}())
{
    rng<T>(seed);
}

template <typename T>
T randint(T min, T max)
{
    static_assert(std::is_integral<T>::value, "Intergal type required");

    std::uniform_int_distribution<T> dist(min, max);

    return dist(rng());
}

template <typename T>
T randreal(T min, T max)
{
    static_assert(std::is_floating_point<T>::value, "Floating point type required");

    std::uniform_real_distribution<T> dist(min, max);

    return dist(rng());
}

}; /// namespace random
}; /// namespace tools
}; /// namespace mh
