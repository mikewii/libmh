#pragma once
#include <libmh/types.hpp>
#include <ostream>

namespace mh {
namespace tools {
namespace ostream {

#ifndef NOEXCEPT_OSTREAM
#ifdef  ENABLE_NOEXCEPT_OSTREAM
#define NOEXCEPT_OSTREAM noexcept
#else
#define NOEXCEPT_OSTREAM noexcept(false)
#endif
#endif

extern auto write(std::ostream& ostream,
                  const void* out,
                  const std::size_t size) NOEXCEPT_OSTREAM -> void;

extern auto copy(std::ostream& ostream,
                 std::istream& istream,
                 const std::size_t size) NOEXCEPT_OSTREAM -> void;

extern auto align(std::ostream& ostream,
                  const std::size_t alignment = sizeof(u64) * 2) NOEXCEPT_OSTREAM -> void;

extern auto fill(std::ostream& ostream,
                 const std::size_t num,
                 const u8 ch = '\0') NOEXCEPT_OSTREAM -> void;

extern auto put(std::ostream& ostream,
                const u8 ch) NOEXCEPT_OSTREAM -> void;

extern auto seekp(std::ostream& ostream,
                  const std::ostream::pos_type pos) NOEXCEPT_OSTREAM -> std::ostream&;

extern auto seekp(std::ostream& ostream,
                  const std::ostream::off_type off,
                  const std::ios_base::seekdir dir) NOEXCEPT_OSTREAM -> std::ostream&;

extern auto clear(std::ostream& ostream,
                  const std::ios_base::iostate state = std::ios_base::goodbit) NOEXCEPT_OSTREAM -> void;

}; /// namespace ostream
}; /// namespace tools
}; /// namespace mh
