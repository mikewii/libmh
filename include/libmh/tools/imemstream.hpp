#pragma once
#include <istream>
#include <streambuf>

namespace mh {
namespace tools {

/// ref: https://stackoverflow.com/a/13059195

struct membuf : std::streambuf
{
    membuf(const char* data, const std::size_t size)
    {
        setg(  const_cast<char*>(data)
             , const_cast<char*>(data)
             , const_cast<char*>(data) + size);
    }
};

struct imemstream : virtual membuf, std::istream
{
    imemstream(const void* data, const std::size_t size)
        : membuf(static_cast<const char*>(data), size)
        , std::istream(static_cast<std::streambuf*>(this))
        , m_size{size}
    {
    }

    auto size(void) const noexcept -> std::size_t { return m_size; }

private:
    std::size_t m_size;
};

}; /// namespace tools
}; /// namespace mh
