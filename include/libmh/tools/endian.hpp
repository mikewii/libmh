#pragma once
#include <libmh/types.hpp>
#include <climits>
#include <utility>

namespace mh {
namespace tools {
namespace endian {

template <typename T>
constexpr auto swap(const T u) -> T
{
    static_assert (CHAR_BIT == 8, "CHAR_BIT != 8");

    union {
        T raw;
        u8 byte[sizeof(T)];
    } source, dest;

    source.raw = std::move(u);

    for (std::size_t i = 0; i < sizeof(T); i++)
        dest.byte[i] = source.byte[sizeof(T) - i - 1];

    return dest.raw;
}

template <typename T, typename U>
constexpr auto swapBytes(T array[],
                         std::size_t size,
                         std::size_t limit) -> void
{
    auto breakpoint = (limit / sizeof(U)) - 1;

    if (breakpoint < 0)
        breakpoint = 0;

    for (auto i = 0u; i < size; i += sizeof(U)) {
        *reinterpret_cast<U*>(&array[i]) = endian::swap(*reinterpret_cast<U*>(&array[i]));

        if (i == breakpoint)
            break;
    }
}

}; /// namespace endianess
}; /// namespace tools
}; /// namespace mh
