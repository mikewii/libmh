#pragma once
#include <libmh/tools/magic.hpp>
#include <libmh/tools/istream.hpp>
#include <libmh/tools/ostream.hpp>

#include <vector>
#include <istream>
#include <ostream>

namespace mh {
template <typename Header, typename Data, typename Container = std::vector<Data>>
class Object
{
protected:
    bool m_valid = false;
    Header m_header;
    Container m_data;

public:
    auto isValid(void) const noexcept -> bool;

    virtual auto dump(std::ostream& ostream) -> bool;

    virtual auto getHeader(void) noexcept -> Header&;
    virtual auto getHeader(void) const noexcept -> const Header&;

    virtual auto getData(void) noexcept -> Container&;
    virtual auto getData(void) const noexcept -> const Container&;

protected:
    virtual auto basicCheck(std::istream& istream) const noexcept -> bool;
    virtual auto dataSizeCheck(std::istream& istream) const noexcept -> bool;
    virtual auto readHeader(std::istream& istream) noexcept -> bool;
    virtual auto readData(std::istream& istream) noexcept -> bool;

    template <typename HeaderF, typename DataF, typename ContainerF>
    friend auto parse(std::istream& istream, Object<HeaderF, DataF, ContainerF>& obj) noexcept -> bool;
};

template<typename Header, typename Data, typename Container>
auto Object<Header, Data, Container>::isValid(void) const noexcept -> bool
{
    return m_valid;
}

template<typename Header, typename Data, typename Container>
auto Object<Header, Data, Container>::dump(std::ostream& ostream) -> bool
{
    std::ostream::pos_type totalSize =   sizeof(Header)
                                       + (m_header.DataNum * sizeof(Data));

    tools::ostream::seekp(  ostream
                          , std::ios::beg
                          , std::ios::beg);

    tools::ostream::write(  ostream
                          , &m_header
                          , sizeof(m_header));

    for (const auto& data : m_data)
        tools::ostream::write(  ostream
                              , &data
                              , sizeof(data));

    return ostream.tellp() == totalSize;
}

template<typename Header, typename Data, typename Container>
auto Object<Header, Data, Container>::getHeader(void) noexcept -> Header&
{
    return m_header;
}

template<typename Header, typename Data, typename Container>
auto Object<Header, Data, Container>::getHeader(void) const noexcept -> const Header&
{
    return m_header;
}

template<typename Header, typename Data, typename Container>
auto Object<Header, Data, Container>::getData(void) noexcept -> Container&
{
    return m_data;
}

template<typename Header, typename Data, typename Container>
auto Object<Header, Data, Container>::getData(void) const noexcept -> const Container&
{
    return m_data;
}

template<typename Header, typename Data, typename Container>
auto Object<Header, Data, Container>::basicCheck(std::istream& istream) const noexcept -> bool
{
    tools::sMagic magic;

    if (tools::istream::size(istream) <= static_cast<std::streamsize>(sizeof(Header)))
        return false;

    magic.read(istream);

    if (!magic.check(Header::MAGIC))
        return false;

    return true;
}

template<typename Header, typename Data, typename Container>
auto Object<Header, Data, Container>::dataSizeCheck(std::istream& istream) const noexcept -> bool
{
    std::streamsize streamSize = tools::istream::size(istream);
    std::streamsize expectedSize = sizeof(Header) + (m_header.DataNum * sizeof(Data));

    return streamSize == expectedSize;
}

template<typename Header, typename Data, typename Container>
auto Object<Header, Data, Container>::readHeader(std::istream& istream) noexcept -> bool
{
    if (tools::istream::size(istream) <= static_cast<std::streamsize>(sizeof(Header)))
        return false;

    tools::istream::read(  istream
                         , &m_header
                         , sizeof(Header));

    return istream.gcount() == sizeof(Header);
}

template<typename Header, typename Data, typename Container>
auto Object<Header, Data, Container>::readData(std::istream& istream) noexcept -> bool
{
    m_data.reserve(m_header.DataNum);

    for (auto i = 0u; i < m_header.DataNum; i++) {
        Data data;

        istream.read(  reinterpret_cast<char*>(&data)
                     , sizeof(data));

        m_data.push_back(std::move(data));
    }

    return m_data.size() == m_header.DataNum;
}

template <typename Header, typename Data, typename Container = std::vector<Data>>
auto parse(std::istream& istream, Object<Header, Data, Container>& obj) noexcept -> bool
{
    if (!obj.basicCheck(istream))
        return false;

    if (!obj.readHeader(istream))
         return false;

    if (!obj.dataSizeCheck(istream))
        return false;

    if(!obj.readData(istream))
        return false;

    obj.m_valid = true;

    return true;
}
}; /// namespace mh
