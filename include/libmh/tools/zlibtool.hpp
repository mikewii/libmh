#pragma once
#include <iosfwd>
#include <zlib.h>

namespace mh {
namespace tools {

/// ref: https://github.com/madler/zlib/blob/develop/examples/zpipe.c

class ZLIBTool
{
private:
#if defined(ENABLE_CTRPF)
    inline static constexpr auto CHUNK = 128 << 0;
#else
    inline static constexpr auto CHUNK = 128 << 7;
#endif

    z_stream m_zstream;
    std::istream& m_istream;
    std::ostream& m_ostream;
    unsigned char m_in[CHUNK];
    unsigned char m_out[CHUNK];

public:
    ZLIBTool(std::istream& istream,
             std::ostream& ostream);

    ///
    /// \brief uncompress
    /// \param compressedSize pass -1 to disable check
    /// \param uncompressedSize
    /// \return
    ///
    auto uncompress(const std::streamsize compressedSize,
                    std::size_t& uncompressedSize,
                    const std::size_t uncompressedSizeLimit = 0) noexcept -> bool;

    auto compress(std::size_t& uncompressedSize,
                  std::size_t& compressedSize,
                  const int level = Z_DEFAULT_COMPRESSION,
                  const int strategy = Z_DEFAULT_STRATEGY) noexcept -> bool;

    /// \return Adler-32 or CRC-32 value of the uncompressed data
    auto checksum(void) const noexcept -> uLong;

private:
    auto init(void) noexcept -> void;

    auto read(void) noexcept -> std::streamsize;
    auto write(const std::size_t size) noexcept -> std::streamsize;

    auto deflate(const int flush) noexcept -> int;
    auto inflate(std::size_t& uncompressSize,
                 const std::size_t uncompressSizeLimit = 0) noexcept -> int;
};

}; /// namespace tools
}; /// namespace mh
