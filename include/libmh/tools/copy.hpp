#pragma once
#include <cstddef>

namespace mh {
namespace tools {

extern auto copyBytes(void* dest,
                      const void* src,
                      const std::size_t size) noexcept -> void*;

}; /// namespace tools
}; /// namespace mh
