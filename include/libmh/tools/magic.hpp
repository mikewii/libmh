#pragma once
#include <libmh/types.hpp>
#include <iosfwd>

namespace mh {
namespace tools {

struct sMagic {
    u32 Raw;

    sMagic()
        : Raw{0}
    {
    }

    constexpr sMagic(const u32 magic)
        : Raw{magic}
    {
    }

    constexpr bool operator == (const sMagic& magic) const noexcept { return Raw == magic.Raw; }
    constexpr bool operator == (const u32 magic) const noexcept { return Raw == magic; }

    constexpr auto check(u32 magic) const noexcept -> bool
    {
        return Raw == magic;
    }

    auto read(std::istream& istream, const bool unwind = true) noexcept -> bool;
};

}; /// namespace tools
}; /// namespace mh
