#pragma once

namespace mh {
namespace tools {

extern auto printHex(const void* offset, unsigned int size) -> void;

}; /// namespace mh
}; /// namespace tools
