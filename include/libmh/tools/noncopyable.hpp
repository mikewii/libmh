#pragma once
#ifndef NONCOPYABLE
#define NONCOPYABLE(T) \
    T(T&) = delete; \
    T(const T&) = delete; \
    T& operator = (T&) = delete; \
    T& operator = (const T&) = delete;
#endif
