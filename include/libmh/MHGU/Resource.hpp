#pragma once
#include <libmh/MH/Common/Resource.hpp>

namespace mh {
namespace mhgu {

struct sResource : sResourceAPI {
    static const std::array<Resource, 266> resource;

    static constexpr auto getMagic(const u32 hash, u32& magic) noexcept -> bool
    {
        return sResourceAPI::getMagic(resource, hash, magic);
    }

    static constexpr auto getVersion(const u32 hash, u32& version) noexcept -> bool
    {
        return sResourceAPI::getVersion(resource, hash, version);
    }

    static constexpr auto getExtention(const u32 hash) noexcept -> const char*
    {
        return sResourceAPI::getExtention(resource, hash);
    }
};

}; /// namespace mhgu
}; /// namespace mh
