#pragma once
#include <libmh/MH/Keys.hpp>
#include <iosfwd>
#include <vector>

namespace mh {
namespace crypto {

#define RSA_SIGNATURE_SIZE 256

struct SignX {
    static auto verify(std::istream& istream,
                       const Key::e id,
                       const std::size_t offset = RSA_SIGNATURE_SIZE) -> bool;

    static auto verify(std::istream& istream,
                       const char* RSAPKeyFilePath,
                       const std::size_t offset = RSA_SIGNATURE_SIZE) -> bool;


    static auto verify(std::istream& istream,
                       const std::vector<u8>& key,
                       const std::size_t offset = RSA_SIGNATURE_SIZE) -> bool;

    static auto verify(std::istream& istream,
                       const u8 key[],
                       const std::size_t keySize,
                       const std::size_t offset = RSA_SIGNATURE_SIZE) -> bool;

    static auto verify(std::istream& istream,
                       std::istream& keyStream) -> bool;

    static auto sign(std::iostream& iostream,
                     std::istream& keyStream) -> bool;

    static auto sign(std::iostream& iostream,
                     const std::vector<u8>& key) -> bool;

    static auto sign(std::iostream& iostream,
                     const u8 key[],
                     const std::size_t keySize) -> bool;
};

}; /// namespace crypto
}; /// namespace mh

