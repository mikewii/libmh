#pragma once
#include <libmh/tools/magic.hpp>
#include <libmh/tools/version.hpp>
#include <array>

namespace mh {

struct Resource {
    u32             Hash;
    tools::sMagic   Magic;
    tools::uVersion Version;
    const char*     Extention;
};

struct sResourceAPI {
    template <typename T, std::size_t N>
    static constexpr auto getMagic(const std::array<T, N>& array,
                                   const u32 hash,
                                   u32& magic) noexcept -> bool
    {
        for (auto it = array.begin(); it != array.end(); it++) {
            if (it->Hash == hash) {
                magic = it->Magic.Raw;

                return true;
            }
        }

        return false;
    }

    template <typename T, std::size_t N>
    static constexpr auto getVersion(const std::array<T, N>& array,
                                     const u32 hash,
                                     u32& version) noexcept -> bool
    {
        for (auto it = array.begin(); it != array.end(); it++) {
            if (it->Hash == hash) {
                version = it->Version.Raw;

                return true;
            }
        }

        return false;
    }

    template <typename T, std::size_t N>
    static constexpr auto getExtention(const std::array<T, N>& array,
                                       const u32 hash) noexcept -> const char*
    {
        for (auto it = array.begin(); it != array.end(); it++)
            if (it->Hash == hash)
                return it->Extention;

        return nullptr;
    }
};

}; /// namespace mh
