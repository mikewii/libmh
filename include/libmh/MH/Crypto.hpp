#pragma once
#include <libmh/MH/Keys.hpp>
#include <mbedtls/blowfish.h>
#include <memory>
#include <random>
#include <iosfwd>

namespace mh {
namespace crypto {

class Crypto
{
private:
#if defined(ENABLE_CTRPF)
    inline static constexpr auto CHUNK = 128 << 0;
#else
    inline static constexpr auto CHUNK = 128 << 7;
#endif

    bool m_valid;
    std::istream& m_istream;
    std::ostream& m_ostream;

    std::unique_ptr<mbedtls_blowfish_context> m_blowfish;
    static std::mt19937 m_rng;

public:
    Crypto(std::istream& istream, std::ostream& ostream);
    ~Crypto();

    auto isValid(void) const noexcept -> bool;

    auto keySizeCheck(const std::size_t size) noexcept -> bool;

    /// Wrapper functions:
    auto decryptSave(const Key::e id = Key::e::MH4U_EXT_DATA) noexcept -> bool;
    auto encryptSave(const Key::e id = Key::e::MH4U_EXT_DATA) noexcept -> bool;

    auto decryptCard(const Key::e id = Key::e::MH4U_EXT_DATA) noexcept -> bool;
    auto encryptCard(const Key::e id = Key::e::MH4U_EXT_DATA) noexcept -> bool;

    auto decryptEXTData(void) noexcept -> bool;
    auto encryptEXTData(void) noexcept -> bool;

    auto decryptDLC(const uint8_t key[]) noexcept -> bool;
    auto decryptDLC(const Key::e id = Key::e::MH4U_DLC_EUR_NA) noexcept -> bool;
    auto encryptDLC(const Key::e id = Key::e::MH4U_DLC_EUR_NA) noexcept -> bool;

    auto decrypt(const Key::e id) noexcept -> bool;
    auto encrypt(const Key::e id) noexcept -> bool;

    auto xorDecrypt(void) noexcept -> bool;
    auto xorEncrypt(void) noexcept -> void;

    /// Direct access function:
    auto crypt(const u8 blowfishKey[],
               const std::size_t blowfishKeySize,
               std::istream& istream,
               std::ostream& ostream,
               const int mode = MBEDTLS_BLOWFISH_DECRYPT) noexcept -> bool;

    auto xorDecrypt(std::istream& istream,
                   std::ostream& ostream) noexcept -> bool;

    auto xorEncrypt(std::istream& istream,
                   std::ostream& ostream) noexcept -> void;

    auto xorMH4U(u32 seed,
                 std::istream& istream,
                 std::ostream& ostream) noexcept -> void;
};

}; /// namespace crypto
}; /// namespace mh

