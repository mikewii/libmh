#pragma once
#include <libmh/types.hpp>
#include <vector>
#include <string>

namespace mh {

inline constexpr u32 NULL_TERMINATOR_SIZE = 1;

class GMDBase
{
protected:
    bool m_valid = false;
    std::string m_filename;
    std::vector<std::string> m_labels;
    std::vector<std::string> m_items;

public:
    auto isValid(void) const noexcept -> bool;

    auto getItemsNum(void) const noexcept -> u32;
    auto getLabelsNum(void) const -> u32;

    auto getLabel(u32 id) noexcept(false) -> std::string&;
    auto getLabel(u32 id) const noexcept(false) -> const std::string&;
    auto getLabel(u32 id, std::string& str) const noexcept -> bool;

    auto getItem(u32 id) noexcept(false) -> std::string&;
    auto getItem(u32 id) const noexcept (false) -> const std::string&;
    auto getItem(u32 id, std::string& str) const noexcept -> bool;

    auto appendLabel(const std::string& str) noexcept -> void;
    auto replaceLabel(u32 id, const std::string& str) noexcept -> bool;
    auto removeLabel(u32 id) noexcept -> bool;

    auto appendItem(const std::string& str) noexcept -> void;
    auto replaceItem(u32 id, const std::string& str) noexcept -> bool;
    auto removeItem(u32 id) noexcept -> bool;

    auto getFilename(void) noexcept -> std::string&;
    auto getFilename(void) const noexcept -> const std::string&;
    auto setFilename(const std::string& str) noexcept -> void;

    auto getLabels(void) noexcept -> std::vector<std::string>&;
    auto getLabels(void) const noexcept -> const std::vector<std::string>&;

    auto getItems(void) noexcept -> std::vector<std::string>&;
    auto getItems(void) const noexcept -> const std::vector<std::string>&;
};

}; /// namespace gmdimpl
