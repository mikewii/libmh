#pragma once
#include <libmh/MH/Common/Resource.hpp>

namespace mh {

/// Merge of all mh resource tables
/// Version removed to not cause confusion
/// For actual version use getVersion from dedicated mh** sResource

struct sResource : sResourceAPI {
    struct MergedResource {
        u32             Hash;
        tools::sMagic   Magic;
        const char*     Extention;
    };

    static const std::array<MergedResource, 310> resource;

    static constexpr auto getMagic(const u32 hash, u32& magic) noexcept -> bool
    {
        return sResourceAPI::getMagic(resource, hash, magic);
    }

    static constexpr auto getExtention(const u32 hash) noexcept -> const char*
    {
        return sResourceAPI::getExtention(resource, hash);
    }
};

}; /// namespace mh
