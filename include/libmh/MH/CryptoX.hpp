#pragma once
#include <libmh/MH/Keys.hpp>
#include <mbedtls/blowfish.h>
#include <memory>
#include <random>
#include <iosfwd>

namespace mh {
namespace crypto {

/// MHX:
//////////////////////////
///                    ///
///                    ///
///                    ///
///                    ///
///        DATA        ///
///                    ///
///                    ///
///                    ///
///                    ///
//////////////////////////
///        HASH        ///
//////////////////////////
///        NONCE       ///
//////////////////////////
///  SERVER SIGNATURE  ///
//////////////////////////
///    DLC SIGNATURE   ///
//////////////////////////

/// MHXX:
//////////////////////////
///                    ///
///                    ///
///                    ///
///                    ///
///        DATA        ///
///                    ///
///                    ///
///                    ///
///                    ///
//////////////////////////
///      UNKNOWN       ///
//////////////////////////
///  SERVER SIGNATURE  ///
//////////////////////////

class CryptoX
{
private:
#if defined(ENABLE_CTRPF)
    inline static constexpr auto CHUNK = 128 << 0;
#else
    inline static constexpr auto CHUNK = 128 << 7; // keep it MOD 8 least 24 bytes
#endif

    bool m_valid;
    std::istream& m_istream;
    std::ostream& m_ostream;
    u32 m_nonce;

    std::unique_ptr<mbedtls_blowfish_context> m_blowfish;

public:
    CryptoX(std::istream& istream, std::ostream& ostream);
    ~CryptoX();

    auto isValid(void) const noexcept -> bool;

    auto keySizeCheck(const std::size_t size) noexcept -> bool;

    /// Wrapper functions:
    auto decryptDLC(const Key::e id = Key::e::MHX_DLC_JP) noexcept -> bool;
    auto encryptDLC(const Key::e id = Key::e::MHX_DLC_JP) noexcept -> bool;

    auto decryptDLC(const u8 blowfishKey[],
                    const std::size_t blowfishKeySize) noexcept -> bool;

    /// Direct access function:
    auto decrypt(const u8 blowfishKey[],
                 const std::size_t blowfishKeySize) noexcept -> bool;

    auto encrypt(const u8 blowfishKey[],
                 const std::size_t keySize) noexcept -> bool;

    auto decrypt(const u8 blowfishKey[],
                 const std::size_t blowfishKeySize,
                 std::istream& istream,
                 std::ostream& ostream) noexcept -> bool;

    auto encrypt(const u8 blowfishKey[],
                 const std::size_t blowfishKeySize,
                 std::istream& istream,
                 std::ostream& ostream) noexcept -> bool;

private:
    auto extractNonce(std::istream &istream) -> void;
};

}; /// namespace crypto
}; /// namespace mh

