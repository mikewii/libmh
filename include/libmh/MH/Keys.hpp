#pragma once
#include <libmh/types.hpp>
#include <utility>

namespace mh {
namespace crypto {

struct Key {
    enum e {
        NONE,

        MH4U_EXT_DATA,
        MH4U_DLC_EUR_NA,
        MH4U_DLC_JPN,
        MH4U_DLC_KOR,
        MH4U_DLC_TW,

        MHX_DLC_JP,
        MHX_DLC_US,
        MHX_DLC_EU,
        MHX_DLC_TW,

        MHXX_DLC_JP,

        LENGTH
    };

    static auto getBlowfishKey(e id) -> std::pair<const u8*, std::size_t>;
    static auto getRSAPublicKey(e id) -> std::pair<const u8*, std::size_t>;
};

}; /// namespace crypto
}; /// namespace mh

