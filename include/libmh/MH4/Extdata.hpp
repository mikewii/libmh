#pragma once
#include <libmh/MH4U/Extentions/qtds.hpp>

#include <memory>
#include <functional>
#include <optional>
#include <sstream>

namespace mh {
namespace mh4 {
namespace extdata {

inline constexpr auto FILES_AMMOUNT = 4;
inline constexpr auto QUEST_FILES_AMMOUNT = 20;
inline constexpr auto FILE_DATA_SIZE = 0x28140;
inline constexpr auto FILE_FULL_SIZE = 0x29000;
inline constexpr auto QUEST_SIZE = 0x2010;

class Reader
{
private:
    std::istream& m_istream;
    std::stringstream m_full;

public:
    using iostreamFunType = std::function<std::unique_ptr<std::iostream>(const mh4u::qtds::cQTDS&) noexcept(false)>;

    Reader(std::istream& istream);

    auto dumpRaw(std::ostream& ostream) noexcept -> bool;
    auto dumpAll(const iostreamFunType& fun, const bool readLanguages = false) noexcept(false) -> bool;

private:
    auto crypto(void) noexcept -> bool;
};

class Writer
{
private:
    std::ostream& m_ostream;
    std::stringstream m_full;

public:
    using istreamFunType = std::function<std::optional<std::unique_ptr<std::istream>>(const u16) noexcept(false)>;

    Writer(std::ostream& ostream);

    auto writeAll(const istreamFunType& fun) noexcept(false) -> bool;

private:
    auto crypto(void) noexcept -> void;
};

}; /// namespace extdata
}; /// namespace mh4
}; /// namespace mh
