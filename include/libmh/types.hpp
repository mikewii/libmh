#pragma once
#include <cstdint>
#include <cstdbool>
#include <cstddef>
/// The maximum value of a u64.
#define U64_MAX	UINT64_MAX

/// would be nice if newlib had this already
#ifndef SSIZE_MAX
#ifdef SIZE_MAX
#define SSIZE_MAX ((SIZE_MAX) >> 1)
#endif
#endif

using utf16 = char16_t;

using u8    = uint8_t;      ///<  8-bit unsigned integer
using u16   = uint16_t;     ///< 16-bit unsigned integer
using u32   = uint32_t;     ///< 32-bit unsigned integer
using u64   = uint64_t;     ///< 64-bit unsigned integer

using s8    = int8_t;       ///<  8-bit signed integer
using s16   = int16_t;      ///< 16-bit signed integer
using s32   = int32_t;      ///< 32-bit signed integer
using s64   = int64_t;      ///< 64-bit signed integer

using vu8   = volatile u8;  ///<  8-bit volatile unsigned integer.
using vu16  = volatile u16; ///< 16-bit volatile unsigned integer.
using vu32  = volatile u32; ///< 32-bit volatile unsigned integer.
using vu64  = volatile u64; ///< 64-bit volatile unsigned integer.

using vs8   = volatile s8;  ///<  8-bit volatile signed integer.
using vs16  = volatile s16; ///< 16-bit volatile signed integer.
using vs32  = volatile s32; ///< 32-bit volatile signed integer.
using vs64  = volatile s64; ///< 64-bit volatile signed integer.
